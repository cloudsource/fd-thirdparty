﻿#pragma once

#include <string>

namespace Poco {
    namespace Util {
        class PropertyFileConfiguration;
    }
}

namespace xk
{

class ConfigFile
{
public:
    ConfigFile();
    ~ConfigFile();

    bool Load(const std::string& strFile);

    bool Save(const std::string& strFile);

    bool Save();

    std::string GetString(const std::string& strKey, const std::string& strDefault);
    int GetInt(const std::string& strKey, int nDefault);
    unsigned int GetUInt(const std::string& strKey, unsigned int nDefault);
    double GetDouble(const std::string& strKey, double fDefault);
    bool GetBool(const std::string& strKey, bool bDefault);

    void SetString(const std::string& strKey, const std::string& strValue);
    void SetInt(const std::string& strKey, int nValue);
    void SetUInt(const std::string& strKey, unsigned int nValue);
    void SetDouble(const std::string& strKey, double fValue);
    void SetBool(const std::string& strKey, bool bValue);

    const std::string& GetError() { return strError; }

private:
    std::string strError;
    std::string strConfigFile;

    Poco::Util::PropertyFileConfiguration* pConfig;
};

} // namespace xk