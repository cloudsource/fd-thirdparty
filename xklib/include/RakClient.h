#pragma once
#include <string>
#include "RakCallback.h"

namespace Poco { class Thread; }

namespace RakNet
{
	class RakPeerInterface;
	struct Packet;
}

namespace xk
{

    class RakClient
    {
    public:
        RakClient(RakCallback* cb = nullptr);
        ~RakClient();

        bool Connect(const std::string& host,
            const std::string& pwd,
            int port);

        void Disconnect();

        bool Start();

        void Stop();

        bool Send(int id, const void* buf, int len);

        void run();

        const std::string& GetErrorString() { return errorString; }

    protected:
        void SetErrorString(const std::string& str) { errorString = str; }
        bool GetPacket(RAK_MSG_PACKET& packet);
        void ReleasePacket();

    private:
        std::string errorString;

        std::string serverHost;
        std::string password;
        int			serverPort;

        bool		bRunning;
        RakCallback* rakCallback;

        Poco::Thread* thread;
        RakNet::RakPeerInterface* rakPeer;
        RakNet::Packet*			rakPacket;
    };

}// namespace xk