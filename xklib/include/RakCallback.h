﻿#pragma once

typedef unsigned long long RAK_GUID;

namespace xk
{

struct RAK_MSG_PACKET
{
	int id;
	void* buf;
	int len;
	RAK_GUID guid;
};

class RakCallback
{
public:
	virtual void OnNewConnection(RAK_GUID guid) = 0;
	virtual void OnDisconnect(RAK_GUID guid) = 0;
	virtual void OnLostConnection(RAK_GUID guid) = 0;
	virtual void OnMessage(RAK_GUID guid, int id, const void* buf, int len) = 0;
};

}// namespace xk