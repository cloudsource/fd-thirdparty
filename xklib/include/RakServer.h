﻿#pragma once

#include <string>
#include "RakCallback.h"

namespace Poco { class Thread; }

namespace RakNet
{
	class RakPeerInterface;
	struct Packet;
}

namespace xk
{

    class RakServer
    {
    public:
        RakServer(RakCallback* cb = nullptr);
        ~RakServer();

        bool Start(const std::string& host,
            const std::string& pwd,
            int port,
            int maxcount);

        void Stop();

        bool Disconnect(RAK_GUID guid);

        bool Send(RAK_GUID guid, int id, const void* buf, int len);
        bool SendBroadcast(int id, const void* buf, int len);

        void run();

        const std::string& GetErrorString() { return errorString; }

    protected:
        bool GetPacket(RAK_MSG_PACKET& packet);
        void ReleasePacket();
        void SetErrorString(const std::string& str) { errorString = str; }

    private:
        std::string serverHost;
        std::string password;
        int			serverPort;
        int			MaxConnection;

        bool		bRunning;
        RakCallback* rakCallback;

        std::string errorString;

        Poco::Thread* thread;
        RakNet::RakPeerInterface* rakPeer;
        RakNet::Packet*			rakPacket;
    };

}; // namespace xk