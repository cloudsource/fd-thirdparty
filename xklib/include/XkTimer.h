#pragma once

namespace Poco { class Stopwatch; }

namespace xk
{

class XkTimer
{
public:
	XkTimer();
	~XkTimer();

	bool IsUpSecond(int second);
	bool IsUpMillisecond(int millisecond);
	void Restart();
	int Elapsed();

private:
	Poco::Stopwatch* sw;
};

}// namespace xk