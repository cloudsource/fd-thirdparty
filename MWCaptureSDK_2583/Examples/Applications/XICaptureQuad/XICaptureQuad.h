
// XICaptureQuad.h : main header file for the XICaptureQuad application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CXICaptureQuadApp:
// See XICaptureQuad.cpp for the implementation of this class
//

class CXICaptureQuadApp : public CWinApp
{
public:
	CXICaptureQuadApp();


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation

public:
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CXICaptureQuadApp theApp;
