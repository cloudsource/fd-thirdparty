#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "CaptureThread.h"

// CSettingDlg dialog

class CSettingDlg : public CDialog
{
	DECLARE_DYNAMIC(CSettingDlg)

public:
	CSettingDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSettingDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_SETTING };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	int		m_cx;
	int		m_cy;
	DWORD	m_dwFrameDuration;

	int		m_nLayoutX;
	int		m_nLayoutY;

	int		m_nNumChannel;
	CHANNEL_INDEX m_arrChannel[MAX_CHANNEL_COUNT];

	void InitListCtrl(void);
	void RefreshStatus();
	int GetCheckedCount();
public:
	virtual BOOL OnInitDialog();
	void OnBnClickedOk();
	afx_msg void OnLvnItemchangedListChannels(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCbnSelchangeLayout();

	CComboBox m_cmbLayout;
	CComboBox m_cmbResolution;
	CComboBox m_cmbFrameDuration;
	CListCtrl m_lstChannel;
	BOOL m_bSync;
};
