
// ChildView.h : interface of the CChildView class
//


#pragma once

#include "MWFOURCC.h"

#include "CaptureThread.h"
#include "D11Renderer.h"

// CChildView window

class CChildView : public CWnd, public ICaptureCallback
{
// Construction
public:
	CChildView();

// Attributes
public:

// Operations
public:

// Overrides
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~CChildView();
	
	double GetCurrentFPS() {
		return m_thread.GetFPS();
	}
	int GetValidChannel() {
		return m_thread.GetValidChannel();
	}

	// ICaptureCallback
	void OnVideoSignalChanged(int cx, int cy, DWORD dwParam);
	void OnCaptureCallback(const BYTE* pbyData, int cbStride, DWORD dwParam);

	// Generated message map functions
protected:
	BOOL OpenPreview();
	void ClosePreview();

	INT_PTR CallFileSettings();

	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg UINT OnPowerBroadcast(UINT nPowerEvent, UINT nEventData);
	afx_msg void OnFileSettings();
protected:
	CD11Renderer*		m_pVideoRenderer;
	HPOWERNOTIFY		m_hPowerNotify;

	CCaptureThread		m_thread;

	int					m_nLayoutX;
	int					m_nLayoutY;

	CMWLock				m_lock;

	int					m_cx;
	int					m_cy;
	DWORD				m_dwFrameDuration;
	DWORD				m_dwFourcc;

	int					m_nNumChannel;
	CHANNEL_INDEX		m_arrChannel[MAX_CHANNEL_COUNT];
	
	BOOL				m_bSyncMode;
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};

