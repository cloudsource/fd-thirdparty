
// OSDPreview.h : main header file for the OSDPreview application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// COSDPreviewApp:
// See OSDPreview.cpp for the implementation of this class
//

class COSDPreviewApp : public CWinApp
{
public:
	COSDPreviewApp();


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation
	ULONG_PTR	m_gdiplusToken;
public:
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern COSDPreviewApp theApp;
