
// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "OSDPreview.h"

#include "StringUtils.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_COMMAND_RANGE(ID_DEVICE_BEGIN, ID_DEVICE_BEGIN + 16, OnChannelItem)
	ON_UPDATE_COMMAND_UI_RANGE(ID_DEVICE_BEGIN, ID_DEVICE_BEGIN + 16, OnUpdateChannelItem)
	ON_WM_MOVE()
END_MESSAGE_MAP()

// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	m_nChlCount = 0;
	m_nCurChl = -1;
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// create a view to occupy the client area of the frame
	if (!m_wndView.Create(NULL, NULL, AFX_WS_DEFAULT_VIEW, CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL))
	{
		TRACE0("Failed to create view window\n");
		return -1;
	}

	// Init Video Device
	CMenu* pMenu = GetMenu();
    CMenu* pSubMenu = pMenu->GetSubMenu(1);

	m_nChlCount = MWGetChannelCount();
	TCHAR szChannelName[MAX_PATH];
	for (int i = 0; i < m_nChlCount; i ++) {
		MWCAP_CHANNEL_INFO chlInfo;
		if (MW_SUCCEEDED == MWGetChannelInfoByIndex(i, &chlInfo)) {
			CAutoConvertString strName(chlInfo.szProductName);
			_stprintf_s(szChannelName, _T("%s %d-%d\0"), (const TCHAR*)strName , chlInfo.byBoardIndex, chlInfo.byChannelIndex);
			pSubMenu->InsertMenuW(i + 1, MF_BYPOSITION, ID_DEVICE_BEGIN + 1 + i, szChannelName);
		}
	}

	pSubMenu->DeleteMenu(0, MF_BYPOSITION);

	if (m_nChlCount > 0) {
		OnChannelItem(ID_DEVICE_BEGIN + 1);
	}

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.lpszClass = AfxRegisterWndClass(0);
	return TRUE;
}

// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG


// CMainFrame message handlers

void CMainFrame::OnSetFocus(CWnd* /*pOldWnd*/)
{
	// forward focus to the view window
	m_wndView.SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	// let the view have first crack at the command
	if (m_wndView.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	// otherwise, do default handling
	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}


void CMainFrame::OnChannelItem(UINT nID)
{
	if (nID <= (UINT)(ID_DEVICE_BEGIN + m_nChlCount)) {	
		int nCurChl = nID - ID_DEVICE_BEGIN - 1;
		if (m_nCurChl == nCurChl)
			return;

		if (m_wndView.SelectChannel(nCurChl)) {
			m_nCurChl = nCurChl;
		}
		else {
			AfxMessageBox(_T("Select Channel error!"));
		}
	}
}

void CMainFrame::OnUpdateChannelItem(CCmdUI *pCmdUI)
{
	if (pCmdUI->m_nID <= (UINT)(ID_DEVICE_BEGIN + m_nChlCount)) {
		if (pCmdUI->m_nID == ID_DEVICE_BEGIN + 1 + m_nCurChl)
			pCmdUI->SetCheck(TRUE);
		else
			pCmdUI->SetCheck(FALSE);
	}

	pCmdUI->Enable(TRUE);
}

void CMainFrame::OnMove(int x, int y)
{
	CFrameWnd::OnMove(x, y);
}
