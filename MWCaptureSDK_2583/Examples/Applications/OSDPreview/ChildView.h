
// ChildView.h : interface of the CChildView class
//


#pragma once

#include "stdafx.h"
#include "D11Renderer.h"

// CChildView window

class CChildView : public CWnd
{
// Construction
public:
	CChildView();

// Attributes
public:
	HANDLE			m_hThread;
	HANDLE			m_hExitThread;

	HCHANNEL		m_hChannel;
	CD11Renderer*	m_pRenderer;

	BOOL			m_bBorder;
	BOOL			m_bText;
	BOOL			m_bIcon;

	Bitmap*			m_pIcon[5];
	int				m_nIconIndex;

	DWORD			m_dwFrameCount;
// Operations
public:
	BOOL SelectChannel(int nIndex);

	static DWORD WINAPI ThreadProc(LPVOID pvParam) {
		CChildView* pThis = (CChildView *)pvParam;
		return pThis->ThreadProc();
	}
	DWORD ThreadProc();

	BOOL LoadOSD(HOSD hOSD);
	BOOL LoadText(HOSD hOSD);

// Overrides
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~CChildView();

	BOOL LoadImageFromResource(Bitmap** ppImage, UINT nResID, LPCTSTR lpTyp);
	// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnFileText();
	afx_msg void OnUpdateFileText(CCmdUI *pCmdUI);
	afx_msg void OnFileBorder();
	afx_msg void OnUpdateFileBorder(CCmdUI *pCmdUI);
	afx_msg void OnFileIcon();
	afx_msg void OnUpdateFileIcon(CCmdUI *pCmdUI);
};

