
// MultiAudioCaptureDlg.h : header file
//

#pragma once

#include "LibMWCapture\MWCapture.h"
#include "WaveFile.h"
#include "ChartCtrl\ChartCtrl.h"
#include "ChartCtrl\ChartLineSerie.h"

#define PCM_FILE_NAME		"MultiAudioCapture"
#define PCM_FILE_SUFFIX		".wav"
#define ENCODED_FILE_NAME	"MultiAudioCapture"
#define ENCODED_FILE_SUFFIX	".raw"

#define VIEW_PERCENT 96
#define VIEW_SAMPLES_PER_FRAME (MWCAP_AUDIO_SAMPLES_PER_FRAME / VIEW_PERCENT)

#define UPDATE_LENGTH 40
#define VIEW_LENGTH	(UPDATE_LENGTH * 5)

#define MAX_BIT_DEPTH_IN_BYTE (sizeof(DWORD))

//#define ALWAYS_USE_16_BITS_PER_SAMPLE

// CMultiAudioCaptureDlg dialog
class CMultiAudioCaptureDlg : public CDialogEx
{
// Construction
public:
	CMultiAudioCaptureDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MULTIAUDIOCAPTURE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	
	HCHANNEL						m_hChannel;
	HANDLE							m_hAudioEvent;
	
	MWCAP_AUDIO_SIGNAL_STATUS		m_audioSignalStatus;
	MWCAP_AUDIO_CAPTURE_FRAME		m_audioFrame;
	BOOL							m_bIsLPCM;
	char							m_szEncodedFmt[MAX_PATH];
	int								m_nPresentChannel;
	int								m_nBitDepthInByte;

	BOOL							m_bAudioCapturing;
	HANDLE							m_hAudioCaptureThread;
	
	BYTE							m_byAudioSamples[MWCAP_AUDIO_SAMPLES_PER_FRAME * MWCAP_AUDIO_MAX_NUM_CHANNELS * MAX_BIT_DEPTH_IN_BYTE];
	BYTE							m_byEncodedAudioSamples[MWCAP_AUDIO_SAMPLES_PER_FRAME * MWCAP_AUDIO_MAX_NUM_CHANNELS * MAX_BIT_DEPTH_IN_BYTE];
	
	BOOL							m_bRecording;
	BOOL							m_bStartRecord;
	CWaveFile						m_file;
	FILE*							m_pEncodedFile;
	LONGLONG						m_nFileSize;

	char							m_szLastRecordFile[MAX_PATH];
	
	// Used only when saving encoded audio 
	BYTE							m_byBurstHead[6]; // without F8 72 sync tag
	int								m_cbBurstHead;
	BOOL							m_bGotSyncF8;
	BOOL							m_bGotSyncTag;
	int								m_nBurstSize;

	// View wave
	CChartCtrl						m_chart[MWCAP_AUDIO_MAX_NUM_CHANNELS];
	CChartLineSerie *				m_pLine[MWCAP_AUDIO_MAX_NUM_CHANNELS];

	int								m_nUpdatePos;
	double							m_szFrameX[UPDATE_LENGTH * VIEW_SAMPLES_PER_FRAME];
	double							m_szFrameY[MWCAP_AUDIO_MAX_NUM_CHANNELS][UPDATE_LENGTH * VIEW_SAMPLES_PER_FRAME];

	// Bottom status
	CHAR							m_szBottomStatus[MAX_PATH];

private:
	static DWORD WINAPI AudioCaptureThreadProc(LPVOID pvParam) {
		CMultiAudioCaptureDlg* pThis = (CMultiAudioCaptureDlg *)pvParam;
		return pThis->AudioCaptureThreadProcEx();
	}
	DWORD AudioCaptureThreadProcEx();

	BOOL UpdateInfo();
	void InitChart();
	void UninitChart();
	void ViewFrame();
	void UpdateBottomStatus();
	void UpdateWaveView();
	void ParseEncodedAudioFrame();

public:
	afx_msg void OnClose();
	afx_msg void OnBnClickedButtonRecord();
	afx_msg void OnBnClickedButtonPlay();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	BOOL m_bDisplay;
	afx_msg void OnBnClickedCheckWave();
};
