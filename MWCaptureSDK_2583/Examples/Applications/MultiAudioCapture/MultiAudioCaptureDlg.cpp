
// MultiAudioCaptureDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MultiAudioCapture.h"
#include "MultiAudioCaptureDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define RENDER_TIMER_ID					1
#define UPDATE_FILE_SIZE_TIMER_ID		2

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMultiAudioCaptureDlg dialog




CMultiAudioCaptureDlg::CMultiAudioCaptureDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMultiAudioCaptureDlg::IDD, pParent)
	, m_bDisplay(TRUE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMultiAudioCaptureDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	for (int i = 0; i < MWCAP_AUDIO_MAX_NUM_CHANNELS; ++i)
		DDX_Control(pDX, IDC_CHARTCTRL_WAVE + i, m_chart[i]);
	DDX_Check(pDX, IDC_CHECK_WAVE, m_bDisplay);
}

BEGIN_MESSAGE_MAP(CMultiAudioCaptureDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON_RECORD, &CMultiAudioCaptureDlg::OnBnClickedButtonRecord)
	ON_BN_CLICKED(IDC_BUTTON_PLAY, &CMultiAudioCaptureDlg::OnBnClickedButtonPlay)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_WAVE, &CMultiAudioCaptureDlg::OnBnClickedCheckWave)
END_MESSAGE_MAP()


// CMultiAudioCaptureDlg message handlers

BOOL CMultiAudioCaptureDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	m_bAudioCapturing		= FALSE;
	m_hAudioCaptureThread	= NULL;
	m_hChannel				= NULL;
	m_bRecording			= FALSE;
	m_bStartRecord			= FALSE;
	m_pEncodedFile			= NULL;
	m_hAudioEvent			= NULL;

	memset(m_szEncodedFmt, 0, MAX_PATH);
	memset(m_szLastRecordFile, 0, MAX_PATH);
	memset(m_szBottomStatus, 0, MAX_PATH);
	memset(m_byAudioSamples, 0, sizeof(m_byAudioSamples));
	memset(m_byEncodedAudioSamples, 0, sizeof(m_byEncodedAudioSamples));

	GetDlgItem(IDC_BUTTON_PLAY)->EnableWindow(FALSE);
	SetDlgItemTextA(GetSafeHwnd(), IDC_STATIC_BOTTOM_STATUS, m_szBottomStatus);
	
	InitChart();

	int nChannelIndex = 0;

	for (; nChannelIndex < ::MWGetChannelCount(); ++nChannelIndex) {
		if (m_hChannel) {
			MWCloseChannel(m_hChannel);
			m_hChannel = NULL;
		}

		WCHAR szDevicePath[MAX_PATH];
		if (MW_SUCCEEDED != ::MWGetDevicePath(nChannelIndex, szDevicePath)) {
			continue;
		}

		m_hChannel = ::MWOpenChannelByPath(szDevicePath);
		if (m_hChannel == NULL) {
			continue;
		}

		DWORD dwInputCount = 0;
		if (MW_SUCCEEDED != MWGetAudioInputSourceArray(m_hChannel, NULL, &dwInputCount))
			continue;

		if (dwInputCount == 0) {
			continue;
		}

		if (!UpdateInfo())
			continue;

		break;
	}

	m_hAudioEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	if (MW_SUCCEEDED != MWStartAudioCapture(m_hChannel)) {
		MessageBox(_T("Start capture audio fail !"), _T("MultiAudioCapture"));
		return 0;
	}

	m_bAudioCapturing = TRUE;
	m_hAudioCaptureThread = CreateThread(NULL, 0, AudioCaptureThreadProc, (LPVOID)this, 0, NULL);
	if (m_hAudioCaptureThread == NULL) {
		m_bAudioCapturing = FALSE;
		return 0;
	}
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMultiAudioCaptureDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMultiAudioCaptureDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMultiAudioCaptureDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

BOOL CMultiAudioCaptureDlg::UpdateInfo()
{
	MWCAP_CHANNEL_INFO infoChannel = {0};
	if (MW_SUCCEEDED != ::MWGetChannelInfo(m_hChannel, &infoChannel)) {
		MessageBox(_T("Get channel info fail !"), _T("MultiAudioCapture"));
		return FALSE;
	}

	CHAR szChannelName[MAX_PATH];
	sprintf_s(szChannelName, "%s %x:%x\0", infoChannel.szProductName, infoChannel.byBoardIndex, infoChannel.byChannelIndex);
	SetDlgItemTextA(GetSafeHwnd(), IDC_STATIC_CHANNEL_NAME, szChannelName);
	SetDlgItemTextA(GetSafeHwnd(), IDC_STATIC_AUDIO_DEVICE, szChannelName);
	SetDlgItemTextA(GetSafeHwnd(), IDC_STATIC_SERIAL_NUMBER, infoChannel.szBoardSerialNo);

	CHAR szVersion[MAX_PATH];
	sprintf_s(szVersion, "%d.%d.%d", infoChannel.dwDriverVersion >> 24, (infoChannel.dwDriverVersion >> 16) & 0x00ff, infoChannel.dwDriverVersion & 0x00ffff);
	SetDlgItemTextA(GetSafeHwnd(), IDC_STATIC_DRIVER, szVersion);

	CHAR szFirmware[MAX_PATH];
	sprintf_s(szFirmware, "%d.%d", infoChannel.dwFirmwareVersion >> 16, infoChannel.dwFirmwareVersion & 0x00ffff);
	SetDlgItemTextA(GetSafeHwnd(), IDC_STATIC_FIRMWARE, szFirmware);

	DWORD dwSource = 0;
	if (MW_SUCCEEDED != ::MWGetAudioInputSource(m_hChannel, &dwSource)) {
		MessageBox(_T("Get audio input source fail !"), _T("MultiAudioCapture"));
		return FALSE;
	}

	//MWCAP_AUDIO_INPUT_TYPE_NONE					= 0x00,
	//MWCAP_AUDIO_INPUT_TYPE_HDMI					= 0x01,
	//MWCAP_AUDIO_INPUT_TYPE_SDI					= 0x02,
	//MWCAP_AUDIO_INPUT_TYPE_LINE_IN				= 0x04,
	//MWCAP_AUDIO_INPUT_TYPE_MIC_IN					= 0x08

	switch (dwSource >> 8) {
	case MWCAP_AUDIO_INPUT_TYPE_HDMI:
		SetDlgItemText(IDC_STATIC_AUDIO_INPUT, _T("HDMI"));
		break;
	case MWCAP_AUDIO_INPUT_TYPE_SDI:
		SetDlgItemText(IDC_STATIC_AUDIO_INPUT, _T("SDI"));
		break;
	case MWCAP_AUDIO_INPUT_TYPE_LINE_IN:
		SetDlgItemText(IDC_STATIC_AUDIO_INPUT, _T("Line In"));
		break;
	case MWCAP_AUDIO_INPUT_TYPE_MIC_IN:
		SetDlgItemText(IDC_STATIC_AUDIO_INPUT, _T("MIC In"));
		break;
	default:
		SetDlgItemText(IDC_STATIC_AUDIO_INPUT, _T("NONE"));
		break;
	}

	if (MW_SUCCEEDED != MWGetAudioSignalStatus(m_hChannel, &m_audioSignalStatus)) {
		MessageBox(_T("Get audio signal status fail !"), _T("MultiAudioCapture"));
		return FALSE;
	}

	if (m_audioSignalStatus.bLPCM) {
		m_bIsLPCM = TRUE;
	}
	else {
		m_bIsLPCM = FALSE;
	}
	
	if (m_audioSignalStatus.wChannelValid == 0) {
		SetDlgItemText(IDC_STATIC_AUDIO_SAMPLERATE, _T("None"));
	}
	else {
		CString strTitle;
		strTitle.Format(_T("%d Hz, %d bits, %s"), m_audioSignalStatus.dwSampleRate, m_audioSignalStatus.cBitsPerSample, m_bIsLPCM ? _T("LPCM") : _T("Encoded"));
		SetDlgItemText(IDC_STATIC_AUDIO_SAMPLERATE, strTitle);
		//SetDlgItemInt(IDC_STATIC_AUDIO_BITS_PER_SAMPLE, m_audioSignalStatus.cBitsPerSample);
	}

#ifdef ALWAYS_USE_16_BITS_PER_SAMPLE
	m_nBitDepthInByte = 2;
#else
	m_nBitDepthInByte = m_audioSignalStatus.cBitsPerSample / 8;
#endif
	
	for (int i = 0; i < MWCAP_AUDIO_MAX_NUM_CHANNELS; ++i) {
		m_chart[i].GetAxis(CChartCtrl::LeftAxis)->SetMinMax(- (0x01 << (m_nBitDepthInByte * 8 - 1)), (0x01 << (m_nBitDepthInByte * 8 - 1)) - 1);
	}

	m_nPresentChannel = 0;
	for (int i = 0; i < 4; ++i) {
		BOOL bPresent = (m_audioSignalStatus.wChannelValid & (0x01 << i)) ? TRUE : FALSE;
		
		m_nPresentChannel += bPresent ? 2 : 0;
			
		SetDlgItemText(IDC_STATIC_CHANNEL12 + i, bPresent ? _T("Present") : _T("Not Present"));
	}

	CString strData;
	for (int i = 0; i < 24; ++i) {
		strData.AppendFormat(_T("%02X "), m_audioSignalStatus.channelStatus.abyData[i]);
	}
	SetDlgItemText(IDC_STATIC_CHANNEL_STATUS_DATA, strData);

	return TRUE;
}

DWORD CMultiAudioCaptureDlg::AudioCaptureThreadProcEx() {	
	SetPriorityClass(m_hAudioCaptureThread, REALTIME_PRIORITY_CLASS);

	HNOTIFY hAudioNotify = MWRegisterNotify(m_hChannel, m_hAudioEvent, MWCAP_NOTIFY_AUDIO_INPUT_SOURCE_CHANGE | MWCAP_NOTIFY_AUDIO_SIGNAL_CHANGE | MWCAP_NOTIFY_AUDIO_FRAME_BUFFERED);

	ULONGLONG ullStatusBits = 0LL;

	MWCAP_AUDIO_SIGNAL_STATUS audioSignalStatus;
	MWGetAudioSignalStatus(m_hChannel, &audioSignalStatus);

	m_bGotSyncF8 = FALSE;
	m_bGotSyncTag = FALSE;
	m_nBurstSize = -1;
	m_cbBurstHead = 0;
					
	while (m_bAudioCapturing) {
		WaitForSingleObject(m_hAudioEvent, INFINITE);

		if (MW_SUCCEEDED != MWGetNotifyStatus(m_hChannel, hAudioNotify, &ullStatusBits))
			continue;

		if (ullStatusBits & (MWCAP_NOTIFY_AUDIO_SIGNAL_CHANGE | MWCAP_NOTIFY_AUDIO_INPUT_SOURCE_CHANGE)) {
			MWGetAudioSignalStatus(m_hChannel, &audioSignalStatus);
			OutputDebugString(_T("Audio Signal Changed\n"));

			if (m_bRecording) {
				OnBnClickedButtonRecord();
			}

			if (!UpdateInfo()) {
				MessageBox(_T("Audio signal is changed and cannot get current aduio signal status. Exit ! Please restart !"), _T("MultiAudioCapture"));
				break;
			}

			m_bGotSyncF8 = FALSE;
			m_bGotSyncTag = FALSE;
			m_nBurstSize = -1;
			m_cbBurstHead = 0;
			memset(m_szEncodedFmt, 0, MAX_PATH);
			memset(m_byAudioSamples, 0, sizeof(m_byAudioSamples));
			memset(m_byEncodedAudioSamples, 0, sizeof(m_byEncodedAudioSamples));
		}

		if (ullStatusBits & MWCAP_NOTIFY_AUDIO_FRAME_BUFFERED) {
			do {
				if (MW_SUCCEEDED != MWCaptureAudioFrame(m_hChannel, &m_audioFrame))
					break;

				BYTE* pbAudioFrame = (BYTE*)m_audioFrame.adwSamples;

				if (m_bIsLPCM) {
					for (int j = 0; j < m_nPresentChannel / 2; ++j) {
						for (int i = 0; i < MWCAP_AUDIO_SAMPLES_PER_FRAME; i++) {
							int nWritePos	= (i * m_nPresentChannel + j * 2) * m_nBitDepthInByte;
							int nReadPos	= (i * MWCAP_AUDIO_MAX_NUM_CHANNELS + j) * MAX_BIT_DEPTH_IN_BYTE;
							int nReadPos2	= (i * MWCAP_AUDIO_MAX_NUM_CHANNELS + j + MWCAP_AUDIO_MAX_NUM_CHANNELS / 2) * MAX_BIT_DEPTH_IN_BYTE;
							for (int k = 0; k < m_nBitDepthInByte; ++k) {
								m_byAudioSamples[nWritePos + k]						= pbAudioFrame[nReadPos + MAX_BIT_DEPTH_IN_BYTE - m_nBitDepthInByte + k];
								m_byAudioSamples[nWritePos + m_nBitDepthInByte + k]	= pbAudioFrame[nReadPos2 + MAX_BIT_DEPTH_IN_BYTE - m_nBitDepthInByte + k];
							}
						}

					}

					if (m_bRecording && m_bStartRecord && m_file.IsOpen()) {
						if (m_file.Write((const BYTE*)m_byAudioSamples, MWCAP_AUDIO_SAMPLES_PER_FRAME * m_nPresentChannel * m_nBitDepthInByte))	
							m_nFileSize += MWCAP_AUDIO_SAMPLES_PER_FRAME * m_nPresentChannel * m_nBitDepthInByte;
					}

					if (m_bDisplay)
						ViewFrame();
				}
				else {
					int nEncodedFrameSize = 0;
					for (int j = 0; j < m_nPresentChannel / 2; ++j) {
						for (int i = 0; i < MWCAP_AUDIO_SAMPLES_PER_FRAME; i++) {
							int nWritePos	= (i * m_nPresentChannel + j * 2) * m_nBitDepthInByte;
							int nReadPos	= (i * MWCAP_AUDIO_MAX_NUM_CHANNELS + j) * MAX_BIT_DEPTH_IN_BYTE;
							int nReadPos2	= (i * MWCAP_AUDIO_MAX_NUM_CHANNELS + j + MWCAP_AUDIO_MAX_NUM_CHANNELS / 2) * MAX_BIT_DEPTH_IN_BYTE;
							for (int k = 0; k < m_nBitDepthInByte; ++k) {
								m_byEncodedAudioSamples[nWritePos + k]						= pbAudioFrame[nReadPos + MAX_BIT_DEPTH_IN_BYTE - k - 1];
								m_byEncodedAudioSamples[nWritePos + m_nBitDepthInByte + k]	= pbAudioFrame[nReadPos2 + MAX_BIT_DEPTH_IN_BYTE - k - 1];
							}
						}
					}

					ParseEncodedAudioFrame();
				}

			} while (TRUE);
		}
	}

	MWUnregisterNotify(m_hChannel, hAudioNotify);

	return 0;
};

void CMultiAudioCaptureDlg::OnBnClickedButtonRecord()
{
	if (m_bRecording) {
		m_bRecording = FALSE;
		m_bStartRecord = FALSE;

		if (m_file.IsOpen()) {
			m_file.Exit();
		}

		if (m_pEncodedFile) {
			fclose(m_pEncodedFile);
			m_pEncodedFile = NULL;
				
			CHAR szNewName[MAX_PATH];
			if (strlen(m_szEncodedFmt) > 0) {
				sprintf_s(szNewName, "%s.%s\0", m_szLastRecordFile, m_szEncodedFmt);
			}
			else {				
				sprintf_s(szNewName, "%s%s\0", m_szLastRecordFile, ENCODED_FILE_SUFFIX);
			}
			rename(m_szLastRecordFile, szNewName);
			strcpy_s(m_szLastRecordFile, szNewName);
		}

		KillTimer(UPDATE_FILE_SIZE_TIMER_ID);

		UpdateBottomStatus();

		SetDlgItemText(IDC_BUTTON_RECORD, _T("Record To File "));
	}
	else {
		do {
			if (m_nPresentChannel <= 0) {
				MessageBox(_T("No audio channel input !\n"), _T("MultiAudioCapture"));
				break;
			}

			if (m_bIsLPCM) {
				time_t timeNow = time(NULL);
				struct tm * tmLocal;
				tmLocal = localtime(&timeNow);  
				sprintf_s(m_szLastRecordFile, "%s-%d-%02d-%02d-%02d-%02d-%02d%s", 
					PCM_FILE_NAME, tmLocal->tm_year + 1900, tmLocal->tm_mon + 1, tmLocal->tm_mday, tmLocal->tm_hour, tmLocal->tm_min, tmLocal->tm_sec, PCM_FILE_SUFFIX);

				if (!m_file.Init(m_szLastRecordFile, m_audioSignalStatus.dwSampleRate, 
					/*m_audioSignalStatus.channelStatus.Consumer.byChannelNumber*/ /*MWCAP_AUDIO_MAX_NUM_CHANNELS*/ m_nPresentChannel
					, m_nBitDepthInByte * 8)) {
						MessageBox(_T("Record wav file fail !\n"), _T("MultiAudioCapture"));
						break;
				}
				m_bStartRecord = TRUE;
			}
			else {
				time_t timeNow = time(NULL);
				struct tm * tmLocal;
				tmLocal = localtime(&timeNow);  
				sprintf_s(m_szLastRecordFile, "%s-%d-%02d-%02d-%02d-%02d-%02d", 
					ENCODED_FILE_NAME, tmLocal->tm_year + 1900, tmLocal->tm_mon + 1, tmLocal->tm_mday, tmLocal->tm_hour, tmLocal->tm_min, tmLocal->tm_sec);

				if (0 != fopen_s(&m_pEncodedFile, m_szLastRecordFile, "wb")) {
					MessageBox(_T("Record encoded audio file fail !\n"), _T("MultiAudioCapture"));
					break;
				}
				m_bStartRecord = FALSE;
			}

			m_nFileSize = 0;
			m_bRecording = TRUE;

			SetDlgItemText(IDC_BUTTON_RECORD, _T("Stop"));
			GetDlgItem(IDC_BUTTON_PLAY)->EnableWindow(TRUE);

			SetTimer(UPDATE_FILE_SIZE_TIMER_ID, 500, NULL);
		} while (FALSE);
	}
}


void CMultiAudioCaptureDlg::OnBnClickedButtonPlay()
{
	if (m_bRecording) {
		//MessageBox(_T("Please stop recording first !\n"), _T("MultiAudioCapture"));
		OnBnClickedButtonRecord();
	}

	if (strlen(m_szLastRecordFile) > 0) {
		ShellExecuteA(NULL, "open", m_szLastRecordFile, NULL, NULL, SW_HIDE);
	}
}

void CMultiAudioCaptureDlg::OnClose()
{
	UninitChart();

	if (m_bAudioCapturing) {
		m_bAudioCapturing = FALSE;
		if (m_hAudioEvent)
			SetEvent(m_hAudioEvent);
		WaitForSingleObject(m_hAudioCaptureThread, INFINITE);
		CloseHandle(m_hAudioCaptureThread);
		m_hAudioCaptureThread = NULL;
	}

	if (m_bRecording) {
		OnBnClickedButtonRecord();
	}

	if (m_hChannel) {
		if (MW_SUCCEEDED != MWStopAudioCapture(m_hChannel))
			OutputDebugStringA("CMultiAudioCaptureDlg::Stop audio capture return error !");

		MWCloseChannel(m_hChannel);
		m_hChannel = NULL;
	}
	
	if (m_hAudioEvent) {
		CloseHandle(m_hAudioEvent);
		m_hAudioEvent = NULL;
	}

	CDialogEx::OnClose();
}

void CMultiAudioCaptureDlg::InitChart()
{
	TCHAR szName[MAX_PATH];

	for (int i = 0; i < MWCAP_AUDIO_MAX_NUM_CHANNELS; ++i) {

		CChartAxis * pAxisHor = m_chart[i].CreateStandardAxis(CChartCtrl::BottomAxis);
		pAxisHor->SetAutomatic(true);
		pAxisHor->SetVisible(false);

		CChartAxis * pAxisVer = m_chart[i].CreateStandardAxis(CChartCtrl::LeftAxis);
		pAxisVer->SetMinMax(-32768, 32768);
		pAxisVer->SetAutomatic(false);
		pAxisVer->SetVisible(false);

		m_chart[i].SetBorderColor(0x00777777);
		m_chart[i].SetBackColor(0x00777777);

		m_chart[i].EnableRefresh(false);

		m_pLine[i] = m_chart[i].CreateLineSerie(); 
		m_pLine[i]->SetSeriesOrdering(poXOrdering);
		m_pLine[i]->SetColor(0x007FFF7F);

		_stprintf_s(szName, _T("Audio channel %d"), i);
		m_pLine[i]->SetName(szName);
		m_pLine[i]->SetShadowDepth(0);
	}

	m_szFrameX[UPDATE_LENGTH * VIEW_SAMPLES_PER_FRAME - 1] = 0;
	m_nUpdatePos = 0;

	SetTimer(RENDER_TIMER_ID, 300, NULL);

}

void CMultiAudioCaptureDlg::UninitChart()
{
	KillTimer(RENDER_TIMER_ID);
}

void CMultiAudioCaptureDlg::ViewFrame()
{
	m_szFrameX[m_nUpdatePos] = m_szFrameX[(m_nUpdatePos + UPDATE_LENGTH * VIEW_SAMPLES_PER_FRAME - 1) % (UPDATE_LENGTH * VIEW_SAMPLES_PER_FRAME)] + 1;

	for (int i = 0; i < VIEW_SAMPLES_PER_FRAME; ++i) {
		if (i < VIEW_SAMPLES_PER_FRAME - 1)
			m_szFrameX[m_nUpdatePos + i + 1] = m_szFrameX[m_nUpdatePos + i] + 1;

		for (int j = 0; j < MWCAP_AUDIO_MAX_NUM_CHANNELS; ++j) {
			if (j >= m_nPresentChannel) {
				m_szFrameY[j][m_nUpdatePos + i] = 0;
				continue;
			}

			int nReadPos = (i * VIEW_PERCENT * m_nPresentChannel + j) * m_nBitDepthInByte;
			DWORD dwValue = 0;
			for (int k = 0; k < m_nBitDepthInByte; ++k) {
				dwValue += (m_byAudioSamples[nReadPos + k] << (k * 8));
			}
			//m_szFrameY[j][m_nUpdatePos + i] = (short)dwValue;
			if (dwValue >= ((DWORD)0x01 << (m_nBitDepthInByte * 8 - 1))) {
				m_szFrameY[j][m_nUpdatePos + i] = (LONGLONG)dwValue - ((LONGLONG)(0x01) << m_nBitDepthInByte * 8);
			}
			else {
				m_szFrameY[j][m_nUpdatePos + i] = dwValue;
			}
		}
	}
	m_nUpdatePos += VIEW_SAMPLES_PER_FRAME;

	if (m_nUpdatePos == UPDATE_LENGTH * VIEW_SAMPLES_PER_FRAME) {
		m_nUpdatePos = 0;

		for (int j = 0; j < MWCAP_AUDIO_MAX_NUM_CHANNELS; ++j) {
			if (m_pLine[j]->GetPointsCount() >= VIEW_SAMPLES_PER_FRAME * VIEW_LENGTH) {
				m_pLine[j]->RemovePointsFromBegin(UPDATE_LENGTH * VIEW_SAMPLES_PER_FRAME);
			}

			m_pLine[j]->AddPoints(m_szFrameX, m_szFrameY[j], UPDATE_LENGTH * VIEW_SAMPLES_PER_FRAME);
		}

		UpdateWaveView();
	}

}

void CMultiAudioCaptureDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch(nIDEvent) {
	case RENDER_TIMER_ID : 
		{
			//UpdateWaveView();
			break;
		}
	case UPDATE_FILE_SIZE_TIMER_ID:
		{
			UpdateBottomStatus();
			break;
		}
	}

	CDialogEx::OnTimer(nIDEvent);
}

void CMultiAudioCaptureDlg::UpdateBottomStatus()
{
	if (m_bRecording)
		sprintf_s(m_szBottomStatus, "Writting %s file ... ", m_bIsLPCM ? "wave" : m_szEncodedFmt);
	else
		sprintf_s(m_szBottomStatus, "Audio %s file saved", m_bIsLPCM ? "wave" : m_szEncodedFmt);

	if (m_nFileSize >= 1024 * 1024) {
		sprintf_s(m_szBottomStatus, "%s %.2f MB", m_szBottomStatus, (double)m_nFileSize / (1024 * 1024));
	}
	else if (m_nFileSize >= 1024) {
		sprintf_s(m_szBottomStatus, "%s %.2f KB", m_szBottomStatus, (double)m_nFileSize / 1024);
	}
	else {
		sprintf_s(m_szBottomStatus, "%s %d B", m_szBottomStatus, m_nFileSize);
	}

	SetDlgItemTextA(GetSafeHwnd(), IDC_STATIC_BOTTOM_STATUS, m_szBottomStatus);
}

void CMultiAudioCaptureDlg::UpdateWaveView()
{
	for (int j = 0; j < MWCAP_AUDIO_MAX_NUM_CHANNELS; ++j) {
		m_chart[j].EnableRefresh(true);
		m_chart[j].EnableRefresh(false);
	}
}

void CMultiAudioCaptureDlg::ParseEncodedAudioFrame()
{
	int nTotallSize = m_nBitDepthInByte * m_nPresentChannel * MWCAP_AUDIO_SAMPLES_PER_FRAME;
	int nReadSize = 0;

	while (nReadSize < nTotallSize) {
		// Write payload
		if (m_nBurstSize > 0) {
			int nValid = min(m_nBurstSize, nTotallSize - nReadSize);
			m_nBurstSize -= nValid;

			if (m_bRecording && m_bStartRecord && m_pEncodedFile) {
				if (1 == fwrite(m_byEncodedAudioSamples + nReadSize, nValid, 1, m_pEncodedFile))
					m_nFileSize += nValid;
			}
			nReadSize += nValid;
		}

		if (m_nBurstSize > 0)
			continue;

		// Find next sync tag "F8 72"
		for (; (nReadSize < nTotallSize) && !m_bGotSyncTag; ++nReadSize) {
			if (m_bGotSyncF8){
				if (m_byEncodedAudioSamples[nReadSize] == 0x72) 
					m_bGotSyncTag = TRUE;
				else 
					m_bGotSyncF8 = FALSE;										
			}

			if (m_byEncodedAudioSamples[nReadSize] == 0xF8) {
				m_bGotSyncF8 = TRUE;
			}
		}

		if (!m_bGotSyncTag) 
			continue;

		// Collect another 6 bytes in burst header
		int nValid = min(nTotallSize - nReadSize, 6 - m_cbBurstHead);
		memcpy(m_byBurstHead + m_cbBurstHead, m_byEncodedAudioSamples + nReadSize, nValid);
		m_cbBurstHead += nValid;
		nReadSize += nValid;

		if (m_cbBurstHead != 6) 
			continue;

		// Parse burst head to get audio format and burst size
		if (strlen(m_szEncodedFmt) == 0) {
			switch(m_byBurstHead[3]) {
			case 0x03:
				strcpy_s(m_szEncodedFmt, "AC-3");
				break;
			case 0x04:
				strcpy_s(m_szEncodedFmt, "MPEG1");
				break;
			case 0x05:
				strcpy_s(m_szEncodedFmt, "MP3");
				break;
			case 0x06:
				strcpy_s(m_szEncodedFmt, "MPEG2");
				break;
			case 0x07:
				strcpy_s(m_szEncodedFmt, "AAC");
				break;
			case 0x08:
				strcpy_s(m_szEncodedFmt, "DTS");
				break;
			case 0x09:
				strcpy_s(m_szEncodedFmt, "ATRAC");
				break;
			}

			if (strlen(m_szEncodedFmt) > 0) {
				SetDlgItemTextA(GetSafeHwnd(), IDC_STATIC_AUDIO_FORMAT, m_szEncodedFmt);
			}
		}

		m_nBurstSize = ((m_byBurstHead[4] << 8) + m_byBurstHead[5]) / 8;
		m_cbBurstHead = 0;
		m_bGotSyncF8 = FALSE;
		m_bGotSyncTag = FALSE;

		// Start recording from the begining of a new audio frame
		if (m_bRecording && !m_bStartRecord && m_pEncodedFile) {
			m_bStartRecord = TRUE;
		}

	}
}

void CMultiAudioCaptureDlg::OnBnClickedCheckWave()
{
	UpdateData();
}
