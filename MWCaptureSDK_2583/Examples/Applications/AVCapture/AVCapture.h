
// AVCapture.h : main header file for the AVCapture application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CAVCaptureApp:
// See AVCapture.cpp for the implementation of this class
//

class CAVCaptureApp : public CWinApp
{
public:
	CAVCaptureApp();


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation

public:
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CAVCaptureApp theApp;
