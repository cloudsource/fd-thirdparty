
#include "stdafx.h"
#include "CaptureThread.h"

RECT SetRect(int x, int y, int cx, int cy)
{
	RECT ret;
	ret.left = x;
	ret.top = y;
	ret.right = cx;
	ret.bottom = cy;
	return ret;
}

CCaptureThread::CCaptureThread() 
{
	m_hVideoThread		= NULL;
	m_hAudioThread		= NULL;
	m_hExitVideoThread	= NULL;
	m_hExitAudioThread	= NULL;

	m_hChannel			= NULL;

	m_dFramePerSecond	= 0.0f;
	m_dSamplePerSecond	= 0.0f;

	m_pCallback			= NULL;

	m_llVideoFrame		= 0LL;
	m_llAudioSample		= 0LL;

	m_bRunning			= FALSE;
	m_nClip				= CLIP_NONE;
}

CCaptureThread::~CCaptureThread() 
{
}

BOOL CCaptureThread::Create(int nIndex, int cx, int cy, DWORD dwFourcc, DWORD dwFrameDuration, ICaptureCallback* pCallback, BOOL bCaptureAudio, DWORD dwParam, int nClip) 
{
	TCHAR szDevicePath[MAX_PATH];
	MWGetDevicePath(nIndex, szDevicePath);

	BOOL bRet = FALSE;
	do {
		m_hChannel = MWOpenChannelByPath(szDevicePath);
		if (m_hChannel == NULL)
			break;

		DWORD dwInputCount = 0;
		if (MW_SUCCEEDED != MWGetAudioInputSourceArray(m_hChannel, NULL, &dwInputCount))
			break;

		MWCAP_VIDEO_SIGNAL_STATUS videoSignalStatus;
		if (MW_SUCCEEDED != MWGetVideoSignalStatus(m_hChannel, &videoSignalStatus))
			break;

		MWCAP_AUDIO_SIGNAL_STATUS audioSignalStatus;
		if (MW_SUCCEEDED != MWGetAudioSignalStatus(m_hChannel, &audioSignalStatus))
			break;

		m_bVideoValid		= (videoSignalStatus.state == MWCAP_VIDEO_SIGNAL_LOCKED);
		if (m_bVideoValid) {
			m_cx			= videoSignalStatus.cx;
			m_cy			= videoSignalStatus.cy;
		}
		else {
			m_cx			= cx;
			m_cy			= cy;
		}

		m_bAudioValid		= (dwInputCount > 0 && ((audioSignalStatus.wChannelValid & 1) != 0));
		m_bLPCM				= audioSignalStatus.bLPCM;
		m_cBitsPerSample	= audioSignalStatus.cBitsPerSample;
		m_dwSampleRate		= audioSignalStatus.dwSampleRate;

		m_pCallback = pCallback;
		m_dwParam	= dwParam;
		m_nClip = nClip;
		m_dwFourcc	= dwFourcc;
		m_dwFrameDuration = dwFrameDuration;

		m_hExitVideoThread = CreateEvent(NULL, FALSE, FALSE, NULL);
		if (m_hExitVideoThread == NULL)
			break;

		m_bRunning = TRUE;

		m_hVideoThread = CreateThread(NULL, 0, VideoThreadProc, (LPVOID)this, 0, NULL);
		if (m_hVideoThread == NULL)
			break;

		if (bCaptureAudio) {
			m_hExitAudioThread = CreateEvent(NULL, FALSE, FALSE, NULL);
			if (m_hExitAudioThread == NULL)
				break;
			m_hAudioThread = CreateThread(NULL, 0, AudioThreadProc, (LPVOID)this, 0, NULL);
			if (m_hAudioThread == NULL)
				break;
		}

		bRet = TRUE;
	} while (FALSE);

	if (!bRet) {
		Destroy();
	}
	return TRUE;
}

void CCaptureThread::Destroy() 
{
	m_bRunning = FALSE;

	if (m_hExitVideoThread != NULL && m_hVideoThread != NULL) {
		SetEvent(m_hExitVideoThread);
		WaitForSingleObject(m_hVideoThread, INFINITE);

		CloseHandle(m_hExitVideoThread);
		m_hExitVideoThread = NULL;

		CloseHandle(m_hVideoThread);
		m_hVideoThread = NULL;
	}
	if (m_hExitAudioThread != NULL && m_hAudioThread != NULL) {
		SetEvent(m_hExitAudioThread);
		WaitForSingleObject(m_hAudioThread, INFINITE);

		CloseHandle(m_hExitAudioThread);
		m_hExitAudioThread = NULL;

		CloseHandle(m_hAudioThread);
		m_hAudioThread = NULL;
	}

	if (m_hChannel) {
		MWCloseChannel(m_hChannel);
		m_hChannel = NULL;
	}

	m_llVideoFrame = 0LL;
	m_llAudioSample = 0LL;
}

DWORD CCaptureThread::GetAdjustFrameDuration()
{
	return m_dwFrameDuration;
}

DWORD CCaptureThread::VideoThreadProc() 
{
	// Preview
	DWORD cbStride		= FOURCC_CalcMinStride(m_dwFourcc, m_cx, 2);
	DWORD dwFrameSize	= FOURCC_CalcImageSize(m_dwFourcc, m_cx, m_cy, cbStride);

	BYTE* byBuffer = NULL;
	byBuffer = new BYTE[dwFrameSize];
	memset(byBuffer, 0xFF, dwFrameSize);

	// Wait Events
	HANDLE hCaptureEvent	= CreateEvent(NULL, FALSE, FALSE, NULL);
	HANDLE hNotifyEvent		= CreateEvent(NULL, FALSE, FALSE, NULL);
	HANDLE hTimerEvent		= CreateEvent(NULL, FALSE, FALSE, NULL);

	MW_RESULT xr;
	do {
		xr = MWStartVideoCapture(m_hChannel, hCaptureEvent);
		if (xr != MW_SUCCEEDED)
			break;

		HNOTIFY hSignalNotify = MWRegisterNotify(m_hChannel, hNotifyEvent, MWCAP_NOTIFY_VIDEO_SIGNAL_CHANGE);
		if (hSignalNotify == NULL)
			break;

		HTIMER hTimerNotify = MWRegisterTimer(m_hChannel, hTimerEvent);
		if (hTimerNotify == NULL)
			break;

		DWORD dwFrameCount = 0;

		LONGLONG llBegin = 0LL;
		xr = MWGetDeviceTime(m_hChannel, &llBegin);
		if (xr != MW_SUCCEEDED)
			break;

		LONGLONG llExpireTime = llBegin;
		DWORD dwFrameDuration = m_dwFrameDuration;

		MWCAP_VIDEO_CAPTURE_STATUS captureStatus;

		LONGLONG llLast = llBegin;
		LONGLONG llNow = 0;

		while (m_bRunning) {
			//BOOL bLog = ((i + 1) % 150 == 0);

			//if (bLog) {
			//	LONGLONG llNow = 0LL;
			//	xr = MWGetDeviceTime(m_hChannel, &llNow);

			//	TCHAR szBuffer[256];
			//	wsprintf(szBuffer, _T("Before Shedule: Begin = %I64d, Now = %I64d, llExpireTime = %I64d, i = %d, dwDuration = %d.\n"), llBegin, llNow, llExpireTime, i, m_dwFrameDuration);
			//	OutputDebugString(szBuffer);
			//}

			if (m_hAudioThread != NULL && ((dwFrameDuration != m_dwFrameDuration) || (dwFrameCount % 10 == 0))) {
				dwFrameDuration = GetAdjustFrameDuration();
			}
			llExpireTime = llExpireTime + dwFrameDuration;

			xr = MWGetDeviceTime(m_hChannel, &llNow);
			if (xr != MW_SUCCEEDED)
				continue;

			if (llNow > llExpireTime)
				continue; // Drop this frame

			xr = MWScheduleTimer(m_hChannel, hTimerNotify, llExpireTime);
			if (xr != MW_SUCCEEDED) {
				continue;
			}

			HANDLE aEventNotify[3] = {m_hExitVideoThread, hTimerEvent, hNotifyEvent};
			DWORD dwRet = WaitForMultipleObjects(3, aEventNotify, FALSE, INFINITE);
			if (dwRet == WAIT_OBJECT_0 || dwRet == WAIT_FAILED) {
				break;
			}
			else if (dwRet == WAIT_OBJECT_0 + 1) {
				//if (bLog) {
				//	LONGLONG llNow = 0LL;
				//	xr = MWGetDeviceTime(m_hChannel, &llNow);

				//	TCHAR szBuffer[256];
				//	wsprintf(szBuffer, _T("Shedule Time: Begin = %I64d, Now = %I64d, i = %d, dwDuration = %d.\n"), llBegin, llNow, i, m_dwFrameDuration);
				//	OutputDebugString(szBuffer);
				//}

				MWCAP_VIDEO_BUFFER_INFO videoBufferInfo;
				if (MW_SUCCEEDED != MWGetVideoBufferInfo(m_hChannel, &videoBufferInfo))
					continue;

				MWCAP_VIDEO_SIGNAL_STATUS videoSignalStatus;
				xr = MWGetVideoSignalStatus(m_hChannel, &videoSignalStatus);
				if (xr != MW_SUCCEEDED)
					continue;

				MWCAP_VIDEO_FRAME_INFO videoFrameInfo;
				xr = MWGetVideoFrameInfo(m_hChannel, videoBufferInfo.iNewestBufferedFullFrame, &videoFrameInfo);
				if (xr != MW_SUCCEEDED)
					continue;

				//xr = MWCaptureVideoFrameToVirtualAddress(m_hChannel, MWCAP_VIDEO_FRAME_ID_NEWEST_BUFFERED, byBuffer, dwFrameSize, cbStride, FALSE, NULL, m_dwFourcc, m_cx, m_cy); 

				RECT rcSrc;
				switch (m_nClip) {
				case CLIP_NONE:
					rcSrc = SetRect(0, 0, videoSignalStatus.cx, videoSignalStatus.cy);
					break;
				case CLIP_TOPLEFT:
					rcSrc = SetRect(0, 0, videoSignalStatus.cx / 2, videoSignalStatus.cy / 2);
					break;
				case CLIP_TOPRIGHT:
					rcSrc = SetRect(videoSignalStatus.cx / 2 , 0, videoSignalStatus.cx, videoSignalStatus.cy / 2);
					break;
				case CLIP_BOTTOMLEFT:
					rcSrc = SetRect(0, videoSignalStatus.cy / 2, videoSignalStatus.cx / 2, videoSignalStatus.cy);
					break;
				case CLIP_BOTTOMRIGHT:
					rcSrc = SetRect(videoSignalStatus.cx / 2, videoSignalStatus.cy / 2, videoSignalStatus.cx, videoSignalStatus.cy);
					break;
				}
				
				xr = MWCaptureVideoFrameToVirtualAddressEx(m_hChannel, MWCAP_VIDEO_FRAME_ID_NEWEST_BUFFERED, byBuffer, dwFrameSize, cbStride, FALSE, NULL, m_dwFourcc, m_cx, m_cy,
					0, 0, NULL, NULL, 0, 100, 0, 100, 0, MWCAP_VIDEO_DEINTERLACE_BLEND, MWCAP_VIDEO_ASPECT_RATIO_IGNORE, &rcSrc, NULL, 0, 0, MWCAP_VIDEO_COLOR_FORMAT_UNKNOWN, MWCAP_VIDEO_QUANTIZATION_UNKNOWN, MWCAP_VIDEO_SATURATION_UNKNOWN);
				
				HANDLE aEventCapture[2] = {m_hExitVideoThread, hCaptureEvent};
				dwRet = WaitForMultipleObjects(2, aEventCapture, FALSE, INFINITE);
				if (dwRet == WAIT_OBJECT_0) {
					break;
				}

				xr = MWGetVideoCaptureStatus(m_hChannel, &captureStatus);

				if (m_pCallback != NULL) {
					m_pCallback->OnCaptureCallback(byBuffer, cbStride, m_dwParam);
				}

				dwFrameCount ++;
				m_llVideoFrame ++;

				//if (bLog) {
				//	LONGLONG llNow = 0LL;
				//	xr = MWGetDeviceTime(m_hChannel, &llNow);

				//	TCHAR szBuffer[256];
				//	wsprintf(szBuffer, _T("Get Frame: Begin = %I64d, Now = %I64d, i = %d, dwDuration = %d.\n"), llBegin, llNow - llBegin, i, m_dwFrameDuration);
				//	OutputDebugString(szBuffer);
				//}

				LONGLONG llCurrent = 0LL;
				MWGetDeviceTime(m_hChannel, &llCurrent);

				if (dwFrameCount % 10 == 0) {
					m_dFramePerSecond = (double)dwFrameCount * 10000000LL / (llCurrent - llLast);

					if (llCurrent - llLast > 30000000LL) {
						llLast = llCurrent;
						dwFrameCount = 0;
					}
				}
			}
			else if (dwRet == WAIT_OBJECT_0 + 2) {
				MWCAP_VIDEO_SIGNAL_STATUS videoSignalStatus;
				xr = MWGetVideoSignalStatus(m_hChannel, &videoSignalStatus);
				if (xr != MW_SUCCEEDED)
					continue;

				if (m_pCallback != NULL) {
					m_pCallback->OnVideoSignalChanged(videoSignalStatus.cx, videoSignalStatus.cy, m_dwParam);
				}
			}
		}

		xr = MWUnregisterNotify(m_hChannel, hSignalNotify);
		xr = MWUnregisterTimer(m_hChannel, hTimerNotify);
		xr = MWStopVideoCapture(m_hChannel);

	} while (FALSE);

	CloseHandle(hCaptureEvent);
	CloseHandle(hTimerEvent);
	CloseHandle(hNotifyEvent);

	if (byBuffer != NULL) {
		delete [] byBuffer;
	}

	return 0;
}

DWORD CCaptureThread::AudioThreadProc() 
{
	HANDLE hAudioEvent		= CreateEvent(NULL, FALSE, FALSE, NULL);
	HNOTIFY hAudioNotify	= NULL;

	MW_RESULT xr;
	do {
		xr = MWStartAudioCapture(m_hChannel);
		if (xr != MW_SUCCEEDED)
			break;

		hAudioNotify = MWRegisterNotify(m_hChannel, hAudioEvent, MWCAP_NOTIFY_AUDIO_SIGNAL_CHANGE | MWCAP_NOTIFY_AUDIO_FRAME_BUFFERED);
		if (hAudioNotify == NULL)
			break;

		LONGLONG llBegin = 0LL;
		xr = MWGetDeviceTime(m_hChannel, &llBegin);
		if (xr != MW_SUCCEEDED)
			break;

		DWORD dwSampleCount = 0;
		LONGLONG llLast = llBegin;

		while (m_bRunning) {
			HANDLE aEvent[] = {m_hExitAudioThread, hAudioEvent};
			DWORD dwRet = WaitForMultipleObjects(2, aEvent, FALSE, INFINITE);
			if (dwRet == WAIT_OBJECT_0 || dwRet == WAIT_FAILED)
				break;
			else if (dwRet == WAIT_OBJECT_0 + 1) {
				ULONGLONG ullStatusBits = 0LL;
				MWGetNotifyStatus(m_hChannel, hAudioNotify, &ullStatusBits);

				if (ullStatusBits & MWCAP_NOTIFY_AUDIO_SIGNAL_CHANGE) {
					DWORD dwInputCount = 0;
					xr = MWGetAudioInputSourceArray(m_hChannel, NULL, &dwInputCount);

					MWCAP_AUDIO_SIGNAL_STATUS audioSignalStatus;
					xr = MWGetAudioSignalStatus(m_hChannel, &audioSignalStatus);
					if (m_pCallback != NULL) {
						m_bAudioValid		= (dwInputCount > 0 && ((audioSignalStatus.wChannelValid & 1) != 0));
						m_bLPCM				= audioSignalStatus.bLPCM;
						m_cBitsPerSample	= audioSignalStatus.cBitsPerSample;
						m_dwSampleRate		= audioSignalStatus.dwSampleRate;

						m_pCallback->OnAudioSignalChanged(m_bAudioValid, m_bLPCM, m_cBitsPerSample, m_dwSampleRate, m_dwParam);
					}
				}

				if (ullStatusBits & MWCAP_NOTIFY_AUDIO_FRAME_BUFFERED) {
					do {
						MWCAP_AUDIO_CAPTURE_FRAME audioFrame;
						xr = MWCaptureAudioFrame(m_hChannel, &audioFrame);

						if (xr == MW_SUCCEEDED) {
							short asAudioSamples[MWCAP_AUDIO_SAMPLES_PER_FRAME * 2];
							for (int i = 0; i < MWCAP_AUDIO_SAMPLES_PER_FRAME; i++) {
								short sLeft = (short)(audioFrame.adwSamples[i * 8] >> 16);
								short sRight = (short)(audioFrame.adwSamples[i * 8 + 4] >> 16);

								asAudioSamples[i * 2] = sLeft;
								asAudioSamples[i * 2 + 1] = sRight;
							}
							if (m_pCallback != NULL) {
								m_pCallback->OnAudioCallback((const BYTE*)asAudioSamples, MWCAP_AUDIO_SAMPLES_PER_FRAME * 2 * sizeof(short), m_dwParam);
							}

							dwSampleCount += MWCAP_AUDIO_SAMPLES_PER_FRAME;
							m_llAudioSample += MWCAP_AUDIO_SAMPLES_PER_FRAME;

							LONGLONG llCurrent = 0LL;
							MWGetDeviceTime(m_hChannel, &llCurrent);

							if (dwSampleCount % (100 * MWCAP_AUDIO_SAMPLES_PER_FRAME) == 0) {
								m_dSamplePerSecond = (double)dwSampleCount * 10000000LL / (llCurrent - llLast);

								if (llCurrent - llLast > 30000000LL) {
									llLast = llCurrent;
									dwSampleCount = 0;
								}
							}

						}
					} while (xr == MW_SUCCEEDED);
				}
			}
		}

		xr = MWUnregisterNotify(m_hChannel, hAudioNotify);
		xr = MWStopAudioCapture(m_hChannel);
	} while (FALSE);

	CloseHandle(hAudioEvent);

	return 0;
}