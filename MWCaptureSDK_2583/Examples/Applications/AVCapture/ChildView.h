
// ChildView.h : interface of the CChildView class
//


#pragma once

#include <MMSystem.h>

#include "MWFOURCC.h"
#include "LockUtils.h"

// CChildView window
#include "CaptureThread.h"
#include "DSoundRenderer.h"
#include "D11Renderer.h"


class CChildView : public CWnd, public ICaptureCallback
{
// Construction
public:
	CChildView();

// Attributes
public:

// Operations
public:

// Overrides
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~CChildView();

	BOOL GetVideoFormat(int& cx, int& cy) {
		return m_previewThread.GetCaptureVideoFormat(cx, cy);
	}
	double GetPreviewFPS() {
		return m_previewThread.GetFPS();
	}
	double GetRecordFPS() {
		return m_recordThread.GetFPS();
	}
	double GetAudioSPS() {
		return m_recordThread.GetSPS();
	}

	typedef enum {
		PREVIEW_THREAD = 0x01,
		RECORD_THREAD
	};

	// ICaptureCallback
	void OnVideoSignalChanged(int cx, int cy, DWORD dwParam);
	void OnCaptureCallback(const BYTE* pbyData, int cbStride, DWORD dwParam);
	void OnAudioSignalChanged(BOOL bChannelValid, BOOL bPCM, BYTE cBitsPerSample, DWORD dwSampleRate, DWORD dwParam);
	void OnAudioCallback(const BYTE* pbyData, int cbSize, DWORD dwParam);
	// Generated message map functions

protected:
	BOOL OpenPreview(int nIndex);
	void ClosePreview();

	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	
	afx_msg void OnDeviceItem(UINT nID);
	afx_msg void OnUpdateDeviceItem(CCmdUI *pCmdUI);

	afx_msg LRESULT OnMsgVideoSignalChanged(WPARAM w, LPARAM l);
	afx_msg UINT OnPowerBroadcast(UINT nPowerEvent, UINT nEventData);
protected:
	CCaptureThread	m_previewThread;
	CCaptureThread	m_recordThread;

	HANDLE			m_hChannel;
	CD11Renderer*	m_pVideoRenderer;
	CDSoundRenderer*m_pAudioRenderer;

	int				m_nChannel;

	HANDLE			m_hPowerNotify;

	CMWLock			m_lockVideo;
	CMWLock			m_lockAudio;

	int m_nClip;
public:
	afx_msg void OnClose();
	afx_msg void OnFileNoclip();
	afx_msg void OnClipTopleft();
	afx_msg void OnClipTopright();
	afx_msg void OnUpdateFileNoclip(CCmdUI *pCmdUI);
	afx_msg void OnUpdateClipTopleft(CCmdUI *pCmdUI);
	afx_msg void OnUpdateClipTopright(CCmdUI *pCmdUI);
	afx_msg void OnClipBottomleft();
	afx_msg void OnUpdateClipBottomleft(CCmdUI *pCmdUI);
	afx_msg void OnClipBottomright();
	afx_msg void OnUpdateClipBottomright(CCmdUI *pCmdUI);
};

