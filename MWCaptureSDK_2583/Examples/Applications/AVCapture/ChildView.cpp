﻿
// ChildView.cpp : implementation of the CChildView class
//

#include "stdafx.h"
#include "AVCapture.h"
#include "ChildView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define CAPTURE_WIDTH		1920
#define CAPTURE_HEIGHT		1080

#define PREVIEW_DURATION	666667

#define PREVIEW_FOURCC		MWFOURCC_RGBA // MWFOURCC_YUY2 // MWFOURCC_BGRA // MWFOURCC_RGBA // MWFOURCC_YUY2

#define WM_VIDEOSIGNAL_CHANGED	(WM_USER + 301)
// CChildView

CChildView::CChildView()
{
	m_hChannel			= NULL;
	m_pVideoRenderer	= NULL;
	m_pAudioRenderer	= NULL;
	
	m_hPowerNotify		= NULL;

	m_nChannel			= -1;

	m_nClip = CLIP_NONE;
}

CChildView::~CChildView()
{
}

BEGIN_MESSAGE_MAP(CChildView, CWnd)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()

	ON_COMMAND_RANGE(ID_DEVICE_BEGIN, ID_DEVICE_BEGIN + 16, OnDeviceItem)
	ON_UPDATE_COMMAND_UI_RANGE(ID_DEVICE_BEGIN, ID_DEVICE_BEGIN + 16, OnUpdateDeviceItem)

	ON_WM_ERASEBKGND()
	ON_MESSAGE(WM_VIDEOSIGNAL_CHANGED, &CChildView::OnMsgVideoSignalChanged)
	//ON_WM_POWERBROADCAST()
	ON_WM_CLOSE()
	ON_COMMAND(IDM_FILE_NOCLIP, &CChildView::OnFileNoclip)
	ON_COMMAND(IDM_CLIP_TOPLEFT, &CChildView::OnClipTopleft)
	ON_COMMAND(IDM_CLIP_TOPRIGHT, &CChildView::OnClipTopright)
	ON_UPDATE_COMMAND_UI(IDM_FILE_NOCLIP, &CChildView::OnUpdateFileNoclip)
	ON_UPDATE_COMMAND_UI(IDM_CLIP_TOPLEFT, &CChildView::OnUpdateClipTopleft)
	ON_UPDATE_COMMAND_UI(IDM_CLIP_TOPRIGHT, &CChildView::OnUpdateClipTopright)
	ON_COMMAND(IDM_CLIP_BOTTOMLEFT, &CChildView::OnClipBottomleft)
	ON_UPDATE_COMMAND_UI(IDM_CLIP_BOTTOMLEFT, &CChildView::OnUpdateClipBottomleft)
	ON_COMMAND(IDM_CLIP_BOTTOMRIGHT, &CChildView::OnClipBottomright)
	ON_UPDATE_COMMAND_UI(IDM_CLIP_BOTTOMRIGHT, &CChildView::OnUpdateClipBottomright)
END_MESSAGE_MAP()

// CChildView message handlers

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

void CChildView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// Do not call CWnd::OnPaint() for painting messages
}


int CChildView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_nChannel = 0;
	OpenPreview(m_nChannel);

	GUID gNotify = GUID_MONITOR_POWER_ON;
	m_hPowerNotify = RegisterPowerSettingNotification(GetSafeHwnd(), &gNotify, DEVICE_NOTIFY_WINDOW_HANDLE);

	return 0;
}

void CChildView::OnDestroy()
{
	ClosePreview();

	if (m_hPowerNotify != NULL) {
		UnregisterPowerSettingNotification(m_hPowerNotify);
		m_hPowerNotify = NULL;
	}

	CWnd::OnDestroy();
}

void CChildView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

}

void CChildView::OnVideoSignalChanged(int cx, int cy, DWORD dwParam)
{
	PostMessage(WM_VIDEOSIGNAL_CHANGED);
}

void CChildView::OnCaptureCallback(const BYTE* pbyData, int cbStride, DWORD dwParam)
{
	CMWAutoLock lock(m_lockVideo);

	if (m_pVideoRenderer != NULL) {
		m_pVideoRenderer->Update(pbyData, cbStride);
	}
}

void CChildView::OnAudioSignalChanged(BOOL bChannelValid, BOOL bPCM, BYTE cBitsPerSample, DWORD dwSampleRate, DWORD dwParam)
{
	CMWAutoLock lock(m_lockAudio);
	
	PostMessage(WM_VIDEOSIGNAL_CHANGED);
}

void CChildView::OnAudioCallback(const BYTE* pbyData, int cbSize, DWORD dwParam)
{
	CMWAutoLock lock(m_lockAudio);

	if (m_pAudioRenderer) {
		m_pAudioRenderer->PutFrame(pbyData, cbSize);
	}
}

void CChildView::OnDeviceItem(UINT nID)
{
	ClosePreview();

	int nChannel = nID - ID_DEVICE_BEGIN - 1;
	OpenPreview(nChannel);
}

void CChildView::OnUpdateDeviceItem(CCmdUI *pCmdUI)
{
	if (pCmdUI->m_nID == ID_DEVICE_BEGIN + 1 + m_nChannel)
		pCmdUI->SetCheck(TRUE);
	else
		pCmdUI->SetCheck(FALSE);

}

BOOL CChildView::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
}

LRESULT CChildView::OnMsgVideoSignalChanged(WPARAM w, LPARAM l)
{
	ClosePreview();

	OpenPreview(m_nChannel);

	return 0;
}

BOOL CChildView::OpenPreview(int nIndex)
{
	if (!m_previewThread.Create(nIndex, CAPTURE_WIDTH, CAPTURE_HEIGHT, PREVIEW_FOURCC, PREVIEW_DURATION, this, TRUE, PREVIEW_THREAD, m_nClip)) {
		AfxMessageBox(_T("Open device error!"));
		return FALSE;
	}
	else {
		int nWidth, nHeight;
		BOOL bVideoValid = m_previewThread.GetCaptureVideoFormat(nWidth, nHeight);
		if (bVideoValid) {
			CRect rcWnd;
			rcWnd.SetRect(0, 0, nWidth, nHeight);
			AdjustWindowRectEx(&rcWnd, WS_OVERLAPPEDWINDOW, TRUE, WS_EX_CLIENTEDGE);
			AfxGetMainWnd()->SetWindowPos(NULL, 0, 0, rcWnd.Width(), rcWnd.Height(), SWP_NOACTIVATE | SWP_NOZORDER | SWP_NOMOVE);
		}
		
		CMWAutoLock lock(m_lockVideo);
		m_pVideoRenderer = new CD11Renderer();
		if (!m_pVideoRenderer->Init(nWidth, nHeight, PREVIEW_FOURCC, GetSafeHwnd())) {
			delete m_pVideoRenderer;
			m_pVideoRenderer = NULL;
			return FALSE;
		}

		BOOL bLPCM = FALSE;
		BYTE cBitsPerSample = 0;
		DWORD dwSampleRate = 0;
		if (!m_previewThread.GetInputAudioFormat(bLPCM, cBitsPerSample, dwSampleRate)) {
			OutputDebugStringA("Get input audio format fail !\n");
			bLPCM = FALSE;
		}

		if (bLPCM) {
			CMWAutoLock lock(m_lockAudio);
			BOOL bRet = FALSE;
			do {
				m_pAudioRenderer = new CDSoundRenderer();
				if (!m_pAudioRenderer)
					break;

				GUID guid = GUID_NULL;
				if (!m_pAudioRenderer->Initialize(&guid))
					break;

				if (!m_pAudioRenderer->Create(dwSampleRate, 2, 192, DEF_AUDIO_JITTER_FRAMES))
					break;

				if (!m_pAudioRenderer->Run())
					break;

				bRet = TRUE;
			} while (FALSE);

			if (!bRet) {
				m_pAudioRenderer->Deinitialize();
				delete m_pAudioRenderer;
				m_pAudioRenderer = NULL;
			}
		}
	}
	m_nChannel = nIndex;

	return TRUE;
}

void CChildView::ClosePreview()
{
	m_previewThread.Destroy();

	if (m_pVideoRenderer != NULL) {
		m_pVideoRenderer->Exit();
		delete m_pVideoRenderer;
		m_pVideoRenderer = NULL;
	}

	if (m_pAudioRenderer) {
		m_pAudioRenderer->Deinitialize();
		delete m_pAudioRenderer;
		m_pAudioRenderer = NULL;	
	}
}

UINT CChildView::OnPowerBroadcast(UINT nPowerEvent, UINT nEventData)
{
	if (nPowerEvent == PBT_POWERSETTINGCHANGE) {
		PPOWERBROADCAST_SETTING pSetting = (PPOWERBROADCAST_SETTING)nEventData;
		DWORD * pdwData = (DWORD*)pSetting->Data;
		if (*pdwData != 0x00) {
			OpenPreview(m_nChannel);
		}
		else {
			ClosePreview();
		}
	}

	return __super::OnPowerBroadcast(nPowerEvent, nEventData);
}

void CChildView::OnClose()
{
	__super::OnClose();
}

void CChildView::OnFileNoclip()
{
	m_nClip = CLIP_NONE;
	ClosePreview();
	OpenPreview(m_nChannel);
}

void CChildView::OnClipTopleft()
{
	m_nClip = CLIP_TOPLEFT;
	ClosePreview();
	OpenPreview(m_nChannel);
}

void CChildView::OnClipTopright()
{
	m_nClip = CLIP_TOPRIGHT;
	ClosePreview();
	OpenPreview(m_nChannel);
}

void CChildView::OnClipBottomleft()
{
	m_nClip = CLIP_BOTTOMLEFT;
	ClosePreview();
	OpenPreview(m_nChannel);
}

void CChildView::OnClipBottomright()
{
	m_nClip = CLIP_BOTTOMRIGHT;
	ClosePreview();
	OpenPreview(m_nChannel);
}

void CChildView::OnUpdateFileNoclip(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_nClip == CLIP_NONE);
}

void CChildView::OnUpdateClipTopleft(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_nClip == CLIP_TOPLEFT);
}

void CChildView::OnUpdateClipTopright(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_nClip == CLIP_TOPRIGHT);
}

void CChildView::OnUpdateClipBottomleft(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_nClip == CLIP_BOTTOMLEFT);
}

void CChildView::OnUpdateClipBottomright(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_nClip == CLIP_BOTTOMRIGHT);
}
