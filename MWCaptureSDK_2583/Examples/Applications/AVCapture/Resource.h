//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AVCapture.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_AVCaptureTYPE               130
#define ID_Menu                         32772
#define ID_FILE_RECORD                  32777
#define ID_FILE_STOP                    32778
#define ID_Menu32779                    32779
#define ID_FILE_MFX                     32780
#define ID_FILE_X264                    32781
#define IDM_FILE_NOCLIP                 32782
#define IDM_CLIP_TOPRIGHT               32783
#define Clip                            32784
#define IDM_CLIP_TOPLEFT                32785
#define IDM_FILE_BOTTOMLEFT             32786
#define IDM_CLIP_BOTTOMLEFT             32787
#define IDM_CLIP_BOTTOMRIGHT            32788
#define ID_DEVICE_BEGIN                 32801

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        310
#define _APS_NEXT_COMMAND_VALUE         32789
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
