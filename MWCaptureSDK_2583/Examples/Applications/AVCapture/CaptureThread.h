
#pragma once

class ICaptureCallback
{
public:
	virtual void OnVideoSignalChanged(int cx, int cy, DWORD dwParam) = NULL;
	virtual void OnCaptureCallback(const BYTE* pbyData, int cbStride, DWORD dwParam) = NULL;

	virtual void OnAudioSignalChanged(BOOL bValid, BOOL bLPCM, BYTE cBitsPerSample, DWORD dwSampleRate, DWORD dwParam) = NULL;
	virtual void OnAudioCallback(const BYTE* pbyData, int cbSize, DWORD dwParam) = NULL;
};

enum {
	CLIP_NONE,
	CLIP_TOPLEFT,
	CLIP_TOPRIGHT,
	CLIP_BOTTOMLEFT,
	CLIP_BOTTOMRIGHT,
};

class CCaptureThread
{
public:
	CCaptureThread();
	virtual ~CCaptureThread();

public:
	double GetFPS() {
		return m_dFramePerSecond;
	}
	double GetSPS() {
		return m_dSamplePerSecond;
	}
	BOOL GetCaptureVideoFormat(int& cx, int& cy) {
		cx = m_cx;
		cy = m_cy;
		return TRUE;
	}
	BOOL GetInputAudioFormat(BOOL& bLPCM, BYTE& cBitsPerSample, DWORD& dwSampleRate) {
		if (m_bAudioValid) {
			bLPCM			= m_bLPCM;
			cBitsPerSample	= m_cBitsPerSample;
			dwSampleRate	= m_dwSampleRate;
			return TRUE;
		}
		return FALSE;
	}

	BOOL Create(int nChannel, int cx, int cy, DWORD dwFourcc, DWORD dwFrameDuration, ICaptureCallback* pCallback, BOOL bCaptureAudio, DWORD dwParam = 0, int nClip = CLIP_NONE);
	void Destroy();

	static DWORD WINAPI VideoThreadProc(LPVOID pvParam) {
		CCaptureThread* pThis = (CCaptureThread *)pvParam;
		return pThis->VideoThreadProc();
	}
	DWORD VideoThreadProc();

	static DWORD WINAPI AudioThreadProc(LPVOID pvParam) {
		CCaptureThread* pThis = (CCaptureThread *)pvParam;
		return pThis->AudioThreadProc();
	}
	DWORD AudioThreadProc();

	DWORD GetAdjustFrameDuration();
protected:
	HANDLE			m_hVideoThread;
	HANDLE			m_hExitVideoThread;

	HANDLE			m_hAudioThread;
	HANDLE			m_hExitAudioThread;

	HCHANNEL		m_hChannel;

	int				m_cx;
	int				m_cy;

	DWORD			m_dwFourcc;
	DWORD			m_dwFrameDuration;

	ICaptureCallback*	m_pCallback;
	DWORD				m_dwParam;

	double			m_dFramePerSecond;
	double			m_dSamplePerSecond;

	BOOL			m_bAudioValid;
	BOOL			m_bLPCM;
	BYTE			m_cBitsPerSample;
	DWORD			m_dwSampleRate;

	BOOL			m_bVideoValid;

	BOOL			m_bCaptureAudio;

	// adjust video frame
	LONGLONG		m_llVideoFrame;
	LONGLONG		m_llAudioSample;

	BOOL			m_bRunning;
	int				m_nClip;
};
