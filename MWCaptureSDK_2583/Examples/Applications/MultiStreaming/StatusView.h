#pragma once


// CStatusView 视图

#define STATUS_AREA_HEIGHT 20

class CStatusView : public CView
{
	DECLARE_DYNCREATE(CStatusView)

protected:
	CStatusView();           // 动态创建所使用的受保护的构造函数
	virtual ~CStatusView();

public:
	virtual void OnDraw(CDC* pDC);      // 重写以绘制该视图
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	void UpdateStatus(int cx, int cy, DWORD dwFmt, double dFps);

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
	
private:
	CString m_strStatus;
};


