
// MainFrm.h : interface of the CMainFrame class
//

#pragma once
#include "ChildView.h"
#include "StatusView.h"

#define CHANNEL_VIEW_COUNT 3
const static int g_szResolution[CHANNEL_VIEW_COUNT][2] = {{1920, 1080}, {1280, 720}, {640, 480}};

class CMainFrame : public CFrameWnd
{
	
public:
	CMainFrame();
protected: 
	DECLARE_DYNAMIC(CMainFrame)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CSplitterWnd  m_wndSplitterMain;
	CSplitterWnd  m_wndSplitterLeft;
	CSplitterWnd  m_wndSplitterRight;

	int			  m_nChannelCount;
	int			  m_nSelChannelIndex;
	int			  m_nChannelIndex;

	HANDLE	      m_hPowerNotify;
// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnChannelItem(UINT nID);
	afx_msg void OnUpdateChannelItem(CCmdUI *pCmdUI);
	afx_msg void OnMove(int x, int y);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	DECLARE_MESSAGE_MAP()

	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);

	BOOL StartPreview();
	void StopPreview();

public:
	afx_msg UINT OnPowerBroadcast(UINT nPowerEvent, UINT nEventData);
};


