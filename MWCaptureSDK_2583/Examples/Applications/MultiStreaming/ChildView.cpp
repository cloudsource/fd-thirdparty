
// ChildView.cpp : implementation of the CChildView class
//

#include "stdafx.h"
#include "MultiStreaming.h"
#include "ChildView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CChildView

IMPLEMENT_DYNCREATE(CChildView, CView);

CChildView::CChildView()
{
	m_pRenderer			= NULL;
	m_hChannel			= NULL;

	m_hExitEvent		= NULL;
	m_hCaptureThread	= NULL;
	
	m_dCurrentFps		= 0.0;

	m_nWidth			= 0;
	m_nHeight			= 0;
}

CChildView::~CChildView()
{
}


BEGIN_MESSAGE_MAP(CChildView, CWnd)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	
END_MESSAGE_MAP()



// CChildView message handlers

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

void CChildView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// Do not call CWnd::OnPaint() for painting messages
}


int CChildView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  在此添加您专用的创建代码

	return 0;
}

void CChildView::OnDestroy()
{
	CWnd::OnDestroy();

	// TODO: 在此处添加消息处理程序代码
	StopPreview();
}

BOOL CChildView::StartPreview(int nChannelId, int cx, int cy)
{
	do {
		m_nWidth = cx;
		m_nHeight = cy;

		// Init renderer
		m_pRenderer = new CD11Renderer();
		if (!m_pRenderer->Init(m_nWidth, m_nHeight, CAPTURE_COLORSPACE, GetSafeHwnd())) {
			MessageBox(_T("Init renderer fail !\n"));
			break;
		}
		
		// Init capture
		TCHAR szDevicePath[MAX_PATH];
		if (MW_SUCCEEDED != MWGetDevicePath(nChannelId, szDevicePath)) {
			MessageBox(_T("MWGetDevicePath fail !\n"));
			break;		
		}
		
		m_hChannel = MWOpenChannelByPath(szDevicePath);
		if (m_hChannel == NULL) {
			MessageBox(_T("Open video capture fail !\n"));
			break;
		}

		m_dCurrentFps = 0.0;
	
		m_hExitEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
		if (m_hExitEvent == NULL)
			break;

		m_hCaptureThread = CreateThread(NULL, 0, VideoThreadProc, (LPVOID)this, 0, NULL);
		if (m_hCaptureThread == NULL)
			break;

		return TRUE;
	} while (FALSE);

	StopPreview();
	return FALSE;
}

void CChildView::StopPreview()
{
	if (m_hCaptureThread != NULL) {
		if (m_hExitEvent != NULL)
			SetEvent(m_hExitEvent);

		WaitForSingleObject(m_hCaptureThread, INFINITE);

		CloseHandle(m_hCaptureThread);
		m_hCaptureThread = NULL;
	}

	if (m_hExitEvent != NULL) {
		CloseHandle(m_hExitEvent);
		m_hExitEvent = NULL;
	}

	if (m_hChannel != NULL) {
		MWCloseChannel(m_hChannel);
		m_hChannel = NULL;
	}

	if (m_pRenderer != NULL) {
		m_pRenderer->Exit();
		delete m_pRenderer;
		m_pRenderer = NULL;
	}
}

void CChildView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

}

DWORD CChildView::VideoThreadProcEx() 
{
	// Preview
	DWORD cbStride		= FOURCC_CalcMinStride(CAPTURE_COLORSPACE, m_nWidth, 2);
	DWORD dwFrameSize	= FOURCC_CalcImageSize(CAPTURE_COLORSPACE, m_nWidth, m_nHeight, cbStride);

	BYTE* byBuffer = NULL;
	byBuffer = new BYTE[dwFrameSize];
	memset(byBuffer, 0xFF, dwFrameSize);

	// Wait Events
	HANDLE hCaptureEvent	= CreateEvent(NULL, FALSE, FALSE, NULL);
	HANDLE hTimerEvent		= CreateEvent(NULL, FALSE, FALSE, NULL);

	MW_RESULT xr;
	do {
		xr = MWStartVideoCapture(m_hChannel, hCaptureEvent);
		if (xr != MW_SUCCEEDED)
			break;

		xr = MWPinVideoBuffer(m_hChannel, byBuffer, dwFrameSize);
		if (xr != MW_SUCCEEDED)
			OutputDebugStringA("MWPinVideoBuffer fail !\n");

		HTIMER hTimerNotify = MWRegisterTimer(m_hChannel, hTimerEvent);
		if (hTimerNotify == NULL)
			break;

		DWORD dwFrameCount = 0;

		LONGLONG llBegin = 0LL;
		xr = MWGetDeviceTime(m_hChannel, &llBegin);
		if (xr != MW_SUCCEEDED)
			break;

		LONGLONG llExpireTime = llBegin;
		DWORD dwFrameDuration = FRAME_DURATION;

		MWCAP_VIDEO_CAPTURE_STATUS captureStatus;

		LONGLONG llLast = llBegin;
			
		HANDLE aEventNotify[] = {m_hExitEvent, hTimerEvent};
		const int nEventNotify = sizeof(aEventNotify) / sizeof(aEventNotify[0]);
		
		HANDLE aEventCapture[] = {m_hExitEvent, hCaptureEvent};
		const int nEventCapture = sizeof(aEventCapture) / sizeof(aEventCapture[0]);

		while (TRUE) {
			llExpireTime = llExpireTime + dwFrameDuration;
			xr = MWScheduleTimer(m_hChannel, hTimerNotify, llExpireTime);
			if (xr != MW_SUCCEEDED) {
				continue;
			}

			DWORD dwRet = WaitForMultipleObjects(nEventNotify, aEventNotify, FALSE, INFINITE);
			if (dwRet == WAIT_OBJECT_0 || dwRet == WAIT_FAILED) {
				break;
			}
			else if (dwRet == WAIT_OBJECT_0 + 1) {
				MWCAP_VIDEO_BUFFER_INFO videoBufferInfo;
				if (MW_SUCCEEDED != MWGetVideoBufferInfo(m_hChannel, &videoBufferInfo))
					continue;

				MWCAP_VIDEO_SIGNAL_STATUS videoSignalStatus;
				xr = MWGetVideoSignalStatus(m_hChannel, &videoSignalStatus);
				if (xr != MW_SUCCEEDED)
					continue;

				MWCAP_VIDEO_FRAME_INFO videoFrameInfo;
				xr = MWGetVideoFrameInfo(m_hChannel, videoBufferInfo.iNewestBufferedFullFrame, &videoFrameInfo);
				if (xr != MW_SUCCEEDED)
					continue;

				xr = MWCaptureVideoFrameToVirtualAddress(m_hChannel, MWCAP_VIDEO_FRAME_ID_NEWEST_BUFFERED, 
					byBuffer, dwFrameSize, cbStride, FALSE, NULL, CAPTURE_COLORSPACE, m_nWidth, m_nHeight); 
				
				dwRet = WaitForMultipleObjects(nEventCapture, aEventCapture, FALSE, INFINITE);
				if (dwRet == WAIT_OBJECT_0) {
					break;
				}

				xr = MWGetVideoCaptureStatus(m_hChannel, &captureStatus);

				if (m_pRenderer != NULL) {
					m_pRenderer->Update(byBuffer, cbStride);
				}

				dwFrameCount ++;

				LONGLONG llCurrent = 0LL;
				MWGetDeviceTime(m_hChannel, &llCurrent);

				if (llCurrent - llLast >= 10000000LL) {
					m_dCurrentFps = (double)dwFrameCount * 10000000LL / (llCurrent - llLast);
				}
			}
		}

		xr = MWUnregisterTimer(m_hChannel, hTimerNotify);
		xr = MWStopVideoCapture(m_hChannel);

		MWUnpinVideoBuffer(m_hChannel, byBuffer);
	} while (FALSE);

	if (hCaptureEvent != NULL)
		CloseHandle(hCaptureEvent);

	if (hTimerEvent != NULL)
		CloseHandle(hTimerEvent);

	if (byBuffer != NULL) {
		delete [] byBuffer;
	}

	return 0;
}

void CChildView::OnDraw(CDC* pDC)
{    
}
