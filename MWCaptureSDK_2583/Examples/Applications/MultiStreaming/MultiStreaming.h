
// MultiStreaming.h : main header file for the MultiStreaming application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CMultiStreamingApp:
// See MultiStreaming.cpp for the implementation of this class
//

class CMultiStreamingApp : public CWinApp
{
public:
	CMultiStreamingApp();


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation

public:
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CMultiStreamingApp theApp;
