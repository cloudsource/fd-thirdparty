
// ChildView.h : interface of the CChildView class
//


#pragma once

#include "stdafx.h"
#include "D11Renderer.h"

#define FRAME_DURATION			333333

#define CAPTURE_COLORSPACE		MWFOURCC_RGBA

// CChildView window

class CChildView : public CView
{
// Construction
public:
	CChildView();
	virtual ~CChildView();
	
// Attributes
public:
	DECLARE_DYNCREATE(CChildView)
// Operations
public:

// Overrides
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	BOOL StartPreview(int nChannelId, int cx, int cy);
	void StopPreview();

	BOOL GetCaptureInfo(int* pcx, int* pcy, DWORD* pdwFmt, double* pdFps) {
		if (m_hChannel == NULL)
			return FALSE;

		*pcx = m_nWidth;
		*pcy = m_nHeight;
		*pdwFmt = CAPTURE_COLORSPACE;
		*pdFps = m_dCurrentFps;

		return TRUE;
	}

public:
	virtual void OnDraw(CDC* /*pDC*/);
	// Generated message map functions
protected:
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);

protected:
	static DWORD WINAPI VideoThreadProc(LPVOID pvParam) {
		CChildView* pThis = (CChildView *)pvParam;
		return pThis->VideoThreadProcEx();
	}
	DWORD VideoThreadProcEx();

private:	
	CD11Renderer*				m_pRenderer;
	HCHANNEL					m_hChannel;

	HANDLE						m_hExitEvent;
	HANDLE						m_hCaptureThread;

	double						m_dCurrentFps;

	int							m_nWidth;
	int							m_nHeight;
};

