#pragma once


#include "PreviewThread.h"

#include "D11Renderer.h"
// CVideoWnd

class CVideoWnd : public CWnd, public IPreviewCallback
{
	DECLARE_DYNAMIC(CVideoWnd)

public:
	CVideoWnd();
	virtual ~CVideoWnd();

public:
	BOOL OpenChannel(int nIndex);
	void CloseChannel();

	// ICaptureCallback
	BOOL OnVideoCallback(const BYTE* lpData, DWORD cbStride);

protected:
	CD11Renderer*	m_pRenderer;

	CPreviewThread	m_thread;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
};


