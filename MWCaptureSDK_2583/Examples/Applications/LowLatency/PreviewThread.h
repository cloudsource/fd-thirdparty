

#pragma once

#include <Windows.h>

class IPreviewCallback
{
public:
	virtual BOOL OnVideoCallback(const BYTE* lpData, DWORD cbStride) = NULL;
};

class CPreviewThread
{
public:
	CPreviewThread();
	virtual ~CPreviewThread();

public:
	BOOL Create(int nIndex, int cx, int cy, DWORD dwFrameDuration, DWORD dwFOURCC, IPreviewCallback* pCallback);
	void Destroy();

	static DWORD WINAPI ThreadProc(LPVOID pvParam) {
		CPreviewThread* pThis = (CPreviewThread *)pvParam;
		return pThis->ThreadProc();
	}
	DWORD ThreadProc();

protected:
	HCHANNEL		m_hChannel;

	HANDLE			m_hThread;
	HANDLE			m_hEvent;

	BOOL			m_bOnSignal;
	BOOL			m_bLowLatency;

	int				m_cx;
	int				m_cy;

	DWORD			m_dwFrameDuration;
	DWORD			m_dwFOURCC;

	MWCAP_VIDEO_SIGNAL_STATUS	m_status;

	IPreviewCallback* m_pCallback;
};