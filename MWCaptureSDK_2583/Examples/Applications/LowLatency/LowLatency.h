
// LowLatency.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CLowLatencyApp:
// See LowLatency.cpp for the implementation of this class
//

class CLowLatencyApp : public CWinApp
{
public:
	CLowLatencyApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
	virtual int ExitInstance();
};

extern CLowLatencyApp theApp;