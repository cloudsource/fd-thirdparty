
// LowLatencyDlg.h : header file
//

#pragma once
#include "afxwin.h"

#include "VideoWnd.h"

#define CAPTURE_WIDTH		1920
#define CAPTURE_HEIGHT		1080
#define CAPTURE_DURATION	166667
#define CAPTURE_COLOR		MWFOURCC_YUY2 // MWFOURCC_BGRA // MWFOURCC_YUY2

#define	CAPTURE_PART_LINE	256

// CLowLatencyDlg dialog
class CLowLatencyDlg : public CDialogEx
{
// Construction
public:
	CLowLatencyDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_LOWLATENCY_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	typedef enum {
		MODE_NORMAL,
		MODE_LOW_LATENCY
	} CAPTURE_MODE;

	typedef struct tagCAPTURE_DATA {
		HCHANNEL					hChannel;
		HANDLE						hThread;
		HANDLE						hEvent;

		MWCAP_VIDEO_SIGNAL_STATUS	status;

		DWORD						dwFrameCount;
		LONGLONG					llCaptureDuration;
		LONGLONG					llTotalDuration;

		int							nSel;

		CVideoWnd					wnd;
	} CAPTURE_DATA, *PCAPTURE_DATA;

	CAPTURE_DATA	m_data[2];

	HICON m_hIcon;

protected:
	static DWORD WINAPI ThreadProc(LPVOID pvParam) {
		CLowLatencyDlg* pThis = (CLowLatencyDlg *)pvParam;
		return pThis->ThreadProc(MODE_NORMAL);
	}
	static DWORD WINAPI ThreadProc1(LPVOID pvParam) {
		CLowLatencyDlg* pThis = (CLowLatencyDlg *)pvParam;
		return pThis->ThreadProc(MODE_LOW_LATENCY);
	}
	DWORD ThreadProc(CAPTURE_MODE mode);
	
	BOOL OpenChannel(CAPTURE_MODE mode, int nIndex);
	void CloseChannel(CAPTURE_MODE mode);

	CString GetStatus(MWCAP_VIDEO_SIGNAL_STATE status) {
		CString strRet;
		switch (status) {
		case MWCAP_VIDEO_SIGNAL_NONE: strRet = _T("None");break;
		case MWCAP_VIDEO_SIGNAL_UNSUPPORTED: strRet = _T("Unsupported");break;
		case MWCAP_VIDEO_SIGNAL_LOCKING: strRet = _T("Locking");break;
		case MWCAP_VIDEO_SIGNAL_LOCKED: strRet = _T("Locked");break;
		}
		return strRet;
	}
	CString GetResolutions(int cx, int cy, DWORD dwFrameDuration) {
		CString strRet;
		strRet.Format(_T("%d x %d, %.02f FPS"), cx, cy, (double)10000000 / dwFrameDuration);
		return strRet;
	}
	CString GetScanSize(int cx, int cy) {
		CString strRet;
		strRet.Format(_T("%d x %d Pixels"), cx, cy);
		return strRet;
	}
	CString GetOffset(int x, int y) {
		CString strRet;
		strRet.Format(_T("X:%d, Y:%d"), x, y);
		return strRet;
	}
	CString GetFormat(DWORD dwFrameDuration) {
		CString strRet;
		strRet.Format(_T("%d x %d, YUYV, %.02f fps"), CAPTURE_WIDTH, CAPTURE_HEIGHT, (double)10000000 / dwFrameDuration);
		return strRet;
	}
	CString GetLatency(DWORD dwFrameDuration) {
		CString strRet;
		strRet.Format(_T("%.02f ms"), (float)dwFrameDuration / 10000);
		return strRet;
	}
	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_cmbDevice0;
	CComboBox m_cmbDevice1;
	CString m_strSignal0;
	CString m_strSignal1;
	CString m_strResolution0;
	CString m_strScanSize0;
	CString m_strOffset0;
	CString m_strFormat0;
	CString m_strCaptureLatency0;
	CString m_strTotalLatency0;
	CString m_strResolution1;
	CString m_strScanSize1;
	CString m_strOffset1;
	CString m_strFormat1;
	CString m_strCaptureLatency1;
	CString m_strTotalLatency1;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnCbnSelchangeCombo0();
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnMove(int x, int y);
};
