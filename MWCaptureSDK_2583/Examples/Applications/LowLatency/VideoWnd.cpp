// VideoWnd.cpp : ʵ���ļ�
//

#include "stdafx.h"
#include "VideoWnd.h"

#define PREVIEW_WIDTH		352
#define PREVIEW_HEIGHT		240
#define PREVIEW_DURATION	333334
#define PREVIEW_FOURCC		MWFOURCC_RGBA

// CVideoWnd
IMPLEMENT_DYNAMIC(CVideoWnd, CWnd)

CVideoWnd::CVideoWnd()
{
	m_pRenderer = NULL;
}

CVideoWnd::~CVideoWnd()
{
}

BEGIN_MESSAGE_MAP(CVideoWnd, CWnd)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

int CVideoWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_pRenderer = new CD11Renderer();
	if (!m_pRenderer->Init(PREVIEW_WIDTH, PREVIEW_HEIGHT, PREVIEW_FOURCC, GetSafeHwnd())) {
		delete m_pRenderer;
		m_pRenderer = NULL;
		return -1;
	}

	return 0;
}

void CVideoWnd::OnDestroy()
{
	if (m_pRenderer != NULL) {
		m_pRenderer->Exit();
		delete m_pRenderer;
		m_pRenderer = NULL;
	}

	CWnd::OnDestroy();
}

BOOL CVideoWnd::OnEraseBkgnd(CDC* pDC)
{

	return CWnd::OnEraseBkgnd(pDC);
}

void CVideoWnd::OnPaint()
{
	CPaintDC dc(this); // device context for painting
}

BOOL CVideoWnd::OpenChannel(int nIndex)
{
	CloseChannel();

	BOOL bRet = FALSE;
	bRet = m_thread.Create(nIndex, PREVIEW_WIDTH, PREVIEW_HEIGHT, PREVIEW_DURATION, PREVIEW_FOURCC, this);
	return bRet;
}

void CVideoWnd::CloseChannel()
{
	m_thread.Destroy();
}

BOOL CVideoWnd::OnVideoCallback(const BYTE* lpData, DWORD cbStride)
{
	if (m_pRenderer != NULL) {
		m_pRenderer->Update(lpData, cbStride);
	}
	return TRUE;
}
