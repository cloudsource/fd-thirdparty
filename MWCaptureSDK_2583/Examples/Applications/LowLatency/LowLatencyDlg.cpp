
// LowLatencyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LowLatency.h"
#include "LowLatencyDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define ID_VIDEO_WND		1001

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CLowLatencyDlg dialog

CLowLatencyDlg::CLowLatencyDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLowLatencyDlg::IDD, pParent)
	, m_strSignal0(_T(""))
	, m_strResolution0(_T(""))
	, m_strScanSize0(_T(""))
	, m_strOffset0(_T(""))
	, m_strFormat0(_T(""))
	, m_strCaptureLatency0(_T(""))
	, m_strTotalLatency0(_T(""))
	, m_strSignal1(_T(""))
	, m_strResolution1(_T(""))
	, m_strScanSize1(_T(""))
	, m_strOffset1(_T(""))
	, m_strFormat1(_T(""))
	, m_strCaptureLatency1(_T(""))
	, m_strTotalLatency1(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	for (int i = 0; i < 2; i ++) {
		m_data[i].hChannel = NULL;
		m_data[i].hEvent = NULL;
		m_data[i].hThread = NULL;

		m_data[i].status.state = MWCAP_VIDEO_SIGNAL_NONE;
	}

}

void CLowLatencyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_0, m_cmbDevice0);
	DDX_Control(pDX, IDC_COMBO_1, m_cmbDevice1);
	DDX_Text(pDX, IDC_STATIC_SIGNAL_0, m_strSignal0);
	DDX_Text(pDX, IDC_STATIC_RESOLUTION_0, m_strResolution0);
	DDX_Text(pDX, IDC_STATIC_SCAN_SIZE_0, m_strScanSize0);
	DDX_Text(pDX, IDC_STATIC_OFFSET_0, m_strOffset0);
	DDX_Text(pDX, IDC_STATIC_FORMAT_0, m_strFormat0);
	DDX_Text(pDX, IDC_STATIC_CAPTURE_LATENCY_0, m_strCaptureLatency0);
	DDX_Text(pDX, IDC_STATIC_TOTAL_0, m_strTotalLatency0);
	DDX_Text(pDX, IDC_STATIC_SIGNAL_1, m_strSignal1);
	DDX_Text(pDX, IDC_STATIC_RESOLUTION_1, m_strResolution1);
	DDX_Text(pDX, IDC_STATIC_SCAN_SIZE_1, m_strScanSize1);
	DDX_Text(pDX, IDC_STATIC_OFFSET_1, m_strOffset1);
	DDX_Text(pDX, IDC_STATIC_FORMAT_1, m_strFormat1);
	DDX_Text(pDX, IDC_STATIC_CAPTURE_LATENCY_1, m_strCaptureLatency1);
	DDX_Text(pDX, IDC_STATIC_TOTAL_1, m_strTotalLatency1);
}

BEGIN_MESSAGE_MAP(CLowLatencyDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_CBN_SELCHANGE(IDC_COMBO_0, &CLowLatencyDlg::OnCbnSelchangeCombo0)
	ON_CBN_SELCHANGE(IDC_COMBO_1, &CLowLatencyDlg::OnCbnSelchangeCombo1)
	ON_WM_MOVE()
END_MESSAGE_MAP()


// CLowLatencyDlg message handlers

BOOL CLowLatencyDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// create renderer
	CRect rcPos;
	CWnd* pWnd;

	pWnd = GetDlgItem(IDC_STATIC_0);
	pWnd->GetWindowRect(rcPos);
	ScreenToClient(rcPos);
	pWnd->ShowWindow(SW_HIDE);
	m_data[MODE_NORMAL].wnd.Create(NULL, NULL, WS_CHILD | WS_VISIBLE, rcPos, this, ID_VIDEO_WND);

	pWnd = GetDlgItem(IDC_STATIC_1);
	pWnd->GetWindowRect(rcPos);
	ScreenToClient(rcPos);
	pWnd->ShowWindow(SW_HIDE);
	m_data[MODE_LOW_LATENCY].wnd.Create(NULL, NULL, WS_CHILD | WS_VISIBLE, rcPos, this, ID_VIDEO_WND + 1);

	// insert device
	int nItem;
	nItem = m_cmbDevice0.InsertString(0, _T("(No select channel)"));
	m_cmbDevice0.SetItemData(nItem, -1);

	nItem = m_cmbDevice1.InsertString(0, _T("(No select channel)"));
	m_cmbDevice1.SetItemData(nItem, -1);

	int nCount = MWGetChannelCount();
	TCHAR szChannelName[MAX_PATH];
	for (int i = 0; i < nCount; i ++) {
		MWCAP_CHANNEL_INFO chlInfo;
		if (MW_SUCCEEDED == MWGetChannelInfoByIndex(i, &chlInfo)) {
			CAutoConvertString strName(chlInfo.szProductName);
			_stprintf_s(szChannelName, _T("%s %d-%d\0"), (const TCHAR*)strName , chlInfo.byBoardIndex, chlInfo.byChannelIndex);
			int nItem = m_cmbDevice0.InsertString(i + 1, szChannelName);
			m_cmbDevice0.SetItemData(nItem, (DWORD_PTR)i);

			nItem = m_cmbDevice1.InsertString(i + 1, szChannelName);
			m_cmbDevice1.SetItemData(nItem, (DWORD_PTR)i);
		}
	}
	m_cmbDevice0.SetCurSel(1);
	OpenChannel(MODE_NORMAL, 0);
	m_data[MODE_NORMAL].nSel = 1;

	if (nCount > 1) {
		m_cmbDevice1.SetCurSel(2);
		OpenChannel(MODE_LOW_LATENCY, 1);
		m_data[MODE_LOW_LATENCY].nSel = 2;
	}
	else {
		m_cmbDevice1.SetCurSel(0);
		m_data[MODE_LOW_LATENCY].nSel = 0;
	}

	SetTimer(1, 500, NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLowLatencyDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLowLatencyDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLowLatencyDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CLowLatencyDlg::OnDestroy()
{
	CloseChannel(MODE_NORMAL);
	CloseChannel(MODE_LOW_LATENCY);

	CDialogEx::OnDestroy();
}

BOOL CLowLatencyDlg::OpenChannel(CAPTURE_MODE mode, int nIndex)
{
	CloseChannel(mode);

	CAPTURE_DATA& data = m_data[mode];
	do {
		data.dwFrameCount		= 0;
		data.llCaptureDuration	= 0LL;
		data.llTotalDuration	= 0LL;

		MWCAP_CHANNEL_INFO videoInfo = {0};
		if (MW_SUCCEEDED != MWGetChannelInfoByIndex(nIndex, &videoInfo)) {
			break;
		}

		// Open channel
		TCHAR szDevicePath[MAX_PATH];
		if (MW_SUCCEEDED != MWGetDevicePath(nIndex, szDevicePath)) {
			OutputDebugStringA("ERROR: MWGetDevicePath fail !\n");
			break;
		}
		data.hChannel = MWOpenChannelByPath(szDevicePath);
		if (data.hChannel == NULL) {
			break;
		}

		m_data[mode].wnd.OpenChannel(nIndex);

		data.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

		if (mode == MODE_NORMAL) 
			data.hThread = CreateThread(NULL, 0, ThreadProc, (LPVOID)this, 0, NULL);
		else 
			data.hThread = CreateThread(NULL, 0, ThreadProc1, (LPVOID)this, 0, NULL);
		if (data.hThread == NULL) {
			break;
		}
	} while (FALSE);

	return TRUE;
}

void CLowLatencyDlg::CloseChannel(CAPTURE_MODE mode)
{
	CAPTURE_DATA& data = m_data[mode];
	if (data.hEvent != NULL && data.hThread != NULL) {
		SetEvent(data.hEvent);
		WaitForSingleObject(data.hThread, INFINITE);

		CloseHandle(data.hThread);
		data.hThread = NULL;

		CloseHandle(data.hEvent);
		data.hEvent = NULL;
	}

	if (data.hChannel != NULL) {
		MWCloseChannel(data.hChannel);
		data.hChannel = NULL;
	}
	m_data[mode].wnd.CloseChannel();
}

DWORD CLowLatencyDlg::ThreadProc(CAPTURE_MODE mode) 
{	
	CAPTURE_DATA& data = m_data[mode];
	SetPriorityClass(data.hThread, REALTIME_PRIORITY_CLASS);

	HANDLE hNotifyEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	HANDLE hCaptureEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	
	DWORD cbStride = FOURCC_CalcMinStride(CAPTURE_COLOR, CAPTURE_WIDTH, 2);
	DWORD dwFrameSize = FOURCC_CalcImageSize(CAPTURE_COLOR, CAPTURE_WIDTH, CAPTURE_HEIGHT, cbStride);
	BYTE* byBuffer = new BYTE[dwFrameSize];

	MW_RESULT xr;
	do {
		xr = MWPinVideoBuffer(data.hChannel, byBuffer, dwFrameSize);
		if (xr != MW_SUCCEEDED)
			break;

		xr = MWStartVideoCapture(data.hChannel, hCaptureEvent);
		if (xr != MW_SUCCEEDED)
			break;

		MWCAP_VIDEO_BUFFER_INFO videoBufferInfo;
		if (MW_SUCCEEDED != MWGetVideoBufferInfo(data.hChannel, &videoBufferInfo))
			break;

		MWCAP_VIDEO_FRAME_INFO videoFrameInfo;
		xr = MWGetVideoFrameInfo(data.hChannel, videoBufferInfo.iNewestBufferedFullFrame, &videoFrameInfo);
		if (xr != MW_SUCCEEDED)
			break;

		HNOTIFY hNotify = MWRegisterNotify(data.hChannel, hNotifyEvent, 
			(mode == MODE_NORMAL ? MWCAP_NOTIFY_VIDEO_FRAME_BUFFERED : MWCAP_NOTIFY_VIDEO_FRAME_BUFFERING) | MWCAP_NOTIFY_VIDEO_SIGNAL_CHANGE | MWCAP_NOTIFY_VIDEO_INPUT_SOURCE_CHANGE);
		if (hNotify == NULL)
			break;

		LONGLONG llBegin = 0LL;
		xr = MWGetDeviceTime(data.hChannel, &llBegin);
		if (xr != MW_SUCCEEDED)
			break;

		xr = MWGetVideoSignalStatus(data.hChannel, &data.status);
		if (xr != MW_SUCCEEDED)
			break;

		MWCAP_VIDEO_CAPTURE_STATUS captureStatus;

		LONGLONG llExpireTime = llBegin;
		int i = 0;
		while (TRUE) {
			HANDLE aEvent[] = {data.hEvent, hNotifyEvent};
			DWORD dwRet = WaitForMultipleObjects(2, aEvent, FALSE, INFINITE);
			if (dwRet == WAIT_OBJECT_0)
				break;

			xr = MWGetVideoSignalStatus(data.hChannel, &data.status);
			if (xr != MW_SUCCEEDED)
				break;

			ULONGLONG ullStatusBits = 0;
			xr = MWGetNotifyStatus(data.hChannel, hNotify, &ullStatusBits);
			if (xr != MW_SUCCEEDED)
				continue;
			
			xr = MWGetVideoBufferInfo(data.hChannel, &videoBufferInfo);
			if (xr != MW_SUCCEEDED)
				continue;

			if (ullStatusBits & MWCAP_NOTIFY_VIDEO_FRAME_BUFFERED) {
				xr = MWGetVideoFrameInfo(data.hChannel, videoBufferInfo.iNewestBufferedFullFrame, &videoFrameInfo);
				if (xr != MW_SUCCEEDED)
					continue;

				xr = MWCaptureVideoFrameToVirtualAddress(data.hChannel, videoBufferInfo.iNewestBufferedFullFrame, byBuffer, dwFrameSize, cbStride, FALSE, NULL, CAPTURE_COLOR, CAPTURE_WIDTH, CAPTURE_HEIGHT); 
				WaitForSingleObject(hCaptureEvent, INFINITE);
			}
			else if (ullStatusBits & MWCAP_NOTIFY_VIDEO_FRAME_BUFFERING) {
				xr = MWGetVideoFrameInfo(data.hChannel, videoBufferInfo.iNewestBuffering, &videoFrameInfo);
				if (xr != MW_SUCCEEDED)
					continue;

				xr = MWCaptureVideoFrameToVirtualAddressEx(data.hChannel, videoBufferInfo.iNewestBuffering, byBuffer, dwFrameSize, cbStride, FALSE, NULL, CAPTURE_COLOR, CAPTURE_WIDTH, CAPTURE_HEIGHT,
					0, CAPTURE_PART_LINE, NULL, NULL, 0, 100, 0, 100, 0, MWCAP_VIDEO_DEINTERLACE_BLEND, MWCAP_VIDEO_ASPECT_RATIO_IGNORE, NULL, NULL, 0, 0, MWCAP_VIDEO_COLOR_FORMAT_UNKNOWN, MWCAP_VIDEO_QUANTIZATION_UNKNOWN, MWCAP_VIDEO_SATURATION_UNKNOWN); 

				BOOL bFirstSlice = TRUE;
				MWCAP_VIDEO_CAPTURE_STATUS captureStatus;
				do {
					WaitForSingleObject(hCaptureEvent, INFINITE);
					xr = MWGetVideoCaptureStatus(data.hChannel, &captureStatus);
				} while (xr == MW_SUCCEEDED && !captureStatus.bFrameCompleted);

				xr = MWGetVideoFrameInfo(data.hChannel, videoBufferInfo.iNewestBuffering, &videoFrameInfo);
				if (xr != MW_SUCCEEDED)
					continue;
			}

			if ((ullStatusBits & (MWCAP_NOTIFY_VIDEO_SIGNAL_CHANGE | MWCAP_NOTIFY_VIDEO_INPUT_SOURCE_CHANGE))
				|| (videoFrameInfo.allFieldStartTimes[0] == -1)
				|| (data.status.bInterlaced && (videoFrameInfo.allFieldStartTimes[1] == -1))
				) {
				data.dwFrameCount		= 0;
				data.llCaptureDuration	= 0LL;
				data.llTotalDuration	= 0LL;
				continue;
			}

			{
				LONGLONG llCurrent = 0LL;
				xr = MWGetDeviceTime(data.hChannel, &llCurrent);
				if (xr != MW_SUCCEEDED)
					continue;
				
				data.llTotalDuration += (llCurrent - videoFrameInfo.allFieldStartTimes[0]);

				if (data.status.bInterlaced) 
					data.llCaptureDuration += (llCurrent - videoFrameInfo.allFieldBufferedTimes[1]);
				else
					data.llCaptureDuration += (llCurrent - videoFrameInfo.allFieldBufferedTimes[0]);
			}

			data.dwFrameCount ++;

			if (data.dwFrameCount % 300 == 0) {
				data.dwFrameCount		= 0;
				data.llCaptureDuration	= 0LL;
				data.llTotalDuration	= 0LL;
			}

			xr = MWGetVideoCaptureStatus(data.hChannel, &captureStatus);
		}

		xr = MWUnregisterNotify(data.hChannel, hNotify);

		xr = MWStopVideoCapture(data.hChannel);

		xr = MWUnpinVideoBuffer(data.hChannel, byBuffer);

	} while (FALSE);

	delete [] byBuffer;

	CloseHandle(hNotifyEvent);
	CloseHandle(hCaptureEvent);

	return 0;
}

void CLowLatencyDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == 1) {
		CAPTURE_DATA& normal = m_data[MODE_NORMAL];
		CAPTURE_DATA& lowlatency = m_data[MODE_LOW_LATENCY];

		int nSel0 = m_cmbDevice0.GetCurSel();
		if (nSel0 == 0 || normal.status.state != MWCAP_VIDEO_SIGNAL_LOCKED) {
			m_strSignal0			= _T("-");
			m_strResolution0		= _T("-");
			m_strScanSize0			= _T("-");
			m_strOffset0			= _T("-");
			m_strFormat0			= _T("-");
			m_strCaptureLatency0	= _T("-");
			m_strTotalLatency0		= _T("-");
		}
		else {
			m_strSignal0			= GetStatus(normal.status.state);
			m_strResolution0		= GetResolutions(normal.status.cx, normal.status.cy, normal.status.dwFrameDuration);
			m_strScanSize0			= GetScanSize(normal.status.cxTotal, normal.status.cyTotal);
			m_strOffset0			= GetOffset(normal.status.x, normal.status.y);
			m_strFormat0			= GetFormat(normal.status.dwFrameDuration);
			if (normal.dwFrameCount > 0) {
				m_strCaptureLatency0	= GetLatency((DWORD)(normal.llCaptureDuration / normal.dwFrameCount));
				m_strTotalLatency0		= GetLatency((DWORD)(normal.llTotalDuration / normal.dwFrameCount));
			}
		}

		int nSel1 = m_cmbDevice1.GetCurSel();
		if (nSel1 == 0 || lowlatency.status.state != MWCAP_VIDEO_SIGNAL_LOCKED) {
			m_strSignal1			= _T("-");
			m_strResolution1		= _T("-");
			m_strScanSize1			= _T("-");
			m_strOffset1			= _T("-");
			m_strFormat1			= _T("-");
			m_strCaptureLatency1	= _T("-");
			m_strTotalLatency1		= _T("-");
		}
		else {
			m_strSignal1			= GetStatus(lowlatency.status.state);
			m_strResolution1		= GetResolutions(lowlatency.status.cx, lowlatency.status.cy, lowlatency.status.dwFrameDuration);
			m_strScanSize1			= GetScanSize(lowlatency.status.cxTotal, lowlatency.status.cyTotal);
			m_strOffset1			= GetOffset(lowlatency.status.x, lowlatency.status.y);
			m_strFormat1			= GetFormat(lowlatency.status.dwFrameDuration);
			if (lowlatency.dwFrameCount > 0) {
				m_strCaptureLatency1	= GetLatency((DWORD)(lowlatency.llCaptureDuration / lowlatency.dwFrameCount));
				m_strTotalLatency1		= GetLatency((DWORD)(lowlatency.llTotalDuration / lowlatency.dwFrameCount));
			}
		}

		UpdateData(FALSE);
	}

	CDialogEx::OnTimer(nIDEvent);
}

void CLowLatencyDlg::OnCbnSelchangeCombo0()
{
	int nSel = m_cmbDevice0.GetCurSel();
	if (nSel == -1)
		return;
	if (nSel == m_data[MODE_NORMAL].nSel)
		return;
	
	int nIndex = (int) m_cmbDevice0.GetItemData(nSel);
	CloseChannel(MODE_NORMAL);

	if (nIndex != -1) {
		OpenChannel(MODE_NORMAL, nIndex);
	}
	m_data[MODE_NORMAL].nSel = nSel;
}

void CLowLatencyDlg::OnCbnSelchangeCombo1()
{
	int nSel = m_cmbDevice1.GetCurSel();
	if (nSel == -1)
		return;
	if (nSel == m_data[MODE_LOW_LATENCY].nSel)
		return;
	
	int nIndex = (int) m_cmbDevice1.GetItemData(nSel);
	CloseChannel(MODE_LOW_LATENCY);

	if (nIndex != -1) {
		OpenChannel(MODE_LOW_LATENCY, nIndex);
	}
	m_data[MODE_LOW_LATENCY].nSel = nSel;
}

void CLowLatencyDlg::OnMove(int x, int y)
{
	CDialogEx::OnMove(x, y);
}

