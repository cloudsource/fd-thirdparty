#include "StdAfx.h"
#include "VideoSend.h"

#define WM_NDIVIDEOSIGNAL_CHANGED	(WM_USER + 301)
IMPLEMENT_DYNAMIC(CVideoSend, CWnd)
CVideoSend::CVideoSend(void)
{
	m_nIndex = 0;
	m_pNDI_send = NULL;
	m_nIndex = 0;
	m_cx = 1920;
	m_cy = 1080;
	m_dwFrameDuration = 333334;
	m_dwFourcc = MWFOURCC_BGRA;
}


CVideoSend::~CVideoSend(void)
{
}

BEGIN_MESSAGE_MAP(CVideoSend, CWnd)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_DESTROY()

	ON_MESSAGE(WM_NDIVIDEOSIGNAL_CHANGED, &CVideoSend::OnMsgVideoSignalChanged)
END_MESSAGE_MAP()

int CVideoSend::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	//video
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

void CVideoSend::OnDestroy()
{
	CWnd::OnDestroy();
}

BOOL CVideoSend::OnEraseBkgnd(CDC* pDC)
{
	return CWnd::OnEraseBkgnd(pDC);
}

void CVideoSend::OnPaint()
{
	CPaintDC dc(this); // device context for painting
}

BOOL CVideoSend::CreateThread(int nIndex, int cx, int cy, DWORD dwFrameDuration, DWORD dwFourcc)
{
	m_nIndex = nIndex;
	m_cx = cx;
	m_cy = cy;
	m_dwFrameDuration = dwFrameDuration;
	m_dwFourcc = dwFourcc;

	CMWAutoLock lock(m_lock);
	// Not required, but "correct" (see the SDK documentation.
	if (!NDIlib_initialize())
	{	// Cannot run NDI. Most likely because the CPU is not sufficient (see SDK documentation).
		// you can check this directly with a call to NDIlib_is_supported_CPU()
		printf("Cannot run NDI.");
		return 0;
	}

	// Create an NDI source that is called "My Video" and is clocked to the video.
	const NDIlib_send_create_t NDI_send_create_desc = { "NDISender", NULL, TRUE, FALSE };

	// We create the NDI sender
	m_pNDI_send = NDIlib_send_create(&NDI_send_create_desc);
	if (!m_pNDI_send) return FALSE;

	// Provide a meta-data registration that allows people to know what we are. Note that this is optional.
	// Note that it is possible for senders to also register their preferred video formats.
	static const char* p_connection_string = "<ndi_product long_name=\"NDILib Send Example.\" "
		"             short_name=\"NDISender\" "
		"             manufacturer=\"(None)\" "
		"             version=\"1.000.000\" "
		"             session=\"default\" "
		"             model_name=\"S1\" "
		"             serial=\"ABCDEFG\"/>";
	const NDIlib_metadata_frame_t NDI_connection_type = {
		// The length
		(DWORD)::strlen(p_connection_string),
		// Timecode (synthesized for us !)
		NDIlib_send_timecode_synthesize,
		// The string
		(CHAR*)p_connection_string
	};
	NDIlib_send_add_connection_metadata(m_pNDI_send, &NDI_connection_type);
	BOOL bRet = m_NDIthread.Create(nIndex, cx, cy, dwFrameDuration, dwFourcc, FALSE, this);
	bRet = SetNDICaptureFrameParams(cx, cy, dwFrameDuration, dwFourcc);

	return TRUE;
}

void CVideoSend::DestoryThread()
{
	m_NDIthread.Destroy();

	if(m_pNDI_send != NULL)
	{
		NDIlib_send_destroy(m_pNDI_send);
		m_pNDI_send = NULL;
		// Not required, but you add it will be better
		NDIlib_destroy();
	}
}

BOOL CVideoSend::OnVideoCallback(const BYTE* lpData, DWORD cbStride, BOOL bSignalValid)
{
	CMWAutoLock lock(m_lock);
	if (!NDIlib_send_get_no_connections(m_pNDI_send, 100))
	{
		return TRUE;
	}
	else 
	{
		m_videoFrame.p_data = (BYTE*)lpData;
		NDIlib_send_send_video(m_pNDI_send, &m_videoFrame);

	}
	return TRUE;
}

BOOL CVideoSend::OnAudioCallback(const BYTE* lpData, int cbSize, DWORD dwSamplerate,BOOL bSignalValid)
{
	CMWAutoLock lock(m_lock);
	if(bSignalValid == TRUE && dwSamplerate == m_audioFrame.sample_rate)
	{
		m_audioFrame.p_data = (short*)lpData;
		NDIlib_util_send_send_audio_interleaved_16s(m_pNDI_send, &m_audioFrame);
	}

	return TRUE;
}

void CVideoSend::OnVideoSignalChanged(int cx, int cy, BOOL bSignalValid)
{
}

void CVideoSend::OnAudioSignalChanged(BOOL bValid, BOOL bLPCM, BYTE cBitsPerSample, DWORD dwSampleRate, BOOL bSignalValid)
{
	if(bSignalValid == TRUE)
		PostMessage(WM_NDIVIDEOSIGNAL_CHANGED);
}

BOOL CVideoSend::SetNDICaptureFrameParams(int cx, int cy, DWORD dwFrameDuration, DWORD dwFourcc)
{
	//video
	m_videoFrame.xres = cx;
	m_videoFrame.yres = cy;
	m_videoFrame.frame_rate_N = dwFrameDuration * 1000;
	m_videoFrame.frame_rate_D = 1001;
	m_videoFrame.picture_aspect_ratio = 16.0f / 9.0f;
	m_videoFrame.is_progressive = FALSE;
	m_videoFrame.timecode = NDIlib_send_timecode_synthesize;
	if(dwFourcc == MWFOURCC_BGRA)
	{
		m_videoFrame.FourCC = NDIlib_FourCC_type_BGRA;
	}
	else if(dwFourcc == MWFOURCC_UYVY)
	{
		m_videoFrame.FourCC = NDIlib_FourCC_type_UYVY;
	}
	else
		return FALSE;

	DWORD cbStride = FOURCC_CalcMinStride(dwFourcc, cx, 2);
	m_videoFrame.line_stride_in_bytes = cbStride;

	BOOL bLPCM = FALSE;
	BYTE cBitsPerSample = 0;
	DWORD dwSampleRate = 0;
	int nChannels = 0;
	if (!m_NDIthread.GetInputAudioFormat(bLPCM, cBitsPerSample, dwSampleRate)) {
		OutputDebugStringA("Get input audio format fail !\n");
		bLPCM = FALSE;
	}

	//audio
	m_audioFrame.sample_rate = dwSampleRate;
	m_audioFrame.no_channels = 8;
	m_audioFrame.no_samples = 192;
	m_audioFrame.timecode = 0LL;
	m_audioFrame.reference_level = 0;
	//m_audioFrame.p_data = new short[192*8];

	return TRUE;
}

LRESULT CVideoSend::OnMsgVideoSignalChanged(WPARAM w, LPARAM l)
{
	m_lockRecrete.Lock();
	m_NDIthread.Destroy();
	m_NDIthread.Create(m_nIndex, m_cx, m_cy, m_dwFrameDuration, m_dwFourcc, FALSE, this);
	CMWAutoLock lock(m_lock);
	SetNDICaptureFrameParams(m_cx, m_cy, m_dwFrameDuration, m_dwFourcc);
	m_lockRecrete.Unlock();
	
	return 0;
}