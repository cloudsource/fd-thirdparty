#include "StdAfx.h"
#include "VideoWnd.h"

#define WM_VIDEOSIGNAL_CHANGED	(WM_USER + 301)
// CVideoWnd
IMPLEMENT_DYNAMIC(CVideoWnd, CWnd)
CVideoWnd::CVideoWnd(void)
{
	m_pRenderer = NULL;
	m_pAudioRenderer = NULL;

	m_nChannelIndex = 0;
	m_cx = PREVIEW_WIDTH;
	m_cy = PREVIEW_HEIGHT;
	m_dwFourcc = PREVIEW_FOURCC;
	m_dwFrameDuration = 333333;
}


CVideoWnd::~CVideoWnd(void)
{
}

BEGIN_MESSAGE_MAP(CVideoWnd, CWnd)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_DESTROY()

	ON_MESSAGE(WM_VIDEOSIGNAL_CHANGED, &CVideoWnd::OnMsgVideoSignalChanged)
END_MESSAGE_MAP()

int CVideoWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	//video
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	OpenChannel(0, 1920, 1080, 333333, MWFOURCC_RGBA);
	
	CloseChannel();
	return 0;
}

void CVideoWnd::OnDestroy()
{
	

	CWnd::OnDestroy();
}

BOOL CVideoWnd::OnEraseBkgnd(CDC* pDC)
{

	return CWnd::OnEraseBkgnd(pDC);
}

void CVideoWnd::OnPaint()
{
	CPaintDC dc(this); // device context for painting
}

BOOL CVideoWnd::OpenChannel(int nIndex, int cx, int cy, DWORD dwFrameDuration, DWORD dwFOURCC)
{
	CloseChannel();
	m_nChannelIndex = nIndex;
	m_cx = cx;
	m_cy = cy;
	m_dwFourcc = dwFOURCC;
	m_dwFrameDuration = dwFrameDuration;

	//init render
	CMWAutoLock lock(m_lockVideo);
	m_pRenderer = new CD11Renderer();
	if (!m_pRenderer->Init(cx, cy, PREVIEW_FOURCC, GetSafeHwnd())) {
		delete m_pRenderer;
		m_pRenderer = NULL;
		return FALSE;
	}

	BOOL bRet = FALSE;
	bRet = m_thread.Create(m_nChannelIndex, m_cx, m_cy, m_dwFrameDuration, PREVIEW_FOURCC, TRUE, this);

	//audio
	BOOL bLPCM = FALSE;
	BYTE cBitsPerSample = 0;
	DWORD dwSampleRate = 0;
	int nChannels = 0;
	if (!m_thread.GetInputAudioFormat(bLPCM, cBitsPerSample, dwSampleRate)) {
		OutputDebugStringA("Get input audio format fail !\n");
		bLPCM = FALSE;
	}

	if (bLPCM)
	{
		CMWAutoLock lock(m_lockAudio);
		BOOL bRet = FALSE;
		do {
			m_pAudioRenderer = new CDSoundRenderer();
			if (!m_pAudioRenderer)
				break;

			GUID guid = GUID_NULL;
			if (!m_pAudioRenderer->Initialize(&guid))
				break;

			if (!m_pAudioRenderer->Create(dwSampleRate, 2, MWCAP_AUDIO_SAMPLES_PER_FRAME, DEF_AUDIO_JITTER_FRAMES))
				break;
			
			if (!m_pAudioRenderer->Run())
				break;

			bRet = TRUE;
		} while (FALSE);

		if (!bRet) {
			m_pAudioRenderer->Deinitialize();
			delete m_pAudioRenderer;
			m_pAudioRenderer = NULL;
		}
	}
	
	return bRet;
}

void CVideoWnd::CloseChannel()
{
	m_thread.Destroy();
	if (m_pRenderer != NULL) {
		m_pRenderer->Exit();
		delete m_pRenderer;
		m_pRenderer = NULL;
	}

	if (m_pAudioRenderer) {
		m_pAudioRenderer->Deinitialize();
		delete m_pAudioRenderer;
		m_pAudioRenderer = NULL;	
	}
}

BOOL CVideoWnd::OnVideoCallback(const BYTE* lpData, DWORD cbStride, BOOL bSignalValid)
{
	CMWAutoLock lock(m_lockVideo);
	if (m_pRenderer != NULL) {
		m_pRenderer->Update(lpData, cbStride);
	}
	return TRUE;
}

BOOL CVideoWnd::OnAudioCallback(const BYTE* lpData, int cbSize, DWORD dwSamplerate, BOOL bSignalValid)
{
	CMWAutoLock lock(m_lockAudio);
	if (m_pAudioRenderer) {
		m_pAudioRenderer->PutFrame(lpData, cbSize);
	}
	
	return TRUE;
}

void CVideoWnd::OnVideoSignalChanged(int cx, int cy, BOOL bSignalValid)
{
}

void CVideoWnd::OnAudioSignalChanged(BOOL bValid, BOOL bLPCM, BYTE cBitsPerSample, DWORD dwSampleRate, BOOL bSignalValid)
{
	CMWAutoLock lock(m_lockAudio);
	PostMessage(WM_VIDEOSIGNAL_CHANGED);
}

LRESULT CVideoWnd::OnMsgVideoSignalChanged(WPARAM w, LPARAM l)
{
	CloseChannel();
	OpenChannel(m_nChannelIndex, m_cx, m_cy, m_dwFrameDuration, PREVIEW_FOURCC);
	return 0;
}