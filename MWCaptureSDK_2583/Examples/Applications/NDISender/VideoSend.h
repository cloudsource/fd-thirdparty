#pragma once
#include "PreviewThread.h"
#include "LockUtils.h"

#define SEND_WIDTH		1920
#define SEND_HEIGHT		1080
#define SEND_DURATION	333334
#define NDIFOURCC		NDIlib_FourCC_type_BGRA
#define SEND_FOURCC		MWFOURCC_ARGB

class CVideoSend :public CWnd, public IPreviewCallback
{
	DECLARE_DYNAMIC(CVideoSend)
public:
	CVideoSend(void);
	~CVideoSend(void);

public:

	BOOL CreateThread(int nIndex, int cx, int cy, DWORD dwFrameDuration, DWORD dwFourcc);
	void DestoryThread();

protected:
	BOOL OnVideoCallback(const BYTE* lpData, DWORD cbStride, BOOL bSignalValid);
	BOOL OnAudioCallback(const BYTE* lpData, int cbSize, DWORD dwSamplerate, BOOL bSignalValid);

	void OnVideoSignalChanged(int cx, int cy, BOOL bSignalValid);
	void OnAudioSignalChanged(BOOL bValid, BOOL bLPCM, BYTE cBitsPerSample, DWORD dwSampleRate, BOOL bSignalValid);
	BOOL SetNDICaptureFrameParams(int cx, int cy, DWORD dwFrameDuration, DWORD dwFourcc);
protected:
	CPreviewThread		m_NDIthread;
	CMWLock				m_lock;
	CMWLock				m_lockRecrete;

	//NDI
	NDIlib_send_instance_t					m_pNDI_send;
	NDIlib_video_frame_t					m_videoFrame;
	NDIlib_audio_frame_interleaved_16s_t	m_audioFrame;

	//capture
	int		m_nIndex;
	int		m_cx;
	int		m_cy;
	DWORD   m_dwFrameDuration;
	DWORD   m_dwFourcc;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();

	afx_msg LRESULT OnMsgVideoSignalChanged(WPARAM w, LPARAM l);

};

