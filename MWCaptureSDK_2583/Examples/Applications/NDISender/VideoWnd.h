#pragma once

#include <MMSystem.h>

#include "MWFOURCC.h"
#include "LockUtils.h"

#include "PreviewThread.h"
#include "DSoundRenderer.h"
#include "D11Renderer.h"

#define PREVIEW_WIDTH		1920
#define PREVIEW_HEIGHT		1080
#define PREVIEW_DURATION	333334
#define PREVIEW_FOURCC		MWFOURCC_RGBA

class CVideoWnd :public CWnd, public IPreviewCallback
{
	DECLARE_DYNAMIC(CVideoWnd)

public:
	CVideoWnd(void);
	~CVideoWnd(void);

public:
	BOOL OpenChannel(int nIndex, int cx, int cy, DWORD dwFrameDuration, DWORD dwFOURCC);
	void CloseChannel();

	// ICaptureCallback
	BOOL OnVideoCallback(const BYTE* lpData, DWORD cbStride, BOOL bSignalValid);
	BOOL OnAudioCallback(const BYTE* lpData, int cbSize, DWORD dwSamplerate, BOOL bSignalValid);

	void OnVideoSignalChanged(int cx, int cy, BOOL bSignalValid);
	void OnAudioSignalChanged(BOOL bValid, BOOL bLPCM, BYTE cBitsPerSample, DWORD dwSampleRate, BOOL bSignalValid);

protected:
	int					m_cx;
	int					m_cy;
	int					m_nChannelIndex;
	DWORD				m_dwFourcc;
	DWORD				m_dwFrameDuration;

	CD11Renderer*		m_pRenderer;
	CDSoundRenderer*	m_pAudioRenderer;
	CPreviewThread		m_thread;
	CMWLock				m_lockAudio;
	CMWLock				m_lockVideo;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();

	afx_msg LRESULT OnMsgVideoSignalChanged(WPARAM w, LPARAM l);
	
};

