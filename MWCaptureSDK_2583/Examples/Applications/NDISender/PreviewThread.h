#pragma once

#include <Windows.h>

class IPreviewCallback
{
public:
	virtual BOOL OnVideoCallback(const BYTE* lpData, DWORD cbStride, BOOL bSignalValid) = NULL;
	virtual BOOL OnAudioCallback(const BYTE* lpData, int cbSize, DWORD dwSamplerate, BOOL bSignalValid) = NULL;
	virtual void OnVideoSignalChanged(int cx, int cy, BOOL bSignalValid) = NULL;
	virtual void OnAudioSignalChanged(BOOL bValid, BOOL bLPCM, BYTE cBitsPerSample, DWORD dwSampleRate, BOOL bSignalValid) = NULL;
};

class CPreviewThread
{
public:
	CPreviewThread(void);
	~CPreviewThread(void);

public:
	BOOL Create(int nIndex, int cx, int cy, DWORD dwFrameDuration, DWORD dwFOURCC, BOOL bPreviewFlag, IPreviewCallback* pCallback);
	void Destroy();

	static DWORD WINAPI VideoCaptureThreadProc(LPVOID pvParam) {
		CPreviewThread* pThis = (CPreviewThread *)pvParam;
		return pThis->VideoCaptureThreadProcEx();
	}
	DWORD VideoCaptureThreadProcEx();

	static DWORD WINAPI AudioCaptureThreadProc(LPVOID pvParam) {
		CPreviewThread* pThis = (CPreviewThread *)pvParam;
		return pThis->AudioCaptureThreadProcEx();
	}
	DWORD AudioCaptureThreadProcEx();

	BOOL GetInputAudioFormat(BOOL& bLPCM, BYTE& cBitsPerSample, DWORD& dwSampleRate) {
		if (m_bAudioValid) {
			bLPCM			= m_bIsLPCM;
			cBitsPerSample	= m_cBitsPerSample;
			dwSampleRate	= m_dwSampleRate;
			return TRUE;
		}
		return FALSE;
	}

protected:
	HCHANNEL		m_hChannel;
	//video
	int				m_cx;
	int				m_cy;
	HANDLE			m_hThread;
	HANDLE			m_hEvent;
	DWORD			m_dwFrameDuration;
	DWORD			m_dwFOURCC;

	MWCAP_VIDEO_SIGNAL_STATUS	m_status;

	//audio
	int				m_nPresentChannel;
	int				m_nBitDepthInByte;
	BYTE			m_cBitsPerSample;
	DWORD			m_dwSampleRate;
	BOOL			m_bPreviewFlag;
	BOOL			m_bIsLPCM;
	BOOL			m_bAudioValid;
	BOOL			m_bAudioCapturing;
	HANDLE			m_hAudioEvent;
	HANDLE			m_hAudioCaptureThread;
	
	MWCAP_AUDIO_CAPTURE_FRAME		m_audioFrame;
	MWCAP_AUDIO_SIGNAL_STATUS		m_audioSignalStatus;
	//BYTE							m_byAudioSamples[MWCAP_AUDIO_SAMPLES_PER_FRAME * MWCAP_AUDIO_MAX_NUM_CHANNELS * MAX_BIT_DEPTH_IN_BYTE];
	short							m_byAudioSamples[MWCAP_AUDIO_SAMPLES_PER_FRAME * 8];
	short							m_byAudioSamplesPreview[MWCAP_AUDIO_SAMPLES_PER_FRAME * 2];
	
	IPreviewCallback* m_pCallback;
};