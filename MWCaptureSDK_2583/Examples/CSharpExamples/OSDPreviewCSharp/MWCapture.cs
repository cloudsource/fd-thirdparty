﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Threading;
using OSDPreviewCSharp.Properties;

namespace OSDPreviewCSharp
{
    public delegate void LPFN_MWCAP_VIDEO_CAPTURE_CALLBACK(IntPtr pbImage, UInt32 cbFrame, int cbImageStride, IntPtr pvParam);

    public struct CAPTURE_PARAMS
    {
        public int cx;
        public int cy;

        public UInt32 dwFourcc;
        public UInt32 dwFrameDuration;
        public LPFN_MWCAP_VIDEO_CAPTURE_CALLBACK pFuncCallback;
    }


    public class MWCapture
    {
        //video device 
        protected IntPtr m_hVideoChannel = IntPtr.Zero;
        protected IntPtr m_hD3DRenderer = IntPtr.Zero;

        //video params
        protected IntPtr m_hWnd = IntPtr.Zero;
        protected CAPTURE_PARAMS m_capParams;

        // video device index
        protected int m_nCurrentIndex = -1;
        protected Boolean m_bIsCapture = false;

        // capture thread
        Thread m_capThread = null;
        IntPtr m_hExitEvent = IntPtr.Zero;

        // count
        long m_llStartTime;
        long m_llFrameCount;

        Boolean m_bBorder;
        Boolean m_bIcon;
        Boolean m_bText;

        int m_nIconIndex = 0;
      
        Bitmap[] m_pIcon = { (Bitmap)Resources.png1.Clone(), (Bitmap)Resources.png2.Clone(), (Bitmap)Resources.png3.Clone(), (Bitmap)Resources.png4.Clone(), (Bitmap)Resources.png5.Clone()};
      
        public MWCapture() {
         
        }

        //Initialize and Uninitialize LibXIStream
        static public void Init()
        {
            LibMWCapture.MWCaptureInitInstance();
        }

        static public void Exit()
        {
            LibMWCapture.MWCaptureExitInstance();
        }

        static public Boolean RefreshDevices()
        {
            LibMWCapture.MW_RESULT mr;
            mr = LibMWCapture.MWRefreshDevice();
            if (mr != LibMWCapture.MW_RESULT.MW_SUCCEEDED)
                return false;

            return true;
        }

        static public int GetChannelCount()
        {
            return LibMWCapture.MWGetChannelCount();
        }

        static public void GetChannelInfobyIndex(int nChannelIndex, ref LibMWCapture.MWCAP_CHANNEL_INFO channelInfo)
        {
            int iSize = Marshal.SizeOf(typeof(LibMWCapture.MWCAP_CHANNEL_INFO));
            IntPtr pChannelInfo = Marshal.AllocCoTaskMem(iSize);
            LibMWCapture.MWGetChannelInfoByIndex(nChannelIndex, pChannelInfo);
            channelInfo = (LibMWCapture.MWCAP_CHANNEL_INFO)Marshal.PtrToStructure(pChannelInfo, typeof(LibMWCapture.MWCAP_CHANNEL_INFO));
            Marshal.FreeCoTaskMem(pChannelInfo);

            return;
        }


        public Boolean OpenVideoChannel(int nChannelIndex,UInt32 dwFourcc, int cx, int cy, UInt32 nFrameDuration, IntPtr hWnd, Rectangle rcPanel)
        {
            // open video device
            LibMWCapture.MW_RESULT mr;

            int iSize = Marshal.SizeOf(typeof(LibMWCapture.MWCAP_CHANNEL_INFO));
            IntPtr pChannelInfo = Marshal.AllocCoTaskMem(iSize);
            mr = LibMWCapture.MWGetChannelInfoByIndex(nChannelIndex, pChannelInfo);
            LibMWCapture.MWCAP_CHANNEL_INFO channelInfo = (LibMWCapture.MWCAP_CHANNEL_INFO)Marshal.PtrToStructure(pChannelInfo, typeof(LibMWCapture.MWCAP_CHANNEL_INFO));
            Marshal.FreeCoTaskMem(pChannelInfo);

            m_hVideoChannel = LibMWCapture.MWOpenChannel(channelInfo.byBoardIndex, channelInfo.byChannelIndex);
            if (m_hVideoChannel == IntPtr.Zero)
                return false;


            // create video renderer
            m_hD3DRenderer = LibMWMedia.MWCreateD3DRenderer(cx, cy, dwFourcc, hWnd);
            if (m_hD3DRenderer == IntPtr.Zero)
            {
                CloseVideoChannel();
                return false;
            }
                

            m_capParams.dwFourcc = dwFourcc;
            m_capParams.cx = cx;
            m_capParams.cy = cy;
            m_capParams.dwFrameDuration = nFrameDuration;
            m_capParams.pFuncCallback = new LPFN_MWCAP_VIDEO_CAPTURE_CALLBACK(MWCapture.VideoCaptureCallback);
           
            m_hWnd = hWnd;



            Boolean bRet = StartVideoCapature(nChannelIndex);
            if (bRet == false)
            {
                CloseVideoChannel();
                return false;
            }

            return true;
        }

        public void CloseVideoChannel()
        {
            if (m_hVideoChannel != IntPtr.Zero)
            {
                StopVideoCapture();
                LibMWCapture.MWCloseChannel(m_hVideoChannel);
                m_hVideoChannel = IntPtr.Zero;
            }
            if (m_hD3DRenderer != IntPtr.Zero)
            {
                LibMWMedia.MWDestroyD3DRenderer(m_hD3DRenderer);
                m_hD3DRenderer = IntPtr.Zero;
            }
        }

        public Boolean StartVideoCapature(int nIndex)
        {
            if (nIndex == m_nCurrentIndex && m_bIsCapture == true)
                return true;

            if (nIndex != m_nCurrentIndex && m_bIsCapture == true)
                StopVideoCapture();

            if (m_hVideoChannel == IntPtr.Zero)
                return false;


            m_hExitEvent = Libkernel32.CreateEvent(IntPtr.Zero, 0, 0, IntPtr.Zero);
            if (m_hExitEvent == IntPtr.Zero)
                return false;

            m_bIsCapture = true;
            m_capThread = new Thread(new ParameterizedThreadStart(VideoCapThread));
            m_capThread.Start(m_capParams);
            
            m_nCurrentIndex = nIndex;

            return true;
        }


        public void StopVideoCapture()
        {
            if (m_bIsCapture == false && m_nCurrentIndex == -1)
                return;

            m_bIsCapture = false;
            Libkernel32.SetEvent(m_hExitEvent);
            m_capThread.Join();

            
            m_nCurrentIndex = -1;
        }

        public double GetAveFps()
        {
            long llCurTime = 0;
            LibMWCapture.MWGetDeviceTime(m_hVideoChannel, ref llCurTime);

            return 1.0 *10000000* m_llFrameCount /(llCurTime - m_llStartTime);
        }

        public void VideoCapThread(object oParams)
        {
            LibMWCapture.MW_RESULT mr;

            CAPTURE_PARAMS cp = (CAPTURE_PARAMS)oParams;

            // event
            UInt64 hTimer = 0;
            IntPtr hTimerEvent = IntPtr.Zero;
            IntPtr hCaptureEvent = IntPtr.Zero;


            hTimerEvent = Libkernel32.CreateEvent(IntPtr.Zero, 0, 0, IntPtr.Zero);
            if (hTimerEvent == IntPtr.Zero)
                return;

            hTimer = LibMWCapture.MWRegisterTimer(m_hVideoChannel, hTimerEvent);
            if (hTimer == 0)
                return;

            hCaptureEvent = Libkernel32.CreateEvent(IntPtr.Zero, 0, 0, IntPtr.Zero);
            if (hCaptureEvent == IntPtr.Zero)
                return;

            mr = LibMWCapture.MWStartVideoCapture(m_hVideoChannel, hCaptureEvent);
            if (mr != LibMWCapture.MW_RESULT.MW_SUCCEEDED)
                return;

            UInt64 hOSD = LibMWCapture.MWCreateImage(m_hVideoChannel, cp.cx, cp.cy);

            m_llStartTime = 0;
            mr = LibMWCapture.MWGetDeviceTime(m_hVideoChannel, ref m_llStartTime);
            if (mr != LibMWCapture.MW_RESULT.MW_SUCCEEDED)
                return;

            int cx = cp.cx;
            int cy = cp.cy;
            UInt32 dwFourcc = cp.dwFourcc;
            UInt32 dwFrameDuration = cp.dwFrameDuration;
            UInt32 dwMinStride = MWCap_FOURCC.FOURCC_CalcMinStride(dwFourcc, cx, 4);
            UInt32 dwImageSize = MWCap_FOURCC.FOURCC_CalcImageSize(dwFourcc, cx, cy, dwMinStride);

            IntPtr pbImage = Marshal.AllocCoTaskMem((int)dwImageSize);
            if (pbImage == IntPtr.Zero)
                return;

            m_llFrameCount = 0;

            int nSize = 0;
            IntPtr pSignalStatus = IntPtr.Zero;
            nSize = Marshal.SizeOf(typeof(LibMWCapture.MWCAP_VIDEO_SIGNAL_STATUS));
            pSignalStatus = Marshal.AllocCoTaskMem(nSize);
            //LibMWCapture.MWCAP_VIDEO_SIGNAL_STATUS signalStatus;

            IntPtr pCapStatus = IntPtr.Zero;
            nSize = Marshal.SizeOf(typeof(LibMWCapture.MWCAP_VIDEO_CAPTURE_STATUS));
            pCapStatus = Marshal.AllocCoTaskMem(nSize);
            LibMWCapture.MWCAP_VIDEO_CAPTURE_STATUS capStatus;

            while (true)
            {
                //LibMWCapture.MWGetVideoSignalStatus(m_hVideoChannel, pSignalStatus);
                //signalStatus = (LibMWCapture.MWCAP_VIDEO_SIGNAL_STATUS)Marshal.PtrToStructure(pSignalStatus, typeof(LibMWCapture.MWCAP_VIDEO_SIGNAL_STATUS));
                
                
                
                ////if (signalStatus.state != LibMWCapture.MWCAP_VIDEO_SIGNAL_STATE.MWCAP_VIDEO_SIGNAL_LOCKED)
                ////{
                ////    Thread.Sleep(500);
                ////    continue;
                ////}

                try
                {
                    mr = LibMWCapture.MWScheduleTimer(m_hVideoChannel, hTimer, m_llStartTime + (m_llFrameCount + 1) * dwFrameDuration);
                    if (mr != LibMWCapture.MW_RESULT.MW_SUCCEEDED)
                        break;

                    IntPtr[] hEvents = { m_hExitEvent, hTimerEvent };
                    UInt32 dwRet = Libkernel32.WaitForMultipleObjects(2, hEvents, 0, 1000);
                    if (dwRet != 1)
                        break;
                    //UInt32 dwRet = Libkernel32.WaitForSingleObject(hTimerEvent, 1000);
                    //if (dwRet != 0)
                    //    break;

                    LoadOSD(hOSD);
                    Rectangle[] aOSDRects = {new Rectangle(0, 0, cp.cx, cp.cy)};

                   // mr = LibMWCapture.MWCaptureVideoFrameToVirtualAddress(m_hVideoChannel, -1, pbImage, dwImageSize, dwMinStride, 0, 0, dwFourcc, cx, cy);
                    mr = LibMWCapture.MWCaptureVideoFrameWithOSDToVirtualAddress(m_hVideoChannel, -1, pbImage, dwImageSize, dwMinStride, 0, 0, dwFourcc, cx, cy, hOSD, aOSDRects, 1);
                    if (mr != LibMWCapture.MW_RESULT.MW_SUCCEEDED)
                        break;

                    dwRet = Libkernel32.WaitForSingleObject(hCaptureEvent, Libkernel32.INFINITE);
                    mr = LibMWCapture.MWGetVideoCaptureStatus(m_hVideoChannel, pCapStatus);
                    if (mr != LibMWCapture.MW_RESULT.MW_SUCCEEDED)
                        break;

                    m_llFrameCount++;

                    if (cp.pFuncCallback != null)
                    {
                        cp.pFuncCallback((IntPtr)pbImage, dwImageSize, (int)dwMinStride, m_hD3DRenderer);
                    }  
                }
                catch (Exception ex) { }           
            
            }

            if (hOSD != 0)
            {
                long lRet = 0;
                LibMWCapture.MWCloseImage(m_hVideoChannel, hOSD, ref lRet);
            }

            if (pSignalStatus != IntPtr.Zero)
                Marshal.FreeCoTaskMem(pSignalStatus);

            if (pCapStatus != IntPtr.Zero)
                Marshal.FreeCoTaskMem(pCapStatus);

            if (pbImage != IntPtr.Zero)
            {
                Marshal.FreeCoTaskMem((IntPtr)pbImage);
                pbImage = IntPtr.Zero;
            }

            if (hTimer != 0)
            {
                LibMWCapture.MWUnregisterTimer(m_hVideoChannel, hTimer);
                hTimer = 0;
            }

            if (hTimerEvent != IntPtr.Zero)
            {
                Libkernel32.CloseHandle(hTimerEvent);
                hTimerEvent = IntPtr.Zero;
            }

            LibMWCapture.MWStopVideoCapture(m_hVideoChannel);

            if (hCaptureEvent != IntPtr.Zero)
            {
                Libkernel32.CloseHandle(hCaptureEvent);
                hCaptureEvent = IntPtr.Zero;
            }          
        }

        public Boolean LoadOSD(UInt64 hOSD)
        {
            ushort cx = (ushort)m_capParams.cx;
            ushort cy = (ushort)m_capParams.cy;
          /*  UInt32 dwFourcc = m_capParams.dwFourcc;*/
            Rectangle rect = new Rectangle(0, 0, cx, cy);
            Bitmap imageOSD = new Bitmap(cx, cy, PixelFormat.Format32bppArgb);
            BitmapData imageDataOSD = new BitmapData();
            //Graphics graphicsOSD = Graphics.FromImage(imageOSD);

            if (m_bBorder)
            {
                Pen penOSD = new Pen(Color.Red);
                Graphics graphicsOSD = Graphics.FromImage(imageOSD);
                graphicsOSD.DrawRectangle(penOSD, 10, 35, 720 - 20, 576 - 70);
            }

            if (m_bIcon)
            {
                int nWidth = m_pIcon[m_nIconIndex].Width;
                int nHeight = m_pIcon[m_nIconIndex].Height;
                Graphics graphicsOSD = Graphics.FromImage(imageOSD);
                graphicsOSD.DrawImage(m_pIcon[m_nIconIndex], cx - nWidth - 20, 35, nWidth, nHeight);

                if (m_llFrameCount % 10 == 0)
                    m_nIconIndex = (m_nIconIndex + 1) % 5;
            }

            imageOSD.LockBits(
                rect,
                ImageLockMode.WriteOnly,
                PixelFormat.Format32bppArgb,
                imageDataOSD
                );
            
            if (hOSD != 0)
            {
                LibMWCapture.MWUploadImageFromVirtualAddress(m_hVideoChannel, hOSD, LibMWCapture.MWCAP_VIDEO_COLOR_FORMAT.MWCAP_VIDEO_COLOR_FORMAT_RGB,
                    LibMWCapture.MWCAP_VIDEO_QUANTIZATION_RANGE.MWCAP_VIDEO_QUANTIZATION_FULL, LibMWCapture.MWCAP_VIDEO_SATURATION_RANGE.MWCAP_VIDEO_SATURATION_FULL,
                    0, 0, cx, cy, (long)imageDataOSD.Scan0, (uint)(imageDataOSD.Stride * cy), (uint)(imageDataOSD.Stride), cx, cy, 0, 1, 1);
            }
            
            imageOSD.UnlockBits(imageDataOSD);

            if (m_bText)
                LoadText(hOSD);

            return false;
        }

        public Boolean LoadText(UInt64 hOSD)
        {
            GraphicsPath path = new GraphicsPath();
            FontFamily family = new FontFamily("Times New Roman");
            
           // PointF      point = new Point(50, 50);
            Rectangle rectText = new Rectangle(100, 350, 0, 0);
            int fontStyle = (int)FontStyle.Regular;
            int emSize = 52;
            StringFormat format = StringFormat.GenericDefault;
            DateTime localtime = DateTime.Now;
            string strTime = localtime.Hour + ":" + localtime.Minute + ":" + localtime.Second + ":" + localtime.Millisecond;
         
            path.AddString(strTime, family, fontStyle, emSize, rectText, StringFormat.GenericTypographic);

//             GraphicsPath path2 = new GraphicsPath();
//             path2 = (GraphicsPath)path.Clone();
            GraphicsPath path2 = (GraphicsPath)path.Clone();

            Pen pen = new Pen(Color.White, 2);
            path2.Widen(pen);

            RectangleF rcBound = new Rectangle();
            rcBound = path2.GetBounds();

            Matrix mx = new Matrix();
            mx.Translate(-rcBound.X, -rcBound.Y);
      
            path.Transform(mx);
            path2.Transform(mx);

            rcBound.Width = (rcBound.Width + 1) / 2 * 2;

            Bitmap bitmapOSD = new Bitmap((int)rcBound.Width, (int)rcBound.Height, PixelFormat.Format32bppArgb);

            Graphics graphics = Graphics.FromImage(bitmapOSD);
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            
            SolidBrush brush2 = new SolidBrush(Color.Red);
            graphics.FillPath(brush2, path2);

            SolidBrush brush = new SolidBrush(Color.Yellow);
            graphics.FillPath(brush, path);

            Rectangle rect2 = new Rectangle(0, 0, (int)rcBound.Width, (int)rcBound.Height);
            BitmapData bitmapDataOSD = new BitmapData();
            bitmapOSD.LockBits(rect2, ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb, bitmapDataOSD);

            LibMWCapture.MW_RESULT hr = LibMWCapture.MWUploadImageFromVirtualAddress(m_hVideoChannel, hOSD, LibMWCapture.MWCAP_VIDEO_COLOR_FORMAT.MWCAP_VIDEO_COLOR_FORMAT_RGB, LibMWCapture.MWCAP_VIDEO_QUANTIZATION_RANGE.MWCAP_VIDEO_QUANTIZATION_FULL,
                LibMWCapture.MWCAP_VIDEO_SATURATION_RANGE.MWCAP_VIDEO_SATURATION_FULL, 20, 45, (ushort)rcBound.Width, (ushort)rcBound.Height, (long)(bitmapDataOSD.Scan0), (uint)(bitmapDataOSD.Stride * rcBound.Height),
                (uint)bitmapDataOSD.Stride, (ushort)rcBound.Width, (ushort)rcBound.Height, 0, 1, 1);

            bitmapOSD.UnlockBits(bitmapDataOSD);
           
            return false;
        }

        public static void VideoCaptureCallback(IntPtr pbyImage, UInt32 cbFrame, int cbImageStride, IntPtr pvParam)
        {
            if (pvParam != IntPtr.Zero)
            {
                LibMWMedia.MWD3DRendererPushFrame(pvParam, pbyImage, cbImageStride);
            }
        }
        public void SetCheck(int nSort, Boolean bChecked)
        {
            if (nSort == 1)
                m_bText = bChecked;
            else if (nSort == 2)
                m_bBorder = bChecked;
            else if (nSort == 3)
                m_bIcon = bChecked;
        }
    }
}
