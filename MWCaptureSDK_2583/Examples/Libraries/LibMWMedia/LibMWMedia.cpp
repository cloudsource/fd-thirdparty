// LibMWMedia.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"



#include "D11Renderer.h"
#include "LibMWMedia\MWMedia.h"

HD3DRENDERER
LIBMWMEDIA_API
MWCreateD3DRenderer(
	int						cx,
	int						cy,
	DWORD					dwFourcc,
	HWND					hWnd
	)
{
	CD11Renderer* pRet = new CD11Renderer();
	if (!pRet->Init(cx, cy, dwFourcc, hWnd)) {
		delete pRet;
		pRet = NULL;
	}
	return (HD3DRENDERER)pRet;
}

void
LIBMWMEDIA_API
MWDestroyD3DRenderer(
	HD3DRENDERER			hRenderer
	)
{
	if (hRenderer == NULL)
		return;

	CD11Renderer* pRet = (CD11Renderer*) hRenderer;
	pRet->Exit();
	delete pRet;
}

BOOL
LIBMWMEDIA_API
MWD3DRendererPushFrame(
	HD3DRENDERER			hRenderer,
	const BYTE *			pbyBuffer,
	int						cbStride
	)
{
	if (hRenderer == NULL)
		return FALSE;

	CD11Renderer* pRet = (CD11Renderer*) hRenderer;
	return pRet->Update(pbyBuffer, cbStride);
}