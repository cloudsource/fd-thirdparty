// LibMWCapture_JAVA.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "LibMWJAVA.h"

JavaVM  *g_pVM = NULL;

char* WCHARToChar(WCHAR *wchar)
{
	int nLength = WideCharToMultiByte(CP_ACP, 0, wchar, -1, NULL, 0, NULL, NULL);
	if(nLength == 0){
		return NULL;
	}
	char *str = (char *)malloc(nLength * sizeof(char));
	WideCharToMultiByte(CP_ACP, 0, wchar, -1, str, nLength, NULL, NULL);

	return str;
}

WCHAR* CharToWCHAR(char *str)
{
	int nLength = MultiByteToWideChar(CP_ACP, NULL, str, -1, NULL, 0);
	if (nLength == 0){
		return NULL;
	}
	WCHAR* wchar = (WCHAR *)malloc(nLength * sizeof(WCHAR));
	MultiByteToWideChar(CP_ACP, 0, str, -1, wchar, nLength);

	return wchar;
}

char* jstringToChar(JNIEnv *env,jstring jstr)
{
	int length = (env)->GetStringLength(jstr );
	const jchar* jcstr = (env)->GetStringChars(jstr, 0);
	char* rtn = (char*)malloc(length * 2 + 1);
	int size = 0;
	size = WideCharToMultiByte(CP_ACP, 0,(LPCWSTR)jcstr, length, rtn,(length * 2 + 1), NULL, NULL);
	if(size <= 0)
		return NULL;
	(env)->ReleaseStringChars(jstr, jcstr);
	rtn[size] = 0;
	return rtn;
}

jstring CharTojstring(JNIEnv* env, char* str)
{
	jstring rtn = 0;
	int slen = strlen(str);
	unsigned short * buffer = 0;
	if(slen == 0)
		rtn = (env)->NewStringUTF(str); 
	else{
		int length = MultiByteToWideChar(CP_ACP, 0,(LPCSTR)str, slen, NULL, 0);
		buffer = (unsigned short *)malloc(length * 2 + 1);
		if( MultiByteToWideChar(CP_ACP, 0,(LPCSTR)str, slen,(LPWSTR)buffer, length ) >0)
			rtn = (env)->NewString((jchar*)buffer, length);
	}
	if(buffer)
		free(buffer);
	return rtn;
}


JNIEXPORT jboolean JNICALL Java_com_magewell_libmwcapture_LibMWCapture_InitNative
	(JNIEnv *env, jclass)
{
	env->GetJavaVM(&g_pVM);

	return true;
}

JNIEXPORT void JNICALL Java_com_magewell_libmwcapture_LibMWCapture_ExitNative
	(JNIEnv *, jclass)
{

}

JNIEXPORT jlong JNICALL Java_com_magewell_libmwcapture_LibMWCapture_GetJavaWnd
	(JNIEnv *env, jclass clazz, jobject objWindow)
{
	HWND hwnd = NULL;
	JAWT_DrawingSurface *ds;
	JAWT_DrawingSurfaceInfo *dsi;
	JAWT_Win32DrawingSurfaceInfo *win;

	JAWT awt;
	awt.version = JAWT_VERSION_1_3;
	jboolean result = JAWT_GetAWT(env, &awt);

	if(result == JNI_TRUE){
		ds = awt.GetDrawingSurface(env, objWindow);
		jint lock = ds->Lock(ds);
		if(lock != JAWT_LOCK_ERROR){
			dsi = ds->GetDrawingSurfaceInfo(ds);
			win = (JAWT_Win32DrawingSurfaceInfo *)dsi->platformInfo;   
			hwnd = win->hwnd;
			ds->FreeDrawingSurfaceInfo(dsi);
			ds->Unlock(ds);
			awt.FreeDrawingSurface(ds);
		}
	}

	return (jlong)hwnd;
}

JNIEXPORT jboolean JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWCaptureInitInstance
	(JNIEnv *, jclass)
{
	return (jboolean)MWCaptureInitInstance();
}

JNIEXPORT void JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWCaptureExitInstance
	(JNIEnv *, jclass)
{
	MWCaptureExitInstance();
}

JNIEXPORT jlong JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWGetFourcc
	(JNIEnv *env, jclass, jstring strColorFormat)
{
	char *ch = new char[4];
	ch = jstringToChar(env, strColorFormat);
	DWORD dwFourcc = ((DWORD)(BYTE)(ch[0]) | ((DWORD)(BYTE)(ch[1]) << 8) | ((DWORD)(BYTE)(ch[2]) << 16) | ((DWORD)(BYTE)(ch[3]) << 24));
	delete[] ch;
	return (jlong)dwFourcc;
}

JNIEXPORT jint JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWGetChannelCount
	(JNIEnv *, jclass)
{
	return (jint)MWGetChannelCount();
}

JNIEXPORT jlong JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWOpenChannelByIndex
	(JNIEnv *, jclass, jint jint_nChannelIndex)
{
	TCHAR szDevicePath[MAX_PATH];
	MWGetDevicePath(jint_nChannelIndex, szDevicePath);

	HCHANNEL hChannel = MWOpenChannelByPath(szDevicePath);
	return (jlong)hChannel;
}

JNIEXPORT void JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWCloseChannel
	(JNIEnv *, jclass, jlong jlong_hChannel)
{
	HCHANNEL hChannel = (HCHANNEL)jlong_hChannel;
	MWCloseChannel(hChannel);
}

JNIEXPORT jboolean JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWStartVideoCapture
	(JNIEnv *, jclass, jlong jlong_hChannel, jlong jlong_hEvent)
{
	HCHANNEL hChannel = (HCHANNEL)jlong_hChannel;
	HANDLE hEvent = (HANDLE)jlong_hEvent;

	return (jboolean)(MW_SUCCEEDED == MWStartVideoCapture(hChannel, hEvent));
}

JNIEXPORT jboolean JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWStopVideoCapture
	(JNIEnv *, jclass, jlong jlong_hChannel)
{
	HCHANNEL hChannel = (HCHANNEL)jlong_hChannel;

	return (jboolean)(MW_SUCCEEDED == MWStopVideoCapture(hChannel));
}

//JNIEXPORT jobject JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWGetVideoSignalStatus
//	(JNIEnv *, jclass, jlong)
//{
//
//}
//
//JNIEXPORT jobject JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWGetVideoBufferInfo
//	(JNIEnv *, jclass, jlong)
//{
//
//}
//
//JNIEXPORT jobject JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWGetVideoFrameInfo
//	(JNIEnv *, jclass, jlong, jint)
//{
//
//}

JNIEXPORT jobject JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWGetVideoCaptureStatus
	(JNIEnv *env, jclass clazz, jlong jlong_hChannel)
{
	HCHANNEL hChannel = (HCHANNEL)jlong_hChannel;
	MWCAP_VIDEO_CAPTURE_STATUS status;
	if (MW_SUCCEEDED != MWGetVideoCaptureStatus(hChannel, &status))
		return NULL;

	jclass jc_captureStatus = env->FindClass("com/magewell/libmwcapture/VIDEO_CAPTURE_STATUS");
	if (jc_captureStatus == NULL)
		return NULL;

	jmethodID jm_initCaptureStatus = env->GetMethodID(jc_captureStatus, "<init>", "(JZIZII)V");
	if (jm_initCaptureStatus == NULL)
		return NULL;

	jobject objCaptureStatus = env->NewObject(jc_captureStatus, jm_initCaptureStatus, (jlong)status.pvContext, status.bPhysicalAddress, (jint)status.iFrame, 
		status.bFrameCompleted, (jint)status.cyCompleted, (jint)status.cyCompletedPrev);

	env->DeleteLocalRef(objCaptureStatus);

	return objCaptureStatus;
}

JNIEXPORT jboolean JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWCaptureVideoFrameToVirtualAddress
	(JNIEnv *env, jclass, jlong jlong_hChannel, jint nFrameIndex, jobject objBuffer, jlong cbFrame, jlong cbStride, jboolean bBottomTop, jlong dwFourcc, jint nWidth, jint nHeight)
{
	jboolean bRet = false;
	HCHANNEL hChannel = (HCHANNEL)jlong_hChannel;
	BYTE *pbyImage = (BYTE *)env->GetDirectBufferAddress(objBuffer);
	if (pbyImage == NULL)
		return bRet;

	bRet = (MW_SUCCEEDED == MWCaptureVideoFrameToVirtualAddress(hChannel, nFrameIndex, pbyImage, (DWORD)cbFrame, (DWORD)cbStride, bBottomTop, NULL, (DWORD)dwFourcc, nWidth, nHeight));
	return bRet;
}

JNIEXPORT jlong JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWGetDeviceTime
	(JNIEnv *, jclass, jlong jlong_hChannel)
{
	HCHANNEL hChannel = (HCHANNEL)jlong_hChannel;
	LONGLONG llBegin = 0LL;
	MWGetDeviceTime(hChannel, &llBegin);

	return (jlong)llBegin;
}

JNIEXPORT jboolean JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWScheduleTimer
	(JNIEnv *, jclass, jlong jlong_hChannel, jlong jlong_hTimer, jlong llExpireTime)
{
	HCHANNEL hChannel = (HCHANNEL)jlong_hChannel;
	HTIMER hTimer = (HTIMER)jlong_hTimer;

	return (jboolean)(MW_SUCCEEDED == MWScheduleTimer(hChannel, hTimer, llExpireTime));
}

JNIEXPORT jlong JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWRegisterTimer
	(JNIEnv *, jclass, jlong jlong_hChannel, jlong jlong_hEvent)
{
	HCHANNEL hChannel = (HCHANNEL)jlong_hChannel;
	HANDLE hEvent = (HANDLE)jlong_hEvent;

	HTIMER hTimer = MWRegisterTimer(hChannel, hEvent);
	return (jlong)hTimer;
}

JNIEXPORT jboolean JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWUnregisterTimer
	(JNIEnv *, jclass, jlong jlong_hChannel, jlong jlong_hTimer)
{
	HCHANNEL hChannel = (HCHANNEL)jlong_hChannel;
	HTIMER hTimer = (HTIMER)jlong_hTimer;

	return (jboolean)(MW_SUCCEEDED == MWUnregisterTimer(hChannel, hTimer));
}

JNIEXPORT jlong JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWRegisterNotify
	(JNIEnv *, jclass, jlong jlong_hChannel, jlong jlong_hEvent, jlong dwEnableBits)
{
	HCHANNEL hChannel = (HCHANNEL)jlong_hChannel;
	HANDLE hEvent = (HANDLE)jlong_hEvent;

	HNOTIFY hNotify = MWRegisterNotify(hChannel, hEvent, (DWORD)dwEnableBits);
	return (jlong) hNotify;
}

JNIEXPORT jboolean JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWUnregisterNotify
	(JNIEnv *, jclass, jlong jlong_hChannel, jlong jlong_hNotify)
{
	HCHANNEL hChannel = (HCHANNEL)jlong_hChannel;
	HNOTIFY hNotify = (HNOTIFY)jlong_hNotify;

	return (jboolean)(MW_SUCCEEDED == MWUnregisterNotify(hChannel, hNotify));
}

JNIEXPORT jlong JNICALL Java_com_magewell_libmwcapture_LibMWCapture_FOURCC_1CalcImageSize
	(JNIEnv *, jclass, jlong dwFourcc, jint nWidth, jint nHeight, jlong cbStride)
{
	DWORD cbImageSize = FOURCC_CalcImageSize((DWORD)dwFourcc, nWidth, nHeight, (DWORD)cbStride);

	return (jlong)cbImageSize;
}

JNIEXPORT jlong JNICALL Java_com_magewell_libmwcapture_LibMWCapture_FOURCC_1CalcMinStride
	(JNIEnv *, jclass, jlong dwFourcc, jint nWidth, jlong dwAlign)
{
	DWORD cbStride = FOURCC_CalcMinStride((DWORD)dwFourcc, nWidth, (DWORD)dwAlign);

	return (jlong)cbStride;
}

JNIEXPORT jlong JNICALL Java_com_magewell_libmwcapture_LibMWCapture_CreateEvent
	(JNIEnv *, jclass)
{
	HANDLE hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	return (jlong)hEvent;
}

JNIEXPORT jboolean JNICALL Java_com_magewell_libmwcapture_LibMWCapture_SetEvent
	(JNIEnv *, jclass, jlong jlong_hEvent)
{
	HANDLE hEvent = (HANDLE)jlong_hEvent;

	return (jboolean)SetEvent(hEvent);
}

JNIEXPORT jboolean JNICALL Java_com_magewell_libmwcapture_LibMWCapture_CloseHandle
	(JNIEnv *, jclass, jlong jlong_hEvent)
{
	HANDLE hEvent = (HANDLE)jlong_hEvent;

	return (jboolean)CloseHandle(hEvent);
}

JNIEXPORT jlong JNICALL Java_com_magewell_libmwcapture_LibMWCapture_WaitForMultipleObjects
	(JNIEnv *env, jclass clazz, jint nEventIndex, jlongArray jlong_hEventArr, jboolean bWaitAll, jlong dwMilliseconds)
{
	jlong* pHandle = env->GetLongArrayElements(jlong_hEventArr, NULL);
	jsize len = env->GetArrayLength(jlong_hEventArr);

	HANDLE hEventArr[32];
	for (int i = 0; i < len; i ++){
		//		printf("C++ HANDLE == %d\n", pHandle[i]);
		hEventArr[i] = (HANDLE)pHandle[i];
	}
	env->ReleaseLongArrayElements(jlong_hEventArr, pHandle, 0);

	return (jlong)WaitForMultipleObjects((DWORD)nEventIndex, hEventArr, bWaitAll, (DWORD)dwMilliseconds);
}

JNIEXPORT jlong JNICALL Java_com_magewell_libmwcapture_LibMWCapture_WaitForSingleObject
	(JNIEnv *, jclass, jlong jlong_hEvent, jlong dwMilliseconds)
{
	HANDLE hEvent = (HANDLE)jlong_hEvent;

	return (jlong)WaitForSingleObject(hEvent, (DWORD)dwMilliseconds);
}

JNIEXPORT jlong JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWCreateD3DRenderer
	(JNIEnv *, jclass, jint nWidth, jint nHeight, jlong dwFourcc, jlong jlong_hWnd)
{
	HWND hWnd = (HWND)jlong_hWnd;
	HD3DRENDERER hD3dRenderer = MWCreateD3DRenderer(nWidth, nHeight, (DWORD)dwFourcc, hWnd);

	return (jlong)hD3dRenderer;
}

JNIEXPORT void JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWDestroyD3DRenderer
	(JNIEnv *, jclass, jlong jlong_D3dRenderer)
{
	HD3DRENDERER hD3dRenderer = (HD3DRENDERER)jlong_D3dRenderer;
	MWDestroyD3DRenderer(hD3dRenderer);
}

JNIEXPORT jboolean JNICALL Java_com_magewell_libmwcapture_LibMWCapture_MWD3DRendererPushFrame
	(JNIEnv *env, jclass, jlong jlong_D3dRenderer, jobject objBuffer, jint cbStride)
{
	jboolean bRet = false;
	HD3DRENDERER hD3dRenderer = (HD3DRENDERER)jlong_D3dRenderer;
	BYTE *pbyImage = (BYTE *)env->GetDirectBufferAddress(objBuffer);
	if (pbyImage == NULL)
		return bRet;

	bRet = MWD3DRendererPushFrame(hD3dRenderer, pbyImage, (DWORD)cbStride);

	return bRet;
}