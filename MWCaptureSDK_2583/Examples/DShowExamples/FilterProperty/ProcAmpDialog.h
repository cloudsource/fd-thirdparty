#pragma once

#include <dshow.h>

// CProcAmpDialog dialog

class CProcAmpDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CProcAmpDialog)

public:
	CProcAmpDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CProcAmpDialog();

// Dialog Data
	enum { IDD = IDD_DIALOG_PROC_AMP };
public:
	void UpdateAmpProperty();
	void SetCurrentAmpProperty();

private:
	void GetProcAmpProperty(VideoProcAmpProperty ampProperty, CSliderCtrl* pslider, CButton* pCheckButton);
	void SetProcAmpProperty(VideoProcAmpProperty ampProperty, CSliderCtrl* pslider);
	void SetProcAmpPropertyDefault(VideoProcAmpProperty ampProperty, CSliderCtrl* pslider);

private:
	CSliderCtrl			m_sldBrightNess;
	CSliderCtrl			m_sldConstract;
	CSliderCtrl			m_sldHue;
	CSliderCtrl			m_sldSat;
	CSliderCtrl			m_sldDefi;
	CSliderCtrl			m_sldGamma;
	CSliderCtrl			m_sldWhite;
	CSliderCtrl			m_sldBacklight;
	CSliderCtrl			m_sldGain;
	
	CEdit			m_editBrightNess;
	CEdit			m_editConstract;
	CEdit			m_editHue;
	CEdit			m_editSat;
	CEdit			m_editDefi;
	CEdit			m_editGamma;
	CEdit			m_editWhite;
	CEdit			m_editBacklight;
	CEdit			m_editGain;
	
	CButton			m_ckbBrightNess;
	CButton			m_ckbConstract;
	CButton			m_ckbHue;
	CButton			m_ckbSat;
	CButton			m_ckbDefi;
	CButton			m_ckbGamma;
	CButton			m_ckbWhite;
	CButton			m_ckbBacklight;
	CButton			m_ckbGain;

	CButton			m_ckbFreq;
	CComboBox		m_cmbFreq;

	CButton			m_ckbDefault;

	HICON			m_hIcon;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnNMCustomdrawSliderProcBrightness(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawSliderProcConstract(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawSliderProcHue(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawSliderProcSat(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawSliderProcDefi(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawSliderProcGamma(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawSliderProcWhite(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawSliderProcBacklight(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawSliderProcGain(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonProcDefault();
};
