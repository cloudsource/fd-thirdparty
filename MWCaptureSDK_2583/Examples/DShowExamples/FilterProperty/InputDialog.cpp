// InputDialog.cpp : implementation file
//

#include "stdafx.h"
#include "FilterProperty.h"
#include "InputDialog.h"
#include "afxdialogex.h"

TCHAR g_wszSignalStatus[][32] = {
	_T("Not present"),
	_T("Unsupported"),
	_T("Locking"),
	_T("Locked")
};

TCHAR g_wszColorFormat[][32] = {
	//_T("Unknown"),
	_T("RGB"),
	_T("YUV BT.601"),
	_T("YUV BT.709"),
	_T("YUV BT.2020"),
	_T("YUV BT.2020C")
};

TCHAR g_wszQuantRange[][32] = {
	_T("Unknown"),
	_T("Full range"),
	_T("Limited range")
};

TCHAR g_wszSatRange[][32] = {
	_T("Unknown"),
	_T("Full range"),
	_T("Limited range"),
	_T("Extended gamut")
};

TCHAR g_wszVideoInputType[][32] = {
	_T("NONE"),
	_T("HDMI"),
	_T("VGA"),
	_T("SDI"),
	_T("COMPONENT"),
	_T("CVBS"),
	_T("YC")
};

TCHAR g_wszAudioInputType[][32] = {
	_T("NONE"),
	_T("HDMI"),
	_T("SDI"),
	_T("LINE_IN"),
	_T("MIC_IN")
};

// CInputDialog dialog

IMPLEMENT_DYNAMIC(CInputDialog, CDialogEx)

	CInputDialog::CInputDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CInputDialog::IDD, pParent)
{

}

CInputDialog::~CInputDialog()
{
}

void CInputDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_INPUT_VIDEO, m_cmbVideoInput);
	DDX_Control(pDX, IDC_COMBO_INPUT_AUDIO, m_cmbAudioInput);
	DDX_Control(pDX, IDC_CHECK_INPUT_AUTO_SCAN, m_ckbAutoScan);
	DDX_Control(pDX, IDC_CHECK_INPUT_LINK_VIDEO, m_ckbLinkVideo);
	DDX_Control(pDX, IDC_TAB_INPUT_STATUS, m_tabStatus);
	DDX_Control(pDX, IDC_LIST_INPUT_STATUS, m_listStatus);
}


BEGIN_MESSAGE_MAP(CInputDialog, CDialogEx)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_INPUT_STATUS, &CInputDialog::OnTcnSelchangeTabInputStatus)
	ON_CBN_SELCHANGE(IDC_COMBO_INPUT_VIDEO, &CInputDialog::OnCbnSelchangeComboInputVideo)
	ON_CBN_SELCHANGE(IDC_COMBO_INPUT_AUDIO, &CInputDialog::OnCbnSelchangeComboInputAudio)
	ON_BN_CLICKED(IDC_CHECK_INPUT_AUTO_SCAN, &CInputDialog::OnBnClickedCheckInputAutoScan)
	ON_BN_CLICKED(IDC_CHECK_INPUT_LINK_VIDEO, &CInputDialog::OnBnClickedCheckInputLinkVideo)
	ON_WM_DESTROY()
	ON_WM_TIMER()
END_MESSAGE_MAP()


BOOL CInputDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_listStatus.ModifyStyle(0, LVS_REPORT);
	m_listStatus.SetExtendedStyle(m_listStatus.GetExtendedStyle() | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);  

	m_listStatus.InsertColumn(0, _T("Name"));  
	m_listStatus.InsertColumn(1, _T("Value"));  

	CRect rect;  
	m_listStatus.GetClientRect(rect);
	m_listStatus.SetColumnWidth(0, rect.Width() / 2); 
	m_listStatus.SetColumnWidth(1, rect.Width() / 2);  

	m_tabStatus.InsertItem(0, _T("Video signal status"));
	m_tabStatus.InsertItem(1, _T("Audio signal status"));
	m_tabStatus.InsertItem(2, _T("Input specific status"));

	SetAVInputType();
	UpdateVideoStatus();

	g_capture.AddNotify(this);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

// CInputDialog message handlers

void CInputDialog::OnTcnSelchangeTabInputStatus(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;

	int nSel = m_tabStatus.GetCurSel();
	switch(nSel)
	{
	case 0:
		UpdateVideoStatus();
		break;
	case 1:
		UpdateAudioStatus();
		break;
	case 2:
		UpdateSpecificStatus();
		break;
	}
}


void CInputDialog::OnCbnSelchangeComboInputVideo()
{
	// TODO: Add your control notification handler code here

	DWORD dwSelVideoInputSource = INPUT_SOURCE(m_szVideoInputType[m_cmbVideoInput.GetCurSel()], 0);
	MWGetExtension()->SetVideoInputSource(dwSelVideoInputSource);

}


void CInputDialog::OnCbnSelchangeComboInputAudio()
{
	// TODO: Add your control notification handler code here

	DWORD dwSelAudioInputSource = INPUT_SOURCE(m_szAudioInputType[m_cmbAudioInput.GetCurSel()], 0);
	MWGetExtension()->SetAudioInputSource(dwSelAudioInputSource);

}


void CInputDialog::OnBnClickedCheckInputAutoScan()
{
	// TODO: Add your control notification handler code here

	BOOLEAN bAutoScan = m_ckbAutoScan.GetCheck();
	MWGetExtension()->SetInputSourceScan(bAutoScan);
	m_cmbVideoInput.EnableWindow(!bAutoScan);
	InputSourceChange();
}


void CInputDialog::OnBnClickedCheckInputLinkVideo()
{
	// TODO: Add your control notification handler code here

	BOOLEAN bLinkVideo = m_ckbLinkVideo.GetCheck();
	MWGetExtension()->SetAVInputSourceLink(bLinkVideo);
	m_cmbAudioInput.EnableWindow(!bLinkVideo);
	InputSourceChange();
}

int GetInputTypeIndex(int nValue, int nMin, int nMax)
{
	int nIndex = 0;
	for (int i = 0; i < 256; ++ i) {
		if (nMin + i == nValue) {
			nIndex = i;
			break;
		}
	}
	return nIndex;
}

void CInputDialog::GetVideoType( MWCAP_VIDEO_INPUT_TYPE& videoInputType, TCHAR* pszType, int nTypeValue, int& nIndex )
{
	int nInputTypeNone = INPUT_SOURCE(MWCAP_VIDEO_INPUT_TYPE_NONE, 0);
	int nInputTypeHDMI = INPUT_SOURCE(MWCAP_VIDEO_INPUT_TYPE_HDMI, 0);
	int nInputTypeVGA = INPUT_SOURCE(MWCAP_VIDEO_INPUT_TYPE_VGA, 0);
	int nValueSDI = INPUT_SOURCE(MWCAP_VIDEO_INPUT_TYPE_SDI, 0);
	int nInputTypeComponent = INPUT_SOURCE(MWCAP_VIDEO_INPUT_TYPE_COMPONENT, 0);
	int nInputTypeCVBS = INPUT_SOURCE(MWCAP_VIDEO_INPUT_TYPE_CVBS, 0);
	int nInputTypeYC = INPUT_SOURCE(MWCAP_VIDEO_INPUT_TYPE_YC, 0);

	if (nTypeValue >= nInputTypeNone && nTypeValue < nInputTypeHDMI) {
		nIndex = GetInputTypeIndex(nTypeValue, nInputTypeNone, nInputTypeHDMI);
		wsprintf(pszType, _T("NONE"));
		videoInputType = MWCAP_VIDEO_INPUT_TYPE_NONE;
	}
	else if (nTypeValue >= nInputTypeHDMI && nTypeValue < nInputTypeVGA) {
		nIndex = GetInputTypeIndex(nTypeValue, nInputTypeHDMI, nInputTypeVGA);
		wsprintf(pszType, _T("HDMI"));
		videoInputType = MWCAP_VIDEO_INPUT_TYPE_HDMI;
	}
	else if (nTypeValue >= nInputTypeVGA && nTypeValue < nValueSDI) {
		nIndex = GetInputTypeIndex(nTypeValue, nInputTypeVGA, nValueSDI);
		wsprintf(pszType, _T("VGA"));
		videoInputType = MWCAP_VIDEO_INPUT_TYPE_VGA;
	}
	else if (nTypeValue >= nValueSDI && nTypeValue < nInputTypeComponent) {
		nIndex = GetInputTypeIndex(nTypeValue, nValueSDI, nInputTypeComponent);
		wsprintf(pszType, _T("SDI"));
		videoInputType = MWCAP_VIDEO_INPUT_TYPE_SDI;
	}
	else if (nTypeValue >= nInputTypeComponent && nTypeValue < nInputTypeCVBS) {
		nIndex = GetInputTypeIndex(nTypeValue, nInputTypeComponent, nInputTypeCVBS);
		wsprintf(pszType, _T("Component"));
		videoInputType = MWCAP_VIDEO_INPUT_TYPE_COMPONENT;
	}
	else if (nTypeValue >= nInputTypeCVBS && nTypeValue < nInputTypeYC) {
		nIndex = GetInputTypeIndex(nTypeValue, nInputTypeCVBS, nInputTypeYC);
		wsprintf(pszType, _T("Composite"));
		videoInputType = MWCAP_VIDEO_INPUT_TYPE_CVBS;
	}
	else if (nTypeValue >= nInputTypeYC) {
		nIndex = GetInputTypeIndex(nTypeValue, nInputTypeYC, nInputTypeYC * 2);
		wsprintf(pszType, _T("YC"));
		videoInputType = MWCAP_VIDEO_INPUT_TYPE_YC;
	}
}

void CInputDialog::GetAudioType( MWCAP_AUDIO_INPUT_TYPE& audioInputType, TCHAR* pszType, int nTypeValue, int& nIndex )
{
	int nInputTypeNone = INPUT_SOURCE(MWCAP_AUDIO_INPUT_TYPE_NONE, 0);
	int nInputTypeHDMI = INPUT_SOURCE(MWCAP_AUDIO_INPUT_TYPE_HDMI, 0);
	int nInputTypeSDI = INPUT_SOURCE(MWCAP_AUDIO_INPUT_TYPE_SDI, 0);
	int nValueLine = INPUT_SOURCE(MWCAP_AUDIO_INPUT_TYPE_LINE_IN, 0);
	int nInputTypeMIC = INPUT_SOURCE(MWCAP_AUDIO_INPUT_TYPE_MIC_IN, 0);

	if (nTypeValue >= nInputTypeNone && nTypeValue < nInputTypeHDMI) {
		nIndex = GetInputTypeIndex(nTypeValue, nInputTypeNone, nInputTypeHDMI);
		wsprintf(pszType, _T("NONE"));
		audioInputType = MWCAP_AUDIO_INPUT_TYPE_NONE;
	}
	else if (nTypeValue >= nInputTypeHDMI && nTypeValue < nInputTypeSDI) {
		nIndex = GetInputTypeIndex(nTypeValue, nInputTypeHDMI, nInputTypeSDI);
		wsprintf(pszType, _T("HDMI"));
		audioInputType = MWCAP_AUDIO_INPUT_TYPE_HDMI;
	}
	else if (nTypeValue >= nInputTypeSDI && nTypeValue < nValueLine) {
		nIndex = GetInputTypeIndex(nTypeValue, nInputTypeSDI, nValueLine);
		wsprintf(pszType, _T("SDI"));
		audioInputType = MWCAP_AUDIO_INPUT_TYPE_SDI;
	}
	else if (nTypeValue >= nValueLine && nTypeValue < nInputTypeMIC) {
		nIndex = GetInputTypeIndex(nTypeValue, nValueLine, nInputTypeMIC);
		wsprintf(pszType, _T("Line"));
		audioInputType = MWCAP_AUDIO_INPUT_TYPE_LINE_IN;
	}
	else if (nTypeValue >= nInputTypeMIC) {
		nIndex = GetInputTypeIndex(nTypeValue, nInputTypeMIC, nInputTypeMIC * 2);
		wsprintf(pszType, _T("MIC"));
		audioInputType = MWCAP_AUDIO_INPUT_TYPE_MIC_IN;
	}
}

void CInputDialog::InputSourceChange()
{
	BOOLEAN bAutoScan = FALSE;
	BOOLEAN bScanning = FALSE;
	BOOLEAN bLinkVideo = FALSE;
	if (S_OK != MWGetExtension()->GetAVInputSourceLink(&bLinkVideo))
		return;

	if (S_OK != MWGetExtension()->GetInputSourceScan(&bAutoScan))
		return;

	if (bAutoScan) {
		if (S_OK != MWGetExtension()->GetInputSourceScanState(&bScanning))
			return;
	}
	
	MWGetExtension()->GetInputSourceScan(&bAutoScan);
	m_ckbAutoScan.SetCheck(bAutoScan);
	m_cmbVideoInput.EnableWindow(!bAutoScan);

	MWGetExtension()->GetAVInputSourceLink(&bLinkVideo);
	m_ckbLinkVideo.SetCheck(bLinkVideo);
	m_cmbAudioInput.EnableWindow(!bLinkVideo);

	if (bScanning) {
		if (m_cmbVideoInput.FindString(0, _T("Scanning")) < 0)
			m_cmbVideoInput.InsertString(0, _T("Scanning"));

		m_cmbVideoInput.SetCurSel(0);
	}
	else {
		if (m_cmbVideoInput.FindString(0, _T("Scanning")) >= 0)
			m_cmbVideoInput.DeleteString(0);

		DWORD szdwVideoInputSources[8];
		DWORD cVideoBufferItems = 8;
		DWORD dwVideoSourceCount = 0;
		MWGetExtension()->GetVideoInputSourceCount(&dwVideoSourceCount);
		MWGetExtension()->GetVideoInputSourceArray(szdwVideoInputSources, &cVideoBufferItems);
		DWORD dwSelVideoInputSource = 0;
		MWGetExtension()->GetVideoInputSource(&dwSelVideoInputSource);

		int nSel = -1;
		for (DWORD i = 0; i < dwVideoSourceCount; ++ i){
			if (dwSelVideoInputSource == szdwVideoInputSources[i]) {
				nSel = i;
				break;
			}
		}
		m_cmbVideoInput.SetCurSel(nSel);
	}
	
	if (bScanning && bLinkVideo) {
		if (m_cmbAudioInput.FindString(0, _T("Scanning")) < 0)
			m_cmbAudioInput.InsertString(0, _T("Scanning"));
		m_cmbAudioInput.SetCurSel(0);
	}
	else {
		if (m_cmbAudioInput.FindString(0, _T("Scanning")) >= 0)
			m_cmbAudioInput.DeleteString(0);

		DWORD dwAudioSourceCount = 0;
		MWGetExtension()->GetAudioInputSourceCount(&dwAudioSourceCount);

		DWORD szdwAudioInputSources[8];
		DWORD cAudioBufferItems = 8;
		MWGetExtension()->GetAudioInputSourceArray(szdwAudioInputSources, &cAudioBufferItems);

		DWORD dwSelAudioInputSource = 0;
		MWGetExtension()->GetAudioInputSource(&dwSelAudioInputSource);

		int nSel = -1;
		for (DWORD i = 0; i < dwAudioSourceCount; ++ i){
			if (dwSelAudioInputSource == szdwAudioInputSources[i]) {
				nSel = i;
				break;
			}
		}
		m_cmbAudioInput.SetCurSel(nSel);
	}

	int nStatusSel = m_tabStatus.GetCurSel();
	switch(nStatusSel)
	{
	case 0:
		UpdateVideoStatus();
		break;
	case 1:
		UpdateAudioStatus();
		break;
	case 2:
		UpdateSpecificStatus();
		break;
	}
}

void CInputDialog::UpdateSpecificStatus()
{
	if (m_tabStatus.GetCurSel() != 2)
		return ;

	int nRowIndex = -1;
	LIST_ITEM item;

	MWCAP_VIDEO_SIGNAL_STATUS videoStatus;
	MWGetExtension()->GetVideoSignalStatus(&videoStatus);

	m_listListItem.RemoveAll();
	if (videoStatus.state != MWCAP_VIDEO_SIGNAL_LOCKED) {
		++ nRowIndex;
		item.strKey = _T("Signal status");
		item.strValue.Format(_T("%s"), g_wszSignalStatus[videoStatus.state]);
		m_listListItem.AddTail(item);
		UpdateTable();
	}
	else {
		MWCAP_INPUT_SPECIFIC_STATUS specificStatus;
		MWGetExtension()->GetInputSpecificStatus(&specificStatus);
		if (specificStatus.bValid) {
			++ nRowIndex;
			item.strKey = _T("Signal status");
			item.strValue.Format(_T("Present"));
			m_listListItem.AddTail(item);
			if (specificStatus.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_HDMI) {//hdmi
				++ nRowIndex;
				item.strKey = _T("Mode");
				if (specificStatus.hdmiStatus.bHDMIMode)
					item.strValue = _T("HDMI");
				else
					item.strValue = _T("No");
				m_listListItem.AddTail(item); 

				++ nRowIndex;
				item.strKey = _T("HDCP encrypted");
				item.strValue = specificStatus.hdmiStatus.bHDCP ? _T("YES") : _T("No");
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Color depth");
				item.strValue.Format(_T("%d Bits"), specificStatus.hdmiStatus.byBitDepth);
				m_listListItem.AddTail(item); 

				++ nRowIndex;
				item.strKey = _T("Pixel encoding");
				switch (specificStatus.hdmiStatus.pixelEncoding)
				{
				case HDMI_ENCODING_RGB_444:
					item.strValue = _T("R/G/B 4:4:4");
					break;
				case HDMI_ENCODING_YUV_422:
					item.strValue = _T("Y/U/V 4:2:2");
					break;
				case HDMI_ENCODING_YUV_444:
					item.strValue = _T("Y/U/V 4:4:4");
					break;
				case HDMI_ENCODING_YUV_420:
					item.strValue = _T("Y/U/V 4:2:0");
					break;
				}
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("VIC");
				item.strValue.Format(_T("%d"), specificStatus.hdmiStatus.byVIC);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("IT content");
				item.strValue = specificStatus.hdmiStatus.bITContent ? (_T("True")) : (_T("False"));
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - scanning format");
				item.strValue = specificStatus.hdmiStatus.videoTiming.bInterlaced ? _T("Interlaced") : _T("Progressive");
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - frame rate");
				int nBefore = 0, nAfter = 0;
				GetVideoFPS(specificStatus.hdmiStatus.videoTiming.dwFrameDuration, specificStatus.hdmiStatus.videoTiming.bInterlaced, nBefore, nAfter);
				item.strValue.Format(_T("%d.%d Hz"), nBefore, nAfter);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - H total");
				item.strValue.Format(_T("%d Pixels"), specificStatus.hdmiStatus.videoTiming.wHTotalWidth);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - H active");
				item.strValue.Format(_T("%d Pixels"), specificStatus.hdmiStatus.videoTiming.wHActive);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - H front porch");
				item.strValue.Format(_T("%d Pixels"), specificStatus.hdmiStatus.videoTiming.wHFrontPorch);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - H sync width");
				item.strValue.Format(_T("%d Pixels"), specificStatus.hdmiStatus.videoTiming.wHSyncWidth);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - H back proch");
				item.strValue.Format(_T("%d Pixels"), specificStatus.hdmiStatus.videoTiming.wHBackPorch);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - field 0 V total");
				item.strValue.Format(_T("%d Lines"), specificStatus.hdmiStatus.videoTiming.wField0VTotalHeight);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - field 0 V active");
				item.strValue.Format(_T("%d Lines"), specificStatus.hdmiStatus.videoTiming.wField0VActive);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - field 0 V front porch");
				item.strValue.Format(_T("%d Lines"), specificStatus.hdmiStatus.videoTiming.wField0VSyncWidth);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - field 0 V sync width");
				item.strValue.Format(_T("%d Lines"), specificStatus.hdmiStatus.videoTiming.wField0VSyncWidth);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - field 0 V back proch");
				item.strValue.Format(_T("%d Lines"), specificStatus.hdmiStatus.videoTiming.wField0VBackPorch);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - field 1 V total");
				item.strValue.Format(_T("%d Lines"), specificStatus.hdmiStatus.videoTiming.wField1VTotalHeight);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - field 1 V active");
				item.strValue.Format(_T("%d Lines"), specificStatus.hdmiStatus.videoTiming.wField1VActive);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - field 1 V front porch");
				item.strValue.Format(_T("%d Lines"), specificStatus.hdmiStatus.videoTiming.wField1VSyncWidth);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - field 1 V sync width");
				item.strValue.Format(_T("%d Lines"), specificStatus.hdmiStatus.videoTiming.wField1VSyncWidth);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - field 1 V back proch");
				item.strValue.Format(_T("%d Lines"), specificStatus.hdmiStatus.videoTiming.wField1VBackPorch);
				m_listListItem.AddTail(item);
			}
			else if (specificStatus.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_VGA) {//vga
				++ nRowIndex;
				item.strKey = _T("Sync type");
				switch (specificStatus.vgaComponentStatus.syncInfo.bySyncType)
				{
				case VIDEO_SYNC_ALL:
					item.strValue = _T("ALL");
					break;
				case VIDEO_SYNC_HS_VS:
					item.strValue = _T("HS_VS");
					break;
				case VIDEO_SYNC_CS:
					item.strValue = _T("CS");
					break;
				case VIDEO_SYNC_EMBEDDED:
					item.strValue = _T("Embendded");
					break;
				}
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Frame rate");
				int nBefore = 0, nAfter = 0;
				GetVideoFPS(specificStatus.vgaComponentStatus.syncInfo.dwFrameDuration, specificStatus.vgaComponentStatus.syncInfo.bInterlaced, nBefore, nAfter);
				item.strValue.Format(_T("%d.%d Hz"), nBefore, nAfter);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Scanning format");
				item.strValue = specificStatus.vgaComponentStatus.syncInfo.bInterlaced ? _T("Interlaced") : _T("Progressive");
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("VS width");
				item.strValue.Format(_T("%d Lines"), specificStatus.vgaComponentStatus.syncInfo.wVSyncLineCount);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Total scan lines");
				item.strValue.Format(_T("%d Lines"), specificStatus.vgaComponentStatus.syncInfo.wFrameLineCount);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - Type");
				switch (specificStatus.vgaComponentStatus.videoTiming.dwType)
				{
				case MWCAP_VIDEO_TIMING_NONE:
					item.strValue = "NONE";
					break;
				case MWCAP_VIDEO_TIMING_LEGACY:
					item.strValue = "LEGACY";
					break;
				case MWCAP_VIDEO_TIMING_DMT:
					item.strValue = "DMT";
					break;
				case MWCAP_VIDEO_TIMING_CEA:
					item.strValue = "CEA";
					break;
				case MWCAP_VIDEO_TIMING_GTF:
					item.strValue = "GTF";
					break;
				case MWCAP_VIDEO_TIMING_CVT:
					item.strValue = "CVT";
					break;
				case MWCAP_VIDEO_TIMING_CVT_RB:
					item.strValue = "CVT_RB";
					break;
					//case MWCAP_VIDEO_TIMING_CUSTOM:
					//	strValue = "CUSTOM";
					//	break;
				case MWCAP_VIDEO_TIMING_FAILSAFE:
					item.strValue = "FAILSAFE";
					break;
				}
				m_listListItem.AddTail(item);		

				++ nRowIndex;
				item.strKey = _T("Timing - Pixel clock");
				item.strValue.Format(_T("%d Hz"), specificStatus.vgaComponentStatus.videoTiming.dwPixelClock);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - H active");
				item.strValue.Format(_T("%d Pixels"), specificStatus.vgaComponentStatus.videoTiming.wHActive);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - H front porch");
				item.strValue.Format(_T("%d Pixels"), specificStatus.vgaComponentStatus.videoTiming.wHFrontPorch);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - H sync width");
				item.strValue.Format(_T("%d Pixels"), specificStatus.vgaComponentStatus.videoTiming.wHSyncWidth);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - H back proch");
				item.strValue.Format(_T("%d Pixels"), specificStatus.vgaComponentStatus.videoTiming.wHBackPorch);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - V active");
				item.strValue.Format(_T("%d Lines"), specificStatus.vgaComponentStatus.videoTiming.wVActive);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - V front porch");
				item.strValue.Format(_T("%d Lines"), specificStatus.vgaComponentStatus.videoTiming.wVFrontPorch);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - V sync width");
				item.strValue.Format(_T("%d Lines"), specificStatus.vgaComponentStatus.videoTiming.wVSyncWidth);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - V back proch");
				item.strValue.Format(_T("%d Lines"), specificStatus.vgaComponentStatus.videoTiming.wVBackPorch);
				m_listListItem.AddTail(item);
			}
			else if (specificStatus.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_SDI) {
				++ nRowIndex;
				item.strKey = _T("Type");
				switch (specificStatus.sdiStatus.sdiType)
				{
				case SDI_TYPE_SD:
					item.strValue = _T("SD");
					break;
				case SDI_TYPE_HD:
					item.strValue = _T("HD");
					break;
				case SDI_TYPE_3GA:
					item.strValue = _T("3GA");
					break;
				case SDI_TYPE_3GB_DL:
					item.strValue = _T("3GB DL");
					break;
				case SDI_TYPE_3GB_DS:
					item.strValue = _T("3GB DS");
					break;
				case SDI_TYPE_DL_CH1:
					item.strValue = _T("DL CH1");
					break;
				case SDI_TYPE_DL_CH2:
					item.strValue = _T("DL CH2");
					break;
				}
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Scanning format");
				switch (specificStatus.sdiStatus.sdiScanningFormat)
				{
				case SDI_SCANING_INTERLACED:
					item.strValue = _T("Interlaced");
					break;
				case SDI_SCANING_SEGMENTED_FRAME:
					item.strValue = _T("Segmented frame");
					break;
				case SDI_SCANING_PROGRESSIVE:
					item.strValue = _T("Progressive");
					break;
				}
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Color depth");
				switch (specificStatus.sdiStatus.sdiBitDepth)
				{
				case SDI_BIT_DEPTH_8BIT:
					item.strValue = _T("8 Bits");
					break;
				case SDI_BIT_DEPTH_10BIT:
					item.strValue = _T("10 Bits");
					break;
				case SDI_BIT_DEPTH_12BIT:
					item.strValue = _T("12 Bits");
					break;
				}
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Sampling struct");
				switch (specificStatus.sdiStatus.sdiSamplingStruct)
				{
				case SDI_SAMPLING_422_YCbCr:
					item.strValue = _T("Y/Cb/Cr, 4:2:2");
					break;
				case SDI_SAMPLING_444_YCbCr:
					item.strValue = _T("Y/Cb/Cr, 4:4:4");
					break;
				case SDI_SAMPLING_444_RGB:
					item.strValue = _T("R/G/B, 4:4:4");
					break;
				case SDI_SAMPLING_420_YCbCr:
					item.strValue = _T("4:2:0");
					break;
				case SDI_SAMPLING_4224_YCbCrA:
					item.strValue = _T("Y/Cb/Cr/A, 4:2:2:4");
					break;
				case SDI_SAMPLING_4444_YCbCrA:
					item.strValue = _T("Y/Cb/Cr/A, 4:4:4:4");
					break;
				case SDI_SAMPLING_4444_RGBA:
					item.strValue = _T("R/G/B/A, 4:4:4:4");
					break;
				case SDI_SAMPLING_4224_YCbCrD:
					item.strValue = _T("Y/Cb/Cr/D, 4:2:2:4");
					break;
				case SDI_SAMPLING_4444_YCbCrD:
					item.strValue = _T("Y/Cb/Cr/A, 4:4:4:4");
					break;
				case SDI_SAMPLING_4444_RGBD:
					item.strValue = _T("R/G/B/D, 4:4:4:4");
					break;
				case SDI_SAMPLING_444_XYZ:
					item.strValue = _T("X/Y/Z, 4:4:4");
					break;
				}
				m_listListItem.AddTail(item);
			}
			else if (specificStatus.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_COMPONENT) { //component
				++ nRowIndex;
				item.strKey = _T("Sync type");
				switch (specificStatus.vgaComponentStatus.videoTiming.bySyncType)
				{
				case VIDEO_SYNC_ALL:
					item.strValue = _T("All sync");
					break;
				case VIDEO_SYNC_HS_VS:
					item.strValue = _T("HS/VS sync");
					break;
				case VIDEO_SYNC_CS:
					item.strValue = _T("CS sync");
					break;
				case VIDEO_SYNC_EMBEDDED:
					item.strValue = _T("Embendded sync");
					break;
				}
				m_listListItem.AddTail(item);

				++ nRowIndex;	
				item.strKey = _T("Frame rate");		

				int nBefore = 0, nAfter = 0;
				GetVideoFPS(specificStatus.vgaComponentStatus.syncInfo.dwFrameDuration, specificStatus.vgaComponentStatus.syncInfo.bInterlaced, nBefore, nAfter);
				item.strValue.Format(_T("%d.%d Hz"), nBefore, nAfter);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Scanning format");
				item.strValue = specificStatus.vgaComponentStatus.syncInfo.bInterlaced ? _T("Interlaced") : _T("Progressive");
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("VS width");
				item.strValue.Format(_T("%d Lines"), specificStatus.vgaComponentStatus.syncInfo.wVSyncLineCount);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Total scan lines");
				item.strValue.Format(_T("%d Lines"), specificStatus.vgaComponentStatus.syncInfo.wFrameLineCount);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Embendded sync");
				GetVideoFPS(specificStatus.hdmiStatus.videoTiming.dwFrameDuration, specificStatus.hdmiStatus.videoTiming.bInterlaced, nBefore, nAfter);
				item.strValue.Format(_T("%d.%d Hz"), nBefore, nAfter);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - Type");
				switch (specificStatus.vgaComponentStatus.videoTiming.dwType)
				{
				case MWCAP_VIDEO_TIMING_NONE:
					item.strValue = "NONE";
					break;
				case MWCAP_VIDEO_TIMING_LEGACY:
					item.strValue = "LEGACY";
					break;
				case MWCAP_VIDEO_TIMING_DMT:
					item.strValue = "DMT";
					break;
				case MWCAP_VIDEO_TIMING_CEA:
					item.strValue = "CEA";
					break;
				case MWCAP_VIDEO_TIMING_GTF:
					item.strValue = "GTF";
					break;
				case MWCAP_VIDEO_TIMING_CVT:
					item.strValue = "CVT";
					break;
				case MWCAP_VIDEO_TIMING_CVT_RB:
					item.strValue = "CVT_RB";
					break;
					//case MWCAP_VIDEO_TIMING_CUSTOM:
					//	strValue = "CUSTOM";
					//	break;
				case MWCAP_VIDEO_TIMING_FAILSAFE:
					item.strValue = "FAILSAFE";
					break;
				}
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - Pixel clock");
				item.strValue.Format(_T("%d Hz"), specificStatus.vgaComponentStatus.videoTiming.dwPixelClock);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - H active");
				item.strValue.Format(_T("%d Pixels"), specificStatus.vgaComponentStatus.videoTiming.wHActive);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - H front porch");
				item.strValue.Format(_T("%d Pixels"), specificStatus.vgaComponentStatus.videoTiming.wHFrontPorch);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - H sync width");
				item.strValue.Format(_T("%d Pixels"), specificStatus.vgaComponentStatus.videoTiming.wHSyncWidth);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - H back proch");
				item.strValue.Format(_T("%d Pixels"), specificStatus.vgaComponentStatus.videoTiming.wHBackPorch);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - V active");
				item.strValue.Format(_T("%d Lines"), specificStatus.vgaComponentStatus.videoTiming.wVActive);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - V front porch");
				item.strValue.Format(_T("%d Lines"), specificStatus.vgaComponentStatus.videoTiming.wVFrontPorch);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - V sync width");
				item.strValue.Format(_T("%d Lines"), specificStatus.vgaComponentStatus.videoTiming.wVSyncWidth);
				m_listListItem.AddTail(item);

				++ nRowIndex;
				item.strKey = _T("Timing - V back proch");
				item.strValue.Format(_T("%d Lines"), specificStatus.vgaComponentStatus.videoTiming.wVBackPorch);
				m_listListItem.AddTail(item);
			}
			else if (specificStatus.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_CVBS) {//composite
				++ nRowIndex;
				item.strKey = _T("TV Standard");
				switch (specificStatus.cvbsYcStatus.standard)
				{
				case MWCAP_SD_VIDEO_NONE:
					item.strValue = _T("NONE");
					break;
				case MWCAP_SD_VIDEO_NTSC_M:
					item.strValue = _T("NTSC-M");
					break;
				case MWCAP_SD_VIDEO_NTSC_433:
					item.strValue = _T("NTSC-433");
					break;
				case MWCAP_SD_VIDEO_PAL_M:
					item.strValue = _T("PAL-M");
					break;
				case MWCAP_SD_VIDEO_PAL_60:
					item.strValue = _T("PAL-60");
					break;
				case MWCAP_SD_VIDEO_PAL_COMBN:
					item.strValue = _T("PAL-C/O/M/B/N");
					break;
				case MWCAP_SD_VIDEO_PAL_BGHID:
					item.strValue = _T("PAL-B/G/H/I/D");
					break;
				case MWCAP_SD_VIDEO_SECAM:
					item.strValue = _T("SECAM");
					break;
				case MWCAP_SD_VIDEO_SECAM_60:
					item.strValue = _T("SECAM-60");
					break;
				}
				m_listListItem.AddTail(item);

				++ nRowIndex;
				if (specificStatus.cvbsYcStatus.b50Hz) {
					item.strKey = _T("Field rate");
					item.strValue.Format(_T("50 Hz"));
					m_listListItem.AddTail(item);
				}
			}
			else if (specificStatus.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_YC) {
			}
		}

		UpdateTable();
	}
}

void CInputDialog::UpdateVideoStatus()
{
	if (m_tabStatus.GetCurSel() != 0)
		return ;

	int nRowIndex = -1;
	LIST_ITEM item;

	MWCAP_VIDEO_SIGNAL_STATUS videoStatus;
	MWGetExtension()->GetVideoSignalStatus(&videoStatus);

	m_listListItem.RemoveAll();
	if (videoStatus.state != MWCAP_VIDEO_SIGNAL_LOCKED) {
		++ nRowIndex;
		item.strKey = _T("Signal status");
		item.strValue.Format(_T("%s"), g_wszSignalStatus[videoStatus.state]);
		m_listListItem.AddTail(item);
		UpdateTable();
	}
	else {
		++ nRowIndex;
		item.strKey = _T("Signal status");
		item.strValue.Format(_T("%s"), g_wszSignalStatus[videoStatus.state]);			
		m_listListItem.AddTail(item);

		++ nRowIndex;
		item.strKey = _T("Resolution");
		int nBefore = 0, nAfter = 0;
		GetVideoFPS(videoStatus.dwFrameDuration, videoStatus.bInterlaced, nBefore, nAfter);
		item.strValue.Format(_T("%dx%d%s, %d.%d Hz"), videoStatus.cx, videoStatus.cy, videoStatus.bInterlaced ? _T("i") : _T("p"),
			nBefore, nAfter);
		m_listListItem.AddTail(item);

		++ nRowIndex;
		item.strKey = _T("Aspect ratio");
		item.strValue.Format(_T("%d:%d"), videoStatus.nAspectX, videoStatus.nAspectY);
		m_listListItem.AddTail(item);

		++ nRowIndex;
		item.strKey = _T("Total scan size");
		item.strValue.Format(_T("%dx%d Pixels"), videoStatus.cxTotal, videoStatus.cyTotal);
		m_listListItem.AddTail(item);

		++ nRowIndex;
		item.strKey = _T("Active area offset");
		item.strValue.Format(_T("X: %d, Y: %d"), videoStatus.x, videoStatus.y);  
		m_listListItem.AddTail(item);

		++ nRowIndex;
		item.strKey = _T("Color format");
		int nFormat = 0;
		switch (videoStatus.colorFormat)
		{
		case MWCAP_VIDEO_COLOR_FORMAT_RGB:
			nFormat = 0;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV601:
			nFormat = 1;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV709:
			nFormat = 2;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV2020:
			nFormat = 3;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV2020C:
			nFormat = 4;
			break;
		}
		item.strValue.Format(_T("%s"), g_wszColorFormat[nFormat]);
		m_listListItem.AddTail(item);

		++ nRowIndex;
		item.strKey = _T("Quantization range");
		nFormat = 0;
		switch (videoStatus.quantRange)
		{
		case MWCAP_VIDEO_QUANTIZATION_UNKNOWN:
			nFormat = 0;
			break;
		case MWCAP_VIDEO_QUANTIZATION_FULL:
			nFormat = 1;
			break;
		case MWCAP_VIDEO_QUANTIZATION_LIMITED:
			nFormat = 2;
			break;
		}
		item.strValue.Format(_T("%s"), g_wszQuantRange[nFormat]);
		m_listListItem.AddTail(item);

		++ nRowIndex;
		item.strKey = _T("Saturation range");
		nFormat = 0;
		switch (videoStatus.satRange)
		{
		case MWCAP_VIDEO_SATURATION_UNKNOWN:
			nFormat = 0;
			break;
		case MWCAP_VIDEO_SATURATION_FULL:
			nFormat = 1;
			break;
		case MWCAP_VIDEO_SATURATION_LIMITED:
			nFormat = 2;
			break;
		case MWCAP_VIDEO_SATURATION_EXTENDED_GAMUT:
			nFormat = 3;
			break;
		}
		item.strValue.Format(_T("%s"), g_wszSatRange[nFormat]);  
		m_listListItem.AddTail(item);

		UpdateTable();
	}
}

void CInputDialog::UpdateAudioStatus()
{
	if (m_tabStatus.GetCurSel() != 1)
		return ;

	int nRowIndex = -1;
	LIST_ITEM item;

	MWCAP_VIDEO_SIGNAL_STATUS videoStatus;
	MWGetExtension()->GetVideoSignalStatus(&videoStatus);

	m_listListItem.RemoveAll();
	if (videoStatus.state != MWCAP_VIDEO_SIGNAL_LOCKED) {
		++ nRowIndex;
		item.strKey = _T("Signal status");
		item.strValue.Format(_T("%s"), g_wszSignalStatus[videoStatus.state]);
		m_listListItem.AddTail(item);
		UpdateTable();
	}
	else {
		MWCAP_AUDIO_SIGNAL_STATUS audioStatus;
		MWGetExtension()->GetAudioSignalStatus(&audioStatus);

		if (audioStatus.wChannelValid > 0) {
			++ nRowIndex;
			item.strKey = _T("Audio format");
			if (audioStatus.bLPCM)
				item.strValue.Format(_T("%d Hz, %d bits, LPCM"), audioStatus.dwSampleRate, audioStatus.cBitsPerSample);
			else
				item.strValue.Format(_T("%d Hz, %d bits"), audioStatus.dwSampleRate, audioStatus.cBitsPerSample);
			m_listListItem.AddTail(item);

			++ nRowIndex;
			item.strKey = _T("Channel 1 & 2");
			if (audioStatus.wChannelValid & 0x0001)
				item.strValue.Format(_T("Preset"));
			else
				item.strValue.Format(_T("Not preset"));
			m_listListItem.AddTail(item);

			++ nRowIndex;
			item.strKey = _T("Channel 3 & 4");
			if (audioStatus.wChannelValid & 0x0002)
				item.strValue.Format(_T("Preset"));
			else
				item.strValue.Format(_T("Not preset"));
			m_listListItem.AddTail(item);  

			++ nRowIndex;
			item.strKey = _T("Channel 5 & 6");
			if (audioStatus.wChannelValid & 0x0004)
				item.strValue.Format(_T("Preset"));
			else
				item.strValue.Format(_T("Not preset"));
			m_listListItem.AddTail(item);  

			++ nRowIndex;
			item.strKey = _T("Channel 7 & 8");
			if (audioStatus.wChannelValid & 0x0008)
				item.strValue.Format(_T("Preset"));
			else
				item.strValue.Format(_T("Not preset")); 
			m_listListItem.AddTail(item); 

			char abyData [1024] = {0};
			char* pMove = abyData;

			int nMove;
			BYTE byValue;
			for (int j = 0; j < 24; ++ j) {
				nMove = 2;
				byValue = audioStatus.channelStatus.abyData[j];
				if (byValue < 16) {
					*pMove++ = '0';
					nMove = 1;
				}

				itoa (byValue, pMove, 16);
				pMove += nMove;
				*pMove++ = ' ';
			}

			++ nRowIndex;
			item.strKey = _T("Channel status data");
			item.strValue.Format(_T("%S"), abyData);
			m_listListItem.AddTail(item);
		}
		else {
			++ nRowIndex;
			item.strKey = _T("Signal status");
			item.strValue.Format(_T("Not present"));
			m_listListItem.AddTail(item); 
		}
		UpdateTable();
	}
}

void CInputDialog::OnInputStatuChanged( ULONGLONG ullStatusBits )
{
	if ((ullStatusBits & MWCAP_NOTIFY_INPUT_SPECIFIC_CHANGE) != 0) {
		UpdateSpecificStatus();
	}

	if ((ullStatusBits & MWCAP_NOTIFY_VIDEO_SIGNAL_CHANGE) != 0) {
		UpdateVideoStatus();
	}

	if ((ullStatusBits & MWCAP_NOTIFY_AUDIO_SIGNAL_CHANGE) != 0) {
		UpdateAudioStatus();
	}
}

void CInputDialog::OnInputSourceChanged( ULONGLONG ullStatusBits )
{
	if ((ullStatusBits & MWCAP_NOTIFY_VIDEO_INPUT_SOURCE_CHANGE) != 0
		|| (ullStatusBits & MWCAP_NOTIFY_AUDIO_INPUT_SOURCE_CHANGE) != 0
		|| (ullStatusBits & MWCAP_NOTIFY_INPUT_SORUCE_SCAN_CHANGE) != 0) {
		InputSourceChange();
	}
}

void CInputDialog::SetAVInputType()
{
	m_cmbVideoInput.ResetContent();
	m_cmbAudioInput.ResetContent();
	m_cmbVideoInput.Clear();
	m_cmbAudioInput.Clear();

	DWORD dwVideoSourceCount = 0;
	MWGetExtension()->GetVideoInputSourceCount(&dwVideoSourceCount);

	DWORD szdwVideoInputSources[8];
	DWORD cVideoBufferItems = 8;
	MWGetExtension()->GetVideoInputSourceArray(szdwVideoInputSources, &cVideoBufferItems);
	DWORD dwSelVideoInputSource = 0;
	MWGetExtension()->GetVideoInputSource(&dwSelVideoInputSource);
	TCHAR wszType[MAX_PATH] = {0};
	int nSel = -1;
	CString strOut;
	int nIndex;
	for (DWORD i = 0; i < dwVideoSourceCount; ++ i){
		nIndex = -1;
		if (dwSelVideoInputSource == szdwVideoInputSources[i])
			nSel = i;

		GetVideoType(m_szVideoInputType[i], wszType, szdwVideoInputSources[i], nIndex);
		strOut.Format(_T("%s input %d"), wszType, nIndex);
		m_cmbVideoInput.InsertString(i, strOut);
	}
	m_cmbVideoInput.SetCurSel(nSel);

	DWORD dwType = INPUT_SOURCE(MWCAP_VIDEO_INPUT_TYPE_HDMI, 0);

	DWORD dwAudioSourceCount = 0;
	MWGetExtension()->GetAudioInputSourceCount(&dwAudioSourceCount);

	DWORD szdwAudioInputSources[8];
	DWORD cAudioBufferItems = 8;
	MWGetExtension()->GetAudioInputSourceArray(szdwAudioInputSources, &cAudioBufferItems);

	DWORD dwSelAudioInputSource = 0;
	MWGetExtension()->GetAudioInputSource(&dwSelAudioInputSource);
	nSel = -1;
	for (DWORD i = 0; i < dwAudioSourceCount; ++ i){
		nIndex = -1;
		if (dwSelAudioInputSource == szdwAudioInputSources[i])
			nSel = i;

		GetAudioType(m_szAudioInputType[i], wszType, szdwAudioInputSources[i], nIndex);
		strOut.Format(_T("%s input %d"), wszType, nIndex);
		m_cmbAudioInput.InsertString(i, strOut);
	}
	m_cmbAudioInput.SetCurSel(nSel);

	BOOLEAN bAutoScan = 0;
	MWGetExtension()->GetInputSourceScan(&bAutoScan);
	m_ckbAutoScan.SetCheck(bAutoScan);
	m_cmbVideoInput.EnableWindow(!bAutoScan);

	BOOLEAN bLinkVideo = 0;
	MWGetExtension()->GetAVInputSourceLink(&bLinkVideo);
	m_ckbLinkVideo.SetCheck(bLinkVideo);
	m_cmbAudioInput.EnableWindow(!bLinkVideo);
}
