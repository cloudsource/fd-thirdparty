#pragma once
#include "DShowCapture.h"

// CInputDialog dialog

class CInputDialog : public CDialogEx, public ISignalNotify
{
	DECLARE_DYNAMIC(CInputDialog)

public:
	CInputDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInputDialog();

	// Dialog Data
	enum { IDD = IDD_DIALOG_INPUT };

public:
	//ISignalNotify
	virtual void OnInputStatuChanged(ULONGLONG ullStatusBits);;
	virtual void OnInputSourceChanged(ULONGLONG ullStatusBits);

	void InputSourceChange();
	
	void SetAVInputType();

private:
	typedef struct _LIST_ITEM {
		CString strKey;
		CString strValue;
	} LIST_ITEM;
	CList<LIST_ITEM>	m_listListItem;
	CComboBox		m_cmbVideoInput;
	CComboBox		m_cmbAudioInput;
	CButton			m_ckbAutoScan;
	CButton			m_ckbLinkVideo;
	CTabCtrl		m_tabStatus;
	CListCtrl		m_listStatus;

	MWCAP_VIDEO_INPUT_TYPE m_szVideoInputType[8];
	MWCAP_AUDIO_INPUT_TYPE m_szAudioInputType[8];

	void UpdateSpecificStatus();
	void UpdateVideoStatus();
	void UpdateAudioStatus();

	void GetVideoType(MWCAP_VIDEO_INPUT_TYPE& videoInputType, TCHAR* pszType, int nTypeValue, int& nIndex);
	void GetAudioType(MWCAP_AUDIO_INPUT_TYPE& audioInputType, TCHAR* pszType, int nTypeValue, int& nIndex);
	
	void UpdateTable() {
		int nLen = m_listListItem.GetSize();
		LIST_ITEM listitem;
		POSITION pos = m_listListItem.GetHeadPosition();				
		m_listStatus.DeleteAllItems();
		for (int i = 0; i < m_listListItem.GetCount(); ++ i) {
			listitem = m_listListItem.GetNext(pos);
			m_listStatus.InsertItem(i, listitem.strKey);  
			m_listStatus.SetItemText(i, 1, listitem.strValue); 
		}
	}

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnTcnSelchangeTabInputStatus(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCbnSelchangeComboInputVideo();
	afx_msg void OnCbnSelchangeComboInputAudio();
	afx_msg void OnBnClickedCheckInputAutoScan();
	afx_msg void OnBnClickedCheckInputLinkVideo();

};
