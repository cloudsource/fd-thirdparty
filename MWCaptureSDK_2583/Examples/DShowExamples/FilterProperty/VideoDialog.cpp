// VideoDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "FilterProperty.h"
#include "VideoDialog.h"
#include "afxdialogex.h"

// CVideoDialog 对话框

IMPLEMENT_DYNAMIC(CVideoDialog, CDialogEx)

CVideoDialog::CVideoDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CVideoDialog::IDD, pParent)
{

}

CVideoDialog::~CVideoDialog()
{
}

void CVideoDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_VIDEOPIN, m_tabVideoPin);

	DDX_Control(pDX, IDC_CHECK_ASPECT, m_ckbAspect);
	DDX_Control(pDX, IDC_SPIN_INPUT_ASPECT_W, m_spinAspectX);
	DDX_Control(pDX, IDC_SPIN_INPUT_ASPECT_H, m_spinAspectY);
	DDX_Control(pDX, IDC_CHECK_FORMAT, m_ckbColorFormat);
	DDX_Control(pDX, IDC_CHECK_QUANT, m_ckbQuantization);
	DDX_Control(pDX, IDC_COMBO_FORMAT, m_cmbColorFormat);
	DDX_Control(pDX, IDC_COMBO_QUANT, m_cmbQuantization);
	DDX_Control(pDX, IDC_GROUP_INPUT_SIGNAL, m_groupInputSignal);
}


BEGIN_MESSAGE_MAP(CVideoDialog, CDialogEx)
	ON_BN_CLICKED(IDC_CHECK_ASPECT, &CVideoDialog::OnBnClickedAspect)
	ON_BN_CLICKED(IDC_CHECK_FORMAT, &CVideoDialog::OnBnClickedColorFormat)
	ON_BN_CLICKED(IDC_CHECK_QUANT, &CVideoDialog::OnBnClickedQuant)
	ON_CBN_SELCHANGE(IDC_COMBO_FORMAT, &CVideoDialog::OnCbnSelchangeComboFormat)
	ON_CBN_SELCHANGE(IDC_COMBO_QUANT, &CVideoDialog::OnCbnSelchangeComboQuant)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_INPUT_ASPECT_W, &CVideoDialog::OnDeltaposSpinInputAspectW)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_INPUT_ASPECT_H, &CVideoDialog::OnDeltaposSpinInputAspectH)
	ON_WM_CREATE()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_VIDEOPIN, &CVideoDialog::OnTcnSelchangeTabVideopin)
END_MESSAGE_MAP()


// CVideoDialog 消息处理程序


BOOL CVideoDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	m_tabVideoPin.InsertItem(0, _T("Capture"));
	m_tabVideoPin.InsertItem(1, _T("Preview"));

	m_cmbColorFormat.InsertString(0, L"RGB");
	m_cmbColorFormat.InsertString(1, L"YUV BT.601");
	m_cmbColorFormat.InsertString(2, L"YUV BT.709");
	m_cmbColorFormat.InsertString(3, L"YUV BT.2020");
	m_cmbColorFormat.SetCurSel(0);

	m_cmbQuantization.InsertString(0, L"Full range");
	m_cmbQuantization.InsertString(1, L"Limited range");
	m_cmbQuantization.SetCurSel(0);

	SetVideoInputStatus();

	m_dlgVideoOutput.Create(CVideoOutputDialog::IDD, this);

	CRect rcBorder;
	CWnd *pBorder = GetDlgItem(IDC_STATIC_VIDEO_DLG);
	pBorder->GetWindowRect(rcBorder);
	ScreenToClient(rcBorder);

	m_dlgVideoOutput.SetWindowPos(pBorder, rcBorder.left, rcBorder.top, rcBorder.Width(), rcBorder.Height(), SWP_NOREDRAW);

	m_dlgVideoOutput.ShowWindow(SW_SHOW);

	m_tabVideoPin.SetCurSel(1);

	g_capture.AddNotify(this);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


void CVideoDialog::OnBnClickedAspect()
{
	// TODO: Add your control notification handler code here
	BOOL bCheck = m_ckbAspect.GetCheck();
	MWCAP_VIDEO_SIGNAL_STATUS videoSignalStatus;
	HRESULT hr = MWGetExtension()->GetVideoSignalStatus(&videoSignalStatus);
	if (!bCheck) {
		MWCAP_VIDEO_ASPECT_RATIO videoAspectRatio = {0, 0};
		hr = MWGetExtension()->SetVideoInputAspectRatio(&videoAspectRatio);
		m_spinAspectX.SetPos(videoSignalStatus.nAspectX);
		m_spinAspectY.SetPos(videoSignalStatus.nAspectY);
	}
	else {
		MWCAP_VIDEO_ASPECT_RATIO videoAspectRatio = {videoSignalStatus.nAspectX, videoSignalStatus.nAspectY};
		hr = MWGetExtension()->SetVideoInputAspectRatio(&videoAspectRatio);
		m_spinAspectX.SetPos(videoAspectRatio.nAspectX);
		m_spinAspectY.SetPos(videoAspectRatio.nAspectY);
	}
	m_spinAspectX.EnableWindow(bCheck);
	m_spinAspectY.EnableWindow(bCheck);

}


void CVideoDialog::OnBnClickedColorFormat()
{
	// TODO: Add your control notification handler code here
	BOOL bCheck = m_ckbColorFormat.GetCheck();
	if (!bCheck) {
		MWCAP_VIDEO_SIGNAL_STATUS videoSignalStatus;
		HRESULT hr = MWGetExtension()->GetVideoSignalStatus(&videoSignalStatus);

		hr = MWGetExtension()->SetVideoInputColorFormat(MWCAP_VIDEO_COLOR_FORMAT_UNKNOWN);

		int nSel = 0;
		switch (videoSignalStatus.colorFormat)
		{
		case MWCAP_VIDEO_COLOR_FORMAT_RGB:
			nSel = 0;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV601:
			nSel = 1;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV709:
			nSel = 2;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV2020:
			nSel = 3;
			break;
		default:
			break;
		}
		m_cmbColorFormat.SetCurSel(nSel);
	}
	else {
		MWGetExtension()->SetVideoInputColorFormat(MWCAP_VIDEO_COLOR_FORMAT(m_cmbColorFormat.GetCurSel() + 1));
	}
	m_cmbColorFormat.EnableWindow(bCheck);
}


void CVideoDialog::OnBnClickedQuant()
{
	// TODO: Add your control notification handler code here
	BOOL bCheck = m_ckbQuantization.GetCheck();
	if (!bCheck) {
		MWCAP_VIDEO_SIGNAL_STATUS videoSignalStatus;
		HRESULT hr = MWGetExtension()->GetVideoSignalStatus(&videoSignalStatus);

		hr = MWGetExtension()->SetVideoInputQuantizationRange(MWCAP_VIDEO_QUANTIZATION_UNKNOWN);

		int nSel = 0;
		switch (videoSignalStatus.quantRange)
		{
		case MWCAP_VIDEO_QUANTIZATION_FULL:
			nSel = 0;
			break;
		case MWCAP_VIDEO_QUANTIZATION_LIMITED:
			nSel = 1;
			break;
		default:
			break;
		}
		m_cmbQuantization.SetCurSel(nSel);
	}
	else {
		MWGetExtension()->SetVideoInputQuantizationRange(MWCAP_VIDEO_QUANTIZATION_RANGE(m_cmbQuantization.GetCurSel() + 1));
	}

	m_cmbQuantization.EnableWindow(bCheck);
}


void CVideoDialog::OnCbnSelchangeComboFormat()
{
	// TODO: Add your control notification handler code here
	int nSel = m_cmbColorFormat.GetCurSel();
	MWCAP_VIDEO_COLOR_FORMAT videoColorFormat;
	switch (nSel)
	{
	case 0:
		videoColorFormat = MWCAP_VIDEO_COLOR_FORMAT_RGB;
		break;
	case 1:
		videoColorFormat = MWCAP_VIDEO_COLOR_FORMAT_YUV601;
		break;
	case 2:
		videoColorFormat = MWCAP_VIDEO_COLOR_FORMAT_YUV709;
		break;
	case 3:
		videoColorFormat = MWCAP_VIDEO_COLOR_FORMAT_YUV2020;
		break;
	default:
		break;
	}

	MWGetExtension()->SetVideoInputColorFormat(videoColorFormat);
}


void CVideoDialog::OnCbnSelchangeComboQuant()
{
	// TODO: Add your control notification handler code here
	int nSel = m_cmbQuantization.GetCurSel();
	MWCAP_VIDEO_QUANTIZATION_RANGE quantRange;
	switch (nSel)
	{
	case 0:
		quantRange = MWCAP_VIDEO_QUANTIZATION_FULL;
		break;
	case 1:
		quantRange = MWCAP_VIDEO_QUANTIZATION_LIMITED;
		break;
	default:
		break;
	}

	MWGetExtension()->SetVideoInputQuantizationRange(quantRange);
}

void CVideoDialog::OnDeltaposSpinInputAspectW(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	MWCAP_VIDEO_ASPECT_RATIO videoAspectRatio = {0};
	HRESULT hr = MWGetExtension()->GetVideoInputAspectRatio(&videoAspectRatio);
	videoAspectRatio.nAspectX = pNMUpDown->iPos + pNMUpDown->iDelta;
	
	hr = MWGetExtension()->SetVideoInputAspectRatio(&videoAspectRatio);
}


void CVideoDialog::OnDeltaposSpinInputAspectH(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	MWCAP_VIDEO_ASPECT_RATIO videoAspectRatio = {0};
	HRESULT hr = MWGetExtension()->GetVideoInputAspectRatio(&videoAspectRatio);
	videoAspectRatio.nAspectY = pNMUpDown->iPos + pNMUpDown->iDelta;

	hr = MWGetExtension()->SetVideoInputAspectRatio(&videoAspectRatio);
}


void CVideoDialog::OnTcnSelchangeTabVideopin(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;

	BOOL bCapture = m_tabVideoPin.GetCurSel() == 0 ? TRUE : FALSE;
	g_capture.RenderVideo(bCapture, -1);

	m_dlgVideoOutput.UpdateVideoInfo(bCapture);
}

void CVideoDialog::UpdateStatus()
{
	m_dlgVideoOutput.UpdateVideoInfo(-1);
}

void CVideoDialog::OnInputSourceChanged( ULONGLONG ullStatusBits )
{
	if ((ullStatusBits & MWCAP_NOTIFY_VIDEO_INPUT_SOURCE_CHANGE) != 0 || (ullStatusBits & MWCAP_NOTIFY_VIDEO_SIGNAL_CHANGE) != 0) {
		SetVideoInputStatus();
	}
}

void CVideoDialog::SetVideoInputStatus()
{
	MWCAP_VIDEO_SIGNAL_STATUS videoSignalStatus;
	HRESULT hr = MWGetExtension()->GetVideoSignalStatus(&videoSignalStatus);

	CString strSignal = _T("");
	if (videoSignalStatus.state == MWCAP_VIDEO_SIGNAL_NONE)
		strSignal.Format(_T("Input - (No Signal)"));
	else {
		if (videoSignalStatus.bInterlaced)
			strSignal.Format(_T("Input - (%dx%di)"), videoSignalStatus.cx, videoSignalStatus.cy);
		else
			strSignal.Format(_T("Input - (%dx%dp)"), videoSignalStatus.cx, videoSignalStatus.cy);
	}
	m_groupInputSignal.SetWindowText(strSignal);

	MWCAP_VIDEO_COLOR_FORMAT colorFormat;
	hr = MWGetExtension()->GetVideoInputColorFormat(&colorFormat);
	BOOL bCheck = TRUE;
	int nSel = 0;
	switch (colorFormat)
	{
	case MWCAP_VIDEO_COLOR_FORMAT_UNKNOWN:
		bCheck = FALSE;
		switch (videoSignalStatus.colorFormat)
		{
		case MWCAP_VIDEO_COLOR_FORMAT_RGB:
			nSel = 0;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV601:
			nSel = 1;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV709:
			nSel = 2;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV2020:
			nSel = 3;
			break;
		default:
			break;
		}
		break;
	case MWCAP_VIDEO_COLOR_FORMAT_RGB:
		nSel = 0;
		break;
	case MWCAP_VIDEO_COLOR_FORMAT_YUV601:
		nSel = 1;
		break;
	case MWCAP_VIDEO_COLOR_FORMAT_YUV709:
		nSel = 2;
		break;
	case MWCAP_VIDEO_COLOR_FORMAT_YUV2020:
		nSel = 3;
		break;
	default:
		break;
	}
	m_ckbColorFormat.SetCheck(bCheck);
	m_cmbColorFormat.EnableWindow(bCheck);
	m_cmbColorFormat.SetCurSel(nSel);

	MWCAP_VIDEO_QUANTIZATION_RANGE quantRange;
	hr = MWGetExtension()->GetVideoInputQuantizationRange(&quantRange);

	bCheck = TRUE;
	nSel = 0;
	switch (quantRange)
	{
	case MWCAP_VIDEO_QUANTIZATION_UNKNOWN:
		bCheck = FALSE;
		switch(videoSignalStatus.quantRange)
		{
		case MWCAP_VIDEO_QUANTIZATION_FULL:
			nSel = 0;
			break;
		case MWCAP_VIDEO_QUANTIZATION_LIMITED: 
			nSel = 1;
			break;
		default:
			break;
		}
		break;
	case MWCAP_VIDEO_QUANTIZATION_FULL:
		nSel = 0;
		break;
	case MWCAP_VIDEO_QUANTIZATION_LIMITED:
		nSel = 1;
		break;
	default:
		break;
	}
	m_ckbQuantization.SetCheck(bCheck);
	m_cmbQuantization.SetCurSel(nSel);
	m_cmbQuantization.EnableWindow(bCheck);

	MWCAP_VIDEO_ASPECT_RATIO videoAspectRatio = {0};
	hr = MWGetExtension()->GetVideoInputAspectRatio(&videoAspectRatio);

	int nAspectX = videoAspectRatio.nAspectX, nAspectY = videoAspectRatio.nAspectY;

	bCheck = TRUE;
	if (nAspectX == 0 && nAspectY == 0) {
		nAspectX = videoSignalStatus.nAspectX;
		nAspectY = videoSignalStatus.nAspectY;
		bCheck = FALSE;
	}
	m_ckbAspect.SetCheck(bCheck);
	MWCAP_VIDEO_CAPS vCaps;
	hr = MWGetExtension()->GetVideoCaps(&vCaps);
	if (hr == S_OK) {
		m_spinAspectX.SetRange(1, vCaps.wMaxInputWidth);
		m_spinAspectY.SetRange(1, vCaps.wMaxInputHeight);
	}
	else {
		m_spinAspectX.SetRange(1, 4096);
		m_spinAspectY.SetRange(1, 4096);
	}
	m_spinAspectX.SetPos(nAspectX);
	m_spinAspectY.SetPos(nAspectY);
	m_spinAspectX.EnableWindow(bCheck);
	m_spinAspectY.EnableWindow(bCheck);
}
