#pragma once

#include "OSDTabDialog.h"

// COSDDialog dialog

class COSDDialog : public CDialogEx
{
	DECLARE_DYNAMIC(COSDDialog)

public:
	COSDDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~COSDDialog();

// Dialog Data
	enum { IDD = IDD_OSD_DIALOG };

public:
	void ChannelChanged();

private:
	CTabCtrl		m_tabOSD;

	COSDTabDialog	m_dlgTabShow;

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTcnSelchangeTabOsd(NMHDR *pNMHDR, LRESULT *pResult);
};
