

#pragma once

#include <tchar.h>
#include <vector>

#include <dshow.h>
#include <ks.h>
#include <initguid.h>
#include <Dshowasf.h>

#pragma comment(lib, "strmiids.lib")
#pragma comment(lib, "dxguid.lib")

#define ABS(x) (((x) > 0) ? (x) : -(x))

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(x) { if (x != NULL) x->Release(); x = NULL; }
#endif

#define BREAK_IF_FAILED(x) {if (FAILED(x)) break;}

#include "StringUtils.h"
#include "LibMWCapture/MWCaptureExtensionIntf.h"

// 3A45B930-FF5C-42AA-A5A2-43771B93EBEF

DEFINE_GUID(IID_IMWCaptureExtension, 0x3A45B930, 0xFF5C, 0x42AA, 0xa5, 0xa2, 0x43, 0x77, 0x1b, 0x93, 0xeb, 0xef);

#define EVENT_COUNT 8

class ISignalNotify
{
public:
	virtual void OnTimingStatuChanged(ULONGLONG ullStatusBits) {};
	virtual void OnHDMIInfoStatuChanged(ULONGLONG ullStatusBits) {};
	virtual void OnInputStatuChanged(ULONGLONG ullStatusBits) {};
	virtual void OnInputSourceChanged(ULONGLONG ullStatusBits) {};
};

typedef std::vector<ISignalNotify*> CNotifyArray;

class CDShowCapture
{
public:
	CDShowCapture() {
		m_pExtension = NULL;

		m_pVideoCapture = NULL;
		m_pVideoMoniker = NULL;

		m_pAudioMoniker = NULL;
		for (int i = 0; i < EVENT_COUNT; ++ i) {
			m_arrpNotifyEvent[i] = NULL;
			m_arrhEvent[i] = NULL;
		}
		m_nEventCount = 0;
		m_hThread = NULL;

		m_bCapture = FALSE;
	}
	virtual ~CDShowCapture() {
	}

	void CreateEventNotify() {
		m_arrhEvent[0] = CreateEvent(NULL, FALSE, FALSE, NULL);
		m_pExtension->RegisterNotify(
			m_arrhEvent[0],
			MWCAP_NOTIFY_INPUT_SPECIFIC_CHANGE
			| MWCAP_NOTIFY_VIDEO_SAMPLING_PHASE_CHANGE,
			m_arrpNotifyEvent[0]
			);

		m_arrhEvent[1] = CreateEvent(NULL, FALSE, FALSE, NULL);
		m_pExtension->RegisterNotify(
			m_arrhEvent[1],
			MWCAP_NOTIFY_HDMI_INFOFRAME_AVI |
			MWCAP_NOTIFY_HDMI_INFOFRAME_AUDIO |
			MWCAP_NOTIFY_HDMI_INFOFRAME_SPD |
			MWCAP_NOTIFY_HDMI_INFOFRAME_MS |
			MWCAP_NOTIFY_HDMI_INFOFRAME_VS |
			MWCAP_NOTIFY_HDMI_INFOFRAME_ACP |
			MWCAP_NOTIFY_HDMI_INFOFRAME_ISRC1 |
			MWCAP_NOTIFY_HDMI_INFOFRAME_ISRC2 |
			MWCAP_NOTIFY_HDMI_INFOFRAME_GAMUT,
			m_arrpNotifyEvent[1]
			);

		m_arrhEvent[2] = CreateEvent(NULL, FALSE, FALSE, NULL);
		m_pExtension->RegisterNotify(
			m_arrhEvent[2],
			MWCAP_NOTIFY_INPUT_SPECIFIC_CHANGE |
			MWCAP_NOTIFY_VIDEO_SIGNAL_CHANGE |
			MWCAP_NOTIFY_AUDIO_SIGNAL_CHANGE,
			m_arrpNotifyEvent[2]
		);

		m_arrhEvent[3] = CreateEvent(NULL, FALSE, FALSE, NULL);
		m_pExtension->RegisterNotify(
			m_arrhEvent[3],
			MWCAP_NOTIFY_VIDEO_INPUT_SOURCE_CHANGE |
			MWCAP_NOTIFY_VIDEO_SIGNAL_CHANGE |
			MWCAP_NOTIFY_AUDIO_INPUT_SOURCE_CHANGE |
			MWCAP_NOTIFY_INPUT_SORUCE_SCAN_CHANGE,
			m_arrpNotifyEvent[3]
		);
		m_nEventCount = 4;
		m_hThread = CreateThread(NULL, 0, ThreadProcEventNotify, this, 0, 0);
	}

	static DWORD WINAPI ThreadProcEventNotify(LPVOID lpvParam) {
		CDShowCapture * pThread = (CDShowCapture *)lpvParam;
		return pThread->ThreadProcEventNotifyProc();
	}
	
	DWORD ThreadProcEventNotifyProc() {		
		while (TRUE) {
			DWORD dwRet = WaitForMultipleObjects(m_nEventCount, m_arrhEvent, FALSE, 100);
			if (dwRet == WAIT_TIMEOUT) {
				continue;
			}  
			else {
				int nEvent = dwRet - WAIT_OBJECT_0;
				ULONGLONG ullStatusBits;
				m_pExtension->GetNotifyStatus(m_arrpNotifyEvent[nEvent], ullStatusBits);
				
				switch (nEvent)
				{
				case 0:
					for (UINT i = 0; i < m_arrNotify.size(); ++ i) {
						m_arrNotify[i]->OnTimingStatuChanged(ullStatusBits);
					}
					break;
				case 1:
					for (UINT i = 0; i < m_arrNotify.size(); ++ i) {
						m_arrNotify[i]->OnHDMIInfoStatuChanged(ullStatusBits);
					}
					break;
				case 2:
					for (UINT i = 0; i < m_arrNotify.size(); ++ i) {
						m_arrNotify[i]->OnInputStatuChanged(ullStatusBits);
						m_arrNotify[i]->OnHDMIInfoStatuChanged(0xFFFF);
					}
					break;
				case 3:
					for (UINT i = 0; i < m_arrNotify.size(); ++ i) {
						m_arrNotify[i]->OnInputSourceChanged(ullStatusBits);
						m_arrNotify[i]->OnHDMIInfoStatuChanged(0xFFFF);
					}
					break;
				}
			}
		}

		return 0;
	}

public:
	BOOL Init() {
		CoInitialize(NULL);

		if (!EnumDevice(TRUE) || !EnumDevice(FALSE))
			return FALSE;

		return TRUE;
	}
	void Exit() {
		Stop();
		ReleaseRenderFilter();
		Close();

		m_lstVideoDevice.clear();
		m_lstAudioDevice.clear();

		CoUninitialize();
	}
	void SetWndPreview(CWnd* wndPreview) {
		m_wndPreview = wndPreview;
	}

	int GetDeviceCount(BOOL bVideo) {
		return (bVideo ? (int)m_lstVideoDevice.size() : (int)m_lstAudioDevice.size());
	}
	const TCHAR* GetDeviceName(BOOL bVideo, int nIndex) {
		return (bVideo ? m_lstVideoDevice[nIndex].szName : m_lstAudioDevice[nIndex].szName);
	}
	const TCHAR* GetDevicePath(BOOL bVideo, int nIndex) {
		return (bVideo ? m_lstVideoDevice[nIndex].szPath : m_lstAudioDevice[nIndex].szPath);
	}

	BOOL Open(int nVideoIndex) {
		Close();

		if (!EnumDevice(TRUE, GetDevicePath(TRUE, nVideoIndex)))
			return FALSE;

		HRESULT hr;
		do {
			hr = m_pVideoMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**)&m_pVideoCapture);
			BREAK_IF_FAILED(hr);

			hr = m_pVideoCapture->QueryInterface(IID_IMWCaptureExtension, (void**)&m_pExtension);
			BREAK_IF_FAILED(hr);

		} while (FALSE);

		RenderVideo(m_bCapture, nVideoIndex);

		CreateEventNotify();

		return TRUE;
	}
	void Close() {
		SAFE_RELEASE(m_pExtension);
		SAFE_RELEASE(m_pVideoCapture);
		SAFE_RELEASE(m_pVideoMoniker);

		if (m_hThread) {
			TerminateThread(m_hThread, 0);
			CloseHandle(m_hThread);
			m_hThread = NULL;

			for (int i = 0; i < EVENT_COUNT; ++ i) {
				if (m_arrhEvent[i]) {
					CloseHandle(m_arrhEvent[i]);
					m_arrhEvent[i] = NULL;
				}
			}
		}
	}

	HRESULT SetProcAmpPropertyValue(VideoProcAmpProperty ampProperty, int nPos) {
		return m_pVideoProcAmp->Set(ampProperty, nPos, 2);
	}

	HRESULT GetProcAmpPropertyValue(VideoProcAmpProperty ampProperty, long* plValue) {
		long lFlags = 0;
		return m_pVideoProcAmp->Get(ampProperty, plValue, &lFlags);
	}

	HRESULT GetProcAmpPropertyRange(VideoProcAmpProperty ampProperty, long* lMin, long* lMax, long* lStep, long* lDefault) {
		long lFlags = 0;
		return m_pVideoProcAmp->GetRange(ampProperty, lMin, lMax, lStep, lDefault, &lFlags);
	}

	BOOL RenderVideo(BOOL bCapture, int nIndex);
	void ReleaseRenderFilter();
	BOOL Run();
	void Stop();

	void AddNotify(ISignalNotify* pNotify) {
		m_arrNotify.push_back(pNotify);
	}
	void DelNotify(ISignalNotify* pNotify) {
	}

	IMWCaptureExtension* GetExtension() {
		return m_pExtension;
	}
protected:
	BOOL EnumDevice(BOOL bVideo, const TCHAR* pszName = NULL);

protected:
	IMWCaptureExtension*	m_pExtension;

	IBaseFilter*			m_pVideoCapture;

    IMoniker*				m_pVideoMoniker;
	TCHAR					m_szVideoName[256];

    IMoniker*				m_pAudioMoniker;
	TCHAR					m_szAudioName[256];

	ICaptureGraphBuilder2*	m_pBuild;

	IGraphBuilder*			m_pGraph;
	IAMStreamConfig*		m_pVideoConfig;

	IVideoWindow*			m_pVideoWnd;
	IBaseFilter*			m_pRender;

	IPin*					m_pVideoPin;
	IAMVideoProcAmp*		m_pVideoProcAmp;

	// device list
	typedef struct tagDEVICE_INFO {
		TCHAR szName[MAX_PATH];
		TCHAR szPath[MAX_PATH];
	} DEVICE_INFO, *PDEVICE_INFO;

	typedef std::vector<DEVICE_INFO> CDeviceList;

	CDeviceList			m_lstVideoDevice;
	CDeviceList			m_lstAudioDevice;

	CNotifyArray		m_arrNotify;

	MWCAP_PTR64			m_arrpNotifyEvent[EVENT_COUNT];
	HANDLE				m_arrhEvent[EVENT_COUNT];
	int					m_nEventCount;

	HANDLE				m_hThread;

	BOOL				m_bCapture;
	CWnd*				m_wndPreview;

	int					m_nVideoIndex;
};

extern CDShowCapture g_capture;

extern IMWCaptureExtension* MWGetExtension();
