
// stdafx.cpp : source file that includes just the standard includes
// FilterProperty.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

void GetVideoFPS( DWORD dwFrameDuration, BOOL bInterlaced, int& nBefore, int& nAfter )
{
	int nFieldRateX100 = 0;
	if (dwFrameDuration != 0)
		nFieldRateX100 = (int)(1000000000LL / dwFrameDuration);
	if (bInterlaced)
		nFieldRateX100 <<= 1;

	nBefore = nFieldRateX100 / 100;
	nAfter = nFieldRateX100 % 100;
}
