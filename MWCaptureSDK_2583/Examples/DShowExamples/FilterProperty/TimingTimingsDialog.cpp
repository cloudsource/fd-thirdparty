// TimingTimingsDialog.cpp : implementation file
//

#include "stdafx.h"
#include "FilterProperty.h"
#include "TimingTimingsDialog.h"
#include "afxdialogex.h"

#include "DShowCapture.h"

// CTimingTimingsDialog dialog

IMPLEMENT_DYNAMIC(CTimingTimingsDialog, CDialogEx)

CTimingTimingsDialog::CTimingTimingsDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTimingTimingsDialog::IDD, pParent)
{

}

CTimingTimingsDialog::~CTimingTimingsDialog()
{
}

void CTimingTimingsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_TIMING_TIMINGS, m_listTimings);
	DDX_Control(pDX, IDC_BUTTON_TIMING_TIMINGS_DELETE, m_btnDelete);
}


BEGIN_MESSAGE_MAP(CTimingTimingsDialog, CDialogEx)
	ON_NOTIFY(NM_CLICK, IDC_LIST_TIMING_TIMINGS, &CTimingTimingsDialog::OnNMClickListTimingTimings)
	ON_NOTIFY(NM_KILLFOCUS, IDC_LIST_TIMING_TIMINGS, &CTimingTimingsDialog::OnNMKillfocusListTimingTimings)
	ON_BN_CLICKED(IDC_BUTTON_TIMING_TIMINGS_DELETE, &CTimingTimingsDialog::OnBnClickedButtonTimingTimingsDelete)
END_MESSAGE_MAP()


// CTimingTimingsDialog message handlers
BOOL CTimingTimingsDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_listTimings.ModifyStyle(0, LVS_REPORT);
	m_listTimings.SetExtendedStyle(m_listTimings.GetExtendedStyle() | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);  

	m_listTimings.InsertColumn(0, _T("Mode"));  
	m_listTimings.InsertColumn(1, _T("Total scan"));  
	m_listTimings.InsertColumn(2, _T("Sync signal"));  

	CRect rect;  
	m_listTimings.GetClientRect(rect);
	m_listTimings.SetColumnWidth(0, rect.Width() * 2 / 5); 
	m_listTimings.SetColumnWidth(1, rect.Width() * 3 / 10);  
	m_listTimings.SetColumnWidth(2, rect.Width() * 3 / 10);  

	UpdateTableTiming();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CTimingTimingsDialog::OnNMClickListTimingTimings(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	m_nSelTableTiming = pNMItemActivate->iItem;

	BOOL bEnable = m_nSelTableTiming != -1 ? TRUE : FALSE;
	m_btnDelete.EnableWindow(bEnable);
}


void CTimingTimingsDialog::OnNMKillfocusListTimingTimings(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;

	HWND hwnd = GetFocus()->m_hWnd;
	if (hwnd == m_btnDelete.GetSafeHwnd()) {

	}
	else {
		m_nSelTableTiming = -1;
		m_btnDelete.EnableWindow(FALSE);
	}
}


void CTimingTimingsDialog::OnBnClickedButtonTimingTimingsDelete()
{
	// TODO: Add your control notification handler code here

	DWORD dwTimingCount = 0;
	HRESULT hr = MWGetExtension()->GetCustomVideoTimingsCount(&dwTimingCount);

	DWORD dwBufferSize = 32;
	MWCAP_VIDEO_CUSTOM_TIMING szVideoTiming[32];
	hr = MWGetExtension()->GetCustomVideoTimingsArray(szVideoTiming, &dwBufferSize);

	for (UINT i = m_nSelTableTiming; i < dwTimingCount; ++ i) {
		szVideoTiming[i] = szVideoTiming[i + 1];
	}
	-- dwTimingCount;

	hr = MWGetExtension()->SetCustomVideoTimingsArray(szVideoTiming, dwTimingCount);

	m_nSelTableTiming = -1;
	m_btnDelete.EnableWindow(FALSE);

	UpdateTableTiming();
}

void CTimingTimingsDialog::UpdateTableTiming()
{
	m_listTimings.DeleteAllItems();

	DWORD dwTimingCount = 0;
	HRESULT hr = MWGetExtension()->GetCustomVideoTimingsCount(&dwTimingCount);

	DWORD dwBufferSize = 32;
	MWCAP_VIDEO_CUSTOM_TIMING szVideoTiming[32];
	hr = MWGetExtension()->GetCustomVideoTimingsArray(szVideoTiming, &dwBufferSize);

	CString strValue;

	for (UINT i = 0; i < dwTimingCount; ++ i) {

		int nFieldRateX100 = 0;
		if (szVideoTiming[i].syncInfo.dwFrameDuration != 0)
			nFieldRateX100 = (int)(1000000000LL / szVideoTiming[i].syncInfo.dwFrameDuration);
		if (szVideoTiming[i].syncInfo.bInterlaced)
			nFieldRateX100 <<= 1;

		m_listTimings.InsertItem(i, _T(""));  
		strValue.Format(_T("%dx%d%s, %d.%02dHz"), szVideoTiming[i].videoTimingSettings.cx, szVideoTiming[i].videoTimingSettings.cy,
			szVideoTiming[i].syncInfo.bInterlaced ? _T("i") : _T("p"), nFieldRateX100 / 100, nFieldRateX100 % 100);
		m_listTimings.SetItemText(i, 0, strValue);  
		strValue.Format(_T("%dx%d"), szVideoTiming[i].videoTimingSettings.cxTotal, szVideoTiming[i].syncInfo.wFrameLineCount);
		m_listTimings.SetItemText(i, 1, strValue);  
		strValue.Format(_T("ES"));
		m_listTimings.SetItemText(i, 2, strValue);  
	}
}