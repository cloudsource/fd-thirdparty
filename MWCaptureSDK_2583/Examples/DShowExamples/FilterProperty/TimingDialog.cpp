// TimingDialog.cpp : implementation file
//

#include "stdafx.h"
#include "FilterProperty.h"
#include "TimingDialog.h"
#include "afxdialogex.h"

#include "DShowCapture.h"

// CTimingDialog dialog

IMPLEMENT_DYNAMIC(CTimingDialog, CDialogEx)

CTimingDialog::CTimingDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTimingDialog::IDD, pParent)
{

}

CTimingDialog::~CTimingDialog()
{
}

void CTimingDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_TIMING, m_tabTiming);
	DDX_Control(pDX, IDC_CHECK_TIMING_AUTO_SAMPLE_PHASE, m_ckbAutoSamplePhase);
	DDX_Control(pDX, IDC_CHECK_TIMING_AUTO_H_ALIGN, m_ckbAutoHAlign);
	DDX_Control(pDX, IDC_EDIT_TIMING_PHASE, m_editSamplePhase);
	DDX_Control(pDX, IDC_SPIN_TIMING_PHASE, m_spinSamplePhase);
	DDX_Control(pDX, IDC_BUTTON_TIMING_AUO_ADJUST, m_btnAutoAdjust);
}


BEGIN_MESSAGE_MAP(CTimingDialog, CDialogEx)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_TIMING_PHASE, &CTimingDialog::OnDeltaposSpinTimingPhase)
	ON_BN_CLICKED(IDC_BUTTON_TIMING_AUO_ADJUST, &CTimingDialog::OnBnClickedButtonTimingAuoAdjust)
	ON_WM_KILLFOCUS()
	ON_WM_CREATE()
	ON_COMMAND_RANGE(ID_CUSTOMIZE_BEGINE, ID_CUSTOMIZE_BEGINE + MWCAP_VIDEO_MAX_NUM_PREFERRED_TIMINGS, OnDeviceItem)
	ON_COMMAND(ID_BUTTON_MENU_MODIFY, &CTimingDialog::OnButtonMenuModify)
	ON_COMMAND(ID_BUTTON_MENU_SAVE, &CTimingDialog::OnButtonMenuSave)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_TIMING, &CTimingDialog::OnTcnSelchangeTabTiming)
	ON_BN_CLICKED(IDC_CHECK_TIMING_AUTO_SAMPLE_PHASE, &CTimingDialog::OnBnClickedCheckTimingAutoSamplePhase)
	ON_BN_CLICKED(IDC_CHECK_TIMING_AUTO_H_ALIGN, &CTimingDialog::OnBnClickedCheckTimingAutoHAlign)
END_MESSAGE_MAP()


// CTimingDialog message handlers
BOOL CTimingDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_nSelTableTiming = -1;
	m_nSelTableResolution = -1;

	BOOLEAN bAutoHAlign = FALSE;
	MWGetExtension()->GetVideoAutoHAlign(&bAutoHAlign);
	m_ckbAutoHAlign.SetCheck(bAutoHAlign);

	m_tabTiming.InsertItem(0, _T("Adjust timing"));
	m_tabTiming.InsertItem(1, _T("Custom timings"));
	m_tabTiming.InsertItem(2, _T("Custom GTF/CVT resolutions"));

	m_dlgTimingAdjust.Create(CTimingAdjustDialog::IDD, this);
	m_dlgTimingTimings.Create(CTimingTimingsDialog::IDD, this);
	m_dlgTimingResolution.Create(CTimingResolutionDialog::IDD, this);

	HRESULT hr = S_FALSE;
	BOOLEAN bAuto = FALSE;
	hr = MWGetExtension()->GetVideoSamplingPhaseAutoAdjust(&bAuto);
	m_ckbAutoSamplePhase.SetCheck(bAuto);
	m_spinSamplePhase.EnableWindow(!bAuto);
	m_spinSamplePhase.SetRange(0, 63);

	BYTE byPhase = 0;
	MWGetExtension()->GetVideoSamplingPhase(&byPhase);
	CString strText;
	strText.Format(_T("%d"), byPhase);
	m_editSamplePhase.SetWindowTextW(strText);

	CRect rcBorder;
	CWnd *pBorder = GetDlgItem(IDC_STATIC_TIMING_ADJUST_DLG);
	pBorder->GetWindowRect(rcBorder);
	ScreenToClient(rcBorder);

	m_dlgTimingAdjust.SetWindowPos(pBorder, rcBorder.left, rcBorder.top, rcBorder.Width(), rcBorder.Height(), SWP_NOREDRAW);
	m_dlgTimingTimings.SetWindowPos(pBorder, rcBorder.left, rcBorder.top, rcBorder.Width(), rcBorder.Height(), SWP_NOREDRAW);
	m_dlgTimingResolution.SetWindowPos(pBorder, rcBorder.left, rcBorder.top, rcBorder.Width(), rcBorder.Height(), SWP_NOREDRAW);

	m_dlgTimingAdjust.ShowWindow(SW_SHOW);

	g_capture.AddNotify(this);

	UpdateTimingSettings();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


void CTimingDialog::OnDeltaposSpinTimingPhase(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	int nValue = pNMUpDown->iDelta == -1 ? -1 : 1;

	BYTE byPhase = m_spinSamplePhase.GetPos() + nValue;
	MWGetExtension()->SetVideoSamplingPhase(byPhase);
}


void CTimingDialog::OnBnClickedButtonTimingAuoAdjust()
{
	// TODO: Add your control notification handler code here
	BYTE byPhase = 0xFF;
	MWGetExtension()->SetVideoSamplingPhase(byPhase);
}

int CTimingDialog::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	
	return 0;
}


void CTimingDialog::OnButtonMenuModify()
{
	// TODO: Add your command handler code here
}


void CTimingDialog::OnButtonMenuSave()
{
	// TODO: Add your command handler code here
}

void CTimingDialog::OnDeviceItem(UINT nID)
{
	int nIndex = nID - ID_CUSTOMIZE_BEGINE - 1;

	DWORD dwNum = MWCAP_VIDEO_MAX_NUM_PREFERRED_TIMINGS;
	MWCAP_VIDEO_TIMING szVideoTiming[MWCAP_VIDEO_MAX_NUM_PREFERRED_TIMINGS] = {0};
	HRESULT hr = MWGetExtension()->GetPreferredVideoTimings(szVideoTiming, &dwNum);

	hr = MWGetExtension()->SetVideoTiming(&szVideoTiming[nIndex]);
}

void CTimingDialog::OnTcnSelchangeTabTiming(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	m_dlgTimingAdjust.ShowWindow(SW_HIDE);
	m_dlgTimingTimings.ShowWindow(SW_HIDE);
	m_dlgTimingResolution.ShowWindow(SW_HIDE);

	int nIndex = m_tabTiming.GetCurSel();
	switch (nIndex) {
	case 0:
		m_dlgTimingAdjust.ShowWindow(SW_SHOW);
		break;
	case 1:
		m_dlgTimingTimings.ShowWindow(SW_SHOW);
		m_dlgTimingTimings.UpdateTableTiming();
		break;
	case 2:
		m_dlgTimingResolution.ShowWindow(SW_SHOW);
		break;
	}
	*pResult = 0;
}


void CTimingDialog::OnBnClickedCheckTimingAutoSamplePhase()
{
	// TODO: Add your control notification handler code here
	BOOLEAN bAuto = m_ckbAutoSamplePhase.GetCheck();
	m_spinSamplePhase.EnableWindow(!bAuto);

	MWGetExtension()->SetVideoSamplingPhaseAutoAdjust(bAuto);
	if (bAuto) {
		MWGetExtension()->SetVideoSamplingPhase(0xFF);
	}
}


void CTimingDialog::OnBnClickedCheckTimingAutoHAlign()
{
	// TODO: Add your control notification handler code here
	BOOLEAN bAutoHAlign = m_ckbAutoHAlign.GetCheck();
	MWGetExtension()->SetVideoAutoHAlign(bAutoHAlign);
}

void CTimingDialog::OnTimingStatuChanged(ULONGLONG ullStatusBits)
{
	BOOLEAN bAutoSamplingPhase;
	MWGetExtension()->GetVideoSamplingPhaseAutoAdjust(&bAutoSamplingPhase);

	BYTE bySamplingPhase;
	MWGetExtension()->GetVideoSamplingPhase(&bySamplingPhase);

	m_spinSamplePhase.SetPos(bySamplingPhase);
	m_ckbAutoSamplePhase.SetCheck(bAutoSamplingPhase ? BST_CHECKED : BST_UNCHECKED);
	m_spinSamplePhase.EnableWindow(!bAutoSamplingPhase);

	if ((ullStatusBits & MWCAP_NOTIFY_INPUT_SPECIFIC_CHANGE) != 0) {
		UpdateTimingSettings();
	}

	if ((ullStatusBits & MWCAP_NOTIFY_VIDEO_SAMPLING_PHASE_CHANGE) != 0) {
		m_spinSamplePhase.SetPos(bySamplingPhase);
	}
}

void CTimingDialog::UpdateTimingSettings()
{
	m_dlgTimingAdjust.UpdateTimingSettings();

	MWCAP_INPUT_SPECIFIC_STATUS status;
	MWGetExtension()->GetInputSpecificStatus(&status);

	BOOLEAN bValidTimings = status.bValid
		&& (status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_VGA || status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_COMPONENT);

	m_btnAutoAdjust.EnableWindow(bValidTimings);
	m_ckbAutoSamplePhase.EnableWindow(bValidTimings);
	m_ckbAutoHAlign.EnableWindow(bValidTimings);
	m_editSamplePhase.EnableWindow(bValidTimings);
	m_spinSamplePhase.EnableWindow(bValidTimings);
	m_tabTiming.EnableWindow(bValidTimings);
}
