#pragma once

#include "DShowCapture.h"
// CTimingAdjustDialog dialog

class CTimingAdjustDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CTimingAdjustDialog)

public:
	CTimingAdjustDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTimingAdjustDialog();

// Dialog Data
	enum { IDD = IDD_TIMING_ADJUST_DIALOG };

public:
	void UpdateTimingSettings();

private:
	CStatic				m_textPreferred;
	CButton				m_btnPreferred;
	
	CScrollBar			m_barHActive;
	CScrollBar			m_barHTotal;
	CScrollBar			m_barHOffset;
	CScrollBar			m_barVActive;
	CScrollBar			m_barVOffset;
	CScrollBar			m_barClampPos;

	CStatic				m_textHActive;
	CStatic				m_textHTotal;
	CStatic				m_textHOffset;
	CStatic				m_textVActive;
	CStatic				m_textVOffset;
	CStatic				m_textClampPos;

	CSpinButtonCtrl		m_spinAspectX;
	CSpinButtonCtrl		m_spinAspectY;

	CButton				m_btnReset;
	CButton				m_btnSave;

private:
	CString GetVideoTimingType(int nType);	
	void AddCustomTiming(const MWCAP_VIDEO_CUSTOM_TIMING * pTiming);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonTimingAdjustPreferred();
	afx_msg void OnDeltaposSpinTimingAdjustAspextX(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinTimingAdjustAspextY(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonTimingAdjustReset();
	afx_msg void OnBnClickedButtonTimingAdjustSave();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnDeviceItem(UINT nID);
};
