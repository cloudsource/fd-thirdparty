// OSDTabDialog.cpp : implementation file
//

#include "stdafx.h"
#include "FilterProperty.h"
#include "OSDTabDialog.h"
#include "afxdialogex.h"
#include "GdiPlusRenderer.h"

// COSDTabDialog dialog

IMPLEMENT_DYNAMIC(COSDTabDialog, CDialogEx)

COSDTabDialog::COSDTabDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(COSDTabDialog::IDD, pParent)
{
	m_bCapture = FALSE;
}

COSDTabDialog::~COSDTabDialog()
{
}

void COSDTabDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_OSD_PNG, m_picOSD);
	DDX_Control(pDX, IDC_BUTTON_SELECT_FILE, m_btnSelectFile);
	DDX_Control(pDX, IDC_EDIT_OSD_PATH, m_editPath);
	DDX_Control(pDX, IDC_BUTTON_OSD_LOAD_PRESENT, m_btnLoadPresent);
	DDX_Control(pDX, IDC_BUTTON_OSD_SAVE_PRESENT, m_btnSavePresent);
	DDX_Control(pDX, IDC_CHECK_OSD_ENABLE, m_ckbEnableOSD);
}


BEGIN_MESSAGE_MAP(COSDTabDialog, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_FILE, &COSDTabDialog::OnBnClickedButtonSelectFile)
	ON_BN_CLICKED(IDC_BUTTON_OSD_LOAD_PRESENT, &COSDTabDialog::OnBnClickedButtonOsdLoadPresent)
	ON_BN_CLICKED(IDC_BUTTON_OSD_SAVE_PRESENT, &COSDTabDialog::OnBnClickedButtonOsdSavePresent)
	ON_BN_CLICKED(IDC_CHECK_OSD_ENABLE, &COSDTabDialog::OnBnClickedCheckOsdEnable)
	ON_WM_PAINT()
END_MESSAGE_MAP()


// COSDTabDialog message handlers
BOOL COSDTabDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	DisplayOSDInfo(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void COSDTabDialog::OnBnClickedButtonSelectFile()
{
	// TODO: Add your control notification handler code here

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, _T("PNG Files (*.png)|*.png||"));
	if(dlg.DoModal() != IDOK) {
		return ;
	}

	CString strFilePath = dlg.GetPathName();

	MWCAP_VIDEO_OSD_SETTINGS videoOSDSettings;
	videoOSDSettings.bEnable = m_ckbEnableOSD.GetCheck();
	wcscpy(videoOSDSettings.wszPNGFilePath, strFilePath); 
	HRESULT hr = S_FALSE;
	if (m_bCapture) 
		hr = MWGetExtension()->SetVideoCaptureOSDSettings(&videoOSDSettings);
	else
		hr = MWGetExtension()->SetVideoPreviewOSDSettings(&videoOSDSettings);

	m_editPath.SetWindowText(strFilePath);

	DisplayOSDInfo(m_bCapture);
}


void COSDTabDialog::OnBnClickedButtonOsdLoadPresent()
{
	// TODO: Add your control notification handler code here
	MWCAP_VIDEO_OSD_SETTINGS videoOSDSettings;

	if (m_bCapture) {
		MWGetExtension()->GetVideoCaptureOSDPreset(&videoOSDSettings);
		MWGetExtension()->SetVideoCaptureOSDSettings(&videoOSDSettings);
	}
	else {
		MWGetExtension()->GetVideoPreviewOSDPreset(&videoOSDSettings);
		MWGetExtension()->SetVideoPreviewOSDSettings(&videoOSDSettings);
	}

	DisplayOSDLogo(videoOSDSettings);
}


void COSDTabDialog::OnBnClickedButtonOsdSavePresent()
{
	// TODO: Add your control notification handler code here
	MWCAP_VIDEO_OSD_SETTINGS videoOSDSettings;

	if (m_bCapture) {
		MWGetExtension()->GetVideoCaptureOSDSettings(&videoOSDSettings);
		MWGetExtension()->SetVideoCaptureOSDPreset(&videoOSDSettings);
	}
	else {
		MWGetExtension()->GetVideoPreviewOSDSettings(&videoOSDSettings);
		MWGetExtension()->SetVideoPreviewOSDPreset(&videoOSDSettings);
	}
}


void COSDTabDialog::OnBnClickedCheckOsdEnable()
{
	// TODO: Add your control notification handler code here
	MWCAP_VIDEO_OSD_SETTINGS videoOSDSettings;
	HRESULT hr = S_FALSE;
	if (m_bCapture) {
		hr = MWGetExtension()->GetVideoCaptureOSDSettings(&videoOSDSettings);
		videoOSDSettings.bEnable = m_ckbEnableOSD.GetCheck();
		hr = MWGetExtension()->SetVideoCaptureOSDSettings(&videoOSDSettings);
	}
	else {
		hr = MWGetExtension()->GetVideoPreviewOSDSettings(&videoOSDSettings);
		videoOSDSettings.bEnable = m_ckbEnableOSD.GetCheck();
		hr = MWGetExtension()->SetVideoPreviewOSDSettings(&videoOSDSettings);
	}
}

void COSDTabDialog::DisplayOSDInfo( BOOL bCapture )
{
	m_bCapture = bCapture;

	MWCAP_VIDEO_OSD_SETTINGS videoOSDSettings;

	if (bCapture) 
		MWGetExtension()->GetVideoCaptureOSDSettings(&videoOSDSettings);
	else
		MWGetExtension()->GetVideoPreviewOSDSettings(&videoOSDSettings);

	DisplayOSDLogo(videoOSDSettings);
}

void COSDTabDialog::DisplayOSDLogo(MWCAP_VIDEO_OSD_SETTINGS videoOSDSettings)
{
	m_editPath.SetWindowText(videoOSDSettings.wszPNGFilePath);
	m_ckbEnableOSD.SetCheck(videoOSDSettings.bEnable);

	CString strPath;
	strPath.Format(_T("%s"), videoOSDSettings.wszPNGFilePath);

	///GDI
	ULONG_PTR m_gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	Gdiplus::GdiplusStartup(&m_gdiplusToken, &gdiplusStartupInput, NULL);

	Gdiplus::Bitmap bitmap(strPath);
	CClientDC *pDC = new CClientDC(&m_picOSD);
	CRect rect;
	m_picOSD.GetWindowRect(&rect);
	
	Gdiplus::Graphics graphics(pDC->m_hDC);
	Color color(255, 255, 255, 255);
	graphics.Clear(color);

	Rect fillRect(0, 0, rect.Width(), rect.Height());
	graphics.DrawImage(&bitmap, fillRect);

	delete pDC;
}


void COSDTabDialog::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	// Do not call CDialogEx::OnPaint() for painting messages

	DisplayOSDInfo(m_bCapture);
}
