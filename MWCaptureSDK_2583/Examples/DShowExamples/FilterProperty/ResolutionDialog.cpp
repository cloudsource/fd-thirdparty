// ResolutionDialog.cpp : implementation file
//

#include "stdafx.h"
#include "FilterProperty.h"
#include "ResolutionDialog.h"
#include "afxdialogex.h"

#include "DShowCapture.h"
// CResolutionDialog dialog

IMPLEMENT_DYNAMIC(CResolutionDialog, CDialogEx)

CResolutionDialog::CResolutionDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CResolutionDialog::IDD, pParent)
{
	m_nIndex = -1;
}

CResolutionDialog::~CResolutionDialog()
{
}

void CResolutionDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_RESOLUTION_WIDTH, m_editWidth);
	DDX_Control(pDX, IDC_EDIT_RESOLUTION_HEIGHT, m_editHeight);
}


BEGIN_MESSAGE_MAP(CResolutionDialog, CDialogEx)
	ON_BN_CLICKED(IDOK, &CResolutionDialog::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CResolutionDialog::OnBnClickedCancel)
END_MESSAGE_MAP()


// CResolutionDialog message handlers
BOOL CResolutionDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	UpdateItemValue(m_nIndex);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


void CResolutionDialog::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnOK();

	CString strText;
	m_editWidth.GetWindowText(strText);
	int nWidth = _wtoi(strText.GetBuffer());

	m_editHeight.GetWindowText(strText);
	int nHeight = _wtoi(strText.GetBuffer());

	if (nWidth < 128 || nWidth > 4096 || nHeight < 128 || nHeight > 4096) {
		MessageBox(_T("Enter a number between 128 and 4096"), _T("ERROR"), MB_OK);
		return ;
	}

	DWORD dwResolutionCount = 0;
	HRESULT hr = MWGetExtension()->GetCustomVideoResolutionsCount(&dwResolutionCount);

	DWORD dwBufferSize = 32;
	SIZE szVideoResolution[32];
	hr = MWGetExtension()->GetCustomVideoResolutionsArray(szVideoResolution, &dwBufferSize);

	if (m_nIndex == -1) { //add
		szVideoResolution[dwResolutionCount].cx = nWidth;
		szVideoResolution[dwResolutionCount].cy = nHeight;
		++ dwResolutionCount;
	}
	else { // modify
		szVideoResolution[m_nIndex].cx = nWidth;
		szVideoResolution[m_nIndex].cy = nHeight;
	}

	hr = MWGetExtension()->SetCustomVideoResolutionsArray(szVideoResolution, dwResolutionCount);
}


void CResolutionDialog::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnCancel();
}

void CResolutionDialog::ModifyItemIndex( int nIndex )
{
	m_nIndex = nIndex;
}

void CResolutionDialog::UpdateItemValue( int nIndex )
{
	if (nIndex != -1) {
		DWORD dwResolutionCount = 0;
		HRESULT hr = MWGetExtension()->GetCustomVideoResolutionsCount(&dwResolutionCount);

		DWORD dwBufferSize = 32;
		SIZE szVideoResolution[32];
		hr = MWGetExtension()->GetCustomVideoResolutionsArray(szVideoResolution, &dwBufferSize);

		SIZE szSel = szVideoResolution[nIndex];

		CString strValue;
		strValue.Format(_T("%d"), szSel.cx);
		m_editWidth.SetWindowText(strValue);

		strValue.Format(_T("%d"), szSel.cy);
		m_editHeight.SetWindowText(strValue);
	}
	else {
		CString strValue = _T("");
		m_editWidth.SetWindowText(strValue);
		m_editHeight.SetWindowText(strValue);
	}
}

