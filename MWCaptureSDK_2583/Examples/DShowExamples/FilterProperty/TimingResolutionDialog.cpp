// TimingResolutionDialog.cpp : implementation file
//

#include "stdafx.h"
#include "FilterProperty.h"
#include "TimingResolutionDialog.h"
#include "afxdialogex.h"

#include "DShowCapture.h"

// CTimingResolutionDialog dialog

IMPLEMENT_DYNAMIC(CTimingResolutionDialog, CDialogEx)

CTimingResolutionDialog::CTimingResolutionDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTimingResolutionDialog::IDD, pParent)
{

}

CTimingResolutionDialog::~CTimingResolutionDialog()
{
}

void CTimingResolutionDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_TIMING_RESOLUTION, m_listResolutions);
	DDX_Control(pDX, IDC_BUTTON_TIMING_RESOLUTION_DELETE, m_btnDelete);
	DDX_Control(pDX, IDC_BUTTON_TIMING_RESOLUTION_ADD, m_btnAdd);
	DDX_Control(pDX, IDC_BUTTON_TIMING_RESOLUTION_MODIFY, m_btnModify);
}


BEGIN_MESSAGE_MAP(CTimingResolutionDialog, CDialogEx)
	ON_NOTIFY(NM_CLICK, IDC_LIST_TIMING_RESOLUTION, &CTimingResolutionDialog::OnNMClickListTimingResolution)
	ON_NOTIFY(NM_KILLFOCUS, IDC_LIST_TIMING_RESOLUTION, &CTimingResolutionDialog::OnNMKillfocusListTimingResolution)
	ON_BN_CLICKED(IDC_BUTTON_TIMING_RESOLUTION_DELETE, &CTimingResolutionDialog::OnBnClickedButtonTimingResolutionDelete)
	ON_BN_CLICKED(IDC_BUTTON_TIMING_RESOLUTION_ADD, &CTimingResolutionDialog::OnBnClickedButtonTimingResolutionAdd)
	ON_BN_CLICKED(IDC_BUTTON_TIMING_RESOLUTION_MODIFY, &CTimingResolutionDialog::OnBnClickedButtonTimingResolutionModify)
END_MESSAGE_MAP()


// CTimingResolutionDialog message handlers
BOOL CTimingResolutionDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_listResolutions.ModifyStyle(0, LVS_REPORT);
	m_listResolutions.SetExtendedStyle(m_listResolutions.GetExtendedStyle() | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP);  

	m_listResolutions.InsertColumn(0, _T("Resolutions"));  
	m_listResolutions.InsertColumn(1, _T(""));  

	CRect rect;  
	m_listResolutions.GetClientRect(rect);
	m_listResolutions.SetColumnWidth(0, rect.Width() * 9 / 10); 
	m_listResolutions.SetColumnWidth(1, rect.Width() * 1 / 10);  

	UpdateTableResolution();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CTimingResolutionDialog::OnNMClickListTimingResolution(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	m_nSelTableResolution = pNMItemActivate->iItem;

	BOOL bEnable = m_nSelTableResolution != -1 ? TRUE : FALSE;
	m_btnModify.EnableWindow(bEnable);
	m_btnDelete.EnableWindow(bEnable);
}


void CTimingResolutionDialog::OnNMKillfocusListTimingResolution(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;

	HWND hwnd = GetFocus()->m_hWnd;
	if (hwnd == m_btnModify.GetSafeHwnd() || hwnd == m_btnDelete.GetSafeHwnd()) {

	}
	else {
		m_nSelTableResolution = -1;
		m_btnModify.EnableWindow(FALSE);
		m_btnDelete.EnableWindow(FALSE);
	}
}


void CTimingResolutionDialog::OnBnClickedButtonTimingResolutionDelete()
{
	// TODO: Add your control notification handler code here

	DWORD dwResolutionCount = 0;
	HRESULT hr = MWGetExtension()->GetCustomVideoResolutionsCount(&dwResolutionCount);

	DWORD dwBufferSize = 32;
	SIZE szVideoResolution[32];
	hr = MWGetExtension()->GetCustomVideoResolutionsArray(szVideoResolution, &dwBufferSize);

	for (UINT i = m_nSelTableResolution; i < dwResolutionCount; ++ i) {
		szVideoResolution[i] = szVideoResolution[i + 1];
	}
	-- dwResolutionCount;

	hr = MWGetExtension()->SetCustomVideoResolutionsArray(szVideoResolution, dwResolutionCount);

	m_nSelTableResolution = -1;
	m_btnDelete.EnableWindow(FALSE);
	m_btnModify.EnableWindow(FALSE);

	UpdateTableResolution();
}


void CTimingResolutionDialog::OnBnClickedButtonTimingResolutionAdd()
{
	// TODO: Add your control notification handler code here

	m_dlgResolution.ModifyItemIndex(-1);
	m_dlgResolution.DoModal();

	UpdateTableResolution();
}


void CTimingResolutionDialog::OnBnClickedButtonTimingResolutionModify()
{
	// TODO: Add your control notification handler code here

	m_dlgResolution.ModifyItemIndex(m_nSelTableResolution);
	m_dlgResolution.DoModal();

	UpdateTableResolution();

	m_listResolutions.SetFocus();  
	m_listResolutions.SetItemState(m_nSelTableResolution, LVIS_SELECTED | LVIS_FOCUSED,	LVIS_SELECTED | LVIS_FOCUSED);
}

void CTimingResolutionDialog::UpdateTableResolution()
{
	m_listResolutions.DeleteAllItems();

	DWORD dwResolutionCount = 0;
	HRESULT hr = MWGetExtension()->GetCustomVideoResolutionsCount(&dwResolutionCount);

	DWORD dwBufferSize = 32;
	SIZE szVideoResolution[32];
	hr = MWGetExtension()->GetCustomVideoResolutionsArray(szVideoResolution, &dwBufferSize);

	CString strValue;

	for (UINT i = 0; i < dwResolutionCount; ++ i) {
		strValue.Format(_T("%dx%d"), szVideoResolution[i].cx, szVideoResolution[i].cy);
		m_listResolutions.InsertItem(i, strValue);  
	}
}