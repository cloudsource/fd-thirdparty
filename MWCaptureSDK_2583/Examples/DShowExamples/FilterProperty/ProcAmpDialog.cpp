// ProcAmpDialog.cpp : implementation file
//

#include "stdafx.h"
#include "FilterProperty.h"
#include "ProcAmpDialog.h"
#include "afxdialogex.h"

#include "StringUtils.h"

#include "DShowCapture.h"

// CProcAmpDialog dialog

IMPLEMENT_DYNAMIC(CProcAmpDialog, CDialogEx)

CProcAmpDialog::CProcAmpDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CProcAmpDialog::IDD, pParent)
{
}

CProcAmpDialog::~CProcAmpDialog()
{
}

void CProcAmpDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDER_PROC_BRIGHTNESS, m_sldBrightNess);
	DDX_Control(pDX, IDC_SLIDER_PROC_CONSTRACT, m_sldConstract);
	DDX_Control(pDX, IDC_SLIDER_PROC_HUE, m_sldHue);
	DDX_Control(pDX, IDC_SLIDER_PROC_SAT, m_sldSat);
	DDX_Control(pDX, IDC_SLIDER_PROC_DEFI, m_sldDefi);
	DDX_Control(pDX, IDC_SLIDER_PROC_GAMMA, m_sldGamma);
	DDX_Control(pDX, IDC_SLIDER_PROC_WHITE, m_sldWhite);
	DDX_Control(pDX, IDC_SLIDER_PROC_BACKLIGHT, m_sldBacklight);
	DDX_Control(pDX, IDC_SLIDER_PROC_GAIN, m_sldGain);

	DDX_Control(pDX, IDC_EDIT_PROC_BRIGHTNESS, m_editBrightNess);
	DDX_Control(pDX, IDC_EDIT_PROC_CONSTRACT, m_editConstract);
	DDX_Control(pDX, IDC_EDIT_PROC_HUE, m_editHue);
	DDX_Control(pDX, IDC_EDIT_PROC_SAT, m_editSat);
	DDX_Control(pDX, IDC_EDIT_PROC_DEFI, m_editDefi);
	DDX_Control(pDX, IDC_EDIT_PROC_GAMMA, m_editGamma);
	DDX_Control(pDX, IDC_EDIT_PROC_WHITE, m_editWhite);
	DDX_Control(pDX, IDC_EDIT_PROC_BACKLIGHT, m_editBacklight);
	DDX_Control(pDX, IDC_EDIT_PROC_GAIN, m_editGain);

	DDX_Control(pDX, IDC_CHECK_PROC_BRIGHTNESS, m_ckbBrightNess);
	DDX_Control(pDX, IDC_CHECK_PROC_CONSTRACT, m_ckbConstract);
	DDX_Control(pDX, IDC_CHECK_PROC_HUE, m_ckbHue);
	DDX_Control(pDX, IDC_CHECK_PROC_SAT, m_ckbSat);
	DDX_Control(pDX, IDC_CHECK_PROC_DEFI, m_ckbDefi);
	DDX_Control(pDX, IDC_CHECK_PROC_GAMMA, m_ckbGamma);
	DDX_Control(pDX, IDC_CHECK_PROC_WHITE, m_ckbWhite);
	DDX_Control(pDX, IDC_CHECK_PROC_BACKLIGHT, m_ckbBacklight);
	DDX_Control(pDX, IDC_CHECK_PROC_GAIN, m_ckbGain);

	DDX_Control(pDX, IDC_CHECK_PROC_FREQ, m_ckbFreq);
	DDX_Control(pDX, IDC_COMBO_PROC_FREQ, m_cmbFreq);
	DDX_Control(pDX, IDC_BUTTON_PROC_DEFAULT, m_ckbDefault);
}


BEGIN_MESSAGE_MAP(CProcAmpDialog, CDialogEx)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_PROC_BRIGHTNESS, &CProcAmpDialog::OnNMCustomdrawSliderProcBrightness)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_PROC_CONSTRACT, &CProcAmpDialog::OnNMCustomdrawSliderProcConstract)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_PROC_HUE, &CProcAmpDialog::OnNMCustomdrawSliderProcHue)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_PROC_SAT, &CProcAmpDialog::OnNMCustomdrawSliderProcSat)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_PROC_DEFI, &CProcAmpDialog::OnNMCustomdrawSliderProcDefi)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_PROC_GAMMA, &CProcAmpDialog::OnNMCustomdrawSliderProcGamma)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_PROC_WHITE, &CProcAmpDialog::OnNMCustomdrawSliderProcWhite)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_PROC_BACKLIGHT, &CProcAmpDialog::OnNMCustomdrawSliderProcBacklight)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_PROC_GAIN, &CProcAmpDialog::OnNMCustomdrawSliderProcGain)
	ON_BN_CLICKED(IDC_BUTTON_PROC_DEFAULT, &CProcAmpDialog::OnBnClickedButtonProcDefault)
END_MESSAGE_MAP()


BOOL CProcAmpDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	UpdateAmpProperty();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

// CProcAmpDialog message handlers


void CProcAmpDialog::OnNMCustomdrawSliderProcBrightness(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	int nPos = m_sldBrightNess.GetPos();
	CString strValue;
	strValue.Format(_T("%d"), nPos);
	m_editBrightNess.SetWindowText(strValue);

	g_capture.SetProcAmpPropertyValue(VideoProcAmp_Brightness, nPos);
}


void CProcAmpDialog::OnNMCustomdrawSliderProcConstract(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	int nPos = m_sldConstract.GetPos();
	CString strValue;
	strValue.Format(_T("%d"), nPos);
	m_editConstract.SetWindowText(strValue);

	g_capture.SetProcAmpPropertyValue(VideoProcAmp_Contrast, nPos);
}


void CProcAmpDialog::OnNMCustomdrawSliderProcHue(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	int nPos = m_sldHue.GetPos();
	CString strValue;
	strValue.Format(_T("%d"), nPos);
	m_editHue.SetWindowText(strValue);

	g_capture.SetProcAmpPropertyValue(VideoProcAmp_Hue, nPos);
}


void CProcAmpDialog::OnNMCustomdrawSliderProcSat(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	int nPos = m_sldSat.GetPos();
	CString strValue;
	strValue.Format(_T("%d"), nPos);
	m_editSat.SetWindowText(strValue);

	g_capture.SetProcAmpPropertyValue(VideoProcAmp_Saturation, nPos);
}


void CProcAmpDialog::OnNMCustomdrawSliderProcDefi(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}


void CProcAmpDialog::OnNMCustomdrawSliderProcGamma(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}


void CProcAmpDialog::OnNMCustomdrawSliderProcWhite(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}


void CProcAmpDialog::OnNMCustomdrawSliderProcBacklight(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}


void CProcAmpDialog::OnNMCustomdrawSliderProcGain(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}



void CProcAmpDialog::OnBnClickedButtonProcDefault()
{
	// TODO: Add your control notification handler code here

	SetProcAmpPropertyDefault(VideoProcAmp_Brightness, &m_sldBrightNess);
	SetProcAmpPropertyDefault(VideoProcAmp_Contrast, &m_sldConstract);
	SetProcAmpPropertyDefault(VideoProcAmp_Hue, &m_sldHue);
	SetProcAmpPropertyDefault(VideoProcAmp_Saturation, &m_sldSat);
}

void CProcAmpDialog::GetProcAmpProperty( VideoProcAmpProperty ampProperty, CSliderCtrl* pslider, CButton* pCheckButton )
{
	long lMin = 0, lMax = 0, lStep = 0, lDefault = 0, lFlags = 0, lValue = 0;
	HRESULT hr = S_FALSE;

	hr = g_capture.GetProcAmpPropertyRange(ampProperty, &lMin, &lMax, &lStep, &lDefault);
	hr = g_capture.GetProcAmpPropertyValue(ampProperty, &lValue);

	if (ampProperty != VideoProcAmp_ColorEnable) {
		if (SUCCEEDED(hr)) {
			pslider->SetRange(lMin, lMax);
			pslider->SetPos(lValue);

			if (ampProperty == VideoProcAmp_Brightness || ampProperty == VideoProcAmp_Hue) {
				pslider->SetPos(1);
				pslider->SetPos(lValue);
			}
		}
		else {
			pslider->EnableWindow(FALSE);
			pCheckButton->EnableWindow(FALSE);
		}
	}
	else {
		if (SUCCEEDED(hr)) {
		}
		else {
			m_ckbFreq.EnableWindow(FALSE);
			m_cmbFreq.EnableWindow(FALSE);
		}
	}

}

void CProcAmpDialog::SetProcAmpProperty( VideoProcAmpProperty ampProperty, CSliderCtrl* pslider )
{
	if (ampProperty != VideoProcAmp_ColorEnable)
		g_capture.SetProcAmpPropertyValue(ampProperty, pslider->GetPos());
}

void CProcAmpDialog::SetProcAmpPropertyDefault( VideoProcAmpProperty ampProperty, CSliderCtrl* pslider )
{
	long lMin = 0, lMax = 0, lStep = 0, lDefault = 0, lFlags = 0;
	HRESULT hr = S_OK;

	if (pslider->IsWindowEnabled()) {
		g_capture.GetProcAmpPropertyRange(ampProperty, &lMin, &lMax, &lStep, &lDefault);
		pslider->SetPos(lDefault);
	}
}

void CProcAmpDialog::UpdateAmpProperty()
{
	GetProcAmpProperty(VideoProcAmp_Brightness, &m_sldBrightNess, &m_ckbBrightNess); 
	GetProcAmpProperty(VideoProcAmp_Contrast, &m_sldConstract, &m_ckbConstract); 
	GetProcAmpProperty(VideoProcAmp_Hue, &m_sldHue, &m_ckbHue); 
	GetProcAmpProperty(VideoProcAmp_Saturation, &m_sldSat, &m_ckbSat); 
	GetProcAmpProperty(VideoProcAmp_Sharpness, &m_sldDefi, &m_ckbDefi); 
	GetProcAmpProperty(VideoProcAmp_Gamma, &m_sldGamma, &m_ckbGamma); 
	GetProcAmpProperty(VideoProcAmp_WhiteBalance, &m_sldWhite, &m_ckbWhite); 
	GetProcAmpProperty(VideoProcAmp_BacklightCompensation, &m_sldBacklight, &m_ckbBacklight); 
	GetProcAmpProperty(VideoProcAmp_Gain, &m_sldGain, &m_ckbGain); 	
	GetProcAmpProperty(VideoProcAmp_ColorEnable, NULL, &m_ckbFreq); 
}

void CProcAmpDialog::SetCurrentAmpProperty()
{
	SetProcAmpProperty(VideoProcAmp_Brightness, &m_sldBrightNess);
	SetProcAmpProperty(VideoProcAmp_Contrast, &m_sldConstract);
	SetProcAmpProperty(VideoProcAmp_Hue, &m_sldHue);
	SetProcAmpProperty(VideoProcAmp_Saturation, &m_sldSat);
	SetProcAmpProperty(VideoProcAmp_Sharpness, &m_sldDefi);
	SetProcAmpProperty(VideoProcAmp_Gamma, &m_sldGamma);
	SetProcAmpProperty(VideoProcAmp_WhiteBalance, &m_sldWhite);
	SetProcAmpProperty(VideoProcAmp_BacklightCompensation, &m_sldBacklight);
	SetProcAmpProperty(VideoProcAmp_Gain, &m_sldGain);
	SetProcAmpProperty(VideoProcAmp_ColorEnable, NULL);
}

