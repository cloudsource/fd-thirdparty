#pragma once

#include "TimingAdjustDialog.h"
#include "TimingTimingsDialog.h"
#include "TimingResolutionDialog.h"

// CTimingDialog dialog

class CTimingDialog : public CDialogEx, public ISignalNotify
{
	DECLARE_DYNAMIC(CTimingDialog)

public:
	CTimingDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTimingDialog();

// Dialog Data
	enum { IDD = IDD_TIMING_DIALOG };

private:
	CTimingAdjustDialog	m_dlgTimingAdjust;
	CTimingTimingsDialog	m_dlgTimingTimings;
	CTimingResolutionDialog	m_dlgTimingResolution;

	CTabCtrl			m_tabTiming;

	CButton				m_ckbAutoSamplePhase;
	CButton				m_ckbAutoHAlign;
	CEdit				m_editSamplePhase;
	CSpinButtonCtrl		m_spinSamplePhase;
	CButton				m_btnAutoAdjust;

	int					m_nSelTableTiming;
	int					m_nSelTableResolution;

public:
	//ISignalNotify
	virtual void OnTimingStatuChanged(ULONGLONG ullStatusBits);

public:
	void UpdateTimingSettings();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposSpinTimingPhase(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonTimingAuoAdjust();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDeviceItem(UINT nID);
	afx_msg void OnButtonMenuModify();
	afx_msg void OnButtonMenuSave();
	afx_msg void OnTcnSelchangeTabTiming(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedCheckTimingAutoSamplePhase();
	afx_msg void OnBnClickedCheckTimingAutoHAlign();
};
