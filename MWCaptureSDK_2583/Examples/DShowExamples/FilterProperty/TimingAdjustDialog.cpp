// TimingAdjustDialog.cpp : implementation file
//

#include "stdafx.h"
#include "FilterProperty.h"
#include "TimingAdjustDialog.h"
#include "afxdialogex.h"

#include "DShowCapture.h"

#include <Atlcoll.h>

#define BAR_STEP 10
// CTimingAdjustDialog dialog

IMPLEMENT_DYNAMIC(CTimingAdjustDialog, CDialogEx)

	CTimingAdjustDialog::CTimingAdjustDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTimingAdjustDialog::IDD, pParent)
{

}

CTimingAdjustDialog::~CTimingAdjustDialog()
{
}

void CTimingAdjustDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_TIMING_ADJUST_PREFERRED, m_textPreferred);
	DDX_Control(pDX, IDC_BUTTON_TIMING_ADJUST_PREFERRED, m_btnPreferred);

	DDX_Control(pDX, IDC_SCROLLBAR_ADJUST_H_ACTIVE, m_barHActive);
	DDX_Control(pDX, IDC_SCROLLBAR_ADJUST_H_TOTAL, m_barHTotal);
	DDX_Control(pDX, IDC_SCROLLBAR_ADJUST_H_OFFSET, m_barHOffset);
	DDX_Control(pDX, IDC_SCROLLBAR_ADJUST_V_ACTIVE, m_barVActive);
	DDX_Control(pDX, IDC_SCROLLBAR_ADJUST__OFFSET, m_barVOffset);
	DDX_Control(pDX, IDC_SCROLLBAR_ADJUST_CLAMP_POS, m_barClampPos);

	DDX_Control(pDX, IDC_STATIC_ADJUST_H_ACTIVE, m_textHActive);
	DDX_Control(pDX, IDC_STATIC_ADJUST_H_TOTAL, m_textHTotal);
	DDX_Control(pDX, IDC_STATIC_ADJUST_H_OFFSET, m_textHOffset);
	DDX_Control(pDX, IDC_STATIC_ADJUST_V_ACTIVE, m_textVActive);
	DDX_Control(pDX, IDC_STATIC_ADJUST_V_OFFSET, m_textVOffset);
	DDX_Control(pDX, IDC_STATIC_ADJUST_CLAMP_POS, m_textClampPos);

	DDX_Control(pDX, IDC_SPIN_TIMING_ADJUST_ASPEXT_X, m_spinAspectX);
	DDX_Control(pDX, IDC_SPIN_TIMING_ADJUST_ASPEXT_Y, m_spinAspectY);

	DDX_Control(pDX, IDC_BUTTON_TIMING_ADJUST_SAVE, m_btnReset);
	DDX_Control(pDX, IDC_BUTTON_TIMING_ADJUST_RESET, m_btnSave);
}


BEGIN_MESSAGE_MAP(CTimingAdjustDialog, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON_TIMING_ADJUST_PREFERRED, &CTimingAdjustDialog::OnBnClickedButtonTimingAdjustPreferred)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_TIMING_ADJUST_ASPEXT_X, &CTimingAdjustDialog::OnDeltaposSpinTimingAdjustAspextX)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_TIMING_ADJUST_ASPEXT_Y, &CTimingAdjustDialog::OnDeltaposSpinTimingAdjustAspextY)
	ON_BN_CLICKED(IDC_BUTTON_TIMING_ADJUST_RESET, &CTimingAdjustDialog::OnBnClickedButtonTimingAdjustReset)
	ON_BN_CLICKED(IDC_BUTTON_TIMING_ADJUST_SAVE, &CTimingAdjustDialog::OnBnClickedButtonTimingAdjustSave)
	ON_WM_HSCROLL()
	ON_COMMAND_RANGE(ID_CUSTOMIZE_BEGINE, ID_CUSTOMIZE_BEGINE + MWCAP_VIDEO_MAX_NUM_PREFERRED_TIMINGS, OnDeviceItem)
	ON_WM_TIMER()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CTimingAdjustDialog message handlers
BOOL CTimingAdjustDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_spinAspectX.SetRange(1, 2048);
	m_spinAspectY.SetRange(1, 2048);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


void CTimingAdjustDialog::OnBnClickedButtonTimingAdjustPreferred()
{
	// TODO: Add your control notification handler code here

	CMenu menu; 
	menu.LoadMenu(IDR_MENU1); 
	CRect rect;
	m_btnPreferred.GetWindowRect(&rect);
	CMenu* pSubMenu = menu.GetSubMenu(0);

	DWORD dwNum = MWCAP_VIDEO_MAX_NUM_PREFERRED_TIMINGS;
	MWCAP_VIDEO_TIMING szVideoTiming[MWCAP_VIDEO_MAX_NUM_PREFERRED_TIMINGS] = {0};
	HRESULT hr = MWGetExtension()->GetPreferredVideoTimings(szVideoTiming, &dwNum);

	for (UINT i = 0; i < dwNum; ++ i) {
		CString strText;
		strText.Format(_T("%s, %dx%di, 30.00 Hz"), GetVideoTimingType(szVideoTiming[i].dwType), szVideoTiming[i].wHActive, szVideoTiming[i].wVActive);
		pSubMenu->InsertMenu(i + 1, MF_BYPOSITION, ID_CUSTOMIZE_BEGINE + 1 + i, strText);
	}
	pSubMenu->DeleteMenu(0, MF_BYPOSITION);

	pSubMenu->TrackPopupMenu(TPM_LEFTALIGN, rect.left, rect.top + rect.Height(), this); 

}

void CTimingAdjustDialog::OnDeltaposSpinTimingAdjustAspextX(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	MWCAP_INPUT_SPECIFIC_STATUS status;
	MWGetExtension()->GetInputSpecificStatus(&status);

	MWCAP_VIDEO_SYNC_INFO * pSyncInfo = &status.vgaComponentStatus.syncInfo;
	MWCAP_VIDEO_TIMING_SETTINGS * pSettings = &status.vgaComponentStatus.videoTimingSettings;

	BOOLEAN bValidTimings = status.bValid
		&& (status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_VGA || status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_COMPONENT);

	if (bValidTimings) {
		MWCAP_VIDEO_CUSTOM_TIMING timing;
		memcpy(&timing.syncInfo, pSyncInfo, sizeof(*pSyncInfo));
		memcpy(&timing.videoTimingSettings, pSettings, sizeof(*pSettings));

		timing.videoTimingSettings.wAspectX = (WORD)max(1, pNMUpDown->iPos + pNMUpDown->iDelta);
		MWGetExtension()->SetCustomVideoTiming(&timing);
	}
}


void CTimingAdjustDialog::OnDeltaposSpinTimingAdjustAspextY(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	MWCAP_INPUT_SPECIFIC_STATUS status;
	MWGetExtension()->GetInputSpecificStatus(&status);

	MWCAP_VIDEO_SYNC_INFO * pSyncInfo = &status.vgaComponentStatus.syncInfo;
	MWCAP_VIDEO_TIMING_SETTINGS * pSettings = &status.vgaComponentStatus.videoTimingSettings;

	BOOLEAN bValidTimings = status.bValid
		&& (status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_VGA || status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_COMPONENT);

	if (bValidTimings) {
		MWCAP_VIDEO_CUSTOM_TIMING timing;
		memcpy(&timing.syncInfo, pSyncInfo, sizeof(*pSyncInfo));
		memcpy(&timing.videoTimingSettings, pSettings, sizeof(*pSettings));

		timing.videoTimingSettings.wAspectY = (WORD)max(1, pNMUpDown->iPos + pNMUpDown->iDelta);
		MWGetExtension()->SetCustomVideoTiming(&timing);
	}

}


void CTimingAdjustDialog::OnBnClickedButtonTimingAdjustReset()
{
	// TODO: Add your control notification handler code here

	MWCAP_VIDEO_CUSTOM_TIMING timing;
	memset(&timing, 0, sizeof(timing));
	MWGetExtension()->SetCustomVideoTiming(&timing);
}


void CTimingAdjustDialog::OnBnClickedButtonTimingAdjustSave()
{
	// TODO: Add your control notification handler code here

	MWCAP_INPUT_SPECIFIC_STATUS status;
	MWGetExtension()->GetInputSpecificStatus(&status);

	if (status.bValid
		&& (status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_VGA || status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_COMPONENT)) {
			MWCAP_VIDEO_CUSTOM_TIMING timing;
			memcpy(&timing.syncInfo, &status.vgaComponentStatus.syncInfo, sizeof(timing.syncInfo));
			memcpy(&timing.videoTimingSettings, &status.vgaComponentStatus.videoTimingSettings, sizeof(timing.videoTimingSettings));
			AddCustomTiming(&timing);
	}
}

BOOLEAN IsEqualSyncInfo(const MWCAP_VIDEO_SYNC_INFO * pSyncInfoCurr, const MWCAP_VIDEO_SYNC_INFO * pSyncInfo)
{
	// Match with sync info
	if (pSyncInfoCurr->bySyncType != pSyncInfo->bySyncType)
		return FALSE;

	DWORD dwChangeThreshold = 100;
	int nFrameDurationMin = (int)pSyncInfoCurr->dwFrameDuration - (int)dwChangeThreshold;
	int nFrameDurationMax = (int)pSyncInfoCurr->dwFrameDuration + (int)dwChangeThreshold;

	if (pSyncInfoCurr->bInterlaced != pSyncInfo->bInterlaced
		|| pSyncInfoCurr->wVSyncLineCount > (pSyncInfo->wVSyncLineCount + 1)
		|| pSyncInfo->wVSyncLineCount > (pSyncInfoCurr->wVSyncLineCount + 1)
		|| pSyncInfoCurr->wFrameLineCount > (pSyncInfo->wFrameLineCount + 1)
		|| pSyncInfo->wFrameLineCount > (pSyncInfoCurr->wFrameLineCount + 1)
		|| ((int)pSyncInfo->dwFrameDuration < nFrameDurationMin)
		|| ((int)pSyncInfo->dwFrameDuration > nFrameDurationMax)
		)
		return FALSE;

	return TRUE;
}

void CTimingAdjustDialog::AddCustomTiming(const MWCAP_VIDEO_CUSTOM_TIMING * pTiming)
{
	CAtlArray<MWCAP_VIDEO_CUSTOM_TIMING> arrTimings;

	DWORD cCustomTimings = 0;
	MWGetExtension()->GetCustomVideoTimingsCount(&cCustomTimings);

	if (!arrTimings.SetCount(cCustomTimings + 1))
		return;

	MWCAP_VIDEO_CUSTOM_TIMING * pTimings = arrTimings.GetData();
	if (cCustomTimings != 0)
		MWGetExtension()->GetCustomVideoTimingsArray(pTimings, &cCustomTimings);

	int iOverwrite = -1;

	for (int i = 0; i < (int)cCustomTimings; i++) {
		if (IsEqualSyncInfo(&pTiming->syncInfo, &pTimings[i].syncInfo)) {
			int nID = MessageBox(_T("Already defined custom timing for current input signal, overwrite?"), _T("Confirm overwrite"), MB_YESNO | MB_ICONQUESTION);
			if (nID != IDYES)
				return;

			iOverwrite = i;
			break;
		}
	}

	if (iOverwrite >= 0)
		memcpy(pTimings + iOverwrite, pTiming, sizeof(MWCAP_VIDEO_CUSTOM_TIMING));
	else {
		memcpy(pTimings + cCustomTimings, pTiming, sizeof(MWCAP_VIDEO_CUSTOM_TIMING));
		cCustomTimings++;
	}

	MWGetExtension()->SetCustomVideoTimingsArray(arrTimings.GetData(), cCustomTimings);
}

void CTimingAdjustDialog::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	MWCAP_INPUT_SPECIFIC_STATUS status;
	MWGetExtension()->GetInputSpecificStatus(&status);

	MWCAP_VIDEO_SYNC_INFO * pSyncInfo = &status.vgaComponentStatus.syncInfo;
	MWCAP_VIDEO_TIMING_SETTINGS * pSettings = &status.vgaComponentStatus.videoTimingSettings;

	BOOLEAN bValidTimings = status.bValid
		&& (status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_VGA || status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_COMPONENT);

	if (!bValidTimings)
		return;

	MWCAP_VIDEO_CUSTOM_TIMING timing;
	memcpy(&timing.syncInfo, pSyncInfo, sizeof(*pSyncInfo));
	memcpy(&timing.videoTimingSettings, pSettings, sizeof(*pSettings));

	SCROLLINFO scrollInfo;
	scrollInfo.cbSize = sizeof(scrollInfo);
	scrollInfo.fMask = SIF_PAGE | SIF_POS | SIF_RANGE;
	pScrollBar->GetScrollInfo(&scrollInfo);

	switch (nSBCode) {
	case SB_LEFT: nPos = scrollInfo.nMin; break;
	case SB_RIGHT: nPos = scrollInfo.nMax; break;
	case SB_LINELEFT: nPos = scrollInfo.nPos - 1; break;
	case SB_LINERIGHT: nPos = scrollInfo.nPos + 1; break;
	case SB_PAGELEFT: nPos = scrollInfo.nPos - 10; break;
	case SB_PAGERIGHT: nPos = scrollInfo.nPos + 10; break;

	case SB_THUMBPOSITION:
	case SB_THUMBTRACK: 
		break;

	default: 
		return;
	}

	WORD wValue = (WORD)max(min(nPos, (UINT)scrollInfo.nMax), (UINT)scrollInfo.nMin);

	CString strText;
	if (pScrollBar == &m_barHActive) {
		strText.Format(_T("%d Pixels"), nPos);
		m_textHActive.SetWindowText(strText);

		if (timing.videoTimingSettings.cx == wValue)
			return;
		else if (wValue + pSettings->x > pSettings->cxTotal)
			timing.videoTimingSettings.cx = pSettings->cxTotal - pSettings->x;
		else
			timing.videoTimingSettings.cx = wValue;
	}
	else if (pScrollBar == &m_barHTotal) {
		strText.Format(_T("%d Pixels"), nPos);
		m_textHTotal.SetWindowText(strText);

		if (timing.videoTimingSettings.cxTotal == wValue)
			return;
		else if (wValue < pSettings->x + pSettings->cx)
			timing.videoTimingSettings.cxTotal = pSettings->x + pSettings->cx;
		else {
			int cxTotalMax = ((165LL * pSyncInfo->dwFrameDuration) / (pSyncInfo->wFrameLineCount * 10));
			timing.videoTimingSettings.cxTotal = min(cxTotalMax, wValue);
		}
	}
	else if (pScrollBar == &m_barHOffset) {
		strText.Format(_T("%d Pixels"), nPos);
		m_textHOffset.SetWindowText(strText);

		if (timing.videoTimingSettings.x == wValue)
			return;
		else if (wValue + pSettings->cx > pSettings->cxTotal)
			timing.videoTimingSettings.x = pSettings->cxTotal - pSettings->cx;
		else
			timing.videoTimingSettings.x = wValue;
	}
	else if (pScrollBar == &m_barVActive) {
		strText.Format(_T("%d Lines"), nPos);
		m_textVActive.SetWindowText(strText);

		if (timing.videoTimingSettings.cy == wValue)
			return;
		else if (wValue + pSettings->y > pSyncInfo->wFrameLineCount)
			timing.videoTimingSettings.cy = pSyncInfo->wFrameLineCount - pSettings->y;
		else
			timing.videoTimingSettings.cy = wValue;
	}
	else if (pScrollBar == &m_barVOffset) {
		strText.Format(_T("%d Lines"), nPos);
		m_textVOffset.SetWindowText(strText);

		if (timing.videoTimingSettings.y == wValue)
			return;
		else if (wValue + pSettings->cy > pSyncInfo->wFrameLineCount)
			timing.videoTimingSettings.y = pSyncInfo->wFrameLineCount - pSettings->cy;
		else
			timing.videoTimingSettings.y = wValue;
	}
	else if (pScrollBar == &m_barClampPos) {
		strText.Format(_T("%d Pixels"), nPos);
		m_textClampPos.SetWindowText(strText);

		timing.videoTimingSettings.byClampPos = (BYTE)wValue;
	}

	MWGetExtension()->SetCustomVideoTiming(&timing);

	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CTimingAdjustDialog::OnDeviceItem(UINT nID)
{
	int nIndex = nID - ID_CUSTOMIZE_BEGINE - 1;

	DWORD dwNum = MWCAP_VIDEO_MAX_NUM_PREFERRED_TIMINGS;
	MWCAP_VIDEO_TIMING szVideoTiming[MWCAP_VIDEO_MAX_NUM_PREFERRED_TIMINGS] = {0};
	HRESULT hr = MWGetExtension()->GetPreferredVideoTimings(szVideoTiming, &dwNum);

	hr = MWGetExtension()->SetVideoTiming(&szVideoTiming[nIndex]);
}

CString CTimingAdjustDialog::GetVideoTimingType( int nType )
{
	CString strType = _T("");
	switch (nType)
	{
	case 0x00:
		strType = _T("None");
		break;
	case 0x01:
		strType = _T("LEGACY");
		break;
	case 0x02:
		strType = _T("DMT");
		break;
	case 0x04:
		strType = _T("CEA");
		break;
	case 0x08:
		strType = _T("CVT");
		break;
	case 0x10:
		strType = _T("CVT");// RB");
		break;
	case 0x20:
		strType = _T("FAILSAFE");
		break;
	}
	return strType;
}

void CTimingAdjustDialog::UpdateTimingSettings()
{
	MWCAP_INPUT_SPECIFIC_STATUS status;
	MWGetExtension()->GetInputSpecificStatus(&status);

	MWCAP_VIDEO_SYNC_INFO * pSyncInfo = &status.vgaComponentStatus.syncInfo;
	MWCAP_VIDEO_TIMING_SETTINGS * pSettings = &status.vgaComponentStatus.videoTimingSettings;
	MWCAP_VIDEO_TIMING * pTiming = &status.vgaComponentStatus.videoTiming;

	BOOLEAN bValidTimings = status.bValid
		&& (status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_VGA || status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_COMPONENT);

	m_btnPreferred.EnableWindow(bValidTimings);

	m_barHActive.EnableWindow(bValidTimings);
	m_barHTotal.EnableWindow(bValidTimings);
	m_barHOffset.EnableWindow(bValidTimings);
	m_barVActive.EnableWindow(bValidTimings);
	m_barVOffset.EnableWindow(bValidTimings);
	m_barClampPos.EnableWindow(bValidTimings);

	m_spinAspectX.EnableWindow(bValidTimings);
	m_spinAspectY.EnableWindow(bValidTimings);

	m_btnReset.EnableWindow(bValidTimings);
	m_btnSave.EnableWindow(bValidTimings);

	if (!bValidTimings) {
		m_textPreferred.SetWindowText(_T("N/A"));
		m_textHActive.SetWindowText(_T("N/A"));
		m_textHTotal.SetWindowText(_T("N/A"));
		m_textHOffset.SetWindowText(_T("N/A"));
		m_textVActive.SetWindowText(_T("N/A"));
		m_textVOffset.SetWindowText(_T("N/A"));
		m_textClampPos.SetWindowText(_T("N/A"));
		m_spinAspectX.SetPos(4);
		m_spinAspectY.SetPos(3);
		return;
	}

	int nBefore = 0, nAfter = 0;
	GetVideoFPS(pSyncInfo->dwFrameDuration, pSyncInfo->bInterlaced, nBefore, nAfter);

	TCHAR szTemp[256];
	wsprintf(szTemp, 
		_T("%s, %dx%d%c, %d.%02d Hz"), 
		(pTiming->dwType == MWCAP_VIDEO_TIMING_NONE) ? _T("Custom") : GetVideoTimingType(pTiming->dwType),
		pSettings->cx,
		pSettings->cy,
		pSyncInfo->bInterlaced ? 'i' : 'p',
		nBefore, 
		nAfter
		);

	m_textPreferred.SetWindowText(szTemp);

	SCROLLINFO barInfo;

	barInfo.nMin = 512;
	barInfo.nMax = 2048;
	barInfo.nPage = 1;
	barInfo.nPos = pSettings->cx;
	m_barHActive.SetScrollRange(barInfo.nMin, barInfo.nMax);
	m_barHActive.SetScrollInfo(&barInfo, TRUE);

	barInfo.nMin = 1;
	barInfo.nMax = pSyncInfo->wFrameLineCount;
	barInfo.nPage = 1;
	barInfo.nPos = pSettings->cy;
	m_barVActive.SetScrollRange(barInfo.nMin, barInfo.nMax);
	m_barVActive.SetScrollInfo(&barInfo);

	barInfo.nMin = 512;
	barInfo.nMax = 4095;
	barInfo.nPage = 1;
	barInfo.nPos = pSettings->cxTotal;
	m_barHTotal.SetScrollRange(barInfo.nMin, barInfo.nMax);
	m_barHTotal.SetScrollInfo(&barInfo);

	barInfo.nMin = 0;
	barInfo.nMax = 2048;
	barInfo.nPage = 1;
	barInfo.nPos = pSettings->x;
	m_barHOffset.SetScrollRange(barInfo.nMin, barInfo.nMax);
	m_barHOffset.SetScrollInfo(&barInfo);

	barInfo.nMin = 0;
	barInfo.nMax = pSyncInfo->wFrameLineCount - 1;
	barInfo.nPage = 1;
	barInfo.nPos = pSettings->y;
	m_barVOffset.SetScrollRange(barInfo.nMin, barInfo.nMax);
	m_barVOffset.SetScrollInfo(&barInfo);

	barInfo.nMin = 0;
	barInfo.nMax = 255;
	barInfo.nPage = 1;
	barInfo.nPos = pSettings->byClampPos;
	m_barClampPos.SetScrollRange(barInfo.nMin, barInfo.nMax);
	m_barClampPos.SetScrollInfo(&barInfo);

	MWCAP_VIDEO_CAPS vCaps;
	HRESULT hr = MWGetExtension()->GetVideoCaps(&vCaps);
	if (hr == S_OK) {
		m_spinAspectX.SetRange(1, vCaps.wMaxInputWidth);
		m_spinAspectY.SetRange(1, vCaps.wMaxInputHeight);
	}
	else {
		m_spinAspectX.SetRange(1, 4096);
		m_spinAspectY.SetRange(1, 4096);
	}
	m_spinAspectX.SetPos(pSettings->wAspectX);
	m_spinAspectY.SetPos(pSettings->wAspectY);

	CString strText;
	strText.Format(_T("%d Pixels"), pSettings->cx);
	m_textHActive.SetWindowText(strText);
	strText.Format(_T("%d Lines"), pSettings->cy);
	m_textVActive.SetWindowText(strText);
	strText.Format(_T("%d Pixels"), pSettings->cxTotal);
	m_textHTotal.SetWindowText(strText);
	strText.Format(_T("%d Pixels"), pSettings->x);
	m_textHOffset.SetWindowText(strText);
	strText.Format(_T("%d Lines"), pSettings->y);
	m_textVOffset.SetWindowText(strText);
	strText.Format(_T("%d Pixels"), pSettings->byClampPos);
	m_textClampPos.SetWindowText(strText);
}
