#pragma once
#include "afxcmn.h"
#include "DShowCapture.h"

// CHDMIDialog 对话框

class CHDMIDialog : public CDialogEx, public ISignalNotify
{
	DECLARE_DYNAMIC(CHDMIDialog)

public:
	CHDMIDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CHDMIDialog();

// 对话框数据
	enum { IDD = IDD_DIALOG_HDMI };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

private:
	CEdit			m_edit;
	CStatic			m_textHDMIInfo;

	void Num2HexStr(int nNum, char* pszBegin);
public:
	void UpdateHDMIInfo(MWCAP_HDMI_INFOFRAME_ID hdmiInfoID, int nInfoIDPage);
	void DeviceChange();

	// ISignalNotify
	virtual void OnHDMIInfoStatuChanged(ULONGLONG ullStatusBits);

	DECLARE_MESSAGE_MAP()
public:
	CTabCtrl m_tabMisc;
	virtual BOOL OnInitDialog();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnBnClickedButtonReset();
	afx_msg void OnBnClickedButtonLoad();
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnTcnSelchangeTabHdmiInfo(NMHDR *pNMHDR, LRESULT *pResult);
};
