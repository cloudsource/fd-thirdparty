#pragma once
#include "afxcmn.h"

#include "VideoOutputDialog.h"
#include "DShowCapture.h"


// CVideoDialog 对话框

class CVideoDialog : public CDialogEx, public ISignalNotify
{
	DECLARE_DYNAMIC(CVideoDialog)

public:
	CVideoDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CVideoDialog();

// 对话框数据
	enum { IDD = IDD_DIALOG_VIDEO };

	void SetVideoInputStatus();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

private:
	CStatic		m_groupInputSignal;
	CButton		m_ckbAspect;
	CSpinButtonCtrl m_spinAspectX;
	CSpinButtonCtrl m_spinAspectY;
	CButton		m_ckbColorFormat;
	CButton		m_ckbQuantization;
	CComboBox	m_cmbColorFormat;
	CComboBox	m_cmbQuantization;

	CTabCtrl	m_tabVideoPin;

	CVideoOutputDialog	m_dlgVideoOutput;

public:
	virtual void OnInputSourceChanged(ULONGLONG ullStatusBits);
	void UpdateStatus();

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	
	afx_msg void OnBnClickedAspect();
	afx_msg void OnBnClickedColorFormat();
	afx_msg void OnBnClickedQuant();
	afx_msg void OnCbnSelchangeComboFormat();
	afx_msg void OnCbnSelchangeComboQuant();
	afx_msg void OnDeltaposSpinInputAspectW(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinInputAspectH(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTcnSelchangeTabVideopin(NMHDR *pNMHDR, LRESULT *pResult);
};
