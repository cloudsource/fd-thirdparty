// OSDDialog.cpp : implementation file
//

#include "stdafx.h"
#include "FilterProperty.h"
#include "OSDDialog.h"
#include "afxdialogex.h"

// COSDDialog dialog

IMPLEMENT_DYNAMIC(COSDDialog, CDialogEx)

COSDDialog::COSDDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(COSDDialog::IDD, pParent)
{

}

COSDDialog::~COSDDialog()
{
}

void COSDDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_OSD, m_tabOSD);
}


BEGIN_MESSAGE_MAP(COSDDialog, CDialogEx)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_OSD, &COSDDialog::OnTcnSelchangeTabOsd)
END_MESSAGE_MAP()


// COSDDialog message handlers
BOOL COSDDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	m_tabOSD.InsertItem(0, _T("Capture"));
	m_tabOSD.InsertItem(1, _T("Preview"));

	m_tabOSD.SetCurSel(1);

	m_dlgTabShow.Create(COSDTabDialog::IDD, this);

	CRect rcBorder;
	CWnd *pBorder = GetDlgItem(IDC_STATIC_OSD_TAB_DLG);
	pBorder->GetWindowRect(rcBorder);
	ScreenToClient(rcBorder);

	m_dlgTabShow.SetWindowPos(pBorder, rcBorder.left, rcBorder.top, rcBorder.Width(), rcBorder.Height(), SWP_NOREDRAW);

	m_dlgTabShow.ShowWindow(SW_SHOW);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void COSDDialog::OnTcnSelchangeTabOsd(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;

	m_dlgTabShow.DisplayOSDInfo(m_tabOSD.GetCurSel() == 0 ? TRUE : FALSE);
}

void COSDDialog::ChannelChanged()
{
	m_dlgTabShow.DisplayOSDInfo(m_tabOSD.GetCurSel() == 0 ? TRUE : FALSE);
}
