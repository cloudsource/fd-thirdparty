
// FilterPropertyDlg.h : header file
//

#pragma once

#include "afxcmn.h"

#include "TimingDialog.h"
#include "OSDDialog.h"
#include "HDMIDialog.h"
#include "VideoDialog.h"
#include "InfoDialog.h"
#include "InputDialog.h"
#include "ProcAmpDialog.h"
#include "GdiPlusRenderer.h"

#include "afxwin.h"

// CFilterPropertyDlg dialog
class CFilterPropertyDlg : public CDialogEx
{
// Construction
public:
	CFilterPropertyDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_FILTERPROPERTY_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

// Implementation

private:
	CGdiPlusRenderer m_renderer;

	BOOL OpenChannel(int nChannelIndex);

protected:
	HICON m_hIcon;

	CTimingDialog	m_dlgTiming;
	COSDDialog		m_dlgOSD;
	CHDMIDialog		m_dlgHDMI;
	CVideoDialog	m_dlgVideo;
	CInfoDialog		m_dlgInfo;
	CInputDialog	m_dlgInput;
	CProcAmpDialog	m_dlgProcAmp;

	CTabCtrl		m_tabProperty;
	CComboBox		m_cmbChannels;

	CWnd			m_wndPreview;
	int				m_nVideoIndex;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:

	afx_msg void OnTcnSelchangeTabProperty(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCbnSelchangeComboChannel();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
};
