#pragma once

// CVideoOutputDialog dialog

#include "LibMWCapture/MWCaptureExtension.h"

class CVideoOutputDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CVideoOutputDialog)

public:
	CVideoOutputDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CVideoOutputDialog();

// Dialog Data
	enum { IDD = IDD_DIALOG_VIDEO_OUTPUT };

protected:
	CStatic		m_textOutputFormat;
	CButton		m_ckbAspect;
	CButton		m_ckbCropInput;
	CButton		m_ckbColorFormat;
	CButton		m_ckbQuant;
	CButton		m_ckbSaturation;
	CButton		m_btnLoadPresent;
	CButton		m_btnSavePresent;

	CButton			m_ckbLowLatency;
	CSpinButtonCtrl	m_spinOutputAspectX;
	CSpinButtonCtrl	m_spinOutputAspectY;
	CSpinButtonCtrl	m_spinCropLeft;
	CSpinButtonCtrl	m_spinCropTop;
	CSpinButtonCtrl	m_spinCropRight;
	CSpinButtonCtrl	m_spinCropBottom;

	CComboBox	m_cmbOutputFormat;
	CComboBox	m_cmbOutputQuant;
	CComboBox	m_cmbOutputSaturation;
	CComboBox	m_cmbOutputDeint;
	CComboBox	m_cmbOutputAR;

	BOOL		m_bVideoCapture;
	
	//modify
	int			m_nOutputAspectXDefault;
	int			m_nOutputAspectYDefault;

public:
	void UpdateVideoInfo(BOOL bVideoCapture);

private:
	void GetVideoSettings(BOOL bVideoCapture, MWCAP_VIDEO_PROCESS_SETTINGS& settings);
	void SetVideoSettings(BOOL bVideoCapture, MWCAP_VIDEO_PROCESS_SETTINGS settings);
	void UpdateVideoSettings(MWCAP_VIDEO_CONNECTION_FORMAT videoFormat, MWCAP_VIDEO_PROCESS_SETTINGS videoProcessSettings);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();

	afx_msg void OnBnClickedCheckVideoCaptureAspect();
	afx_msg void OnBnClickedCheckVideoCaptureCropInput();
	afx_msg void OnBnClickedCheckVideoCaptureFormat();
	afx_msg void OnBnClickedCheckVideoCaptureQuant();
	afx_msg void OnBnClickedCheckVideoCaptureSaturation();
	afx_msg void OnCbnSelchangeComboCaptureFormat();
	afx_msg void OnCbnSelchangeComboCaptureQuant();
	afx_msg void OnCbnSelchangeComboCaptureSaturation();
	afx_msg void OnCbnSelchangeComboCaptureDeint();
	afx_msg void OnCbnSelchangeComboCaptureAr();
	afx_msg void OnBnClickedButtonVideoCaptureLoad();
	afx_msg void OnBnClickedButtonVideoCaptureSave();
	afx_msg void OnDeltaposSpinVideoCaptureAspectX(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinVideoCaptureAspectY(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinVideoCaptureCropLeft(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinVideoCaptureCropTop(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinVideoCaptureCropRight(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinVideoCaptureCropBottom(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedCheckIfLowLatency();
};
