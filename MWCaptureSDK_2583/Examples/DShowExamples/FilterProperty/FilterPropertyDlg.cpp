
// FilterPropertyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FilterProperty.h"
#include "FilterPropertyDlg.h"
#include "afxdialogex.h"

#include "DShowCapture.h"
#include "MWFOURCC.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define CAPTURE_COLORSPACE		MWFOURCC_RGB24//MWFOURCC_YUYV //MWFOURCC_RGB24
#define CAPTURE_WIDTH			720
#define CAPTURE_HEIGHT			576

#define TIMER_ID		1

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CFilterPropertyDlg dialog




CFilterPropertyDlg::CFilterPropertyDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CFilterPropertyDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFilterPropertyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_PROPERTY, m_tabProperty);
	DDX_Control(pDX, IDC_COMBO_CHANNEL, m_cmbChannels);
	DDX_Control(pDX, IDC_STATIC_PREVIEW, m_wndPreview);
}

BEGIN_MESSAGE_MAP(CFilterPropertyDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_PROPERTY, &CFilterPropertyDlg::OnTcnSelchangeTabProperty)
	ON_CBN_SELCHANGE(IDC_COMBO_CHANNEL, &CFilterPropertyDlg::OnCbnSelchangeComboChannel)
	ON_WM_TIMER()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()


// CFilterPropertyDlg message handlers

BOOL CFilterPropertyDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	g_capture.SetWndPreview(&m_wndPreview);
	int nVideo = g_capture.GetDeviceCount(TRUE);
	for (int i = 0; i < nVideo; i ++) {
		m_cmbChannels.InsertString(i, g_capture.GetDeviceName(TRUE, i));
	}
	m_cmbChannels.SetCurSel(0);

	m_nVideoIndex = 0;
	if (nVideo > 0)
		OpenChannel(m_nVideoIndex);
	
	if (MWGetExtension() == NULL) {
		MessageBox(_T("Cannot find capture device"), _T("ERROR"), MB_OK);
		AfxGetMainWnd()->SendMessage(WM_CLOSE);
		return FALSE;
	}

	m_tabProperty.InsertItem(0, _T("Timing"));
	m_tabProperty.InsertItem(1, _T("OSD"));
	m_tabProperty.InsertItem(2, _T("HDMI"));
	m_tabProperty.InsertItem(3, _T("Video"));
	m_tabProperty.InsertItem(4, _T("Info"));
	m_tabProperty.InsertItem(5, _T("Input"));
	m_tabProperty.InsertItem(6, _T("Video Proc Amp"));

	m_dlgTiming.Create(CTimingDialog::IDD, this);
	m_dlgOSD.Create(COSDDialog::IDD, this);
	m_dlgHDMI.Create(CHDMIDialog::IDD, this);
	m_dlgVideo.Create(CVideoDialog::IDD, this);
	m_dlgInfo.Create(CInfoDialog::IDD, this);
	m_dlgInput.Create(CInputDialog::IDD, this);
	m_dlgProcAmp.Create(CProcAmpDialog::IDD, this);

	CRect rcBorder;
	CWnd *pBorder = GetDlgItem(IDC_STATIC_PROPERTYDLG);
	pBorder->GetWindowRect(rcBorder);
	ScreenToClient(rcBorder);

	m_dlgTiming.SetWindowPos(pBorder, rcBorder.left, rcBorder.top, rcBorder.Width(), rcBorder.Height(), SWP_NOREDRAW);
	m_dlgOSD.SetWindowPos(pBorder, rcBorder.left, rcBorder.top, rcBorder.Width(), rcBorder.Height(), SWP_NOREDRAW);
	m_dlgHDMI.SetWindowPos(pBorder, rcBorder.left, rcBorder.top, rcBorder.Width(), rcBorder.Height(), SWP_NOREDRAW);
	m_dlgVideo.SetWindowPos(pBorder, rcBorder.left, rcBorder.top, rcBorder.Width(), rcBorder.Height(), SWP_NOREDRAW);
	m_dlgInfo.SetWindowPos(pBorder, rcBorder.left, rcBorder.top, rcBorder.Width(), rcBorder.Height(), SWP_NOREDRAW);
	m_dlgInput.SetWindowPos(pBorder, rcBorder.left, rcBorder.top, rcBorder.Width(), rcBorder.Height(), SWP_NOREDRAW);
	m_dlgProcAmp.SetWindowPos(pBorder, rcBorder.left, rcBorder.top, rcBorder.Width(), rcBorder.Height(), SWP_NOREDRAW);

	m_tabProperty.SetCurSel(4);
	m_dlgInfo.ShowWindow(SW_SHOW);

	m_renderer.Init(CAPTURE_WIDTH, CAPTURE_HEIGHT);

	SetTimer(TIMER_ID, 500, NULL);	

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CFilterPropertyDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CFilterPropertyDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CFilterPropertyDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CFilterPropertyDlg::OnTcnSelchangeTabProperty(NMHDR *pNMHDR, LRESULT *pResult)
{
	m_dlgTiming.ShowWindow(SW_HIDE);
	m_dlgOSD.ShowWindow(SW_HIDE);
	m_dlgHDMI.ShowWindow(SW_HIDE);
	m_dlgVideo.ShowWindow(SW_HIDE);
	m_dlgInfo.ShowWindow(SW_HIDE);
	m_dlgInput.ShowWindow(SW_HIDE);
	m_dlgProcAmp.ShowWindow(SW_HIDE);

	int nIndex = m_tabProperty.GetCurSel();
	switch (nIndex) {
	case 0:
		m_dlgTiming.ShowWindow(SW_SHOW);
		break;
	case 1:
		m_dlgOSD.ShowWindow(SW_SHOW);
		break;
	case 2:
		m_dlgHDMI.ShowWindow(SW_SHOW);
		break;
	case 3:
		m_dlgVideo.ShowWindow(SW_SHOW);
		m_dlgVideo.UpdateStatus();
		break;
	case 4:
		m_dlgInfo.ShowWindow(SW_SHOW);
		break;
	case 5:
		m_dlgInput.ShowWindow(SW_SHOW);
		break;
	case 6:
		m_dlgProcAmp.ShowWindow(SW_SHOW);
		break;
	}
	*pResult = 0;
}

void CFilterPropertyDlg::OnCbnSelchangeComboChannel()
{
	// TODO: Add your control notification handler code here
	int nCurSel = m_cmbChannels.GetCurSel();
	if (m_nVideoIndex == nCurSel)
		return;

	m_nVideoIndex = nCurSel;
	OpenChannel(nCurSel);

	m_dlgInfo.UpdateInfoTable();

	m_dlgHDMI.DeviceChange();

	m_dlgTiming.UpdateTimingSettings();

	m_dlgProcAmp.UpdateAmpProperty();

	m_dlgInput.SetAVInputType();
	m_dlgInput.InputSourceChange();

	m_dlgVideo.SetVideoInputStatus();
	m_dlgVideo.UpdateStatus();

	m_dlgOSD.ChannelChanged();
}


void CFilterPropertyDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default

	m_dlgInfo.UpdateCoreTemperature();

	CDialogEx::OnTimer(nIDEvent);
}


int CFilterPropertyDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here

	return 0;
}


void CFilterPropertyDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: Add your message handler code here
	KillTimer(TIMER_ID);

	g_capture.Close();
}

BOOL CFilterPropertyDlg::OpenChannel( int nChannelIndex )
{
	g_capture.Open(nChannelIndex);

	return TRUE;
}


BOOL CFilterPropertyDlg::PreTranslateMessage( MSG* pMsg )
{
	if (pMsg->message==WM_KEYDOWN) {
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		}

	}
	return CDialogEx::PreTranslateMessage(pMsg);
}
