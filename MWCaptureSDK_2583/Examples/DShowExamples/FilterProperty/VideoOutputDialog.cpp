// VideoOutputDialog.cpp : implementation file
//

#include "stdafx.h"
#include "FilterProperty.h"
#include "VideoOutputDialog.h"
#include "afxdialogex.h"

#include "DShowCapture.h"

// CVideoOutputDialog dialog

IMPLEMENT_DYNAMIC(CVideoOutputDialog, CDialogEx)

CVideoOutputDialog::CVideoOutputDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CVideoOutputDialog::IDD, pParent)
{

}

CVideoOutputDialog::~CVideoOutputDialog()
{
}

void CVideoOutputDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_VIDEO_OUTPUT_FORMAT, m_textOutputFormat);
	DDX_Control(pDX, IDC_CHECK_VIDEO_OUTPUT_RASPECT, m_ckbAspect);
	DDX_Control(pDX, IDC_CHECK_VIDEO_OUTPUT_CROP_INPUT, m_ckbCropInput);
	DDX_Control(pDX, IDC_CHECK_VIDEO_OUTPUT_FORMAT, m_ckbColorFormat);
	DDX_Control(pDX, IDC_CHECK_VIDEO_OUTPUT_QUANT, m_ckbQuant);
	DDX_Control(pDX, IDC_CHECK_VIDEO_OUTPUT_SATURATION, m_ckbSaturation);
	DDX_Control(pDX, IDC_BUTTON_VIDEO_OUTPUT_LOAD, m_btnLoadPresent);
	DDX_Control(pDX, IDC_BUTTON_VIDEO_OUTPUT_SAVE, m_btnSavePresent);

	DDX_Control(pDX, IDC_COMBO_OUTPUT_FORMAT, m_cmbOutputFormat);
	DDX_Control(pDX, IDC_COMBO_OUTPUT_QUANT, m_cmbOutputQuant);
	DDX_Control(pDX, IDC_COMBO_OUTPUT_SATURATION, m_cmbOutputSaturation);
	DDX_Control(pDX, IDC_COMBO_OUTPUT_DEINT, m_cmbOutputDeint);
	DDX_Control(pDX, IDC_COMBO_OUTPUT_AR, m_cmbOutputAR);

	DDX_Control(pDX, IDC_CHECK_VIDEO_OUTPUT_LOW_LATENCY, m_ckbLowLatency);
	DDX_Control(pDX, IDC_SPIN_VIDEO_OUTPU_ASPECT_WIDTH, m_spinOutputAspectX);
	DDX_Control(pDX, IDC_SPIN_VIDEO_OUTPU_ASPECT_HEIGHT, m_spinOutputAspectY);
	DDX_Control(pDX, IDC_SPIN_VIDEO_OUTPU_CROP_WIDTH1, m_spinCropLeft);
	DDX_Control(pDX, IDC_SPIN_VIDEO_OUTPU_CROP_WIDTH2, m_spinCropTop);
	DDX_Control(pDX, IDC_SPIN_VIDEO_OUTPU_CROP_HEIGHT1, m_spinCropRight);
	DDX_Control(pDX, IDC_SPIN_VIDEO_OUTPU_CROP_HEIGHT2, m_spinCropBottom);

}


BEGIN_MESSAGE_MAP(CVideoOutputDialog, CDialogEx)
	ON_BN_CLICKED(IDC_CHECK_VIDEO_OUTPUT_RASPECT, &CVideoOutputDialog::OnBnClickedCheckVideoCaptureAspect)
	ON_BN_CLICKED(IDC_CHECK_VIDEO_OUTPUT_CROP_INPUT, &CVideoOutputDialog::OnBnClickedCheckVideoCaptureCropInput)
	ON_BN_CLICKED(IDC_CHECK_VIDEO_OUTPUT_FORMAT, &CVideoOutputDialog::OnBnClickedCheckVideoCaptureFormat)
	ON_BN_CLICKED(IDC_CHECK_VIDEO_OUTPUT_QUANT, &CVideoOutputDialog::OnBnClickedCheckVideoCaptureQuant)
	ON_BN_CLICKED(IDC_CHECK_VIDEO_OUTPUT_SATURATION, &CVideoOutputDialog::OnBnClickedCheckVideoCaptureSaturation)
	ON_CBN_SELCHANGE(IDC_COMBO_OUTPUT_FORMAT, &CVideoOutputDialog::OnCbnSelchangeComboCaptureFormat)
	ON_CBN_SELCHANGE(IDC_COMBO_OUTPUT_QUANT, &CVideoOutputDialog::OnCbnSelchangeComboCaptureQuant)
	ON_CBN_SELCHANGE(IDC_COMBO_OUTPUT_SATURATION, &CVideoOutputDialog::OnCbnSelchangeComboCaptureSaturation)
	ON_CBN_SELCHANGE(IDC_COMBO_OUTPUT_DEINT, &CVideoOutputDialog::OnCbnSelchangeComboCaptureDeint)
	ON_CBN_SELCHANGE(IDC_COMBO_OUTPUT_AR, &CVideoOutputDialog::OnCbnSelchangeComboCaptureAr)
	ON_BN_CLICKED(IDC_BUTTON_VIDEO_OUTPUT_LOAD, &CVideoOutputDialog::OnBnClickedButtonVideoCaptureLoad)
	ON_BN_CLICKED(IDC_BUTTON_VIDEO_OUTPUT_SAVE, &CVideoOutputDialog::OnBnClickedButtonVideoCaptureSave)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_VIDEO_OUTPU_ASPECT_WIDTH, &CVideoOutputDialog::OnDeltaposSpinVideoCaptureAspectX)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_VIDEO_OUTPU_ASPECT_HEIGHT, &CVideoOutputDialog::OnDeltaposSpinVideoCaptureAspectY)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_VIDEO_OUTPU_CROP_WIDTH1, &CVideoOutputDialog::OnDeltaposSpinVideoCaptureCropLeft)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_VIDEO_OUTPU_CROP_WIDTH2, &CVideoOutputDialog::OnDeltaposSpinVideoCaptureCropTop)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_VIDEO_OUTPU_CROP_HEIGHT1, &CVideoOutputDialog::OnDeltaposSpinVideoCaptureCropRight)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_VIDEO_OUTPU_CROP_HEIGHT2, &CVideoOutputDialog::OnDeltaposSpinVideoCaptureCropBottom)
	ON_BN_CLICKED(IDC_CHECK_IF_LOW_LATENCY, &CVideoOutputDialog::OnBnClickedCheckIfLowLatency)
END_MESSAGE_MAP()

BOOL CVideoOutputDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_bVideoCapture = FALSE;

	m_spinOutputAspectX.SetRange(0, 100);
	m_spinOutputAspectY.SetRange(0, 100);

	m_spinCropLeft.SetRange(0, 4096);
	m_spinCropTop.SetRange(0, 4096);
	m_spinCropRight.SetRange(0, 4096);
	m_spinCropBottom.SetRange(0, 4096);

	m_cmbOutputFormat.InsertString(0, L"RGB");
	m_cmbOutputFormat.InsertString(1, L"YUV BT.601");
	m_cmbOutputFormat.InsertString(2, L"YUV BT.709");
	m_cmbOutputFormat.InsertString(3, L"YUV BT.2020");
	m_cmbOutputFormat.SetCurSel(0);

	m_cmbOutputQuant.InsertString(0, L"Full range");
	m_cmbOutputQuant.InsertString(1, L"Limited range");
	m_cmbOutputQuant.SetCurSel(0);

	m_cmbOutputSaturation.InsertString(0, L"Full range");
	m_cmbOutputSaturation.InsertString(1, L"Limited range");
	m_cmbOutputSaturation.InsertString(2, L"Extended gamut");
	m_cmbOutputSaturation.SetCurSel(0);

	m_cmbOutputDeint.InsertString(0, L"Weave");
	m_cmbOutputDeint.InsertString(1, L"Blend top & bottom fields");
	m_cmbOutputDeint.InsertString(2, L"Top field only");
	m_cmbOutputDeint.InsertString(3, L"Bottom field only");
	m_cmbOutputDeint.SetCurSel(0);

	m_cmbOutputAR.InsertString(0, L"Ignore");
	m_cmbOutputAR.InsertString(1, L"Cropping");
	m_cmbOutputAR.InsertString(2, L"Padding");
	m_cmbOutputAR.SetCurSel(0);	

	m_nOutputAspectXDefault = 0;
	m_nOutputAspectYDefault = 0;

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

// CVideoOutputDialog message handlers


void CVideoOutputDialog::OnBnClickedCheckVideoCaptureAspect()
{
	// TODO: Add your control notification handler code here
	BOOL bCheck = m_ckbAspect.GetCheck();
	m_spinOutputAspectX.EnableWindow(bCheck);
	m_spinOutputAspectY.EnableWindow(bCheck);

	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);
	/*MWCAP_VIDEO_SIGNAL_STATUS videoSignalStatus;
	HRESULT hr = MWGetExtension()->GetVideoSignalStatus(&videoSignalStatus);
	if (!bCheck) {
		videoSettings.nAspectX = 0;
		videoSettings.nAspectY = 0;
	}
	else {
		videoSettings.nAspectX = videoSignalStatus.nAspectX;
		videoSettings.nAspectY = videoSignalStatus.nAspectY;
	}*/

	videoSettings.nAspectX = m_nOutputAspectXDefault;
	videoSettings.nAspectY = m_nOutputAspectYDefault;
	m_spinOutputAspectX.SetPos(m_nOutputAspectXDefault);
	m_spinOutputAspectY.SetPos(m_nOutputAspectYDefault);
	SetVideoSettings(m_bVideoCapture, videoSettings);
}


void CVideoOutputDialog::OnBnClickedCheckVideoCaptureCropInput()
{
	// TODO: Add your control notification handler code here
	BOOL bCheck = m_ckbCropInput.GetCheck();
	m_spinCropLeft.EnableWindow(bCheck);
	m_spinCropTop.EnableWindow(bCheck);
	m_spinCropRight.EnableWindow(bCheck);
	m_spinCropBottom.EnableWindow(bCheck);

	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);
	MWCAP_VIDEO_SIGNAL_STATUS videoSignalStatus;
	HRESULT hr = MWGetExtension()->GetVideoSignalStatus(&videoSignalStatus);
	if (!bCheck) {
		videoSettings.rectSource.left = 0;
		videoSettings.rectSource.top = 0;
		videoSettings.rectSource.right = 0;
		videoSettings.rectSource.bottom = 0;

		m_spinCropLeft.SetPos(0);
		m_spinCropTop.SetPos(0);
		m_spinCropRight.SetPos(videoSignalStatus.cx);
		m_spinCropBottom.SetPos(videoSignalStatus.cy);
	}
	else {
		videoSettings.rectSource.left = 0;
		videoSettings.rectSource.top = 0;
		videoSettings.rectSource.right = videoSignalStatus.cx;
		videoSettings.rectSource.bottom = videoSignalStatus.cy;		
	}
	SetVideoSettings(m_bVideoCapture, videoSettings);
}


void CVideoOutputDialog::OnBnClickedCheckVideoCaptureFormat()
{
	// TODO: Add your control notification handler code here
	BOOL bCheck = m_ckbColorFormat.GetCheck();
	m_cmbOutputFormat.EnableWindow(bCheck);

	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);
	if (!bCheck) {
		videoSettings.colorFormat = MWCAP_VIDEO_COLOR_FORMAT_UNKNOWN;

		MWCAP_VIDEO_CONNECTION_FORMAT videoFormat; 
		if (m_bVideoCapture)
			HRESULT hr = MWGetExtension()->GetVideoCaptureConnectionFormat(&videoFormat);
		else
			HRESULT hr = MWGetExtension()->GetVideoPreviewConnectionFormat(&videoFormat);
		int nSel = 0;
		switch (videoFormat.colorFormat)
		{
		case MWCAP_VIDEO_COLOR_FORMAT_RGB:
			nSel = 0;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV601:
			nSel = 1;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV709:
			nSel = 2;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV2020:
			nSel = 3;
			break;
		}
		m_cmbOutputFormat.SetCurSel(nSel);
	}
	else {
		int nSel = m_cmbOutputFormat.GetCurSel();
		switch (nSel)
		{
		case 0:
			videoSettings.colorFormat = MWCAP_VIDEO_COLOR_FORMAT_RGB;
			break;
		case 1:
			videoSettings.colorFormat = MWCAP_VIDEO_COLOR_FORMAT_YUV601;
			break;
		case 2:
			videoSettings.colorFormat = MWCAP_VIDEO_COLOR_FORMAT_YUV709;
			break;
		case 3:
			videoSettings.colorFormat = MWCAP_VIDEO_COLOR_FORMAT_YUV2020;
			break;
		}
	}
	SetVideoSettings(m_bVideoCapture, videoSettings);
}


void CVideoOutputDialog::OnBnClickedCheckVideoCaptureQuant()
{
	// TODO: Add your control notification handler code here
	BOOL bCheck = m_ckbQuant.GetCheck();
	m_cmbOutputQuant.EnableWindow(bCheck);

	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);
	if (!bCheck) {
		videoSettings.quantRange = MWCAP_VIDEO_QUANTIZATION_UNKNOWN;

		MWCAP_VIDEO_CONNECTION_FORMAT videoFormat; 
		if (m_bVideoCapture)
			HRESULT hr = MWGetExtension()->GetVideoCaptureConnectionFormat(&videoFormat);
		else
			HRESULT hr = MWGetExtension()->GetVideoPreviewConnectionFormat(&videoFormat);
		int nSel = 0;
		switch (videoFormat.quantRange)
		{
		case MWCAP_VIDEO_QUANTIZATION_FULL:
			nSel = 0;
			break;
		case MWCAP_VIDEO_QUANTIZATION_LIMITED:
			nSel = 1;
			break;
		}
		m_cmbOutputQuant.SetCurSel(nSel);
	}
	else {
		int nSel = m_cmbOutputQuant.GetCurSel();
		switch (nSel)
		{
		case 0:
			videoSettings.quantRange = MWCAP_VIDEO_QUANTIZATION_FULL;
			break;
		case 1:
			videoSettings.quantRange = MWCAP_VIDEO_QUANTIZATION_LIMITED;
			break;
		}
	}
	SetVideoSettings(m_bVideoCapture, videoSettings);
}


void CVideoOutputDialog::OnBnClickedCheckVideoCaptureSaturation()
{
	// TODO: Add your control notification handler code here
	BOOL bCheck = m_ckbSaturation.GetCheck();
	m_cmbOutputSaturation.EnableWindow(bCheck);

	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);
	if (!bCheck) {
		videoSettings.satRange = MWCAP_VIDEO_SATURATION_UNKNOWN;

		MWCAP_VIDEO_CONNECTION_FORMAT videoFormat; 
		if (m_bVideoCapture)
			HRESULT hr = MWGetExtension()->GetVideoCaptureConnectionFormat(&videoFormat);
		else
			HRESULT hr = MWGetExtension()->GetVideoPreviewConnectionFormat(&videoFormat);
		int nSel = 0;
		switch (videoFormat.satRange)
		{
		case MWCAP_VIDEO_SATURATION_FULL:
			nSel = 0;
			break;
		case MWCAP_VIDEO_SATURATION_LIMITED:
			nSel = 1;
			break;
		case MWCAP_VIDEO_SATURATION_EXTENDED_GAMUT:
			nSel = 2;
			break;
		}
		m_cmbOutputSaturation.SetCurSel(nSel);
	}
	else {
		int nSel = m_cmbOutputSaturation.GetCurSel();
		switch (nSel)
		{
		case 0:
			videoSettings.satRange = MWCAP_VIDEO_SATURATION_FULL;
			break;
		case 1:
			videoSettings.satRange = MWCAP_VIDEO_SATURATION_LIMITED;
			break;
		case 2:
			videoSettings.satRange = MWCAP_VIDEO_SATURATION_EXTENDED_GAMUT;
			break;
		}
	}
	SetVideoSettings(m_bVideoCapture, videoSettings);
}


void CVideoOutputDialog::OnCbnSelchangeComboCaptureFormat()
{
	// TODO: Add your control notification handler code here
	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);

	int nSel = m_cmbOutputFormat.GetCurSel();
	switch (nSel)
	{
	case 0:
		videoSettings.colorFormat = MWCAP_VIDEO_COLOR_FORMAT_RGB;
		break;
	case 1:
		videoSettings.colorFormat = MWCAP_VIDEO_COLOR_FORMAT_YUV601;
		break;
	case 2:
		videoSettings.colorFormat = MWCAP_VIDEO_COLOR_FORMAT_YUV709;
		break;
	case 3:
		videoSettings.colorFormat = MWCAP_VIDEO_COLOR_FORMAT_YUV2020;
		break;
	default:
		break;
	}
	SetVideoSettings(m_bVideoCapture, videoSettings);
}


void CVideoOutputDialog::OnCbnSelchangeComboCaptureQuant()
{
	// TODO: Add your control notification handler code here
	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);

	int nSel = m_cmbOutputFormat.GetCurSel();
	switch (nSel)
	{
	case 0:
		videoSettings.quantRange = MWCAP_VIDEO_QUANTIZATION_FULL;
		break;
	case 1:
		videoSettings.quantRange = MWCAP_VIDEO_QUANTIZATION_LIMITED;
		break;
	default:
		break;
	}
	SetVideoSettings(m_bVideoCapture, videoSettings);
}


void CVideoOutputDialog::OnCbnSelchangeComboCaptureSaturation()
{
	// TODO: Add your control notification handler code here
	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);

	int nSel = m_cmbOutputFormat.GetCurSel();
	switch (nSel)
	{
	case 0:
		videoSettings.satRange = MWCAP_VIDEO_SATURATION_FULL;
		break;
	case 1:
		videoSettings.satRange = MWCAP_VIDEO_SATURATION_LIMITED;
		break;
	case 2:
		videoSettings.satRange = MWCAP_VIDEO_SATURATION_EXTENDED_GAMUT;
		break;
	default:
		break;
	}
	SetVideoSettings(m_bVideoCapture, videoSettings);
}


void CVideoOutputDialog::OnCbnSelchangeComboCaptureDeint()
{
	// TODO: Add your control notification handler code here
	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);

	int nSel = m_cmbOutputDeint.GetCurSel();
	switch (nSel)
	{
	case 0:
		videoSettings.deinterlaceMode = MWCAP_VIDEO_DEINTERLACE_WEAVE;
		break;
	case 1:
		videoSettings.deinterlaceMode = MWCAP_VIDEO_DEINTERLACE_BLEND;
		break;
	case 2:
		videoSettings.deinterlaceMode = MWCAP_VIDEO_DEINTERLACE_TOP_FIELD;
		break;
	case 3:
		videoSettings.deinterlaceMode = MWCAP_VIDEO_DEINTERLACE_BOTTOM_FIELD;
		break;
	default:
		break;
	}
	SetVideoSettings(m_bVideoCapture, videoSettings);
}


void CVideoOutputDialog::OnCbnSelchangeComboCaptureAr()
{
	// TODO: Add your control notification handler code here
	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);

	int nSel = m_cmbOutputAR.GetCurSel();
	switch (nSel)
	{
	case 0:
		videoSettings.aspectRatioConvertMode = MWCAP_VIDEO_ASPECT_RATIO_IGNORE;
		break;
	case 1:
		videoSettings.aspectRatioConvertMode = MWCAP_VIDEO_ASPECT_RATIO_CROPPING;
		break;
	case 2:
		videoSettings.aspectRatioConvertMode = MWCAP_VIDEO_ASPECT_RATIO_PADDING;
		break;
	default:
		break;
	}
	SetVideoSettings(m_bVideoCapture, videoSettings);
}


void CVideoOutputDialog::OnBnClickedButtonVideoCaptureLoad()
{
	// TODO: Add your control notification handler code here	
	MWCAP_VIDEO_PROCESS_SETTINGS videoPreset;
	MWCAP_VIDEO_CONNECTION_FORMAT videoFormat; 
	HRESULT hr = S_FALSE;

	if (m_bVideoCapture) {
		hr = MWGetExtension()->GetVideoCaptureProcessPreset(&videoPreset);
		hr = MWGetExtension()->SetVideoCaptureProcessSettings(&videoPreset);
		hr = MWGetExtension()->GetVideoCaptureConnectionFormat(&videoFormat);
	}
	else {
		hr = MWGetExtension()->GetVideoPreviewProcessPreset(&videoPreset);
		hr = MWGetExtension()->SetVideoPreviewProcessSettings(&videoPreset);
		hr = MWGetExtension()->GetVideoPreviewConnectionFormat(&videoFormat);
	}

	UpdateVideoSettings(videoFormat, videoPreset);
}


void CVideoOutputDialog::OnBnClickedButtonVideoCaptureSave()
{
	// TODO: Add your control notification handler code here
	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	HRESULT hr = S_FALSE;

	if (m_bVideoCapture) {
		hr = MWGetExtension()->GetVideoCaptureProcessSettings(&videoSettings);
		hr = MWGetExtension()->SetVideoCaptureProcessPreset(&videoSettings);
	}
	else {
		hr = MWGetExtension()->GetVideoPreviewProcessSettings(&videoSettings);
		hr = MWGetExtension()->SetVideoPreviewProcessPreset(&videoSettings);
	}
}

void CVideoOutputDialog::UpdateVideoSettings( MWCAP_VIDEO_CONNECTION_FORMAT videoFormat, MWCAP_VIDEO_PROCESS_SETTINGS videoProcessSettings )
{		
	MWCAP_VIDEO_SIGNAL_STATUS videoSignalStatus;
	HRESULT hr = MWGetExtension()->GetVideoSignalStatus(&videoSignalStatus);
	CString strOut;

	if (!videoFormat.bConnected)
		strOut = _T("Not connected");
	else {
		CString strFormat;
		switch (videoFormat.colorFormat)
		{
		case MWCAP_VIDEO_COLOR_FORMAT_RGB:
			strFormat = _T("BGRX 32Bits");
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV601:
			strFormat = _T("YUYV 16Bits");
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV709:
			strFormat = _T("YUYV 16Bits");
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV2020:
			strFormat = _T("YUYV 16Bits");
			break;
		}
		int nBefore = 0, nAfter = 0;
		GetVideoFPS(videoFormat.dwFrameDuration, FALSE, nBefore, nAfter);
		strOut.Format(_T("%dx%d, %d.%d fps, %s"), videoFormat.cx, videoFormat.cy, nBefore, nAfter, strFormat);
	}
	m_textOutputFormat.SetWindowText(strOut);

	BOOL bChecked = TRUE;
	int nAspectX = 0, nAspectY = 0;
	if (videoProcessSettings.nAspectX == 0 && videoProcessSettings.nAspectY == 0) {
		bChecked = FALSE;
		//if (videoSignalStatus.state == MWCAP_VIDEO_SIGNAL_LOCKED) {
			videoProcessSettings.nAspectX = videoFormat.nAspectX;
			videoProcessSettings.nAspectY = videoFormat.nAspectY;
		//}
	}
	m_ckbLowLatency.SetCheck(videoProcessSettings.bLowLatency);
	m_ckbAspect.SetCheck(bChecked);
	MWCAP_VIDEO_CAPS vCaps;
	hr = MWGetExtension()->GetVideoCaps(&vCaps);
	if (hr == S_OK) {
		m_spinOutputAspectX.SetRange(1, vCaps.wMaxOutputWidth);
		m_spinOutputAspectY.SetRange(1, vCaps.wMaxOutputHeight);
	}
	else {
		m_spinOutputAspectX.SetRange(1, 4096);
		m_spinOutputAspectY.SetRange(1, 4096);
	}
	m_spinOutputAspectX.SetPos(videoProcessSettings.nAspectX);
	m_spinOutputAspectY.SetPos(videoProcessSettings.nAspectY);
	m_nOutputAspectXDefault = videoProcessSettings.nAspectX;
	m_nOutputAspectYDefault = videoProcessSettings.nAspectY;
	m_spinOutputAspectX.EnableWindow(bChecked);
	m_spinOutputAspectY.EnableWindow(bChecked);

	bChecked = TRUE;
	RECT rect = videoProcessSettings.rectSource;
	if (rect.left == 0 && rect.top == 0 && rect.bottom == 0 && rect.right == 0) {
		bChecked = FALSE;
		if (videoSignalStatus.state == MWCAP_VIDEO_SIGNAL_LOCKED) {
			rect.right = videoSignalStatus.cx;
			rect.bottom = videoSignalStatus.cy;

			m_spinCropLeft.SetRange(0, (short)videoSignalStatus.cx);
			m_spinCropTop.SetRange(0, (short)videoSignalStatus.cy);
			m_spinCropRight.SetRange(0, (short)videoSignalStatus.cx);
			m_spinCropBottom.SetRange(0, (short)videoSignalStatus.cy);
		}
	}
	m_ckbCropInput.SetCheck(bChecked);

	m_spinCropLeft.SetPos(rect.left);
	m_spinCropTop.SetPos(rect.top);
	m_spinCropRight.SetPos(rect.right);
	m_spinCropBottom.SetPos(rect.bottom);

	m_spinCropLeft.EnableWindow(bChecked);
	m_spinCropTop.EnableWindow(bChecked);
	m_spinCropRight.EnableWindow(bChecked);
	m_spinCropBottom.EnableWindow(bChecked);

	bChecked = TRUE;
	int nSel = 0;
	switch (videoProcessSettings.colorFormat)
	{
	case MWCAP_VIDEO_COLOR_FORMAT_UNKNOWN:
		bChecked = FALSE;
		switch (videoFormat.colorFormat)
		{
		case MWCAP_VIDEO_COLOR_FORMAT_RGB:
			nSel = 0;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV601:
			nSel = 1;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV709:
			nSel = 2;
			break;
		case MWCAP_VIDEO_COLOR_FORMAT_YUV2020:
			nSel = 3;
			break;
		}
		break;
	case MWCAP_VIDEO_COLOR_FORMAT_RGB:
		nSel = 0;
		break;
	case MWCAP_VIDEO_COLOR_FORMAT_YUV601:
		nSel = 1;
		break;
	case MWCAP_VIDEO_COLOR_FORMAT_YUV709:
		nSel = 2;
		break;
	case MWCAP_VIDEO_COLOR_FORMAT_YUV2020:
		nSel = 3;
		break;
	}
	m_ckbColorFormat.SetCheck(bChecked);
	m_cmbOutputFormat.SetCurSel(nSel);
	m_cmbOutputFormat.EnableWindow(bChecked);

	bChecked = TRUE;
	nSel = 0;
	switch (videoProcessSettings.quantRange)
	{
	case MWCAP_VIDEO_QUANTIZATION_UNKNOWN:
		bChecked = FALSE;
		switch (videoFormat.quantRange)
		{
		case MWCAP_VIDEO_QUANTIZATION_FULL:
			nSel = 0;
			break;
		case MWCAP_VIDEO_QUANTIZATION_LIMITED:
			nSel = 1;
			break;
		}
		m_cmbOutputQuant.SetCurSel(nSel);
		break;
	case MWCAP_VIDEO_QUANTIZATION_FULL:
		nSel = 0;
		break;
	case MWCAP_VIDEO_QUANTIZATION_LIMITED:
		nSel = 1;
		break;
	}
	m_ckbQuant.SetCheck(bChecked);
	m_cmbOutputQuant.SetCurSel(nSel);
	m_cmbOutputQuant.EnableWindow(bChecked);

	bChecked = TRUE;
	nSel = 0;
	switch (videoProcessSettings.satRange)
	{
	case MWCAP_VIDEO_SATURATION_UNKNOWN:
		bChecked = FALSE;
		switch (videoFormat.satRange)
		{
		case MWCAP_VIDEO_SATURATION_FULL:
			nSel = 0;
			break;
		case MWCAP_VIDEO_SATURATION_LIMITED:
			nSel = 1;
			break;
		case MWCAP_VIDEO_SATURATION_EXTENDED_GAMUT:
			nSel = 2;
			break;
		}
		m_cmbOutputSaturation.SetCurSel(nSel);
		break;
	case MWCAP_VIDEO_SATURATION_FULL:
		nSel = 0;
		break;
	case MWCAP_VIDEO_SATURATION_LIMITED:
		nSel = 1;
		break;
	case MWCAP_VIDEO_SATURATION_EXTENDED_GAMUT:
		nSel = 2;
		break;
	}
	m_ckbSaturation.SetCheck(bChecked);
	m_cmbOutputSaturation.SetCurSel(nSel);
	m_cmbOutputSaturation.EnableWindow(bChecked);

	nSel = 0;
	switch (videoProcessSettings.deinterlaceMode)
	{
	case MWCAP_VIDEO_DEINTERLACE_WEAVE:
		nSel = 0;
		break;
	case MWCAP_VIDEO_DEINTERLACE_BLEND:
		nSel = 1;
		break;
	case MWCAP_VIDEO_DEINTERLACE_TOP_FIELD:
		nSel = 2;
		break;
	case MWCAP_VIDEO_DEINTERLACE_BOTTOM_FIELD:
		nSel = 3;
		break;
	}
	m_cmbOutputDeint.SetCurSel(nSel);

	nSel = 0;
	switch (videoProcessSettings.aspectRatioConvertMode)
	{
	case MWCAP_VIDEO_ASPECT_RATIO_IGNORE:
		nSel = 0;
		break;
	case MWCAP_VIDEO_ASPECT_RATIO_CROPPING:
		nSel = 1;
		break;
	case MWCAP_VIDEO_ASPECT_RATIO_PADDING:
		nSel = 2;
		break;
	}
	m_cmbOutputAR.SetCurSel(nSel);
}

void CVideoOutputDialog::UpdateVideoInfo( BOOL bVideoCapture )
{
	if (bVideoCapture == -1)
		bVideoCapture = m_bVideoCapture;

	MWCAP_VIDEO_CONNECTION_FORMAT videoFormat; 
	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	HRESULT hr = S_FALSE;

	if (bVideoCapture) {
		hr = MWGetExtension()->GetVideoCaptureConnectionFormat(&videoFormat);
		hr = MWGetExtension()->GetVideoCaptureProcessSettings(&videoSettings);
		UpdateVideoSettings(videoFormat, videoSettings);
	}
	else {
		hr = MWGetExtension()->GetVideoPreviewConnectionFormat(&videoFormat);
		hr = MWGetExtension()->GetVideoPreviewProcessSettings(&videoSettings);
		UpdateVideoSettings(videoFormat, videoSettings);
	}

	m_bVideoCapture = bVideoCapture;
}


void CVideoOutputDialog::OnDeltaposSpinVideoCaptureAspectX(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);
	videoSettings.nAspectX = pNMUpDown->iPos + pNMUpDown->iDelta;
	if (videoSettings.nAspectX > 0) {
		SetVideoSettings(m_bVideoCapture, videoSettings);
	}
}


void CVideoOutputDialog::OnDeltaposSpinVideoCaptureAspectY(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);
	videoSettings.nAspectY = pNMUpDown->iPos + pNMUpDown->iDelta;
	if (videoSettings.nAspectY > 0) {
		SetVideoSettings(m_bVideoCapture, videoSettings);
	}
}


void CVideoOutputDialog::OnDeltaposSpinVideoCaptureCropLeft(NMHDR *pNMHDR, LRESULT *pResult)	
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);
	videoSettings.rectSource.left = pNMUpDown->iPos + pNMUpDown->iDelta;
	if (videoSettings.rectSource.left > 0) {
		SetVideoSettings(m_bVideoCapture, videoSettings);
	}
}


void CVideoOutputDialog::OnDeltaposSpinVideoCaptureCropTop(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);
	videoSettings.rectSource.top = pNMUpDown->iPos + pNMUpDown->iDelta;
	if (videoSettings.rectSource.top > 0) {
		SetVideoSettings(m_bVideoCapture, videoSettings);
	}
}


void CVideoOutputDialog::OnDeltaposSpinVideoCaptureCropRight(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);
	videoSettings.rectSource.right = pNMUpDown->iPos + pNMUpDown->iDelta;
	if (videoSettings.rectSource.right > 0) {
		SetVideoSettings(m_bVideoCapture, videoSettings);
	}
}


void CVideoOutputDialog::OnDeltaposSpinVideoCaptureCropBottom(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);
	videoSettings.rectSource.bottom = pNMUpDown->iPos + pNMUpDown->iDelta;
	if (videoSettings.rectSource.bottom > 0) {
		SetVideoSettings(m_bVideoCapture, videoSettings);
	}
}

void CVideoOutputDialog::OnBnClickedCheckIfLowLatency()
{
	MWCAP_VIDEO_PROCESS_SETTINGS videoSettings;
	GetVideoSettings(m_bVideoCapture, videoSettings);
	if (videoSettings.bLowLatency != m_ckbLowLatency.GetCheck()) {
		videoSettings.bLowLatency = m_ckbLowLatency.GetCheck();
		SetVideoSettings(m_bVideoCapture, videoSettings);
	}
}

void CVideoOutputDialog::GetVideoSettings( BOOL bVideoCapture, MWCAP_VIDEO_PROCESS_SETTINGS& settings )
{
	HRESULT hr = S_FALSE;
	if (bVideoCapture)
		hr = MWGetExtension()->GetVideoCaptureProcessSettings(&settings);
	else
		hr = MWGetExtension()->GetVideoPreviewProcessSettings(&settings);
}

void CVideoOutputDialog::SetVideoSettings( BOOL bVideoCapture, MWCAP_VIDEO_PROCESS_SETTINGS settings )
{
	HRESULT hr = S_FALSE;
	if (bVideoCapture)
		hr = MWGetExtension()->SetVideoCaptureProcessSettings(&settings);
	else
		hr = MWGetExtension()->SetVideoPreviewProcessSettings(&settings);
}

