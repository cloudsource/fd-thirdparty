#pragma once

#include "DShowCapture.h"

// COSDTabDialog dialog

class COSDTabDialog : public CDialogEx
{
	DECLARE_DYNAMIC(COSDTabDialog)

public:
	COSDTabDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~COSDTabDialog();

// Dialog Data
	enum { IDD = IDD_OSD_TAB_DIALOG };

private:
	CStatic		m_picOSD;
	CEdit		m_editPath;
	CButton		m_btnSelectFile;
	CButton		m_ckbEnableOSD;
	CButton		m_btnLoadPresent;
	CButton		m_btnSavePresent;

	BOOL		m_bCapture;

public:
	void DisplayOSDInfo(BOOL bCapture);
	void DisplayOSDLogo(MWCAP_VIDEO_OSD_SETTINGS videoOSDSettings);

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonSelectFile();
	afx_msg void OnBnClickedButtonOsdLoadPresent();
	afx_msg void OnBnClickedButtonOsdSavePresent();
	afx_msg void OnBnClickedCheckOsdEnable();
	afx_msg void OnPaint();
};
