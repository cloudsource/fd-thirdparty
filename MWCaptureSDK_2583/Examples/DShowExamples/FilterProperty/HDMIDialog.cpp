// HDMIDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "FilterProperty.h"
#include "HDMIDialog.h"
#include "afxdialogex.h"


// CHDMIDialog 对话框

IMPLEMENT_DYNAMIC(CHDMIDialog, CDialogEx)

CHDMIDialog::CHDMIDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CHDMIDialog::IDD, pParent)
{
}

CHDMIDialog::~CHDMIDialog()
{
}

void CHDMIDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_HDMI_INFO, m_tabMisc);
	DDX_Control(pDX, IDC_EDIT1, m_edit);	
	DDX_Control(pDX, IDC_STATIC_HDMI_INFO, m_textHDMIInfo);	
}


BEGIN_MESSAGE_MAP(CHDMIDialog, CDialogEx)
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_BUTTON_RESET, &CHDMIDialog::OnBnClickedButtonReset)
	ON_BN_CLICKED(IDC_BUTTON_LOAD, &CHDMIDialog::OnBnClickedButtonLoad)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CHDMIDialog::OnBnClickedButtonSave)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_HDMI_INFO, &CHDMIDialog::OnTcnSelchangeTabHdmiInfo)
	ON_WM_TIMER()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CHDMIDialog 消息处理程序


BOOL CHDMIDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_tabMisc.InsertItem(0, _T("AVI"));
	m_tabMisc.InsertItem(1, _T("Audio"));
	m_tabMisc.InsertItem(2, _T("SPD"));
	m_tabMisc.InsertItem(3, _T("MS"));
	m_tabMisc.InsertItem(4, _T("VS"));
	m_tabMisc.InsertItem(5, _T("ACP"));
	m_tabMisc.InsertItem(6, _T("ISRC1"));
	m_tabMisc.InsertItem(7, _T("ISRC2"));
	m_tabMisc.InsertItem(8, _T("Gamut"));
	
	g_capture.AddNotify(this);

	DeviceChange();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

int CHDMIDialog::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here

	return 0;
}


void CHDMIDialog::OnBnClickedButtonReset()
{
	// TODO: Add your control notification handler code here
	BYTE byEDID[1024] = {0};
	ULONG lEDID = 1024;
	HRESULT hr = MWGetExtension()->GetEDID(byEDID, &lEDID);

	MWGetExtension()->SetEDID(byEDID, lEDID);
}


void CHDMIDialog::OnBnClickedButtonLoad()
{
	// TODO: Add your control notification handler code here
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, _T("Bin Files(*.bin)|*.bin||"), this);

	if(IDOK != dlg.DoModal())
		return;

	CString strPath = dlg.GetPathName();

	CFile file;
	file.Open(strPath, CFile::modeRead, NULL); 
	ULONGLONG len = file.GetLength( );
	BYTE byEDID[1024] = {0};
	byEDID[len] = 0; 
	int nRead = file.Read(byEDID, (UINT)len);
	MWGetExtension()->SetEDID(byEDID, nRead);
}


void CHDMIDialog::OnBnClickedButtonSave()
{
	// TODO: Add your control notification handler code here

	CFileDialog dlg(FALSE, _T("bin"), NULL, 4 | 2, _T("Bin Files (*.bin)|*.bin||"), this);
	if (dlg.DoModal() != IDOK) 
		return ;

	CString strFilePath = dlg.GetPathName();

	CFile file;
	CFileException fileException;

	BOOL bOpenResult = file.Open(strFilePath, CFile::modeCreate | CFile::modeReadWrite, &fileException);

	if(!bOpenResult) {
		TCHAR szError[1024];
		fileException.GetErrorMessage(szError, 1024);
		file.Close();
	}
	else {
		BYTE byEDID[1024] = {0};
		ULONG lEDID = 1024;
		HRESULT hr = MWGetExtension()->GetEDID(byEDID, &lEDID);

		file.Write(byEDID, lEDID);
		file.Close();
	}
}


void CHDMIDialog::OnTcnSelchangeTabHdmiInfo(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: Add your control notification handler code here
	*pResult = 0;
	
	OnHDMIInfoStatuChanged(0xFFFF);
}

void CHDMIDialog::Num2HexStr( int nNum, char* pszBegin )
{
	if (nNum < 16) {
		pszBegin[0] = '0';
		++ pszBegin;
	}
	
	itoa (nNum, pszBegin, 16);
}

void CHDMIDialog::UpdateHDMIInfo( MWCAP_HDMI_INFOFRAME_ID hdmiInfoID, int nInfoIDPage)
{
	int nSel = m_tabMisc.GetCurSel();
	if (nInfoIDPage != nSel)
		return ;

	CString strInfo;
	strInfo.Append(L"N/A");

	DWORD dwAVI = 0;
	HRESULT hr = MWGetExtension()->GetHDMIInfoFrameValidFlags(&dwAVI);

	if (SUCCEEDED(hr)) {
		HDMI_INFOFRAME_PACKET packet = {0};
		hr = MWGetExtension()->GetHDMIInfoFramePacket(hdmiInfoID, &packet);

		if (SUCCEEDED(hr)) {
			//BYTE byEDID[1024] = {0};
			char szHexEDID [1024] = {0};
			char* pMove = szHexEDID;

			ULONG lEDID = 1024;
			int nMove;
			BYTE byValue;
			for(int i = 0; i < packet.header.byLength; i += 8) {
				strcpy(pMove, " Data  ");
				pMove += 7;
				nMove = 2;
				if (i < 16)
					*pMove++ = '0';
				itoa (i, pMove, 16);
				if (i < 16) {			
					nMove = 1;
				}
				pMove += nMove;
				*pMove++ = ':';
				*pMove++ = ' ';
				*pMove++ = ' ';

				for (int j = i; j < i + 8 && j < packet.header.byLength; ++ j) {
					nMove = 2;
					byValue = packet.abyPayload[j];
					if (byValue < 16) {
						*pMove++ = '0';
						nMove = 1;
					}

					itoa (byValue, pMove, 16);
					pMove += nMove;

					if (j < 15)
						*pMove++ = ' ';
				}
				*pMove ++ = '\r';
				*pMove ++ = '\n';
			}
			for (int i = 0; i < 1024; ++ i)
				szHexEDID[i] = toupper(szHexEDID[i]);

			strInfo.Format(_T("        Type:  0x%02X\n    Version:  0x%02X\n     Length:  %d Bytes\nChecksum:  0x%02X\n%S"), packet.header.byPacketType, packet.header.byVersion, packet.header.byLength, packet.byChecksum, szHexEDID);
		}
		m_textHDMIInfo.SetWindowText(strInfo);
	}
}

void CHDMIDialog::DeviceChange()
{
	char szHexEDID [1024] = {0};
	char* pMove = szHexEDID;

	BYTE byEDID[1024] = {0};
	ULONG lEDID = 1024;
	HRESULT hr = MWGetExtension()->GetEDID(byEDID, &lEDID);
	int nMove;
	BYTE byValue;
	for(int i = 0; i < 16; ++ i) {
		nMove = 2;
		itoa (i, pMove, 16);
		if (i < 16) {			
			nMove = 1;
		}
		pMove += nMove;
		if (i < 16)
			*pMove++ = '0';
		*pMove++ = ':';
		*pMove++ = ' ';
		*pMove++ = ' ';
		*pMove++ = ' ';

		int nBeginIndex = i * 16;
		for (int j = 0; j < 16; ++ j) {
			nMove = 2;
			int nIndex = nBeginIndex + j;
			byValue = byEDID[nIndex];
			if (byValue < 16) {
				*pMove++ = '0';
				nMove = 1;
			}

			itoa (byValue, pMove, 16);
			pMove += nMove;

			if (j < 15)
				*pMove++ = ' ';
		}
		*pMove ++ = '\r';
		*pMove ++ = '\n';
	}
	for (int i = 0; i < 1024; ++ i)
		szHexEDID[i] = toupper(szHexEDID[i]);

	CString strEDID;
	strEDID.Format(_T("%S"), szHexEDID);

	m_edit.Clear();
	m_edit.SetWindowText(strEDID);

	OnHDMIInfoStatuChanged(0xFFFF);
}


void CHDMIDialog::OnHDMIInfoStatuChanged( ULONGLONG ullStatusBits )
{
	int nPage = 0;
	if (ullStatusBits & MWCAP_HDMI_INFOFRAME_MASK_AVI)
		UpdateHDMIInfo(MWCAP_HDMI_INFOFRAME_ID_AVI, nPage);
	++ nPage;
	if (ullStatusBits & MWCAP_HDMI_INFOFRAME_MASK_AUDIO)
		UpdateHDMIInfo(MWCAP_HDMI_INFOFRAME_ID_AUDIO, nPage);
	++ nPage;
	if (ullStatusBits & MWCAP_HDMI_INFOFRAME_MASK_SPD)
		UpdateHDMIInfo(MWCAP_HDMI_INFOFRAME_ID_SPD, nPage);
	++ nPage;
	if (ullStatusBits & MWCAP_HDMI_INFOFRAME_MASK_MS)
		UpdateHDMIInfo(MWCAP_HDMI_INFOFRAME_ID_MS, nPage);
	++ nPage;
	if (ullStatusBits & MWCAP_HDMI_INFOFRAME_MASK_VS)
		UpdateHDMIInfo(MWCAP_HDMI_INFOFRAME_ID_VS, nPage);
	++ nPage;
	if (ullStatusBits & MWCAP_HDMI_INFOFRAME_MASK_ACP)
		UpdateHDMIInfo(MWCAP_HDMI_INFOFRAME_ID_ACP, nPage);
	++ nPage;
	if (ullStatusBits & MWCAP_HDMI_INFOFRAME_MASK_ISRC1)
		UpdateHDMIInfo(MWCAP_HDMI_INFOFRAME_ID_ISRC1, nPage);
	++ nPage;
	if (ullStatusBits & MWCAP_HDMI_INFOFRAME_MASK_ISRC2)
		UpdateHDMIInfo(MWCAP_HDMI_INFOFRAME_ID_ISRC2, nPage);
	++ nPage;
	if (ullStatusBits & MWCAP_HDMI_INFOFRAME_MASK_GAMUT)
		UpdateHDMIInfo(MWCAP_HDMI_INFOFRAME_ID_GAMUT, nPage);
}
