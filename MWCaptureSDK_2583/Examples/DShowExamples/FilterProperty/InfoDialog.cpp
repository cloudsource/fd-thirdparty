// InfoDialog.cpp : implementation file
//

#include "stdafx.h"
#include "FilterProperty.h"
#include "InfoDialog.h"
#include "afxdialogex.h"

#include "DShowCapture.h"

// CInfoDialog dialog

IMPLEMENT_DYNAMIC(CInfoDialog, CDialogEx)

CInfoDialog::CInfoDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(CInfoDialog::IDD, pParent)
{

}

CInfoDialog::~CInfoDialog()
{
}

void CInfoDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_INFO, m_listInfo);
}


BEGIN_MESSAGE_MAP(CInfoDialog, CDialogEx)
END_MESSAGE_MAP()


BOOL CInfoDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	m_listInfo.ModifyStyle(0, LVS_REPORT);
	m_listInfo.SetExtendedStyle(m_listInfo.GetExtendedStyle() | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);  
	
	m_listInfo.InsertColumn(0, _T("Name"));  
	m_listInfo.InsertColumn(1, _T("Value"));  

	CRect rect;  
	m_listInfo.GetClientRect(rect);
	m_listInfo.SetColumnWidth(0, rect.Width() / 3); 
	m_listInfo.SetColumnWidth(1, rect.Width() * 2 / 3);  

	UpdateInfoTable();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CInfoDialog::UpdateInfoTable()
{
	m_listInfo.DeleteAllItems();
	int nRowIndex = -1;
	CString strValue;

	MWCAP_CHANNEL_INFO channelInfo;
	MWGetExtension()->GetChannelInfo(&channelInfo);

	++ nRowIndex;
	strValue.Format(_T("%S"), channelInfo.szFamilyName);
	m_listInfo.InsertItem(nRowIndex, _T("Family name"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	++ nRowIndex;
	strValue.Format(_T("%S"), channelInfo.szProductName);
	m_listInfo.InsertItem(nRowIndex, _T("Product name"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	++ nRowIndex;
	strValue.Format(_T("%S"), channelInfo.szFirmwareName);
	m_listInfo.InsertItem(nRowIndex, _T("Firmware name"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	++ nRowIndex;
	strValue.Format(_T("%S"), channelInfo.szBoardSerialNo);
	m_listInfo.InsertItem(nRowIndex, _T("Serial number"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	++ nRowIndex;
	char szVersion[8] = {0};
	szVersion[0] = channelInfo.chHardwareVersion;
	strValue.Format(_T("%S"), szVersion);
	m_listInfo.InsertItem(nRowIndex, _T("Hardware version"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	++ nRowIndex;
	strValue.Format(_T("%d.%d"), channelInfo.dwFirmwareVersion >> 16, channelInfo.dwFirmwareVersion & 0xffff);
	m_listInfo.InsertItem(nRowIndex, _T("Firmware version"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	++ nRowIndex;
	strValue.Format(_T("%d.%d.%d"), channelInfo.dwDriverVersion >> 24, (channelInfo.dwDriverVersion >> 16) & 0x00ff, channelInfo.dwDriverVersion & 0xffff);
	m_listInfo.InsertItem(nRowIndex, _T("Driver version"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	++ nRowIndex;
	strValue.Format(_T("%d"), channelInfo.byBoardIndex);
	m_listInfo.InsertItem(nRowIndex, _T("Border index"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	++ nRowIndex;
	strValue.Format(_T("%d"), channelInfo.byChannelIndex);
	m_listInfo.InsertItem(nRowIndex, _T("Channel index"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	ULONG lSize = MAX_PATH;
	WCHAR wszInstanceID[MAX_PATH] = {0};
	MWGetExtension()->GetDeviceInstanceID(wszInstanceID, &lSize);
	++ nRowIndex;
	strValue.Format(_T("%s"), wszInstanceID);
	m_listInfo.InsertItem(nRowIndex, _T("Device instance ID"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	MWCAP_PRO_CAPTURE_INFO captureInfo;
	ULONG cbFamilyInfo = sizeof(MWCAP_PRO_CAPTURE_INFO);
	MWGetExtension()->GetFamilyInfo(&captureInfo, &cbFamilyInfo);

	++ nRowIndex;
	strValue.Format(_T("Bus %d, Device %d"), captureInfo.byPCIBusID, captureInfo.byPCIDevID);
	m_listInfo.InsertItem(nRowIndex, _T("PCIe address"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	++ nRowIndex;
	strValue.Format(_T("Gen %d"), captureInfo.byLinkType);
	m_listInfo.InsertItem(nRowIndex, _T("PCIe speed"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	++ nRowIndex;
	strValue.Format(_T("x%d"), captureInfo.byLinkWidth);
	m_listInfo.InsertItem(nRowIndex, _T("PCIe width"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	++ nRowIndex;
	strValue.Format(_T("%d Bytes"), captureInfo.wMaxPayloadSize);
	m_listInfo.InsertItem(nRowIndex, _T("PCIe max payload size"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	++ nRowIndex;
	strValue.Format(_T("%d Bytes"), captureInfo.wMaxReadRequestSize);
	m_listInfo.InsertItem(nRowIndex, _T("PCIe max read request size"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	++ nRowIndex;
	strValue.Format(_T("%d Bytes"), captureInfo.cbTotalMemorySize);
	m_listInfo.InsertItem(nRowIndex, _T("Total memory size"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	++ nRowIndex;
	strValue.Format(_T("%d Bytes"), captureInfo.cbFreeMemorySize);
	m_listInfo.InsertItem(nRowIndex, _T("Free memory size"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	MWCAP_VIDEO_CAPS videoCaps;
	MWGetExtension()->GetVideoCaps(&videoCaps);

	++ nRowIndex;
	strValue.Format(_T("%dx%d Pixels"), videoCaps.wMaxInputWidth, videoCaps.wMaxInputHeight);
	m_listInfo.InsertItem(nRowIndex, _T("Max input dimension"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	++ nRowIndex;
	strValue.Format(_T("%dx%d Pixels"), videoCaps.wMaxOutputWidth, videoCaps.wMaxOutputHeight);
	m_listInfo.InsertItem(nRowIndex, _T("Max output dimension"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  

	LONG lTemperatureDegC_X10 = 0;
	MWGetExtension()->GetCoreTemperature(&lTemperatureDegC_X10);

	++ nRowIndex;
	strValue.Format(_T("%d.%d deg C"), lTemperatureDegC_X10 / 10, lTemperatureDegC_X10 % 10);
	m_listInfo.InsertItem(nRowIndex, _T("Core temperature"));  
	m_listInfo.SetItemText(nRowIndex, 1, strValue);  
}

void CInfoDialog::UpdateCoreTemperature()
{
	LONG lTemperatureDegC_X10 = 0;
	MWGetExtension()->GetCoreTemperature(&lTemperatureDegC_X10);

	CString strValue;
	strValue.Format(_T("%d.%d deg C"), lTemperatureDegC_X10 / 10, lTemperatureDegC_X10 % 10);

	m_listInfo.SetItemText(19, 1, strValue); 
}

// CInfoDialog message handlers
