
#include "stdafx.h"
#include "DShowCapture.h"

CDShowCapture g_capture;

void WINAPI FreeMediaType(AM_MEDIA_TYPE& mt)
{
	if (mt.cbFormat != 0) {
		CoTaskMemFree((PVOID)mt.pbFormat);

		// Strictly unnecessary but tidier
		mt.cbFormat = 0;
		mt.pbFormat = NULL;
	}
	if (mt.pUnk != NULL) {
		mt.pUnk->Release();
		mt.pUnk = NULL;
	}
}

void WINAPI DeleteMediaType(AM_MEDIA_TYPE *pmt)
{
	// allow NULL pointers for coding simplicity

	if (pmt == NULL) {
		return;
	}

	FreeMediaType(*pmt);
	CoTaskMemFree((PVOID)pmt);
}

BOOL CDShowCapture::EnumDevice(BOOL bVideo, const TCHAR* pszPath) 
{
	HRESULT hr;
	ICreateDevEnum* pDevEnum = NULL;
	IEnumMoniker* pEnumMoniker = NULL;
	IMoniker *pMoniker = NULL;

	do {
		hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER, IID_ICreateDevEnum, (void**)&pDevEnum);
		if (hr != NOERROR)
			break;

		hr = pDevEnum->CreateClassEnumerator(bVideo ? CLSID_VideoInputDeviceCategory : CLSID_AudioInputDeviceCategory, &pEnumMoniker, 0);
		if (hr != NOERROR)
			break;

		pEnumMoniker->Reset();

		ULONG cFetched;
		while ((pEnumMoniker->Next(1, &pMoniker, &cFetched) == S_OK))
		{
			IPropertyBag* pBag = NULL;
			hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pBag);
			if (SUCCEEDED(hr))
			{
				VARIANT var;
				var.vt = VT_BSTR;
				hr = pBag->Read(L"FriendlyName", &var, NULL);

				CComBSTR bstrDevID;
				LPOLESTR strName = NULL;
				hr = pMoniker->GetDisplayName(NULL, NULL, &strName);
				if (SUCCEEDED(hr)) {
					bstrDevID = strName;
					CoTaskMemFree(strName);
				}

				TCHAR * pszDevID = _tcsdup(bstrDevID);
				
				BOOL bFind = FALSE;
				if ((hr == NOERROR) && (pszDevID != NULL) 
					&& (_tcsstr(pszDevID, _T("pci#ven_1cd7")) != NULL)) {
					CAutoConvertString strDevice(var.bstrVal);

					if (pszPath == NULL) {
						DEVICE_INFO di = {0};
						_tcscpy_s(di.szName, MAX_PATH, (TCHAR *)strDevice);
						_tcscpy_s(di.szPath, MAX_PATH, pszDevID);
						if (bVideo)
							m_lstVideoDevice.push_back(di);
						else
							m_lstAudioDevice.push_back(di);
					}
					else {
						if (_tcsicmp(pszPath, pszDevID) == 0) {
							if (bVideo)
								wcscpy_s(m_szVideoName, MAX_PATH, (WCHAR *)strDevice);
							else
								wcscpy_s(m_szAudioName, MAX_PATH, (WCHAR *)strDevice);
							bFind = TRUE;
						}
					}
				}

				if (pszDevID != NULL) 
					free(pszDevID);

				pBag->Release();

				if (bFind) {
					if (bVideo)
						m_pVideoMoniker = pMoniker;
					else
						m_pAudioMoniker = pMoniker;
					break;
				}
			}
			SAFE_RELEASE(pMoniker);
		}
	} while (FALSE);

	SAFE_RELEASE(pDevEnum);
	SAFE_RELEASE(pEnumMoniker);

	if (pszPath == NULL)
		return TRUE;

	return (m_pVideoMoniker != NULL || m_pAudioMoniker != NULL); 
}

BOOL CDShowCapture::RenderVideo( BOOL bCapture, int nIndex )
{
	Stop();
	ReleaseRenderFilter();

	if (nIndex == -1)
		nIndex = m_nVideoIndex;

	m_bCapture = bCapture;
	m_nVideoIndex = nIndex;

	HRESULT hr;

	BOOL bRet = FALSE;
	do {
		hr = CoCreateInstance(CLSID_CaptureGraphBuilder2, NULL, CLSCTX_INPROC_SERVER, IID_ICaptureGraphBuilder2, (void**)&m_pBuild);
		BREAK_IF_FAILED(hr);

		hr = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC, IID_IGraphBuilder, (LPVOID *)&m_pGraph);
		BREAK_IF_FAILED(hr);

		hr = m_pBuild->SetFiltergraph(m_pGraph);
		BREAK_IF_FAILED(hr);

		hr = m_pGraph->AddFilter(m_pVideoCapture, GetDeviceName(TRUE, nIndex));
		BREAK_IF_FAILED(hr);

		hr = m_pVideoCapture->QueryInterface(IID_IAMVideoProcAmp, (void**)&m_pVideoProcAmp);
		BREAK_IF_FAILED(hr);

		// Get video output pin
		GUID guid = bCapture ? PIN_CATEGORY_CAPTURE : PIN_CATEGORY_PREVIEW;
		hr = m_pBuild->FindPin(m_pVideoCapture, PINDIR_OUTPUT, &guid, &MEDIATYPE_Video, TRUE, 0, &m_pVideoPin);
		if (FAILED(hr) || m_pVideoPin == NULL) {
			hr = m_pBuild->FindPin(m_pVideoCapture, PINDIR_OUTPUT, NULL, &MEDIATYPE_Video, TRUE, 0, &m_pVideoPin);
		}

		if (NULL == m_pVideoPin)
			break;

		hr = m_pBuild->FindInterface(&guid, &MEDIATYPE_Interleaved, m_pVideoCapture, IID_IAMStreamConfig, (void **)&m_pVideoConfig);
		if (FAILED(hr)) {
			hr = m_pBuild->FindInterface(&guid, &MEDIATYPE_Video, m_pVideoCapture, IID_IAMStreamConfig, (void **)&m_pVideoConfig);
			if (FAILED(hr)) {
				OutputDebugString(_T("Cannot find VideoCapture:IAMStreamConfig\n"));
			}
		}

		AM_MEDIA_TYPE* pmt = NULL;
		// default capture format
		if (m_pVideoConfig && m_pVideoConfig->GetFormat(&pmt) == S_OK) {
			if (pmt->formattype == FORMAT_VideoInfo) {
				VIDEOINFO * pVideoInfo = (VIDEOINFO*) pmt->pbFormat;

				LONG lWidth = HEADER(pmt->pbFormat)->biWidth;
				LONG lHeight = ABS(HEADER(pmt->pbFormat)->biHeight);

				HEADER(pmt->pbFormat)->biWidth = 720;
				HEADER(pmt->pbFormat)->biHeight = 576;

				pVideoInfo->AvgTimePerFrame = (int)(10000000 / 25);
				pmt->subtype = MEDIASUBTYPE_YUY2;

				hr = m_pVideoConfig->SetFormat(pmt);
			}

			if (pmt->formattype == FORMAT_VideoInfo) {
				LONG lWidth = HEADER(pmt->pbFormat)->biWidth;
				LONG lHeight = ABS(HEADER(pmt->pbFormat)->biHeight);
			}

			DeleteMediaType(pmt);
		}

		// create renderer
		if (1) {
			hr = CoCreateInstance(CLSID_VideoMixingRenderer9, 0,  CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&m_pRender);
			BREAK_IF_FAILED(hr);

			hr = m_pGraph->AddFilter(m_pRender, L"VMR9");
			BREAK_IF_FAILED(hr);

			hr = m_pBuild->RenderStream(NULL, &MEDIATYPE_Video, m_pVideoPin, NULL, m_pRender);
			BREAK_IF_FAILED(hr);

			hr = m_pGraph->QueryInterface(IID_IVideoWindow, (void **)&m_pVideoWnd);
			BREAK_IF_FAILED(hr);

			if (m_wndPreview) {
				m_pVideoWnd->put_Owner((OAHWND)m_wndPreview->GetSafeHwnd()); 
				m_pVideoWnd->put_WindowStyle(WS_CHILD);   

				CRect rcVideo;
				m_wndPreview->GetClientRect(rcVideo);

				m_pVideoWnd->SetWindowPosition(rcVideo.left, rcVideo.top, rcVideo.right - rcVideo.left, rcVideo.bottom - rcVideo.top);
				m_pVideoWnd->put_Visible(OATRUE);
			}
		}

		// run preview
		Run();

		bRet = TRUE;
	} while (FALSE);

	if (!bRet) {
		AfxMessageBox(_T("Error Create DShow Object!"));
	}

	return bRet;
}

BOOL CDShowCapture::Run()
{
	if (m_pGraph == NULL)
		return FALSE;

	IMediaControl* pControl = NULL;
	HRESULT hr = m_pGraph->QueryInterface(IID_IMediaControl, (void **)&pControl);
	if (FAILED(hr))
		return FALSE;

	hr = pControl->Run();
	SAFE_RELEASE(pControl);

	return SUCCEEDED(hr);
}

void CDShowCapture::Stop()
{
	if (m_pGraph == NULL)
		return;

	HRESULT hr;
	IMediaControl* pControl = NULL;

	hr = m_pGraph->QueryInterface(IID_IMediaControl, (void **)&pControl);
	if (FAILED(hr))
		return;

	pControl->Stop();

	SAFE_RELEASE(pControl);
}

void CDShowCapture::ReleaseRenderFilter()
{
	SAFE_RELEASE(m_pVideoPin);

	SAFE_RELEASE(m_pRender);
	SAFE_RELEASE(m_pVideoWnd);

	SAFE_RELEASE(m_pVideoConfig);
	SAFE_RELEASE(m_pGraph);
	SAFE_RELEASE(m_pBuild);
}

IMWCaptureExtension* MWGetExtension()
{
	return g_capture.GetExtension();
}
