
#pragma once

#include <Windows.h>
#include <gdiplus.h>

#include "StringUtils.h"

using namespace Gdiplus;

class CVoIPCritSec
{
public:
	CVoIPCritSec() {
		InitializeCriticalSection(&m_cs);
	}

	virtual ~CVoIPCritSec() {
		DeleteCriticalSection(&m_cs);
	}

public:
	void Lock() {
		EnterCriticalSection(&m_cs);
	}

	void Unlock() {
		LeaveCriticalSection(&m_cs);
	}

	BOOL TryLock() {
		return TryEnterCriticalSection(&m_cs);
	}
protected:
	 CRITICAL_SECTION	m_cs;
};

class CVoIPAutoLock
{
public:
	CVoIPAutoLock(CVoIPCritSec & section) : m_section(section) 
	{
		m_section.Lock();
	}

	virtual ~CVoIPAutoLock() {
		m_section.Unlock();
	}

protected:
	CVoIPCritSec& m_section;
};

class CGdiPlusRenderer
{
public:
	CGdiPlusRenderer() {
		m_gdiplusToken = NULL;
		m_pBitmap	= NULL;
		m_pLogo		= NULL;
		m_pCaption	= NULL;
	};
	virtual ~CGdiPlusRenderer() {
	}

public:
	BOOL Init(int cx, int cy) {
		if (m_gdiplusToken == NULL) {
			GdiplusStartupInput gdiplusStartupInput;
			GdiplusStartup(&m_gdiplusToken, &gdiplusStartupInput, NULL);
		}
		m_cx = cx;
		m_cy = cy;

		m_pBitmap = new Bitmap(m_cx, m_cy, PixelFormat24bppRGB);

		return TRUE;
	}
	void Exit() {
		if (m_pLogo != NULL) {
			delete m_pLogo;
			m_pLogo = NULL;
		}
		if (m_pCaption != NULL) {
			delete m_pCaption;
			m_pCaption = NULL;
		}
		if (m_pBitmap != NULL) {
			delete m_pBitmap;
			m_pBitmap = NULL;
		}
		if (m_gdiplusToken != NULL) {
			GdiplusShutdown(m_gdiplusToken);
			m_gdiplusToken = NULL;
		}
	}

	BOOL SetLogo(const TCHAR* pszImagePath, RECT rcPos) {
		if (m_pLogo != NULL) {
			delete m_pLogo;
			m_pLogo = NULL;	
		}
		m_pLogo = new Bitmap(pszImagePath);
		if (m_pLogo == NULL)
			return FALSE;

		m_rcLogo.X = rcPos.left;
		m_rcLogo.Y = rcPos.top;
		m_rcLogo.Width = rcPos.right - rcPos.left;
		m_rcLogo.Height = rcPos.bottom - rcPos.top;

		return TRUE;
	}
	BOOL SetCaption(const TCHAR* pszCaption, int nFontSize, RECT rcPos) {
		CAutoConvertString strFont("Times New Roman");

		GraphicsPath path;
		FontFamily family(strFont);

		INT style = FontStyleRegular;

		//style |= FontStyleUnderline;
		//style |= FontStyleBold;
		//style |= FontStyleItalic;

		CAutoConvertString strCaption(pszCaption);
		path.AddString(strCaption, -1, &family, style, (Gdiplus::REAL)nFontSize, PointF(0, 0), StringFormat::GenericTypographic());

		GraphicsPath * path2 = path.Clone();
		if (NULL == path2)
			return 0;

		Pen pen(Color(0, 0, 0), (Gdiplus::REAL)2);
		path2->Widen(&pen);

		Rect rcBound;
		path2->GetBounds(&rcBound);

		Matrix mx;
		mx.Translate((Gdiplus::REAL)(-rcBound.X), (Gdiplus::REAL)(-rcBound.Y));
		path.Transform(&mx);
		path2->Transform(&mx);

		rcBound.Width = (rcBound.Width + 1) / 2 * 2;

		m_pCaption = new Bitmap(rcBound.Width, rcBound.Height, PixelFormat32bppARGB);

		Graphics graphics(m_pCaption);
		graphics.SetSmoothingMode(SmoothingModeHighQuality);

		SolidBrush brush2(Color(255, 0, 255, 0));
		graphics.FillPath(&brush2, path2);
	
		SolidBrush brush(Color(255, 0, 0, 255));
		graphics.FillPath(&brush, &path);

		m_rcCaption.X = rcPos.left;
		m_rcCaption.Y = rcPos.top;
		m_rcCaption.Width = rcPos.right - rcPos.left;
		m_rcCaption.Height = rcPos.bottom - rcPos.top;

		m_pCaption->RotateFlip(Rotate180FlipX);

		return TRUE;
	}

	void DrawGraphics(HDC hdc, CRect rc) {
		CVoIPAutoLock lock(m_cs);

		Graphics graphics(hdc);
		// Create a Rect object.
		Rect fillRect(rc.left, rc.top, rc.Width(), rc.Height());
		graphics.DrawImage(m_pBitmap, fillRect);
	}

	void PutFrame(const BYTE* pbyData, int cbStride, BOOL bTopDown) {
		if (m_pBitmap == NULL)
			return;

		CVoIPAutoLock lock(m_cs);

		BitmapData bitmapData; 
		Rect rect(0, 0, m_cx, m_cy);
		Status status = m_pBitmap->LockBits(&rect, ImageLockModeWrite, PixelFormat24bppRGB, &bitmapData); 

		BOOL bBottomUp = FALSE;
		if (bitmapData.Stride < 0) {
			bitmapData.Scan0 = ((LPBYTE)bitmapData.Scan0) + bitmapData.Stride * (m_cy - 1);
			bitmapData.Stride = -bitmapData.Stride;
			bBottomUp = TRUE;
		}

		for (int i = 0; i < m_cy; i ++) {
			if ((bTopDown && !bBottomUp) || (!bTopDown && bBottomUp)) {
				memcpy((LPBYTE)bitmapData.Scan0 + bitmapData.Stride * i, pbyData + cbStride * i, cbStride);
			}
			else {
				memcpy((LPBYTE)bitmapData.Scan0 + bitmapData.Stride * (m_cy - i - 1), pbyData + cbStride * i, cbStride);
			}
		}

		m_pBitmap->UnlockBits(&bitmapData); 

		// Write Logo & Caption
		if (m_pLogo != NULL || m_pCaption != NULL) {
			Graphics graphics(m_pBitmap);
			graphics.SetSmoothingMode(SmoothingModeHighQuality);
			if (m_pLogo != NULL)
				graphics.DrawImage(m_pLogo, m_rcLogo);
			if (m_pCaption != NULL) {
				graphics.DrawImage(m_pCaption, m_rcCaption);
			}
		}
	}
protected:
	CVoIPCritSec	m_cs;

	ULONG_PTR	m_gdiplusToken;

	int m_cx;
	int m_cy;

	Bitmap* m_pBitmap;

	Bitmap* m_pLogo;
	Rect	m_rcLogo;

	Bitmap* m_pCaption;
	Rect	m_rcCaption;
};