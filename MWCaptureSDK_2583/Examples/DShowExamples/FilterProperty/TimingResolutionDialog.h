#pragma once
#include "ResolutionDialog.h"

// CTimingResolutionDialog dialog

class CTimingResolutionDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CTimingResolutionDialog)

public:
	CTimingResolutionDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTimingResolutionDialog();

// Dialog Data
	enum { IDD = IDD_TIMING_RESOLUTION_DIALOG };

private:
	CResolutionDialog	m_dlgResolution;

	CListCtrl		m_listResolutions;
	CButton			m_btnDelete;
	CButton			m_btnAdd;
	CButton			m_btnModify;

	int				m_nSelTableResolution;

	void UpdateTableResolution();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnNMClickListTimingResolution(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMKillfocusListTimingResolution(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonTimingResolutionDelete();
	afx_msg void OnBnClickedButtonTimingResolutionAdd();
	afx_msg void OnBnClickedButtonTimingResolutionModify();
};
