#pragma once


// CTimingTimingsDialog dialog

class CTimingTimingsDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CTimingTimingsDialog)

public:
	CTimingTimingsDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTimingTimingsDialog();

// Dialog Data
	enum { IDD = IDD_TIMINGTIMINGS_DIALOG };

private:
	CListCtrl		m_listTimings;
	CButton			m_btnDelete;

	int				m_nSelTableTiming;

public:
	void UpdateTableTiming();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnNMClickListTimingTimings(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMKillfocusListTimingTimings(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonTimingTimingsDelete();
};
