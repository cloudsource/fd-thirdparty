#pragma once


// CResolutionDialog dialog

class CResolutionDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CResolutionDialog)

public:
	CResolutionDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CResolutionDialog();

// Dialog Data
	enum { IDD = IDD_RESOLUTION_DIALOG };

private:
	CEdit		m_editWidth;
	CEdit		m_editHeight;

	int			m_nIndex;

	void UpdateItemValue(int nIndex);

public:
	void ModifyItemIndex(int nIndex);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
};
