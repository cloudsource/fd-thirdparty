
// DShowCapture.h : main header file for the DShowCapture application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CDShowCaptureApp:
// See DShowCapture.cpp for the implementation of this class
//

class CDShowCaptureApp : public CWinApp
{
public:
	CDShowCaptureApp();


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation

public:
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CDShowCaptureApp theApp;
