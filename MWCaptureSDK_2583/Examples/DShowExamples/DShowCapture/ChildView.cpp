// ChildView.cpp : CChildView class
//
// MAGEWELL PROPRIETARY INFORMATION
// This file is a part of the XI Series Adapter Develop SDK.
// Nanjing Magewell Electronics Co., Ltd., All Rights Reserved.
//
// THIS SOURCE FILE IS THE PROPERTY OF MAGEWELL AND IS NOT TO 
// BE RE-DISTRIBUTED BY ANY MEANS WHATSOEVER WITHOUT THE EXPRESSED 
// WRITTEN CONSENT OF MAGEWELL.
//
// THIS SOURCE CODE CAN ONLY BE USED UNDER THE TERMS AND CONDITIONS 
// OUTLINED IN THE MAGEWELL LICENSE AGREEMENT.  MAGEWELL GRANTS TO 
// YOU (ONE SOFTWARE DEVELOPER) THE LIMITED RIGHT TO USE THIS SOFTWARE
// ON A SINGLE COMPUTER.
//
// CONTACT INFORMATION:
// james.liu@magewell.com
// http://www.magewell.com/
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DShowCapture.h"
#include "ChildView.h"

#include "StringUtils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//#define NOAUDIO

#define ABS(x) (((x) > 0) ? (x) : -(x))

#define SAFE_RELEASE(x) { if (x != NULL) x->Release(); x = NULL; }

#define BREAK_IF_FAILED(x) {if (FAILED(x)) break;}

#define VIDEO_WIDTH		720
#define VIDEO_HEIGHT	576
#define VIDEO_FRAMERATE	15
#define VIDEO_BITRATE	6000000
#define VIDEO_COMPLEXITY	1	// 3
#define VIDEO_QUALITY		30	// 100

void WINAPI FreeMediaType(AM_MEDIA_TYPE& mt)
{
	if (mt.cbFormat != 0) {
		CoTaskMemFree((PVOID)mt.pbFormat);

		// Strictly unnecessary but tidier
		mt.cbFormat = 0;
		mt.pbFormat = NULL;
	}
	if (mt.pUnk != NULL) {
		mt.pUnk->Release();
		mt.pUnk = NULL;
	}
}

void WINAPI DeleteMediaType(AM_MEDIA_TYPE *pmt)
{
	// allow NULL pointers for coding simplicity

	if (pmt == NULL) {
		return;
	}

	FreeMediaType(*pmt);
	CoTaskMemFree((PVOID)pmt);
}

// CChildView

CChildView::CChildView()
{
	m_pBuild		= NULL;
	m_pVideoCapture = NULL;
	m_pVideoMoniker = NULL;

	m_pAudioCapture = NULL;
	m_pAudioMoniker = NULL;

	m_pGraph = NULL;
	m_pVideoConfig	= NULL;
	m_pAudioConfig	= NULL;

	m_pVideoWnd		= NULL;
	m_pRender		= NULL;

	m_pVideoPin		= NULL;

	m_pInfTee		= NULL;

	m_pAsfWriter	= NULL;
	m_pFileSink		= NULL;

	for(int i = 0; i < MAX_DEVICE_NUMBER; ++ i) {
		m_pszAudio[i] = NULL;
		m_pszVideo[i] = NULL;
		m_pszVideoPath[i] = NULL;
		m_pszAudioPath[i] = NULL;
	}
}

CChildView::~CChildView()
{
}


BEGIN_MESSAGE_MAP(CChildView, CWnd)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_CLOSE()
	ON_COMMAND(ID_FILE_RECORD, &CChildView::OnStartWriteAsfFile)
	ON_UPDATE_COMMAND_UI(ID_FILE_RECORD, &CChildView::OnUpdateStartAsfFile)
	ON_COMMAND(ID_FILE_STOP, &CChildView::OnStopWriteAsfFile)
	ON_UPDATE_COMMAND_UI(ID_FILE_STOP, &CChildView::OnUpdateStopAsfFile)

	ON_COMMAND_RANGE(ID_DEVICE_DEVICEBEGIN, ID_DEVICE_DEVICEBEGIN + 16, OnDeviceItem)
	ON_UPDATE_COMMAND_UI_RANGE(ID_DEVICE_DEVICEBEGIN, ID_DEVICE_DEVICEBEGIN + 16, OnUpdateDeviceItem)
	//ON_COMMAND(ID_APP_EXIT, &CChildView::OnAppExit)
END_MESSAGE_MAP()



// 

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

void CChildView::OnPaint() 
{
	CPaintDC dc(this); 
}

HRESULT GetMediaTypeDetails(IWMMediaProps * pProps, BYTE** outDetails)
{
	if (pProps == NULL)
		return E_FAIL;

	HRESULT hr = NOERROR;
	BYTE * pMediaType = NULL;
	DWORD cbTypeSize = 0;

	do {
		hr = pProps->GetMediaType(NULL, &cbTypeSize);
		BREAK_IF_FAILED(hr);

		pMediaType = new BYTE[cbTypeSize];
		if (pMediaType == NULL) {
			hr = E_FAIL;
			break;
		}
		hr = pProps->GetMediaType((WM_MEDIA_TYPE*)pMediaType, &cbTypeSize);
	} while (FALSE);

	if (SUCCEEDED(hr)) {
		*outDetails = pMediaType;
	}
	else {
		if (pMediaType) {
			delete [] pMediaType;
			pMediaType = NULL;
		}
	}
	return hr;
}

HRESULT wmCopyMediaType(WM_MEDIA_TYPE** ppmtDest, WM_MEDIA_TYPE* pmtSrc)
{
	*ppmtDest = (WM_MEDIA_TYPE*)new BYTE[sizeof(WM_MEDIA_TYPE) + pmtSrc->cbFormat];
	memcpy(*ppmtDest, pmtSrc, sizeof(WM_MEDIA_TYPE));
	(*ppmtDest)->pbFormat = (((BYTE*)*ppmtDest) + sizeof(WM_MEDIA_TYPE));
	memcpy((*ppmtDest)->pbFormat, pmtSrc->pbFormat, pmtSrc->cbFormat);
	return S_OK;
}

BOOL IsCodecAvailable(IWMProfileManager* pMng, GUID inMajortype, GUID inSubtype, BYTE ** outMt)
{
	HRESULT hr = NOERROR;
	BOOL bFound = FALSE;

	IWMCodecInfo * pCodecInfo = NULL;

	do {
		hr = pMng->QueryInterface(IID_IWMCodecInfo, (void **)&pCodecInfo);
		BREAK_IF_FAILED(hr);

		IWMStreamConfig* pConfig = NULL;
		IWMMediaProps* pProps = NULL;
		BYTE * pMediaType = NULL;

		DWORD cCodecs = 0;
		hr = pCodecInfo->GetCodecInfoCount(inMajortype, &cCodecs);
		for (DWORD i = 0; i < cCodecs; i ++) {
			DWORD cFormats = 0;
			hr = pCodecInfo->GetCodecFormatCount(inMajortype, i, &cFormats);
			BREAK_IF_FAILED(hr);
			if (cFormats < 0)
				continue;

			hr = pCodecInfo->GetCodecFormat(inMajortype, i, 0, &pConfig);
			BREAK_IF_FAILED(hr);

			hr = pConfig->QueryInterface(IID_IWMMediaProps, (void **)&pProps);
			BREAK_IF_FAILED(hr);

			hr = GetMediaTypeDetails(pProps, &pMediaType);
			BREAK_IF_FAILED(hr);

			WM_MEDIA_TYPE* pwmType = (WM_MEDIA_TYPE*) pMediaType;
			if (pwmType->subtype == inSubtype) {
				WMVIDEOINFOHEADER * pvi = (WMVIDEOINFOHEADER*) pwmType->pbFormat;
				LONG formatSize = pwmType->cbFormat;
				LONG viSize = sizeof(WMVIDEOINFOHEADER);
				if (inMajortype == WMMEDIATYPE_Audio) {
					WAVEFORMATEX * pwfx = (WAVEFORMATEX *)(* outMt);

					HRESULT hr2 = NOERROR;
					IWMStreamConfig* pConfig2 = NULL;
					IWMMediaProps* pProp2 = NULL;
					BYTE * pMediaType2 = NULL;

					for (DWORD j = 0; j < cFormats; j ++) {
						hr2 = pCodecInfo->GetCodecFormat(WMMEDIATYPE_Audio, i, j, &pConfig2);
						BREAK_IF_FAILED(hr2);

						hr2 = pConfig2->QueryInterface(IID_IWMMediaProps, (void **)&pProp2);
						BREAK_IF_FAILED(hr2);

						hr2 = GetMediaTypeDetails(pProp2, &pMediaType2);
						BREAK_IF_FAILED(hr2);

						pwmType = (WM_MEDIA_TYPE*)pMediaType2;
						WAVEFORMATEX * pStream = (WAVEFORMATEX *)pwmType->pbFormat;
						if (pStream->nSamplesPerSec == pwfx->nSamplesPerSec
							&& pStream->nChannels == pwfx->nChannels
							&& pStream->wBitsPerSample == pwfx->wBitsPerSample
							&& pStream->nAvgBytesPerSec == pwfx->nAvgBytesPerSec)
						{
							wmCopyMediaType((WM_MEDIA_TYPE**)outMt, pwmType);
							bFound = TRUE;
							break;
						}

						SAFE_RELEASE(pProp2);
						SAFE_RELEASE(pConfig2);
					}
					SAFE_RELEASE(pProp2);
					SAFE_RELEASE(pConfig2);
				}
				else {
					wmCopyMediaType((WM_MEDIA_TYPE**)outMt, pwmType);
					bFound = TRUE;
					break;
				}
			}

			if (pMediaType != NULL) {
				delete [] pMediaType;
				pMediaType = NULL;
			}

			SAFE_RELEASE(pProps);
			SAFE_RELEASE(pConfig);
		}

		if (pMediaType != NULL) {
			delete [] pMediaType;
			pMediaType = NULL;
		}

		SAFE_RELEASE(pProps);
		SAFE_RELEASE(pConfig);
		SAFE_RELEASE(pCodecInfo);

	} while (FALSE);

	return bFound;
}

int CChildView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	m_bRecording = FALSE;

	m_nVideoDeviceSel = 0;
	m_nAudioDeviceSel = 0;
	BOOL bRet = OpenDevice(m_nVideoDeviceSel, m_nAudioDeviceSel);

	if (!bRet) {
		AfxMessageBox(_T("Error Create DShow Object!"));
		return -1;
	}

	return 0;
}

void CChildView::OnDestroy()
{
	ReleaseBuffer();

	for (int i = 0; i < MAX_DEVICE_NUMBER; ++ i) {
		if (m_pszVideo[i]) {
			delete []m_pszVideo[i];
			m_pszVideo[i] = NULL;
		}
		if (m_pszAudio[i]) {
			delete []m_pszAudio[i];
			m_pszAudio[i] = NULL;
		}
		if (m_pszVideoPath[i]) {
			delete []m_pszVideoPath[i];
			m_pszVideoPath[i] = NULL;
		}
		if (m_pszAudioPath[i]) {
			delete []m_pszAudioPath[i];
			m_pszAudioPath[i] = NULL;
		}
	}

	CWnd::OnDestroy();
}

BOOL CChildView::EnumDevice()
{
	// enumerate all video capture devices
	HRESULT hr;
	ICreateDevEnum* pDevEnum = NULL;
	IEnumMoniker* pEnumMoniker = NULL;
	IMoniker *pMoniker = NULL;

	m_nVideoCount = 0;
	m_nAudioCount = 0;

	do {
		hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER, IID_ICreateDevEnum, (void**)&pDevEnum);
		BREAK_IF_FAILED(hr);

		hr = pDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory, &pEnumMoniker, 0);
		BREAK_IF_FAILED(hr);

		pEnumMoniker->Reset();

		ULONG cFetched;
		while ((pEnumMoniker->Next(1, &pMoniker, &cFetched) == S_OK)) {
			IPropertyBag* pBag = NULL;
			hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pBag);
			if (SUCCEEDED(hr)) {
				VARIANT var;
				var.vt = VT_BSTR;
				hr = pBag->Read(L"FriendlyName", &var, NULL);
				CAutoConvertString strDevice(var.bstrVal);
				
				CComBSTR bstrDevID;
				LPOLESTR strName = NULL;
				hr = pMoniker->GetDisplayName(NULL, NULL, &strName);
				if (SUCCEEDED(hr)) {
					bstrDevID = strName;
					CoTaskMemFree(strName);
				}
				TCHAR * pszDevID = _tcsdup(bstrDevID);

				if (m_pszVideo[m_nVideoCount]) {
					delete []m_pszVideo[m_nVideoCount];
					m_pszVideo[m_nVideoCount] = NULL;
				}
				m_pszVideo[m_nVideoCount] = new TCHAR[MAX_DEVICE_NAME];
				wcscpy_s(m_pszVideo[m_nVideoCount], MAX_DEVICE_NAME, (WCHAR *)strDevice);
				
				if (m_pszVideoPath[m_nVideoCount]) {
					delete []m_pszVideoPath[m_nVideoCount];
					m_pszVideoPath[m_nVideoCount] = NULL;
				}
				m_pszVideoPath[m_nVideoCount] = new TCHAR[MAX_PATH];
				wcscpy_s(m_pszVideoPath[m_nVideoCount], MAX_PATH, pszDevID);

				if (pszDevID)
					free(pszDevID);

				m_nVideoCount++;

				pBag->Release();
			}
			SAFE_RELEASE(pMoniker);
		}

		SAFE_RELEASE(pEnumMoniker);
		hr = pDevEnum->CreateClassEnumerator(CLSID_AudioInputDeviceCategory, &pEnumMoniker, 0);
		BREAK_IF_FAILED(hr);

		pEnumMoniker->Reset();

		while ((pEnumMoniker->Next(1, &pMoniker, &cFetched) == S_OK)) {
			IPropertyBag* pBag = NULL;
			hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pBag);
			if (SUCCEEDED(hr)) {
				VARIANT var;
				var.vt = VT_BSTR;
				hr = pBag->Read(L"FriendlyName", &var, NULL);
				CAutoConvertString strDevice(var.bstrVal);
				
				CComBSTR bstrDevID;
				LPOLESTR strName = NULL;
				hr = pMoniker->GetDisplayName(NULL, NULL, &strName);
				if (SUCCEEDED(hr)) {
					bstrDevID = strName;
					CoTaskMemFree(strName);
				}
				TCHAR * pszDevID = _tcsdup(bstrDevID);

				if (m_pszAudio[m_nAudioCount]) {
					delete []m_pszAudio[m_nAudioCount];
					m_pszAudio[m_nAudioCount] = NULL;
				}
				m_pszAudio[m_nAudioCount] = new TCHAR[MAX_DEVICE_NAME];
				wcscpy_s(m_pszAudio[m_nAudioCount], MAX_DEVICE_NAME, (WCHAR *)strDevice);
				
				if (m_pszAudioPath[m_nAudioCount]) {
					delete []m_pszAudioPath[m_nAudioCount];
					m_pszAudioPath[m_nAudioCount] = NULL;
				}
				m_pszAudioPath[m_nAudioCount] = new TCHAR[MAX_PATH];
				wcscpy_s(m_pszAudioPath[m_nAudioCount], MAX_PATH, pszDevID);

				if (pszDevID)
					free(pszDevID);

				m_nAudioCount++;

				pBag->Release();

			}
			SAFE_RELEASE(pMoniker);
		}
	} while (FALSE);

	SAFE_RELEASE(pDevEnum);
	SAFE_RELEASE(pEnumMoniker);

	return ((m_nVideoCount > 0) || (m_nAudioCount > 0));
}

BOOL CChildView::OpenDevice(int nVideo, int nAudio)
{
	HRESULT hr;

	BOOL bRet = FALSE;
	do {
		hr = CoCreateInstance(CLSID_CaptureGraphBuilder2, NULL, CLSCTX_INPROC_SERVER, IID_ICaptureGraphBuilder2, (void**)&m_pBuild);
		BREAK_IF_FAILED(hr);

		hr = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC, IID_IGraphBuilder, (LPVOID *)&m_pGraph);
		BREAK_IF_FAILED(hr);

		hr = m_pBuild->SetFiltergraph(m_pGraph);
		BREAK_IF_FAILED(hr);

		OpenVideoDevice(nVideo);
#ifndef NOAUDIO
		if (nAudio >= 0)
			OpenAudioDevice(nAudio);
#endif
		hr = CoCreateInstance(CLSID_InfTee, 0,  CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&m_pInfTee);
		BREAK_IF_FAILED(hr);

		hr = m_pGraph->AddFilter(m_pInfTee, L"Infinite Pin Tee");
		BREAK_IF_FAILED(hr);

		hr = m_pBuild->RenderStream(NULL, &MEDIATYPE_Video, m_pVideoCapture, NULL, m_pInfTee);
		BREAK_IF_FAILED(hr);

		// set format
		SetAVFormat();

		// Create Tee
		SetAVProfile();

		// create renderer
		CreateVideoRender();

		//IReferenceClock *pClock = NULL;
		//hr = CoCreateInstance(CLSID_SystemClock, NULL, CLSCTX_INPROC_SERVER, IID_IReferenceClock, reinterpret_cast <void **> (&pClock));
		//IEnumFilters *pFiltEnum = NULL;
		//hr = m_pGraph->EnumFilters(&pFiltEnum);
		//if (FAILED (hr))
		//	throw "Couldn't enumerate graph filters";

		//IBaseFilter *pCurrFilt = NULL;
		//while (pFiltEnum->Next(1, &pCurrFilt, 0) == S_OK)
		//{
		//	pCurrFilt->SetSyncSource(NULL);
		//	pCurrFilt->Release();
		//}
		//SAFE_RELEASE(pClock);
		//SAFE_RELEASE(pFiltEnum);

		// Set the graph clock.
		//IMediaFilter *pMediaFilter = 0;
		//hr = m_pGraph->QueryInterface(IID_IMediaFilter, (void**)&pMediaFilter);
		//hr = pMediaFilter->SetSyncSource(NULL);
		//pMediaFilter->Release();

		//m_pGraph->SetDefaultSyncSource();

		// run preview
		Run();

		bRet = TRUE;
	} while (FALSE);

	return TRUE;
}

BOOL CChildView::OpenVideoDevice( int nIndex )
{
	HRESULT hr;
	ICreateDevEnum* pDevEnum = NULL;
	IEnumMoniker* pEnumMoniker = NULL;
	IMoniker *pMoniker = NULL;

	SAFE_RELEASE(m_pVideoMoniker);

	do {
		hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER, IID_ICreateDevEnum, (void**)&pDevEnum);
		if (hr != NOERROR)
			break;

		hr = pDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory, &pEnumMoniker, 0);
		if (hr != NOERROR)
			break;

		pEnumMoniker->Reset();

		ULONG cFetched;
		while ((pEnumMoniker->Next(1, &pMoniker, &cFetched) == S_OK)) {
			IPropertyBag* pBag = NULL;
			hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pBag);
			if (SUCCEEDED(hr)) {
				VARIANT var;
				var.vt = VT_BSTR;
				hr = pBag->Read(L"FriendlyName", &var, NULL);
				CAutoConvertString strDevice(var.bstrVal);
				
				CComBSTR bstrDevID;
				LPOLESTR strName = NULL;
				hr = pMoniker->GetDisplayName(NULL, NULL, &strName);
				if (SUCCEEDED(hr)) {
					bstrDevID = strName;
					CoTaskMemFree(strName);
				}
				TCHAR * pszDevID = _tcsdup(bstrDevID);

				BOOL bFind = FALSE;
				if (hr == NOERROR) {
					if (_tcsicmp(pszDevID, m_pszVideoPath[nIndex]) == 0) {
						bFind = TRUE;
						m_pVideoMoniker = pMoniker;
					}
				}

				if (pszDevID)
					free(pszDevID);

				pBag->Release();

				if (bFind)
					break;
			}
			SAFE_RELEASE(pMoniker);
		}

		hr = m_pVideoMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**)&m_pVideoCapture);
		BREAK_IF_FAILED(hr);

		hr = m_pGraph->AddFilter(m_pVideoCapture, m_pszVideo[nIndex]);
		BREAK_IF_FAILED(hr);

		hr = m_pBuild->FindPin(m_pVideoCapture, PINDIR_OUTPUT, &PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, TRUE, 0, &m_pVideoPin);
		if (FAILED(hr) || m_pVideoPin == NULL) {
			hr = m_pBuild->FindPin(m_pVideoCapture, PINDIR_OUTPUT, NULL, &MEDIATYPE_Video, TRUE, 0, &m_pVideoPin);
		}

		if (NULL == m_pVideoPin)
			break;

		hr = m_pBuild->FindInterface(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Interleaved, m_pVideoCapture, IID_IAMStreamConfig, (void **)&m_pVideoConfig);
		if (FAILED(hr)) {
			hr = m_pBuild->FindInterface(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, m_pVideoCapture, IID_IAMStreamConfig, (void **)&m_pVideoConfig);
			if (FAILED(hr)) {
				OutputDebugString(_T("Cannot find VideoCapture:IAMStreamConfig\n"));
			}
		}

	} while (FALSE);

	SAFE_RELEASE(pDevEnum);
	SAFE_RELEASE(pEnumMoniker);

	return TRUE;
}

void CChildView::CloseVideoDevice()
{
	Stop(TRUE);
	m_pGraph->RemoveFilter(m_pVideoCapture);

	ReleaseBuffer();
}

BOOL CChildView::OpenAudioDevice( int nIndex )
{
	HRESULT hr;
	ICreateDevEnum* pDevEnum = NULL;
	IEnumMoniker* pEnumMoniker = NULL;
	IMoniker *pMoniker = NULL;

	SAFE_RELEASE(m_pAudioMoniker);

	do {
		hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER, IID_ICreateDevEnum, (void**)&pDevEnum);
		BREAK_IF_FAILED(hr);

		hr = pDevEnum->CreateClassEnumerator(CLSID_AudioInputDeviceCategory, &pEnumMoniker, 0);
		BREAK_IF_FAILED(hr);

		pEnumMoniker->Reset();

		ULONG cFetched;
		while ((pEnumMoniker->Next(1, &pMoniker, &cFetched) == S_OK)) {
			IPropertyBag* pBag = NULL;
			hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pBag);
			if (SUCCEEDED(hr)) {
				VARIANT var;
				var.vt = VT_BSTR;
				hr = pBag->Read(L"FriendlyName", &var, NULL);
				CAutoConvertString strDevice(var.bstrVal);
								
				CComBSTR bstrDevID;
				LPOLESTR strName = NULL;
				hr = pMoniker->GetDisplayName(NULL, NULL, &strName);
				if (SUCCEEDED(hr)) {
					bstrDevID = strName;
					CoTaskMemFree(strName);
				}
				TCHAR * pszDevID = _tcsdup(bstrDevID);

				BOOL bFind = FALSE;
				if (hr == NOERROR) {
					if (_tcsicmp(pszDevID, m_pszAudioPath[nIndex]) == 0) {
						bFind = TRUE;
						m_pAudioMoniker = pMoniker;
					}
				}
				
				if (pszDevID)
					free(pszDevID);

				pBag->Release();

				if (bFind)
					break;
			}
			SAFE_RELEASE(pMoniker);
		}

		hr = m_pAudioMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**)&m_pAudioCapture);
		BREAK_IF_FAILED(hr);

		hr = m_pGraph->AddFilter(m_pAudioCapture, m_pszAudio[nIndex]);
		BREAK_IF_FAILED(hr);

		hr = m_pBuild->FindInterface(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Interleaved, m_pAudioCapture, IID_IAMStreamConfig, (void **)&m_pAudioConfig);
		if (FAILED(hr)) {
			hr = m_pBuild->FindInterface(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Audio, m_pAudioCapture, IID_IAMStreamConfig, (void **)&m_pAudioConfig);
			if (FAILED(hr)) {
				OutputDebugString(_T("Cannot find VideoCapture:IAMStreamConfig\n"));
			}
		}
		
#ifndef NOAUDIO
		if (m_nAudioDeviceSel >= 0) {
			// set format
			AM_MEDIA_TYPE * pmtAudio = NULL;
			hr = m_pAudioConfig->GetFormat(&pmtAudio);
			BREAK_IF_FAILED(hr);

			// Fill in values for the new format
			WAVEFORMATEX *pwfe = (WAVEFORMATEX *) pmtAudio->pbFormat;
			pwfe->nChannels = (WORD) 2;
			pwfe->nSamplesPerSec = 44100;
			pwfe->wBitsPerSample = 16;
			pwfe->nAvgBytesPerSec = 176400;
			pwfe->nBlockAlign = 4;

			// Set the new formattype for the output pin
			hr = m_pAudioConfig->SetFormat(pmtAudio);
			DeleteMediaType(pmtAudio);

			IAMBufferNegotiation* pNeg = NULL; 
			hr = m_pBuild->FindInterface(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Audio, m_pAudioCapture, IID_IAMBufferNegotiation, (void **)&pNeg);

			ALLOCATOR_PROPERTIES prop={0};
			prop.cbBuffer = 7056;	// Size of an audio frame
			prop.cBuffers = 32;		// Count of buffer
			prop.cbAlign = 16;
			hr = pNeg->SuggestAllocatorProperties(&prop);
			pNeg->Release();
		}
#endif

	} while (FALSE);

	SAFE_RELEASE(pDevEnum);
	SAFE_RELEASE(pEnumMoniker);

	return TRUE;
}

void CChildView::CloseAudioDevice()
{
	m_pGraph->RemoveFilter(m_pAudioCapture);
	SAFE_RELEASE(m_pAudioCapture);
	SAFE_RELEASE(m_pAudioConfig);
}

void CChildView::ReleaseBuffer()
{
	SAFE_RELEASE(m_pVideoPin);
	SAFE_RELEASE(m_pInfTee);

	SAFE_RELEASE(m_pAsfWriter);
	SAFE_RELEASE(m_pFileSink);

	SAFE_RELEASE(m_pRender);
	SAFE_RELEASE(m_pVideoWnd);

	SAFE_RELEASE(m_pVideoConfig);
	SAFE_RELEASE(m_pAudioConfig);
	SAFE_RELEASE(m_pGraph);
	SAFE_RELEASE(m_pVideoCapture);
	SAFE_RELEASE(m_pVideoMoniker);
	SAFE_RELEASE(m_pBuild);

	SAFE_RELEASE(m_pAudioCapture);
	SAFE_RELEASE(m_pAudioMoniker);

	SAFE_RELEASE(m_pProfile);
}

void CChildView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if (m_pVideoWnd != NULL) {
		CRect rcVideo;
		GetClientRect(rcVideo);
		m_pVideoWnd->SetWindowPosition(rcVideo.left, rcVideo.top, rcVideo.Width(), rcVideo.Height());
	}
}

BOOL CChildView::Run()
{
	if (m_pGraph == NULL)
		return FALSE;

	IMediaControl* pControl = NULL;
	HRESULT hr = m_pGraph->QueryInterface(IID_IMediaControl, (void **)&pControl);
	if (FAILED(hr))
		return FALSE;

	hr = pControl->Run();
	SAFE_RELEASE(pControl);

	return SUCCEEDED(hr);
}

void CChildView::Stop(BOOL bDestoryVideoRender)
{
	if (m_pGraph == NULL)
		return;

	IMediaControl* pControl = NULL;

	HRESULT hr = m_pGraph->QueryInterface(IID_IMediaControl, (void **)&pControl);
	if (FAILED(hr))
		return;

	pControl->Stop();
	SAFE_RELEASE(pControl);

	if (bDestoryVideoRender) {
		m_pVideoWnd->put_Visible(OAFALSE);
		m_pVideoWnd->put_Owner(NULL);
	}
	
	m_bRecording = FALSE;
}


BOOL CChildView::OnEraseBkgnd(CDC* pDC)
{
	// TODO: Add your message handler code here and/or call default

	return TRUE;
}

void CChildView::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	Stop(TRUE);

	CWnd::OnClose();
}

BOOL CChildView::CreateVideoRender()
{
	HRESULT hr;
	do {
		hr = CoCreateInstance(CLSID_VideoMixingRenderer9, 0,  CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&m_pRender);
		BREAK_IF_FAILED(hr);

		hr = m_pGraph->AddFilter(m_pRender, L"VMR9");
		BREAK_IF_FAILED(hr);

		hr = m_pBuild->RenderStream(NULL, &MEDIATYPE_Video, m_pInfTee, NULL, m_pRender);
		BREAK_IF_FAILED(hr);

		hr = m_pGraph->QueryInterface(IID_IVideoWindow, (void **)&m_pVideoWnd);
		BREAK_IF_FAILED(hr);

		m_pVideoWnd->put_Owner((OAHWND)GetSafeHwnd()); 
		m_pVideoWnd->put_WindowStyle(WS_CHILD);   

		CRect rcVideo;
		GetClientRect(rcVideo);

		m_pVideoWnd->SetWindowPosition(rcVideo.left, rcVideo.top, rcVideo.right - rcVideo.left, rcVideo.bottom - rcVideo.top);
		m_pVideoWnd->put_Visible(OATRUE);

		//hr = m_pRender->SetSyncSource(NULL);

	} while (FALSE);

	return SUCCEEDED(hr);
}

BOOL CChildView::SetAVProfile()
{
	HRESULT hr;
	do {
		// use custome profile
		CComPtr <IWMProfileManager> pProfileMng;
		hr = WMCreateProfileManager( &pProfileMng );
		BREAK_IF_FAILED(hr);

		CComPtr <IWMStreamConfig> pVideoConfig;
		CComPtr <IWMMediaProps> pVideoMediaProps;

		CComPtr <IWMPropertyVault> pVault;
		CComPtr <IWMVideoMediaProps> pVidProps;

		CComPtr <IWMStreamConfig> pAudioConfig;
		CComPtr <IWMMediaProps> pAudioMediaProps;

		WM_MEDIA_TYPE * pMediaType = NULL;
		BYTE * pType = NULL;

		do {
			hr = pProfileMng->CreateEmptyProfile(WMT_VER_9_0, &m_pProfile);
			BREAK_IF_FAILED(hr);

			hr = m_pProfile->CreateNewStream(WMMEDIATYPE_Video, &pVideoConfig);
			BREAK_IF_FAILED(hr);

			hr = pVideoConfig->QueryInterface(IID_IWMMediaProps, (void**)&pVideoMediaProps);
			BREAK_IF_FAILED(hr);

			hr = m_pProfile->AddStream(pVideoConfig);
			BREAK_IF_FAILED(hr);

			if (!IsCodecAvailable(pProfileMng, WMMEDIATYPE_Video, WMMEDIASUBTYPE_WMV3, &pType)) {
				hr = E_FAIL;
				break;
			}

			pMediaType = (WM_MEDIA_TYPE*)pType;
			WMVIDEOINFOHEADER* pVidHdr = (WMVIDEOINFOHEADER*)pMediaType->pbFormat;
			pVidHdr->AvgTimePerFrame = 10000000 / VIDEO_FRAMERATE;
			pVidHdr->dwBitRate = VIDEO_BITRATE;
			pVidHdr->bmiHeader.biWidth = VIDEO_WIDTH;
			pVidHdr->bmiHeader.biHeight = VIDEO_HEIGHT;

			hr = pVideoMediaProps->QueryInterface(IID_IWMPropertyVault, (void **)&pVault);
			if (SUCCEEDED(hr)) {
				WMT_ATTR_DATATYPE attrType;
				WORD level = 0;
				DWORD dwSize = sizeof(WORD);
				hr = pVault->GetPropertyByName(g_wszComplexity, &attrType, (BYTE*) &level, &dwSize);
				level = VIDEO_COMPLEXITY;
				hr = pVault->SetProperty(g_wszComplexity, WMT_TYPE_WORD, (BYTE*) &level, dwSize);
			}

			hr = pVideoMediaProps->QueryInterface(IID_IWMVideoMediaProps, (void **)&pVidProps);
			if (SUCCEEDED(hr)) {
				DWORD quality = 0;
				hr = pVidProps->GetQuality(&quality);
				quality = VIDEO_QUALITY;
				hr = pVidProps->SetQuality(quality);

				LONGLONG maxSpacing = 0;
				hr = pVidProps->GetMaxKeyFrameSpacing(&maxSpacing);
				maxSpacing = 2 * 10000000;
				hr = pVidProps->SetMaxKeyFrameSpacing(maxSpacing);
			}

			hr = pVideoMediaProps->SetMediaType(pMediaType);
			BREAK_IF_FAILED(hr);

			hr = pVideoConfig->SetStreamName(L"Video Stream");
			hr = pVideoConfig->SetConnectionName(L"Video");
			hr = pVideoConfig->SetBitrate(VIDEO_BITRATE);
			hr = pVideoConfig->SetBufferWindow(5000);
			hr = pVideoConfig->SetStreamNumber(1);

			hr = m_pProfile->ReconfigStream(pVideoConfig);
			BREAK_IF_FAILED(hr);

			if (pType != NULL) {
				delete [] pType;
				pType = NULL;
			}
#ifndef NOAUDIO
			if (m_nAudioDeviceSel >= 0) {
				hr = m_pProfile->CreateNewStream(WMMEDIATYPE_Audio, &pAudioConfig);
				BREAK_IF_FAILED(hr);

				hr = pAudioConfig->QueryInterface(IID_IWMMediaProps, (void**)&pAudioMediaProps);
				BREAK_IF_FAILED(hr);

				hr = m_pProfile->AddStream(pAudioConfig);
				BREAK_IF_FAILED(hr);

				WAVEFORMATEX wfx = {0};
				wfx.nSamplesPerSec = 8000;//22050;
				wfx.nChannels = 1;
				wfx.wBitsPerSample = 16;
				wfx.nAvgBytesPerSec = 500;//2500;

				pType = (BYTE*)&wfx;

				if (!IsCodecAvailable(pProfileMng, WMMEDIATYPE_Audio, WMMEDIASUBTYPE_WMAudioV8, &pType)) {
					hr = E_FAIL;
					break;
				}

				pMediaType = (WM_MEDIA_TYPE *) pType;
				hr = pAudioMediaProps->SetMediaType(pMediaType);
				BREAK_IF_FAILED(hr);

				hr = pAudioConfig->SetStreamName(L"Audio Stream");
				hr = pAudioConfig->SetConnectionName(L"Audio");
				hr = pAudioConfig->SetBitrate(176400);
				hr = pAudioConfig->SetBufferWindow(3000);
				hr = pAudioConfig->SetStreamNumber(2);

				hr = m_pProfile->ReconfigStream(pAudioConfig);
				BREAK_IF_FAILED(hr);
			}	

			if (m_nAudioDeviceSel != -1) {
				hr = m_pBuild->RenderStream(NULL, &MEDIATYPE_Audio, m_pAudioCapture, NULL, m_pAsfWriter);
				BREAK_IF_FAILED(hr);
			}
#endif
		} while (FALSE);

		m_pProfile->SetName(L"Profile");
		m_pProfile->SetDescription(L"DShow Profile");
	} while (FALSE);

	if (FAILED(hr))
		return FALSE;

	return TRUE;
}

BOOL CChildView::SetAVFormat()
{
	HRESULT hr;
	do {
		int iCount = 0, iSize = 0;
		hr = m_pVideoConfig->GetNumberOfCapabilities(&iCount, &iSize);
		// Check the size to make sure we pass in the correct structure.
		if (iSize == sizeof(VIDEO_STREAM_CONFIG_CAPS)) {
			// Use the video capabilities structure.
			for (int iFormat = 0; iFormat < iCount; iFormat++) {
				VIDEO_STREAM_CONFIG_CAPS scc;
				AM_MEDIA_TYPE *pmtConfig;
				hr = m_pVideoConfig->GetStreamCaps(iFormat, &pmtConfig, (BYTE*)&scc);
				if (SUCCEEDED(hr)) {
					/* Examine the format, and possibly use it. */
					if ((pmtConfig->majortype == MEDIATYPE_Video) && (pmtConfig->subtype == MEDIASUBTYPE_YUY2)) {
						if (pmtConfig->formattype == FORMAT_VideoInfo) {
							VIDEOINFO * pVideoInfo = (VIDEOINFO*) pmtConfig->pbFormat;

							LONG lWidth = HEADER(pmtConfig->pbFormat)->biWidth;
							LONG lHeight = ABS(HEADER(pmtConfig->pbFormat)->biHeight);

							TRACE(_T("lWidth = %d, lHeight = %d, Framerate = %.2f\n"), lWidth, lHeight,
								(float)((double)10000000 / pVideoInfo->AvgTimePerFrame));

							TRACE(_T("lWidth = %d, lHeight = %d\n"), lWidth, lHeight);

							if (lWidth == VIDEO_WIDTH && lHeight == VIDEO_HEIGHT) {
								//hr = m_pVideoConfig->SetFormat(pmtConfig);
								//DeleteMediaType(pmtConfig);
								//break;
							}
							if (lWidth == 1920 && lHeight == 1080 && pVideoInfo->AvgTimePerFrame == 400000) {
								break;
							}
						}
					}

					DeleteMediaType(pmtConfig);
				}
			}
		}

		AM_MEDIA_TYPE* pmt = NULL;
		// default capture format
		if (m_pVideoConfig && m_pVideoConfig->GetFormat(&pmt) == S_OK) {
			if (pmt->formattype == FORMAT_VideoInfo)
			{
				VIDEOINFO * pVideoInfo = (VIDEOINFO*) pmt->pbFormat;
				
				LONG lWidth = HEADER(pmt->pbFormat)->biWidth;
				LONG lHeight = ABS(HEADER(pmt->pbFormat)->biHeight);

				HEADER(pmt->pbFormat)->biWidth = 3072;
				HEADER(pmt->pbFormat)->biHeight = 768;

				pVideoInfo->AvgTimePerFrame = (int)(10000000 / 25);
				pmt->subtype = MEDIASUBTYPE_YUY2;

				hr = m_pVideoConfig->SetFormat(pmt);
			}

			if (pmt->formattype == FORMAT_VideoInfo) {
				m_lWidth = HEADER(pmt->pbFormat)->biWidth;
				m_lHeight = ABS(HEADER(pmt->pbFormat)->biHeight);
			}

			DeleteMediaType(pmt);
		}


	} while (FALSE);

	return SUCCEEDED(hr);
}


void CChildView::OnStartWriteAsfFile()
{
	if (m_nAudioDeviceSel == -1 || m_nVideoDeviceSel == -1) {
		MessageBox(_T("Start write asf file fail !\nOne video channel and one audio channel must be selected !"), _T("DShowCapture"));
		return;
	}

	CFileDialog dlg(FALSE, _T("asf"), NULL, 4 | 2, _T("ASF Files (*.asf)|*.asf||"), this);
	if (dlg.DoModal() != IDOK) 
		return ;

	CString strFilePath = dlg.GetPathName();//CString("1.asf");//dlg.GetPathName();
	TRACE(strFilePath);

	Stop(FALSE);

	HRESULT hr = S_FALSE;
	do {
		hr = CoCreateInstance(CLSID_WMAsfWriter, 0,  CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&m_pAsfWriter);
		BREAK_IF_FAILED(hr);

		hr = m_pAsfWriter->QueryInterface(IID_IFileSinkFilter, (void **) &m_pFileSink);
		BREAK_IF_FAILED(hr);

		hr = m_pFileSink->SetFileName(strFilePath, NULL);
		hr = m_pFileSink->SetMode(AM_FILE_OVERWRITE);
		BREAK_IF_FAILED(hr);

		hr = m_pGraph->AddFilter(m_pAsfWriter, L"Asf Writer");
		BREAK_IF_FAILED(hr);

		IConfigAsfWriter *pAsf = NULL;
		hr = m_pAsfWriter->QueryInterface( IID_IConfigAsfWriter, (void**)&pAsf);
		if (pAsf != NULL) {
			hr = pAsf->ConfigureFilterUsingProfile(m_pProfile);
			SAFE_RELEASE(pAsf);
		}

		hr = m_pBuild->RenderStream(NULL, &MEDIATYPE_Video, m_pInfTee, NULL, m_pAsfWriter);
		BREAK_IF_FAILED(hr);

#ifndef NOAUDIO
		if (m_nAudioDeviceSel != -1) {
			hr = m_pBuild->RenderStream(NULL, &MEDIATYPE_Audio, m_pAudioCapture, NULL, m_pAsfWriter);
			BREAK_IF_FAILED(hr);
		}
#endif

		m_bRecording = Run();
		return;
	} while(FALSE);

	Run();
}


void CChildView::OnUpdateStartAsfFile(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_pAsfWriter == NULL);
}


void CChildView::OnStopWriteAsfFile()
{
	Stop(FALSE);

	HRESULT hr = m_pGraph->RemoveFilter(m_pAsfWriter);
	SAFE_RELEASE(m_pAsfWriter);
	SAFE_RELEASE(m_pFileSink);

	Run();
	// TODO: Add your command handler code here
}


void CChildView::OnUpdateStopAsfFile(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(m_pAsfWriter && m_pFileSink);
}

void CChildView::OnDeviceItem(UINT nID)
{
	if (nID > (UINT)(ID_DEVICE_DEVICEBEGIN + m_nVideoCount)) {
		int nAudioSel = nID - ID_DEVICE_DEVICEBEGIN - m_nVideoCount - 1;
		if (nAudioSel != m_nAudioDeviceSel) {
			CloseAudioDevice();

			OpenAudioDevice(nAudioSel);
			m_nAudioDeviceSel = nAudioSel;
		}
	}
	else {	
		int nVideoIndex = nID - ID_DEVICE_DEVICEBEGIN - 1;
		if (nVideoIndex != m_nVideoDeviceSel) {
			m_nVideoDeviceSel = nVideoIndex;

			CloseVideoDevice();

			if (!OpenDevice(nVideoIndex, m_nAudioDeviceSel)) {
				AfxMessageBox(_T("Open device error!"));
			}
		}
	}
}

void CChildView::OnUpdateDeviceItem(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(!m_bRecording);

	if (pCmdUI->m_nID <= (UINT)(ID_DEVICE_DEVICEBEGIN + m_nVideoCount)) {
		if (pCmdUI->m_nID == ID_DEVICE_DEVICEBEGIN + 1 + m_nVideoDeviceSel)
			pCmdUI->SetCheck(TRUE);
		else
			pCmdUI->SetCheck(FALSE);
	}
	else {
		if (pCmdUI->m_nID == ID_DEVICE_DEVICEBEGIN + 1 + m_nVideoCount + m_nAudioDeviceSel)
			pCmdUI->SetCheck(TRUE);
		else
			pCmdUI->SetCheck(FALSE);
	}
}

void CChildView::OnAppExit()
{
	// TODO: Add your command handler code here
	if (0) {
		int iCount = 0, iSize = 0;
		HRESULT hr = m_pVideoConfig->GetNumberOfCapabilities(&iCount, &iSize);
		// Check the size to make sure we pass in the correct structure.
		if (iSize == sizeof(VIDEO_STREAM_CONFIG_CAPS)) {
			// Use the video capabilities structure.
			for (int iFormat = 0; iFormat < iCount; iFormat++) {
				VIDEO_STREAM_CONFIG_CAPS scc;
				AM_MEDIA_TYPE *pmtConfig;
				hr = m_pVideoConfig->GetStreamCaps(iFormat, &pmtConfig, (BYTE*)&scc);
				if (SUCCEEDED(hr)) {
					/* Examine the format, and possibly use it. */
					if ((pmtConfig->majortype == MEDIATYPE_Video) && (pmtConfig->subtype == MEDIASUBTYPE_YUY2)) {
						if (pmtConfig->formattype == FORMAT_VideoInfo) {
							VIDEOINFO * pVideoInfo = (VIDEOINFO*) pmtConfig->pbFormat;

							LONG lWidth = HEADER(pmtConfig->pbFormat)->biWidth;
							LONG lHeight = ABS(HEADER(pmtConfig->pbFormat)->biHeight);

							TRACE(_T("lWidth = %d, lHeight = %d, Framerate = %d\n"), lWidth, lHeight,
								(int)(10000000 / pVideoInfo->AvgTimePerFrame));
						}
					}
				}
			}
		}
	}

	OnClose();
}
