// MainFrm.cpp : CMainFrame class
//
// MAGEWELL PROPRIETARY INFORMATION
// This file is a part of the XI Series Adapter Develop SDK.
// Nanjing Magewell Electronics Co., Ltd., All Rights Reserved.
//
// THIS SOURCE FILE IS THE PROPERTY OF MAGEWELL AND IS NOT TO 
// BE RE-DISTRIBUTED BY ANY MEANS WHATSOEVER WITHOUT THE EXPRESSED 
// WRITTEN CONSENT OF MAGEWELL.
//
// THIS SOURCE CODE CAN ONLY BE USED UNDER THE TERMS AND CONDITIONS 
// OUTLINED IN THE MAGEWELL LICENSE AGREEMENT.  MAGEWELL GRANTS TO 
// YOU (ONE SOFTWARE DEVELOPER) THE LIMITED RIGHT TO USE THIS SOFTWARE
// ON A SINGLE COMPUTER.
//
// CONTACT INFORMATION:
// james.liu@magewell.com
// http://www.magewell.com/
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DShowCapture.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define TIMER_ID 1

// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_TIMER()
	ON_WM_DESTROY()

END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	m_llRecordTime = 0;
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	m_wndView.EnumDevice();

	// Init Video Device
	int nVideo = m_wndView.GetDeviceCount(TRUE);
	TCHAR szName[MAX_DEVICE_NAME] = {0};
	CMenu* pMenu = GetMenu();
	CMenu* pSubMenu = pMenu->GetSubMenu(1);
	for (int i = 0; i < nVideo; i ++) {
		if (m_wndView.GetDeviceName(TRUE, i, szName)) {
			pSubMenu->InsertMenu(i + 1, MF_BYPOSITION, ID_DEVICE_DEVICEBEGIN + 1 + i, szName);
		}
	}

	// Init Audio Device
	int nAudio = m_wndView.GetDeviceCount(FALSE);
	if (nAudio > 0) {
		pSubMenu->InsertMenu(nVideo + 1, MF_BYPOSITION, MF_SEPARATOR);

		for (int i = 0; i < nAudio; i ++) {
			if (m_wndView.GetDeviceName(FALSE, i, szName)) {
				pSubMenu->InsertMenu(nVideo + i + 2, MF_BYPOSITION, ID_DEVICE_DEVICEBEGIN + nVideo + 1 + i, szName);
			}
		}
	}

	pSubMenu->DeleteMenu(0, MF_BYPOSITION);

	// create a view to occupy the client area of the frame
	if (!m_wndView.Create(NULL, NULL, AFX_WS_DEFAULT_VIEW, CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL))
	{
		TRACE0("Failed to create view window\n");
		return -1;
	}

	if (!m_wndStatusBar.Create(this))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
	m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT));
	
	SetTimer(TIMER_ID, 1000, NULL);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.lpszClass = AfxRegisterWndClass(0);
	return TRUE;
}

// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG


void CMainFrame::OnDestroy()
{
	KillTimer(TIMER_ID);

	CFrameWnd::OnDestroy();
}

// CMainFrame message handlers

void CMainFrame::OnSetFocus(CWnd* /*pOldWnd*/)
{
	// forward focus to the view window
	m_wndView.SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	// let the view have first crack at the command
	if (m_wndView.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	// otherwise, do default handling
	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

void CMainFrame::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == TIMER_ID) {
		LONG cx = 0;
		LONG cy = 0;
		m_wndView.GetVideoFormat(&cx, &cy);

		CString strInfo;
		strInfo.Format(_T("%d x %d  "), cx, cy);
		if (m_wndView.IsRecording()) {
			m_llRecordTime++;
			strInfo.AppendFormat(_T("Recording : %02lld:%02lld:%02lld"), m_llRecordTime / 3600, (m_llRecordTime / 60) % 60, m_llRecordTime % 60);
		}
		else {
			m_llRecordTime = 0;
		}
		m_wndStatusBar.SetWindowText (strInfo);
	}

	CFrameWnd::OnTimer(nIDEvent);
}
