// ChildView.h : header file
//
// MAGEWELL PROPRIETARY INFORMATION
// This file is a part of the XI Series Adapter Develop SDK.
// Nanjing Magewell Electronics Co., Ltd., All Rights Reserved.
//
// THIS SOURCE FILE IS THE PROPERTY OF MAGEWELL AND IS NOT TO 
// BE RE-DISTRIBUTED BY ANY MEANS WHATSOEVER WITHOUT THE EXPRESSED 
// WRITTEN CONSENT OF MAGEWELL.
//
// THIS SOURCE CODE CAN ONLY BE USED UNDER THE TERMS AND CONDITIONS 
// OUTLINED IN THE MAGEWELL LICENSE AGREEMENT.  MAGEWELL GRANTS TO 
// YOU (ONE SOFTWARE DEVELOPER) THE LIMITED RIGHT TO USE THIS SOFTWARE
// ON A SINGLE COMPUTER.
//
// CONTACT INFORMATION:
// james.liu@magewell.com
// http://www.magewell.com/
//
//////////////////////////////////////////////////////////////////////



#pragma once

#include <dshow.h>
#include <ks.h>

#include <initguid.h>

#pragma comment(lib, "strmiids.lib")
#pragma comment(lib, "dxguid.lib")

#include <Dshowasf.h>


// CChildView window

#define MAX_DEVICE_NUMBER 16

class CChildView : public CWnd
{
// Construction
public:
	CChildView();

// Attributes
public:

// Operations
public:
	BOOL EnumDevice();
	int GetDeviceCount(BOOL bVideo) {
		return bVideo ? m_nVideoCount : m_nAudioCount;
	}
	BOOL GetDeviceName(BOOL bVideo, int nIndex, TCHAR* pszName) {
		if (bVideo) {
			if (nIndex >= m_nVideoCount)
				return FALSE;

			_tcscpy_s(pszName, MAX_DEVICE_NAME, m_pszVideo[nIndex]);
		}
		else {
			if (nIndex >= m_nAudioCount)
				return FALSE;

			_tcscpy_s(pszName, MAX_DEVICE_NAME, m_pszAudio[nIndex]);
		}
		return TRUE;
	}

	void GetVideoFormat(LONG* plWidth, LONG* plHeight) {
		*plWidth = m_lWidth;
		*plHeight = m_lHeight;
	}

	BOOL IsRecording() {
		return m_bRecording;
	}

private:
	BOOL Run();
	void Stop(BOOL bDestoryVideoRender);

	BOOL OpenDevice(int nVideo, int nAudio);
	BOOL OpenVideoDevice(int nIndex);
	void CloseVideoDevice();
	BOOL OpenAudioDevice(int nIndex);
	void CloseAudioDevice();

	BOOL CreateVideoRender();
	BOOL SetAVProfile();
	BOOL SetAVFormat();
	
	void ReleaseBuffer();

// Overrides
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~CChildView();

protected:
	ICaptureGraphBuilder2*	m_pBuild;
	IBaseFilter*			m_pVideoCapture;
	IBaseFilter*			m_pAudioCapture;

	IGraphBuilder*			m_pGraph;
	IAMStreamConfig*		m_pVideoConfig;
	IAMStreamConfig*		m_pAudioConfig;

	IVideoWindow*			m_pVideoWnd;
	IBaseFilter*			m_pRender;

	IPin*					m_pVideoPin;
	IBaseFilter*			m_pInfTee;
	IBaseFilter*			m_pAsfWriter;
	IFileSinkFilter2*		m_pFileSink;

	IMoniker*				m_pVideoMoniker;
	TCHAR					m_szVideoName[256];
	IMoniker*				m_pAudioMoniker;
	TCHAR					m_szAudioName[256];

	IWMProfile*				m_pProfile;

	TCHAR*					m_pszVideo[MAX_DEVICE_NUMBER];
	TCHAR*					m_pszAudio[MAX_DEVICE_NUMBER];

	TCHAR*					m_pszVideoPath[MAX_DEVICE_NUMBER];
	TCHAR*					m_pszAudioPath[MAX_DEVICE_NUMBER];

	int						m_nVideoCount;
	int						m_nAudioCount;
	int						m_nVideoDeviceSel;
	int						m_nAudioDeviceSel;
	LONG					m_lWidth;
	LONG					m_lHeight;

	BOOL					m_bRecording;

// Generated message map functions
protected:
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnClose();
	afx_msg void OnStartWriteAsfFile();
	afx_msg void OnUpdateStartAsfFile(CCmdUI *pCmdUI);
	afx_msg void OnStopWriteAsfFile();
	afx_msg void OnUpdateStopAsfFile(CCmdUI *pCmdUI);
	afx_msg void OnDeviceItem(UINT nID);
	afx_msg void OnUpdateDeviceItem(CCmdUI *pCmdUI);
	afx_msg void OnAppExit();
};

