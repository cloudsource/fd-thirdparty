// HDMIInfoFrame.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"


#include <windows.h>

#include "LibMWCapture\MWCapture.h"


int _tmain(int argc, _TCHAR* argv[])
{
	printf("Magewell MWCapture SDK 3.2 - HDMIInfoFrame\n");

	printf("Usage: HDMIInfoFrame.exe <board id>:<channel id>\n");
	
	MWCaptureInitInstance();

	// Version
	BYTE byMaj, byMin;
	WORD wBuild;
	MWGetVersion(&byMaj, &byMin, &wBuild);
	printf("LibMWCapture Version V%d.%d.%d\n\n", byMaj, byMin, wBuild);

	HCHANNEL hChannel = NULL;

	do {
		int nChannelCount = MWGetChannelCount();
		if (nChannelCount == 0) {
			printf("ERROR: Can't find channels!\n");
			break;
		}
		printf("Find %d channels.\n", nChannelCount);

		// Get board id and channel id
		BYTE byBoardId = 0;
		BYTE byChannelId = 0;

		MWCAP_CHANNEL_INFO videoInfo = { 0 };
		if (argc == 1) {
			if (MW_SUCCEEDED != MWGetChannelInfoByIndex(0, &videoInfo)) {
				printf("ERROR: Can't get channel info!\n");
				break;
			}
			byBoardId = videoInfo.byBoardIndex;
			byChannelId = videoInfo.byChannelIndex;
		}
		else {
			byBoardId = (argv[1][0] >= 'A' && argv[1][0] <= 'F') ? (argv[1][0] - 'A' + 10) : _tstoi(argv[1]);
			TCHAR* pChannelId = _tcsstr(argv[1], _T(":"));
			if (pChannelId == NULL) {
				printf("Invalid arguments.\n");
				break;
			}
			byChannelId = _tstoi(pChannelId + _tcslen(_T(":")));
		}

		// Open channel
		hChannel = MWOpenChannel(byBoardId, byChannelId);
		if (hChannel == NULL) {
			printf("ERROR: Open channel %X:%d error!\n", byBoardId, byChannelId);
			break;
		}

		if (MW_SUCCEEDED != MWGetChannelInfo(hChannel, &videoInfo)) {
			printf("ERROR: Can't get channel info!\n");
			break;
		}

		printf("Open channel - BoardIndex = %X, ChannelIndex = %d.\n", videoInfo.byBoardIndex, videoInfo.byChannelIndex);
		printf("Product Name: %s\n", videoInfo.szProductName);
		printf("Board SerialNo: %s\n\n", videoInfo.szBoardSerialNo);

		MW_RESULT xr;
		MWCAP_INPUT_SPECIFIC_STATUS status;
		xr = MWGetInputSpecificStatus(hChannel, &status);
		if (xr == MW_SUCCEEDED) {
			if (!status.bValid) {
				printf("ERROR: Input signal is invalid!\n");
			}
			else if (status.dwVideoInputType != 1) {
				printf("ERROR: Input signal is not HDMI!\n");
			}
			else {
				DWORD dwValidFlag = 0;
				xr = MWGetHDMIInfoFrameValidFlag(hChannel, &dwValidFlag);
				if (xr == MW_SUCCEEDED) {
					if (dwValidFlag == 0) {
						printf("No HDMI InfoFrame!\n");
					}
					else {
						HDMI_INFOFRAME_PACKET packet;
						if (dwValidFlag & MWCAP_HDMI_INFOFRAME_MASK_AVI) {
							xr = MWGetHDMIInfoFramePacket(hChannel, MWCAP_HDMI_INFOFRAME_ID_AVI, &packet);
							if (xr == MW_SUCCEEDED) {
								printf("Get HDMI InfoFrame AVI OK!\n");

								printf("Length = %d, Buffer is ", packet.header.byLength);
								for (int i = 0; i < packet.header.byLength; i++) {
									printf("%02x ", packet.abyPayload[i]);
								}
								printf("\n\n");
							}
						}
						if (dwValidFlag & MWCAP_HDMI_INFOFRAME_MASK_AUDIO) {
							xr = MWGetHDMIInfoFramePacket(hChannel, MWCAP_HDMI_INFOFRAME_ID_AUDIO, &packet);
							if (xr == MW_SUCCEEDED) {
								printf("Get HDMI InfoFrame Audio OK!\n");
								printf("Length = %d, Buffer is ", packet.header.byLength);
								for (int i = 0; i < packet.header.byLength; i++) {
									printf("%02x ", packet.abyPayload[i]);
								}
								printf("\n\n");
							}
						}
						if (dwValidFlag & MWCAP_HDMI_INFOFRAME_MASK_SPD) {
							xr = MWGetHDMIInfoFramePacket(hChannel, MWCAP_HDMI_INFOFRAME_ID_SPD, &packet);
							if (xr == MW_SUCCEEDED) {
								printf("Get HDMI InfoFrame SPD OK!\n");
							}
						}
						if (dwValidFlag & MWCAP_HDMI_INFOFRAME_MASK_MS) {
							xr = MWGetHDMIInfoFramePacket(hChannel, MWCAP_HDMI_INFOFRAME_ID_MS, &packet);
							if (xr == MW_SUCCEEDED) {
								printf("Get HDMI InfoFrame MS OK!\n");
							}
						}
						if (dwValidFlag & MWCAP_HDMI_INFOFRAME_MASK_VS) {
							xr = MWGetHDMIInfoFramePacket(hChannel, MWCAP_HDMI_INFOFRAME_ID_VS, &packet);
							if (xr == MW_SUCCEEDED) {
								printf("Get HDMI InfoFrame VS OK!\n");
							}
						}
						if (dwValidFlag & MWCAP_HDMI_INFOFRAME_MASK_ACP) {
							xr = MWGetHDMIInfoFramePacket(hChannel, MWCAP_HDMI_INFOFRAME_ID_ACP, &packet);
							if (xr == MW_SUCCEEDED) {
								printf("Get HDMI InfoFrame ACP OK!\n");
							}
						}
						if (dwValidFlag & MWCAP_HDMI_INFOFRAME_MASK_ISRC1) {
							xr = MWGetHDMIInfoFramePacket(hChannel, MWCAP_HDMI_INFOFRAME_ID_ISRC1, &packet);
							if (xr == MW_SUCCEEDED) {
								printf("Get HDMI InfoFrame ISRC1 OK!\n");
							}
						}
						if (dwValidFlag & MWCAP_HDMI_INFOFRAME_MASK_ISRC2) {
							xr = MWGetHDMIInfoFramePacket(hChannel, MWCAP_HDMI_INFOFRAME_ID_ISRC2, &packet);
							if (xr == MW_SUCCEEDED) {
								printf("Get HDMI InfoFrame ISRC2 OK!\n");
							}
						}
						if (dwValidFlag & MWCAP_HDMI_INFOFRAME_MASK_GAMUT) {
							xr = MWGetHDMIInfoFramePacket(hChannel, MWCAP_HDMI_INFOFRAME_ID_GAMUT, &packet);
							if (xr == MW_SUCCEEDED) {
								printf("Get HDMI InfoFrame GAMUT OK!\n");
							}
						}
					}
				}
				else {
					printf("ERROR: Get HDMI InfoFrame Flag!\n");
				}
			}
		}
		else {
			printf("ERROR: Get Specific Status error!\n");
		}

	} while (FALSE);

	if (hChannel != NULL)
		MWCloseChannel(hChannel);

	MWCaptureExitInstance();

	printf("\nPress 'Enter' to exit!\n");
	getchar();

	return 0;
}

