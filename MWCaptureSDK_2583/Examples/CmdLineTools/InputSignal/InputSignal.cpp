// InputSignal.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

#include <windows.h>

#include "LibMWCapture\MWCapture.h"

void GetVideoColorName(MWCAP_VIDEO_COLOR_FORMAT color, char* pszName, int nSize)
{
	switch (color) {
	case MWCAP_VIDEO_COLOR_FORMAT_UNKNOWN:
		strcpy_s(pszName, nSize, "Unknown");
		break;
	case MWCAP_VIDEO_COLOR_FORMAT_RGB:
		strcpy_s(pszName, nSize, "RGB");
		break;
	case MWCAP_VIDEO_COLOR_FORMAT_YUV601:
		strcpy_s(pszName, nSize, "YUY601");
		break;
	case MWCAP_VIDEO_COLOR_FORMAT_YUV709:
		strcpy_s(pszName, nSize, "YUY709");
		break;
	case MWCAP_VIDEO_COLOR_FORMAT_YUV2020:
		strcpy_s(pszName, nSize, "YUV2020");
		break;
	case MWCAP_VIDEO_COLOR_FORMAT_YUV2020C:
		strcpy_s(pszName, nSize, "YUV2020C");
		break;
	default:
		strcpy_s(pszName, nSize, "Unknown");
		break;
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	printf("Magewell MWCapture SDK 3.2 - InputSignal\n");

	printf("Usage: InputSignal.exe <board id>:<channel id>\n");
	
	MWCaptureInitInstance();

	// Version
	BYTE byMaj, byMin;
	WORD wBuild;
	MWGetVersion(&byMaj, &byMin, &wBuild);
	printf("LibMWCapture Version V%d.%d.%d\n\n", byMaj, byMin, wBuild);

	HCHANNEL hChannel = NULL;

	do {
		int nChannelCount = MWGetChannelCount();
		if (nChannelCount == 0) {
			printf("ERROR: Can't find channels!\n");
			break;
		}
		printf("Find %d channels.\n", nChannelCount);

		// Get board id and channel id
		BYTE byBoardId = 0;
		BYTE byChannelId = 0;

		MWCAP_CHANNEL_INFO videoInfo = { 0 };
		if (argc == 1) {
			if (MW_SUCCEEDED != MWGetChannelInfoByIndex(0, &videoInfo)) {
				printf("ERROR: Can't get channel info!\n");
				break;
			}
			byBoardId = videoInfo.byBoardIndex;
			byChannelId = videoInfo.byChannelIndex;
		}
		else {
			byBoardId = (argv[1][0] >= 'A' && argv[1][0] <= 'F') ? (argv[1][0] - 'A' + 10) : _tstoi(argv[1]);
			TCHAR* pChannelId = _tcsstr(argv[1], _T(":"));
			if (pChannelId == NULL) {
				printf("Invalid arguments.\n");
				break;
			}
			byChannelId = _tstoi(pChannelId + _tcslen(_T(":")));
		}

		// Open channel
		hChannel = MWOpenChannel(byBoardId, byChannelId);
		if (hChannel == NULL) {
			printf("ERROR: Open channel %X:%d error!\n", byBoardId, byChannelId);
			break;
		}

		if (MW_SUCCEEDED != MWGetChannelInfo(hChannel, &videoInfo)) {
			printf("ERROR: Can't get channel info!\n");
			break;
		}

		printf("Open channel - BoardIndex = %X, ChannelIndex = %d.\n", videoInfo.byBoardIndex, videoInfo.byChannelIndex);
		printf("Product Name: %s\n", videoInfo.szProductName);
		printf("Board SerialNo: %s\n\n", videoInfo.szBoardSerialNo);

		MW_RESULT xr;

		// Specific Status
		MWCAP_INPUT_SPECIFIC_STATUS status;
		xr = MWGetInputSpecificStatus(hChannel, &status);
		if (xr == MW_SUCCEEDED) {
			printf("Input Signal Valid: %d\n", status.bValid);
			if (status.bValid) {
				printf("---Type: %d\n", status.dwVideoInputType);
				if (status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_HDMI) {
					printf("---HDMI HDCP: %d\n", status.hdmiStatus.bHDCP);
					printf("---HDMI HDMI Mode: %d\n", status.hdmiStatus.bHDMIMode);
					printf("---HDMI Bit Depth: %d\n", status.hdmiStatus.byBitDepth);
				}
				else if (status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_SDI) {
					printf("---SDI Type: %d\n", status.sdiStatus.sdiType);
					printf("---SDI Scanning Format: %d\n", status.sdiStatus.sdiScanningFormat);
					printf("---SDI Bit Depth: %d\n", status.sdiStatus.sdiBitDepth);
					printf("---SDI Sampling Struct: %d\n", status.sdiStatus.sdiSamplingStruct);
				}
				else if (status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_VGA) {
				}
				else if (status.dwVideoInputType == MWCAP_VIDEO_INPUT_TYPE_CVBS) {
				}
			}
		}
		printf("\n");

		// Video Signal Status
		MWCAP_VIDEO_SIGNAL_STATUS vStatus;
		xr = MWGetVideoSignalStatus(hChannel, &vStatus);
		if (xr == MW_SUCCEEDED) {
			char szColorName[16];
			GetVideoColorName(vStatus.colorFormat, szColorName, 16);

			printf("Video Signal: \n");
			printf("---Status: %d\n", vStatus.state);
			printf("---x, y: (%d, %d)\n", vStatus.x, vStatus.y);
			printf("---cx x cy: (%d x %d)\n", vStatus.cx, vStatus.cy);
			printf("---cxTotal x cyTotal: (%d x %d)\n", vStatus.cxTotal, vStatus.cyTotal);
			printf("---bInterlaced: %d\n", vStatus.bInterlaced);
			printf("---dwFrameDuration: %d\n", vStatus.dwFrameDuration);
			printf("---nAspectX: %d\n", vStatus.nAspectX);
			printf("---nAspectY: %d\n", vStatus.nAspectY);
			printf("---bSegmentedFrame: %d\n", vStatus.bSegmentedFrame);
			printf("---colorFormat: %s\n", szColorName);
		}
		printf("\n");

		// Audio Signal Status
		MWCAP_AUDIO_SIGNAL_STATUS aStatus;
		xr = MWGetAudioSignalStatus(hChannel, &aStatus);
		if (xr == MW_SUCCEEDED) {
			printf("Audio Signal: \n");
			printf("---wChannelValid: 0x%x\n", aStatus.wChannelValid);
			printf("---bLPCM: %d\n", aStatus.bLPCM);
			printf("---cBitsPerSample: %d\n", aStatus.cBitsPerSample);
			printf("---dwSampleRate: %d\n", aStatus.dwSampleRate);
			printf("---bChannelStatusValid: %d\n", aStatus.bChannelStatusValid);
		}

	} while (FALSE);

	if (hChannel != NULL)
		MWCloseChannel(hChannel);

	MWCaptureExitInstance();

	printf("\nPress 'Enter' to exit!\n");
	getchar();

	return 0;
}

