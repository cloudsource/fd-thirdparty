// CaptureByInput.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

#include <windows.h>
#include <gdiplus.h>

#include "LibMWCapture\MWCapture.h"

using namespace Gdiplus;

#define NUM_CAPTURE 100

int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes

	ImageCodecInfo* pImageCodecInfo = NULL;

	GetImageEncodersSize(&num, &size);
	if (size == 0)
		return -1;  // Failure

	pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
	if (pImageCodecInfo == NULL)
		return -1;  // Failure

	GetImageEncoders(num, size, pImageCodecInfo);

	for (UINT j = 0; j < num; ++j)
	{
		if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}
	}

	free(pImageCodecInfo);
	return -1;  // Failure
}

int _tmain(int argc, _TCHAR* argv[])
{
	printf("Magewell MWCapture SDK 3.2 - CaptureByInput\n");

	printf("Usage: CaptureByInput.exe <board id>:<channel id>\n");
	
	MWCaptureInitInstance();

	// Version
	BYTE byMaj, byMin;
	WORD wBuild;
	MWGetVersion(&byMaj, &byMin, &wBuild);
	printf("LibMWCapture Version V%d.%d.%d\n\n", byMaj, byMin, wBuild);

	// Initialize GDI+
	GdiplusStartupInput gdiplusStartupInput;

	ULONG_PTR gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	HCHANNEL hChannel = NULL;

	do {
		int nChannelCount = MWGetChannelCount();
		if (nChannelCount == 0) {
			printf("ERROR: Can't find channels!\n");
			break;
		}
		printf("Find %d channels.\n", nChannelCount);

		// Get board id and channel id
		BYTE byBoardId = 0;
		BYTE byChannelId = 0;

		MWCAP_CHANNEL_INFO videoInfo = { 0 };
		if (argc == 1) {
			if (MW_SUCCEEDED != MWGetChannelInfoByIndex(0, &videoInfo)) {
				printf("ERROR: Can't get channel info!\n");
				break;
			}
			byBoardId = videoInfo.byBoardIndex;
			byChannelId = videoInfo.byChannelIndex;
		}
		else {
			byBoardId = (argv[1][0] >= 'A' && argv[1][0] <= 'F') ? (argv[1][0] - 'A' + 10) : _tstoi(argv[1]);
			TCHAR* pChannelId = _tcsstr(argv[1], _T(":"));
			if (pChannelId == NULL) {
				printf("Invalid arguments.\n");
				break;
			}
			byChannelId = _tstoi(pChannelId + _tcslen(_T(":")));
		}

		// Open channel
		hChannel = MWOpenChannel(byBoardId, byChannelId);
		if (hChannel == NULL) {
			printf("ERROR: Open channel %X:%d error!\n", byBoardId, byChannelId);
			break;
		}

		if (MW_SUCCEEDED != MWGetChannelInfo(hChannel, &videoInfo)) {
			printf("ERROR: Can't get channel info!\n");
			break;
		}

		printf("Open channel - BoardIndex = %X, ChannelIndex = %d.\n", videoInfo.byBoardIndex, videoInfo.byChannelIndex);
		printf("Product Name: %s\n", videoInfo.szProductName);
		printf("Board SerialNo: %s\n\n", videoInfo.szBoardSerialNo);

		// Allocate capture buffer
		int cx = 1920;
		int cy = 1080;

		Bitmap bitmap(cx, cy, PixelFormat32bppRGB);
		Rect rect(0, 0, cx, cy);
		BitmapData bitmapData;

		Status status = bitmap.LockBits(
			&rect,
			ImageLockModeWrite,
			PixelFormat32bppRGB,
			&bitmapData
			);

		BOOL bBottomUp = FALSE;
		if (bitmapData.Stride < 0) {
			bitmapData.Scan0 = ((LPBYTE)bitmapData.Scan0) + bitmapData.Stride * (cy - 1);
			bitmapData.Stride = -bitmapData.Stride;
			bBottomUp = TRUE;
		}

		// Capture frames on input signal frequency
		HANDLE hNotifyEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
		HANDLE hCaptureEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

		MW_RESULT xr;
		xr = MWStartVideoCapture(hChannel, hCaptureEvent);
		if (xr != MW_SUCCEEDED) {
			printf("ERROR: Open Video Capture error!\n");
		}
		else {
			MWCAP_VIDEO_BUFFER_INFO videoBufferInfo;
			MWGetVideoBufferInfo(hChannel, &videoBufferInfo);

			MWCAP_VIDEO_FRAME_INFO videoFrameInfo;
			MWGetVideoFrameInfo(hChannel, videoBufferInfo.iNewestBufferedFullFrame, &videoFrameInfo);

			MWCAP_VIDEO_SIGNAL_STATUS videoSignalStatus;
			MWGetVideoSignalStatus(hChannel, &videoSignalStatus);

			switch (videoSignalStatus.state) {
			case MWCAP_VIDEO_SIGNAL_NONE:
				printf("Input signal status: NONE\n");
				break;
			case MWCAP_VIDEO_SIGNAL_UNSUPPORTED:
				printf("Input signal status: Unsupported\n");
				break;
			case MWCAP_VIDEO_SIGNAL_LOCKING:
				printf("Input signal status: Locking\n");
				break;
			case MWCAP_VIDEO_SIGNAL_LOCKED:
				printf("Input signal status: Locked\n");
				break;
			}

			if (videoSignalStatus.state == MWCAP_VIDEO_SIGNAL_LOCKED) {
				printf("Input signal resolution: %d x %d\n", videoSignalStatus.cx, videoSignalStatus.cy);
				double fps = (double)10000000LL / videoSignalStatus.dwFrameDuration;
				printf("Input signal fps: %.2f\n", fps);
				printf("Input signal interlaced: %d\n", videoSignalStatus.bInterlaced);
				printf("Input signal frame segmented: %d\n", videoSignalStatus.bSegmentedFrame);

				HNOTIFY hNotify = MWRegisterNotify(hChannel, hNotifyEvent, MWCAP_NOTIFY_VIDEO_FRAME_BUFFERED);
				if (hNotify == NULL) {
					printf("ERROR: Register Notify error.\n");
				}
				else {
					printf("Begin to capture %d frames by %.2f fps...\n", NUM_CAPTURE, fps);

					LONGLONG llTotalTime = 0LL;
					for (int i = 0; i < NUM_CAPTURE; i++) {
						WaitForSingleObject(hNotifyEvent, INFINITE);

						ULONGLONG ullStatusBits = 0;
						xr = MWGetNotifyStatus(hChannel, hNotify, &ullStatusBits);
						if (xr != MW_SUCCEEDED)
							continue;

						xr = MWGetVideoBufferInfo(hChannel, &videoBufferInfo);
						if (xr != MW_SUCCEEDED)
							continue;

						xr = MWGetVideoFrameInfo(hChannel, videoBufferInfo.iNewestBufferedFullFrame, &videoFrameInfo);
						if (xr != MW_SUCCEEDED)
							continue;

						if (ullStatusBits & MWCAP_NOTIFY_VIDEO_FRAME_BUFFERED) {
							xr = MWCaptureVideoFrameToVirtualAddress(hChannel, videoBufferInfo.iNewestBufferedFullFrame, (LPBYTE)bitmapData.Scan0, bitmapData.Stride * cy, bitmapData.Stride, bBottomUp, NULL, MWFOURCC_BGRA, cx, cy);
							WaitForSingleObject(hCaptureEvent, INFINITE);

							MWCAP_VIDEO_CAPTURE_STATUS captureStatus;
							xr = MWGetVideoCaptureStatus(hChannel, &captureStatus);

							LONGLONG llCurrent = 0LL;
							xr = MWGetDeviceTime(hChannel, &llCurrent);

							llTotalTime += (llCurrent - (videoSignalStatus.bInterlaced ? videoFrameInfo.allFieldBufferedTimes[1] : videoFrameInfo.allFieldBufferedTimes[0]));
						}
					}
					printf("End capture.\n");

					printf("Each frame average capture duration is %d ms.\n", (LONG)(llTotalTime / (NUM_CAPTURE * 10000)));

					xr = MWUnregisterNotify(hChannel, hNotify);

					xr = MWStopVideoCapture(hChannel);

					bitmap.UnlockBits(&bitmapData);

					CLSID pngClsid;
					GetEncoderClsid(_T("image/png"), &pngClsid);
					bitmap.Save(L"CaptureByInput.png", &pngClsid, NULL);

					printf("Write last frame to CaptureByInput.png.\n");
				}
				CloseHandle(hNotifyEvent);
			}
			CloseHandle(hCaptureEvent);
		}

	} while (FALSE);
	
	if (hChannel != NULL)
		MWCloseChannel(hChannel);
	
	MWCaptureExitInstance();

	printf("\nPress 'Enter' to exit!\n");
	getchar();

	return 0;
}

