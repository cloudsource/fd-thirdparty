// ReadWriteEDID.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

#include <windows.h>

#include "LibMWCapture\MWCapture.h"

int _tmain(int argc, _TCHAR* argv[])
{
	printf("Magewell MWCapture SDK 3.2 - ReadWriteEDID\n");

	printf("Usage:\n");
	printf("Read EDID: ReadWriteEDID.exe <board id>:<channel id>\n");
	printf("Write EDID: ReadWriteEDID.exe <board id>:<channel id> <EDID file full path>\n");

	if (argc > 3) {
		printf("ERROR: unknown argv!\n");
		return -1;
	}

	BOOL bWriteMode = FALSE;
	if (argc == 3) {
		if (_taccess(argv[2], 4) != 0) {
			printf("ERROR: unknown EDID file!\n");
			return -1;
		}
		bWriteMode = TRUE;
	}
	
	MWCaptureInitInstance();

	// Version
	BYTE byMaj, byMin;
	WORD wBuild;
	MWGetVersion(&byMaj, &byMin, &wBuild);
	printf("LibMWCapture Version V%d.%d.%d\n\n", byMaj, byMin, wBuild);

	HCHANNEL hChannel = NULL;

	do {
		int nChannelCount = MWGetChannelCount();
		if (nChannelCount == 0) {
			printf("ERROR: Can't find channels!\n");
			break;
		}
		printf("Find %d channels.\n", nChannelCount);

		// Get board id and channel id
		BYTE byBoardId = 0;
		BYTE byChannelId = 0;

		MWCAP_CHANNEL_INFO videoInfo = { 0 };
		if (argc == 1) {
			if (MW_SUCCEEDED != MWGetChannelInfoByIndex(0, &videoInfo)) {
				printf("ERROR: Can't get channel info!\n");
				break;
			}
			byBoardId = videoInfo.byBoardIndex;
			byChannelId = videoInfo.byChannelIndex;
		}
		else {
			byBoardId = (argv[1][0] >= 'A' && argv[1][0] <= 'F') ? (argv[1][0] - 'A' + 10) : _tstoi(argv[1]);
			TCHAR* pChannelId = _tcsstr(argv[1], _T(":"));
			if (pChannelId == NULL) {
				printf("Invalid arguments.\n");
				break;
			}
			byChannelId = _tstoi(pChannelId + _tcslen(_T(":")));
		}

		// Open channel
		hChannel = MWOpenChannel(byBoardId, byChannelId);
		if (hChannel == NULL) {
			printf("ERROR: Open channel %X:%d error!\n", byBoardId, byChannelId);
			break;
		}

		if (MW_SUCCEEDED != MWGetChannelInfo(hChannel, &videoInfo)) {
			printf("ERROR: Can't get channel info!\n");
			break;
		}

		printf("Open channel - BoardIndex = %X, ChannelIndex = %d.\n", videoInfo.byBoardIndex, videoInfo.byChannelIndex);
		printf("Product Name: %s\n", videoInfo.szProductName);
		printf("Board SerialNo: %s\n\n", videoInfo.szBoardSerialNo);

		DWORD dwVideoSource = 0;
		DWORD dwAudioSource = 0;
		if (MW_SUCCEEDED != MWGetVideoInputSource(hChannel, &dwVideoSource)) {
			printf("ERROR: Can't get video input source!\n");
			break;
		}
		if (MW_SUCCEEDED != MWGetAudioInputSource(hChannel, &dwAudioSource)) {
			printf("ERROR: Can't get audio input source!\n");
			break;
		}
		if (INPUT_TYPE(dwVideoSource) != MWCAP_VIDEO_INPUT_TYPE_HDMI || INPUT_TYPE(dwAudioSource) != MWCAP_AUDIO_INPUT_TYPE_HDMI) {
			printf("Type of input source is not HDMI !\n");
			break;
		}

		MW_RESULT xr;

		if (bWriteMode) {
			FILE * pFile;
			_tfopen_s(&pFile, argv[2], _T("rb"));
			if (pFile != NULL) {
				BYTE byData[1024];
				int nSize = (int)fread(byData, 1, 1024, pFile);

				xr = MWSetEDID(hChannel, byData, nSize);
				if (xr == MW_SUCCEEDED) {
					printf("Set EDID succeeded!\n");
				}
				else {
					printf("ERROR: Set EDID!\n");
				}

				fclose(pFile);
			}
			else {
				printf("ERROR: Read EDID file!\n");
			}
		}
		else {
			FILE * pFile;
			_tfopen_s(&pFile, _T("ReadWriteEDID.bin"), _T("wb"));
			if (pFile != NULL) {
				ULONG ulSize = 256;
				BYTE byData[256];
				xr = MWGetEDID(hChannel, byData, &ulSize);
				if (xr == MW_SUCCEEDED) {
					int nWriteSize = (int)fwrite(byData, 1, 256, pFile);
					if (nWriteSize == ulSize) {
						printf("Write EDID to ReadWriteEDID.bin OK!\n");
					}
					else {
						printf("ERROR: Write ReadWriteEDID.bin!\n");
					}
				}
				else {
					printf("ERROR: Get EDID Info!\n");
				}

				fclose(pFile);
			}
			else {
				printf("ERROR: Open ReadWriteEDID.bin!\n");
			}
		}

	} while (FALSE);
	
	if (hChannel != NULL)
		MWCloseChannel(hChannel);

	MWCaptureExitInstance();

	printf("\nPress 'Enter' to exit!\n");
	getchar();

	return 0;
}

