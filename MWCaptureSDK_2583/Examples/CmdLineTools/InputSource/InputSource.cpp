// InputSource.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

#include <windows.h>

#include "LibMWCapture\MWCapture.h"

void GetVideoInputName(DWORD dwVideoInput, char* pszName, int nSize)
{
	switch (INPUT_TYPE(dwVideoInput)) {
	case MWCAP_VIDEO_INPUT_TYPE_NONE:
		strcpy_s(pszName, nSize, "None");
		break;
	case MWCAP_VIDEO_INPUT_TYPE_HDMI:
		strcpy_s(pszName, nSize, "HDMI");
		break;
	case MWCAP_VIDEO_INPUT_TYPE_VGA:
		strcpy_s(pszName, nSize, "VGA");
		break;
	case MWCAP_VIDEO_INPUT_TYPE_SDI:
		strcpy_s(pszName, nSize, "SDI");
		break;
	case MWCAP_VIDEO_INPUT_TYPE_COMPONENT:
		strcpy_s(pszName, nSize, "Component");
		break;
	case MWCAP_VIDEO_INPUT_TYPE_CVBS:
		strcpy_s(pszName, nSize, "CVBS");
		break;
	case MWCAP_VIDEO_INPUT_TYPE_YC:
		strcpy_s(pszName, nSize, "YC");
		break;
	}
}

void GetAudioInputName(DWORD dwAudioInput, char* pszName, int nSize)
{
	switch (INPUT_TYPE(dwAudioInput)) {
	case MWCAP_AUDIO_INPUT_TYPE_NONE:
		strcpy_s(pszName, nSize, "None");
		break;
	case MWCAP_AUDIO_INPUT_TYPE_HDMI:
		strcpy_s(pszName, nSize, "HDMI");
		break;
	case MWCAP_AUDIO_INPUT_TYPE_SDI:
		strcpy_s(pszName, nSize, "SDI");
		break;
	case MWCAP_AUDIO_INPUT_TYPE_LINE_IN:
		strcpy_s(pszName, nSize, "Line In");
		break;
	case MWCAP_AUDIO_INPUT_TYPE_MIC_IN:
		strcpy_s(pszName, nSize, "Mic In");
		break;
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	printf("Magewell MWCapture SDK 3.2 - InputSource\n");

	printf("Usage: InputSource.exe <board id>:<channel id>\n");
	
	MWCaptureInitInstance();

	// Version
	BYTE byMaj, byMin;
	WORD wBuild;
	MWGetVersion(&byMaj, &byMin, &wBuild);
	printf("LibMWCapture Version V%d.%d.%d\n\n", byMaj, byMin, wBuild);

	HCHANNEL hChannel = NULL;

	do {
		int nChannelCount = MWGetChannelCount();
		if (nChannelCount == 0) {
			printf("ERROR: Can't find channels!\n");
			break;
		}
		printf("Find %d channels.\n", nChannelCount);

		// Get board id and channel id
		BYTE byBoardId = 0;
		BYTE byChannelId = 0;

		MWCAP_CHANNEL_INFO videoInfo = { 0 };
		if (argc == 1) {
			if (MW_SUCCEEDED != MWGetChannelInfoByIndex(0, &videoInfo)) {
				printf("ERROR: Can't get channel info!\n");
				break;
			}
			byBoardId = videoInfo.byBoardIndex;
			byChannelId = videoInfo.byChannelIndex;
		}
		else {
			byBoardId = (argv[1][0] >= 'A' && argv[1][0] <= 'F') ? (argv[1][0] - 'A' + 10) : _tstoi(argv[1]);
			TCHAR* pChannelId = _tcsstr(argv[1], _T(":"));
			if (pChannelId == NULL) {
				printf("Invalid arguments.\n");
				break;
			}
			byChannelId = _tstoi(pChannelId + _tcslen(_T(":")));
		}

		// Open channel
		hChannel = MWOpenChannel(byBoardId, byChannelId);
		if (hChannel == NULL) {
			printf("ERROR: Open channel %X:%d error!\n", byBoardId, byChannelId);
			break;
		}

		if (MW_SUCCEEDED != MWGetChannelInfo(hChannel, &videoInfo)) {
			printf("ERROR: Can't get channel info!\n");
			break;
		}

		printf("Open channel - BoardIndex = %X, ChannelIndex = %d.\n", videoInfo.byBoardIndex, videoInfo.byChannelIndex);
		printf("Product Name: %s\n", videoInfo.szProductName);
		printf("Board SerialNo: %s\n\n", videoInfo.szBoardSerialNo);

		MW_RESULT xr;

		// Video Input Source
		DWORD dwVideoInputCount = 0;
		xr = MWGetVideoInputSourceArray(hChannel, NULL, &dwVideoInputCount);
		if (xr == MW_SUCCEEDED && dwVideoInputCount > 0) {
			printf("Video Input Count : %d\n", dwVideoInputCount);

			DWORD* pVideoInput = new DWORD[dwVideoInputCount];
			xr = MWGetVideoInputSourceArray(hChannel, pVideoInput, &dwVideoInputCount);
			if (xr == MW_SUCCEEDED) {
				char szInputName[16] = { 0 };
				for (DWORD i = 0; i < dwVideoInputCount; i++) {
					GetVideoInputName(pVideoInput[i], szInputName, 16);
					printf("[%d] %s\n", i, szInputName);
				}
			}
			delete[] pVideoInput;
		}

		DWORD dwVideoInput = 0;
		xr = MWGetVideoInputSource(hChannel, &dwVideoInput);
		if (xr == MW_SUCCEEDED) {
			char szInputName[16] = { 0 };
			GetVideoInputName(dwVideoInput, szInputName, 16);
			printf("Current Video Input Source: %s\n\n", szInputName);
		}

		// Audio Input Source
		DWORD dwAudioInputCount = 0;
		xr = MWGetAudioInputSourceArray(hChannel, NULL, &dwAudioInputCount);
		if (xr == MW_SUCCEEDED && dwAudioInputCount > 0) {
			printf("Audio Input Count : %d\n", dwAudioInputCount);

			DWORD* pAudioInput = new DWORD[dwAudioInputCount];
			xr = MWGetAudioInputSourceArray(hChannel, pAudioInput, &dwAudioInputCount);
			if (xr == MW_SUCCEEDED) {
				char szInputName[16] = { 0 };
				for (DWORD i = 0; i < dwAudioInputCount; i++) {
					GetAudioInputName(pAudioInput[i], szInputName, 16);
					printf("[%d] %s\n", i, szInputName);
				}
			}
			delete[] pAudioInput;
		}

		DWORD dwAudioInput = 0;
		xr = MWGetAudioInputSource(hChannel, &dwAudioInput);
		if (xr == MW_SUCCEEDED) {
			char szInputName[16] = { 0 };
			GetAudioInputName(dwAudioInput, szInputName, 16);
			printf("Current Audio Input Source: %s\n", szInputName);
		}

	} while (FALSE);
	
	if (hChannel != NULL)
		MWCloseChannel(hChannel);
	
	MWCaptureExitInstance();

	printf("\nPress 'Enter' to exit!\n");
	getchar();

	return 0;
}

