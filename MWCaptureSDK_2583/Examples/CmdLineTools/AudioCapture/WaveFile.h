

#pragma once

#include <Windows.h>


class CWaveFile
{
public:
	CWaveFile(void) {
		m_pFile = NULL;
	}
	virtual ~CWaveFile(void) {
		if (m_pFile != NULL) {
			Exit();
		}
	}

public:
	BOOL Init(const TCHAR * pszFilePath, int nSamplesPerSec, int nChannels = 2, WORD wBitsPerSample = 16)
	{
		m_wfHeader.dwRiffFlag = 'FFIR';
		m_wfHeader.dwFileSize = sizeof(WAVE_FILE_HEADER) - 12;

		m_wfHeader.dwWaveFlag = 'EVAW';
		m_wfHeader.dwFmtFlag = ' tmf';
		m_wfHeader.dwFmtSize = sizeof(PCMWAVEFORMAT);

		m_wfHeader.pcmFormat.wBitsPerSample = wBitsPerSample;
		m_wfHeader.pcmFormat.wf.wFormatTag = WAVE_FORMAT_PCM;
		m_wfHeader.pcmFormat.wf.nChannels = nChannels;
		m_wfHeader.pcmFormat.wf.nSamplesPerSec = nSamplesPerSec;
		m_wfHeader.pcmFormat.wf.nBlockAlign = m_wfHeader.pcmFormat.wBitsPerSample * nChannels / 8;
		m_wfHeader.pcmFormat.wf.nAvgBytesPerSec = m_wfHeader.pcmFormat.wf.nBlockAlign * nSamplesPerSec;

		m_wfHeader.dwDataFlag = 'atad';
		m_wfHeader.dwDataSize = 0;

		_tfopen_s(&m_pFile, pszFilePath, _T("wb"));
		if (NULL == m_pFile)
			return FALSE;

		if (fwrite(&m_wfHeader, sizeof(WAVE_FILE_HEADER), 1, m_pFile) != 1) {
			fclose(m_pFile);
			return FALSE;
		}
		return TRUE;
	}
	void Exit()
	{
		if (m_pFile) {
			m_wfHeader.dwFileSize = m_wfHeader.dwDataSize + sizeof(WAVE_FILE_HEADER) - 12;

			fseek(m_pFile, 0, SEEK_SET);
			fwrite(&m_wfHeader, sizeof(m_wfHeader), 1, m_pFile);

			fclose(m_pFile);
			m_pFile = NULL;
		}
	}
	BOOL Write(const BYTE * pData, int nSize)
	{
		if (fwrite(pData, nSize,1,  m_pFile) != 1)
			return FALSE;

		m_wfHeader.dwDataSize += nSize;
		return TRUE;
	}
	BOOL IsOpen() {
		return (m_pFile != NULL);
	}

protected:
	typedef struct tagWAVE_FILE_HEADER {
		DWORD dwRiffFlag; // 'RIFF'
		DWORD dwFileSize;
		DWORD dwWaveFlag; // 'WAVE'
		DWORD dwFmtFlag;  // 'fmt'
		DWORD dwFmtSize;
		PCMWAVEFORMAT pcmFormat;
		DWORD dwDataFlag; // 'data'
		DWORD dwDataSize;
	} WAVE_FILE_HEADER, *PWAVE_FILE_HEADER;

	FILE * m_pFile;
	WAVE_FILE_HEADER m_wfHeader;
};