// AudioCapture.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

#include <windows.h>

#include "WaveFile.h"

#include "LibMWCapture\MWCapture.h"

int _tmain(int argc, _TCHAR* argv[])
{
	printf("Magewell MWCapture SDK 3.2 - AudioCapture\n");

	printf("Usage: AudioCapture.exe <board id>:<channel id>\n");

	MWCaptureInitInstance();

	// Version
	BYTE byMaj, byMin;
	WORD wBuild;
	MWGetVersion(&byMaj, &byMin, &wBuild);
	printf("LibMWCapture Version V%d.%d.%d\n\n", byMaj, byMin, wBuild);

	HCHANNEL hChannel = NULL;

	do {
		int nChannelCount = MWGetChannelCount();
		if (nChannelCount == 0) {
			printf("ERROR: Can't find channels!\n");
			break;
		}
		printf("Find %d channels.\n", nChannelCount);

		// Get board id and channel id
		BYTE byBoardId = 0;
		BYTE byChannelId = 0;

		MWCAP_CHANNEL_INFO videoInfo = { 0 };
		if (argc == 1) {
			if (MW_SUCCEEDED != MWGetChannelInfoByIndex(0, &videoInfo)) {
				printf("ERROR: Can't get channel info!\n");
				break;
			}
			byBoardId = videoInfo.byBoardIndex;
			byChannelId = videoInfo.byChannelIndex;
		}
		else {
			byBoardId = (argv[1][0] >= 'A' && argv[1][0] <= 'F') ? (argv[1][0] - 'A' + 10) : _tstoi(argv[1]);
			TCHAR* pChannelId = _tcsstr(argv[1], _T(":"));
			if (pChannelId == NULL) {
				printf("Invalid arguments.\n");
				break;
			}
			byChannelId = _tstoi(pChannelId + _tcslen(_T(":")));
		}

		// Open channel
		hChannel = MWOpenChannel(byBoardId, byChannelId);
		if (hChannel == NULL) {
			printf("ERROR: Open channel %X:%d error!\n", byBoardId, byChannelId);
			break;
		}

		if (MW_SUCCEEDED != MWGetChannelInfo(hChannel, &videoInfo)) {
			printf("ERROR: Can't get channel info!\n");
			break;
		}

		printf("Open channel - BoardIndex = %X, ChannelIndex = %d.\n", videoInfo.byBoardIndex, videoInfo.byChannelIndex);
		printf("Product Name: %s\n", videoInfo.szProductName);
		printf("Board SerialNo: %s\n\n", videoInfo.szBoardSerialNo);

		MW_RESULT xr;
		DWORD dwInputCount = 0;
		xr = MWGetAudioInputSourceArray(hChannel, NULL, &dwInputCount);
		if (dwInputCount == 0) {
			printf("ERROR: Can't find audio input!\n");
		}
		else {
			MWCAP_AUDIO_SIGNAL_STATUS audioSignalStatus;
			MWGetAudioSignalStatus(hChannel, &audioSignalStatus);

			printf("Audio Signal: bValid = %d, SampleRate = %d, ChannelValid = 0x%x\n",
				audioSignalStatus.bChannelStatusValid, audioSignalStatus.dwSampleRate, audioSignalStatus.wChannelValid);

			// Open Audio Capture
			xr = MWStartAudioCapture(hChannel);
			if (xr != MW_SUCCEEDED) {
				printf("ERROR: Open Audio Capture error!\n");
			}
			else {
				HANDLE hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
				HNOTIFY hNotify = MWRegisterNotify(hChannel, hEvent, MWCAP_NOTIFY_AUDIO_SIGNAL_CHANGE | MWCAP_NOTIFY_AUDIO_FRAME_BUFFERED);

				LONGLONG llBegin = 0LL;
				xr = MWGetDeviceTime(hChannel, &llBegin);

				CWaveFile file;
				file.Init(_T("AudioCapture.wav"), audioSignalStatus.dwSampleRate);

				printf("Begin capture 1000 frames...\n");
				for (int i = 0; i < 1000; i++) {
					WaitForSingleObject(hEvent, INFINITE);

					ULONGLONG ullStatusBits = 0LL;
					MWGetNotifyStatus(hChannel, hNotify, &ullStatusBits);

					if (ullStatusBits & MWCAP_NOTIFY_AUDIO_SIGNAL_CHANGE) {
						MWGetAudioSignalStatus(hChannel, &audioSignalStatus);
						printf("Audio Signal Changed: bValid = %d, SampleRate = %d, ChannelValid = 0x%x\n",
							audioSignalStatus.bChannelStatusValid, audioSignalStatus.dwSampleRate, audioSignalStatus.wChannelValid);

						break;
					}

					if (ullStatusBits & MWCAP_NOTIFY_AUDIO_FRAME_BUFFERED) {
						do {
							MWCAP_AUDIO_CAPTURE_FRAME audioFrame;
							xr = MWCaptureAudioFrame(hChannel, &audioFrame);

							if (xr == MW_SUCCEEDED && file.IsOpen()) {
								short asAudioSamples[MWCAP_AUDIO_SAMPLES_PER_FRAME * 2];
								for (int i = 0; i < MWCAP_AUDIO_SAMPLES_PER_FRAME; i++) {
									short sLeft = (short)(audioFrame.adwSamples[i * 8] >> 16);
									short sRight = (short)(audioFrame.adwSamples[i * 8 + 4] >> 16);

									asAudioSamples[i * 2] = sLeft;
									asAudioSamples[i * 2 + 1] = sRight;
								}

								file.Write((const BYTE*)asAudioSamples, MWCAP_AUDIO_SAMPLES_PER_FRAME * 2 * sizeof(short));
							}
						} while (xr == MW_SUCCEEDED);
					}
				}
				file.Exit();

				LONGLONG llEnd = 0LL;
				xr = MWGetDeviceTime(hChannel, &llEnd);

				printf("End capture, samples is %d, the duration is %d ms. \n", MWCAP_AUDIO_SAMPLES_PER_FRAME * 1000, (llEnd - llBegin) / 10000);

				printf("Write audio samples to AudioCapture.wav .\n");

				MWUnregisterNotify(hChannel, hNotify);
				CloseHandle(hEvent);

				MWStopAudioCapture(hChannel);
			}
		}

	} while (FALSE);
	
	if (hChannel != NULL)
		MWCloseChannel(hChannel);

	MWCaptureExitInstance();

	printf("\nPress 'Enter' to exit!\n");
	getchar();

	return 0;
}

