#ifndef _LOWLATENCY_H
#define _LOWLATENCY_H

#include <Windows.h>
#include "StringUtils.h"
#include "LibMWCapture\MWCapture.h"
#include <iostream>
using namespace std;

#define CAPTURE_WIDTH		1920 
#define CAPTURE_HEIGHT		1080
#define CAPTURE_DURATION	166667
#define CAPTURE_COLOR		MWFOURCC_NV12 // MWFOURCC_BGR24 // MWFOURCC_YUY2

#define	CAPTURE_PART_LINE	256

class IPreviewCallback
{
public:
	virtual BOOL OnVideoCallback(const BYTE* lpData, DWORD cbStride) = NULL;
};

class CLowLatencyCap
{
public:
	CLowLatencyCap();
	~CLowLatencyCap();

	int GetChannelCount();
	BOOL Create(int nIndex, int cx, int cy, DWORD dwFrameDuration, DWORD dwFOURCC, IPreviewCallback* pCallback);
	void Destroy();

protected:
	static DWORD WINAPI ThreadProc(LPVOID pvParam){
		CLowLatencyCap* pThis = (CLowLatencyCap *)pvParam;
		return pThis->ThreadProcEx();
	};
	DWORD ThreadProcEx();

private:
	IPreviewCallback* m_pCallback;
	HCHANNEL m_hChannel;
	HANDLE   m_hThread;
	HANDLE	 m_hEvent;

	//
	int				m_cx;
	int				m_cy;
	DWORD			m_dwFrameDuration;
	DWORD			m_dwFOURCC;

	//Status
	MWCAP_VIDEO_SIGNAL_STATUS	m_status;
	DWORD						m_dwFrameCount;
	LONGLONG					m_llCaptureDuration;
	LONGLONG					m_llTotalDuration;
};

#endif