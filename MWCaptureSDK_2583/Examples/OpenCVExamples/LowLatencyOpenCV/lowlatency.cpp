#include "lowlatency.h"

CLowLatencyCap::CLowLatencyCap()
{
	m_pCallback = NULL;
	m_hChannel = NULL;
	m_hThread = NULL;
	m_hEvent = NULL;

	//
	m_cx = CAPTURE_WIDTH;
	m_cy = CAPTURE_HEIGHT;
	m_dwFrameDuration = CAPTURE_DURATION;
	m_dwFOURCC = CAPTURE_COLOR;

	//
	m_dwFrameCount = 0;
	m_llCaptureDuration = 0LL;
	m_llTotalDuration = 0LL;
}

CLowLatencyCap::~CLowLatencyCap()
{

}

int CLowLatencyCap::GetChannelCount()
{
	return MWGetChannelCount();
}

BOOL CLowLatencyCap::Create(int nIndex, int cx, int cy, DWORD dwFrameDuration, DWORD dwFOURCC, IPreviewCallback* pCallback)
{
	BOOL bRet = FALSE;
	do {
		MWCAP_CHANNEL_INFO videoInfo = { 0 };
		if (MW_SUCCEEDED != MWGetChannelInfoByIndex(nIndex, &videoInfo)) {
			break;
		}

		// Open channel
		TCHAR szDevicePath[MAX_PATH];
		if (MW_SUCCEEDED != MWGetDevicePath(nIndex, szDevicePath)) {
			OutputDebugStringA("ERROR: MWGetDevicePath fail !\n");
			break;
		}
		m_hChannel = MWOpenChannelByPath(szDevicePath);
		if (m_hChannel == NULL) {
			break;
		}

		m_cx = cx;
		m_cy = cy;
		m_dwFrameDuration = dwFrameDuration;//Not used in lowlatency mode
		m_dwFOURCC = dwFOURCC;
		m_pCallback = pCallback;
		m_dwFrameCount = 0;
		m_llCaptureDuration = 0LL;
		m_llTotalDuration = 0LL;

		m_hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

		m_hThread = CreateThread(NULL, 0, ThreadProc, (LPVOID)this, 0, NULL);
		if (m_hThread == NULL) {
			break;
		}

		bRet = TRUE;
	} while (FALSE);

	if (!bRet) {
		Destroy();
		return FALSE;
	}

	return bRet;
}

void CLowLatencyCap::Destroy()
{
	if (m_hEvent != NULL && m_hThread != NULL) {
		SetEvent(m_hEvent);
		WaitForSingleObject(m_hThread, INFINITE);

		CloseHandle(m_hThread);
		m_hThread = NULL;

		CloseHandle(m_hEvent);
		m_hEvent = NULL;
	}

	if (m_hChannel != NULL) {
		MWCloseChannel(m_hChannel);
		m_hChannel = NULL;
	}
}

DWORD CLowLatencyCap::ThreadProcEx()
{
	cout << "--------------------- Video Thread in -----------------------" << endl;

	HANDLE hNotifyEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	HANDLE hCaptureEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD cbStride = FOURCC_CalcMinStride(m_dwFOURCC, m_cx, 2);
	DWORD dwFrameSize = FOURCC_CalcImageSize(m_dwFOURCC, m_cx, m_cy, cbStride);
	BYTE* byBuffer = new BYTE[dwFrameSize];

	MW_RESULT xr;
	do {
		xr = MWPinVideoBuffer(m_hChannel, byBuffer, dwFrameSize);
		if (xr != MW_SUCCEEDED)
			break;

		xr = MWStartVideoCapture(m_hChannel, hCaptureEvent);
		if (xr != MW_SUCCEEDED)
			break;
		
		MWCAP_VIDEO_BUFFER_INFO videoBufferInfo;
		if (MW_SUCCEEDED != MWGetVideoBufferInfo(m_hChannel, &videoBufferInfo))
			break;

		MWCAP_VIDEO_FRAME_INFO videoFrameInfo;
		xr = MWGetVideoFrameInfo(m_hChannel, videoBufferInfo.iNewestBufferedFullFrame, &videoFrameInfo);
		if (xr != MW_SUCCEEDED)
			break;
		
		
		/**
		* NOTE: 
		* Normal Mode:MWCAP_NOTIFY_VIDEO_FRAME_BUFFERED   
		* LowLatency:MWCAP_NOTIFY_VIDEO_FRAME_BUFFERING  
		*/
		HNOTIFY hNotify = MWRegisterNotify(m_hChannel, hNotifyEvent,
			 MWCAP_NOTIFY_VIDEO_FRAME_BUFFERING | MWCAP_NOTIFY_VIDEO_SIGNAL_CHANGE | MWCAP_NOTIFY_VIDEO_INPUT_SOURCE_CHANGE);
		if (hNotify == NULL)
			break;

		LONGLONG llBegin = 0LL;
		xr = MWGetDeviceTime(m_hChannel, &llBegin);
		if (xr != MW_SUCCEEDED)
			break;

		xr = MWGetVideoSignalStatus(m_hChannel, &m_status);
		if (xr != MW_SUCCEEDED)
			break;
		
		MWCAP_VIDEO_CAPTURE_STATUS captureStatus;

		LONGLONG llExpireTime = llBegin;
		int i = 0;
		while (TRUE) {
			HANDLE aEvent[] = { m_hEvent, hNotifyEvent };
			DWORD dwRet = WaitForMultipleObjects(2, aEvent, FALSE, INFINITE);
			if (dwRet == WAIT_OBJECT_0)
				break;
		
			xr = MWGetVideoSignalStatus(m_hChannel, &m_status);
			if (xr != MW_SUCCEEDED)
				break;
			
			ULONGLONG ullStatusBits = 0;
			xr = MWGetNotifyStatus(m_hChannel, hNotify, &ullStatusBits);
			if (xr != MW_SUCCEEDED)
				continue;
			
			xr = MWGetVideoBufferInfo(m_hChannel, &videoBufferInfo);
			if (xr != MW_SUCCEEDED)
				continue;
			
			if (ullStatusBits & MWCAP_NOTIFY_VIDEO_FRAME_BUFFERED) {
				xr = MWGetVideoFrameInfo(m_hChannel, videoBufferInfo.iNewestBufferedFullFrame, &videoFrameInfo);
				if (xr != MW_SUCCEEDED)
					continue;

				xr = MWCaptureVideoFrameToVirtualAddress(m_hChannel, videoBufferInfo.iNewestBufferedFullFrame, byBuffer, dwFrameSize, cbStride, FALSE, NULL, CAPTURE_COLOR, CAPTURE_WIDTH, CAPTURE_HEIGHT);
				WaitForSingleObject(hCaptureEvent, INFINITE);
			}
			else if (ullStatusBits & MWCAP_NOTIFY_VIDEO_FRAME_BUFFERING) {
				xr = MWGetVideoFrameInfo(m_hChannel, videoBufferInfo.iNewestBuffering, &videoFrameInfo);
				if (xr != MW_SUCCEEDED)
					continue;

				xr = MWCaptureVideoFrameToVirtualAddressEx(m_hChannel, videoBufferInfo.iNewestBuffering, byBuffer, dwFrameSize, cbStride, FALSE, NULL, CAPTURE_COLOR, CAPTURE_WIDTH, CAPTURE_HEIGHT,
					0, CAPTURE_PART_LINE, NULL, NULL, 0, 100, 0, 100, 0, MWCAP_VIDEO_DEINTERLACE_BLEND, MWCAP_VIDEO_ASPECT_RATIO_IGNORE, NULL, NULL, 0, 0, MWCAP_VIDEO_COLOR_FORMAT_UNKNOWN, MWCAP_VIDEO_QUANTIZATION_UNKNOWN, MWCAP_VIDEO_SATURATION_UNKNOWN);

				BOOL bFirstSlice = TRUE;
				MWCAP_VIDEO_CAPTURE_STATUS captureStatus;
				do {
					WaitForSingleObject(hCaptureEvent, INFINITE);
					xr = MWGetVideoCaptureStatus(m_hChannel, &captureStatus);
				} while (xr == MW_SUCCEEDED && !captureStatus.bFrameCompleted);

				xr = MWGetVideoFrameInfo(m_hChannel, videoBufferInfo.iNewestBuffering, &videoFrameInfo);
				if (xr != MW_SUCCEEDED)
					continue;
			}
			
			if ((ullStatusBits & (MWCAP_NOTIFY_VIDEO_SIGNAL_CHANGE | MWCAP_NOTIFY_VIDEO_INPUT_SOURCE_CHANGE))
				|| (videoFrameInfo.allFieldStartTimes[0] == -1)
				|| (m_status.bInterlaced && (videoFrameInfo.allFieldStartTimes[1] == -1))
				) {
				m_dwFrameCount = 0;
				m_llCaptureDuration = 0LL;
				m_llTotalDuration = 0LL;
				continue;
			}

			{
				LONGLONG llCurrent = 0LL;
				xr = MWGetDeviceTime(m_hChannel, &llCurrent);
				if (xr != MW_SUCCEEDED)
					continue;

				m_llTotalDuration += (llCurrent - videoFrameInfo.allFieldStartTimes[0]);

				if (m_status.bInterlaced)
					m_llCaptureDuration += (llCurrent - videoFrameInfo.allFieldBufferedTimes[1]);
				else
					m_llCaptureDuration += (llCurrent - videoFrameInfo.allFieldBufferedTimes[0]);
			}

			m_dwFrameCount++;

			if (m_dwFrameCount % 100 == 0 && m_dwFrameCount != 0) {
				cout << "Capture Latency:" << (m_llCaptureDuration/m_dwFrameCount)/10000 << "ms ,Total Latency:" << (m_llTotalDuration / m_dwFrameCount)/10000 << "ms" << endl;
				m_dwFrameCount = 0;
				m_llCaptureDuration = 0LL;
				m_llTotalDuration = 0LL;
			}

			if (m_pCallback != NULL)
				m_pCallback->OnVideoCallback(byBuffer, dwFrameSize);

			xr = MWGetVideoCaptureStatus(m_hChannel, &captureStatus);
		}

		xr = MWUnregisterNotify(m_hChannel, hNotify);

		xr = MWStopVideoCapture(m_hChannel);

		xr = MWUnpinVideoBuffer(m_hChannel, byBuffer);
	} while (FALSE);

	delete[] byBuffer;

	CloseHandle(hNotifyEvent);
	CloseHandle(hCaptureEvent);

	cout << "--------------------- Video Thread out -----------------------" << endl;
	return 0;
}
