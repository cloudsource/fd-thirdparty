#ifndef _OPENCVSHOW_H
#define _OPENCVSHOW_H

#include <iostream>
#include "opencv.hpp"
#include "lowlatency.h"

using namespace std;
using namespace cv;


#define WIN_NAME "OpenCV-Video"

class CCvShow : public IPreviewCallback
{
public:
	CCvShow();
	~CCvShow();

	BOOL StartCapture();
	void StopCapture();

	virtual BOOL OnVideoCallback(const BYTE* lpData, DWORD cbStride);

private:
	CLowLatencyCap m_capture;

	//OpenCV
};

#endif
