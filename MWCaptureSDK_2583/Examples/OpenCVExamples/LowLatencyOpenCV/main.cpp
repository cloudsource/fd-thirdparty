#include "opencvshow.h"

int main()
{
	MWCaptureInitInstance();

	CCvShow cvshow;

	if (cvshow.StartCapture() == FALSE)
	{
		cout << "Open Channel 0 Error!" << endl;
		return -1;
	}

	cout << "Please Enter to Stop Capture:\n";
	getchar();

	cvshow.StopCapture();
	MWCaptureExitInstance();

	return 0;
}