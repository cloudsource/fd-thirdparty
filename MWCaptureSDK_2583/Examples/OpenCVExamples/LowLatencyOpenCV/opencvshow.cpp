#include "opencvshow.h"

CCvShow::CCvShow()
{
}

CCvShow::~CCvShow()
{
}

BOOL CCvShow::StartCapture()
{
	int nCount = m_capture.GetChannelCount();
	if (nCount < 1)
	{
		cout << "No Channel Available!" << endl;
		return FALSE;
	}

	return m_capture.Create(0,CAPTURE_WIDTH, CAPTURE_HEIGHT, CAPTURE_DURATION, CAPTURE_COLOR,this);
}

void CCvShow::StopCapture()
{
	m_capture.Destroy();
}

BOOL CCvShow::OnVideoCallback(const BYTE* lpData, DWORD cbSize)
{	
	/* //If use BGR24 Format,no need to convert color format.
	Mat bgrFrame;

	cvNamedWindow(WIN_NAME, CV_WINDOW_NORMAL);
	cvResizeWindow(WIN_NAME, 400, 300);
	bgrFrame.create(CAPTURE_HEIGHT, CAPTURE_WIDTH, CV_8UC3);
	memcpy(bgrFrame.data, lpData, cbSize);
	imshow(WIN_NAME, bgrFrame);
	waitKey(5);
	*/

	//Use YUV Format,eg: NV12
	Mat yuvFrame;
	Mat bgrFrame;

	cvNamedWindow(WIN_NAME, CV_WINDOW_NORMAL);
	cvResizeWindow(WIN_NAME, 700, 500);
	yuvFrame.create(CAPTURE_HEIGHT*3/2, CAPTURE_WIDTH, CV_8UC1);
	memcpy(yuvFrame.data, lpData, cbSize);
	cvtColor(yuvFrame, bgrFrame, CV_YUV2BGR_NV12);
	
	imshow(WIN_NAME, bgrFrame);
	waitKey(5);
	
	return TRUE;
}
