
#pragma once

#ifdef LIBMWMEDIA_EXPORTS
#define LIBMWMEDIA_API __declspec(dllexport)
#else
#define LIBMWMEDIA_API __declspec(dllimport)
#endif

#include <Windows.h>
#include <tchar.h>


#ifdef __cplusplus

extern "C"
{
#endif

/////////////////////////
// D3D 11 Renderer

DECLARE_HANDLE(HD3DRENDERER);

HD3DRENDERER
LIBMWMEDIA_API
MWCreateD3DRenderer(
	int						cx,
	int						cy,
	DWORD					dwFourcc,
	HWND					hWnd
	);

void
LIBMWMEDIA_API
MWDestroyD3DRenderer(
	HD3DRENDERER			hRenderer
	);

BOOL
LIBMWMEDIA_API
MWD3DRendererPushFrame(
	HD3DRENDERER			hRenderer,
	const BYTE *			pbyBuffer,
	int						cbStride
	);

#ifdef __cplusplus
}
#endif
