
What's here
===========
  The official MWCapture SDK

Version
=======
  version 3.2.1
  available at http://www.magewell.com/sdk

Compatible OS
=============
  Windows 7       (32 bit and 64 bit)
  Windows 8       (32 bit and 64 bit)
  Windows 8.1     (32 bit and 64 bit)
  Windows 10      (32 bit and 64 bit)
  Windows 2008 R2 (64 bit)
  Windows 2012    (64 bit)

Compatible Device
=================
  Pro Capture AIO
  Pro Capture DVI
  Pro Capture HDMI
  Pro Capture SDI
  Pro Capture Dual SDI
  Pro Capture Dual HDMI
  Pro Capture Dual DVI
  Pro Capture Quad SDI
  Pro Capture Quad HDMI
  Pro Capture Mini HDMI
  Pro Capture Mini SDI
  Pro Capture HDMI 4K
  Pro Capture HDMI 4K Plus
  Pro Capture DVI 4K 
  Pro Capture AIO 4K Plus

Compiler info
=============
  SDK Compiler
      Microsoft Visual Studio C++ 12.0.21005.1 RP1Rel

  Examples Compiler
      Microsoft Visual Studio C++ 10.0.40219.1 RP1Rel

Copyright notice
================
  Copyright (C) 2011-2016 Magewell

Contact
=======
  support@magewell.net
  Magewell Support Team
