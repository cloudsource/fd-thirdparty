
2016.10.17
----------
* 增加 P010, P210, NV16, NV61, I422, YV16的格式支持
* Add to support P010, P210, NV16, NV61, I422, YV16

* 增加 LowLatencyOpenCV 示例
* Add LowLatencyOpenCV in Examples


2016.9.22
----------
* 增加开发手册文档
* Add "Software Development Manual" in documents.


2016.6.3
----------
* AVCapture 添加 "interlaced" 选项
* add "interlaced option in AVCapture.

* LowLatency 中添加分辨率设置
* add "resulotion" option in LowLatency.


2016.5.27
----------

* 添加了裁减功能到示例 AVCapture 中
* Add video clip in AVCapture

2016.5.26
----------
* 添加 "NDISender" 到 Examples 目录下
* add NDISender in examples.

2016.4.29
----------

* 添加 "OSDPreviewCSharp" 到 Examples 目录下
* add OSDPreviewCSharp in examples.


2016.4.22
----------

* 添加 "Display wav" 选择框到 MultiAudioCapture 中
* add checkbox of "display wav" to MultiAudioCapture.


2016.3.7
----------

* 添加 XICaptureQuad 到 Examples 目录中
* Add XICaptureQuad in examples.


2016.1.14
----------

* 添加 LibMWJAVA 库 和 JavaExamples
* Add LibMWJAVA and JavaExamples.


2015.12.23
----------

* 添加 LibMWMedia 库到 Examples 目录中
* Add LibMWMedia in examples for C# project.

* 修改 C# 示例代码 XICaptureCSharp
* Modify C# example code of XICaptureCSharp.


2015.10.27
----------

* 重构 SDK 各功能模块
* Reconsitution SDK modules.

* 提升 SDK 版本到 V3.2, 使其和 Pro Capture Driver V1.2 兼容
* Promote SDK version to V3.2 and make it compatible with Pro Capture Driver V1.2. 



2015.09.18
----------

* 添加 XIE_AACEncoderResetInputFormat 接口用于录制时音频格式发生改变的情况
* Add XIE_AACEncoderResetInputFormat for audio format changed when AAC recording.

* 重构 XICapture 示例代码，增加了设备选择、双流采集、MP4录制、按输入信号格式动态修改窗口大小等功能
* Reconsitution XICapture and add some features, such as selecting channels, capturing dual streams, recording mp4 file, etc.


2015.08.26
----------

* 修改授权文件
* Update License files.

2015.07.23
----------

* 增加 HTML5 WebRTC示例
* Add HTML5 WebRTC Examples


2015.07.22
----------

* 增加 Java 示例
* Add Java Examples


2015.06.30
----------

* 增加 MixViewer 示例
* Add MixViewer in VC examples

2015.06.04
----------

* 增加 System 层的库和示例
* Add System Layer library files and examples.


2015.06.03
----------

* 增加 SDK 介绍文档
* Add SDK Brief

* 整理示例
* Reorganization examples.

2015.05.25
----------

* 分离设备层 SDK
* Split SDK for Device Layer.

2015.05.11
----------

* 自动构造版本
* The Automatic building version.

* 增加对 LibXIGraphics 模块
* Add LibXIGraphics Module.

2015.04.1
----------

* 最初版本
* The first version.

